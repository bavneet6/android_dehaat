package com.intspvt.app.dehaat2.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;
import android.widget.TextView;

import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.intspvt.app.dehaat2.R;
import com.intspvt.app.dehaat2.activity.MainActivity;
import com.intspvt.app.dehaat2.adapter.RecyclerAdapterOrderHistory;
import com.intspvt.app.dehaat2.rest.response.OrderDataHistory;
import com.intspvt.app.dehaat2.utilities.AppPreference;
import com.intspvt.app.dehaat2.utilities.AppUtils;
import com.intspvt.app.dehaat2.viewmodel.OrderHistoryViewModel;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by DELL on 11/15/2017.
 */

public class OrderHistoryFragment extends BaseFragment implements SearchView.OnQueryTextListener {
    public static final String TAG = OrderHistoryFragment.class.getSimpleName();
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private FirebaseAnalytics mFirebaseAnalytics;
    private TextView no_data_mssg;
    private List<OrderDataHistory> orderHistoryList = new ArrayList<>();
    private String format2, format1, diffTime;
    private SearchView searchView;
    private RecyclerAdapterOrderHistory recyclerAdapterOrderHistory;
    private List<OrderDataHistory> orderDataHistories = new ArrayList<>();
    private OrderHistoryViewModel orderHistoryViewModel;

    public static OrderHistoryFragment newInstance() {
        return new OrderHistoryFragment();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_history, null);
        showActionBar(true);
        ((MainActivity) activity).setTitle(getString(R.string.Ahistory));
        recyclerView = v.findViewById(R.id.order_rec);
        recyclerView.setNestedScrollingEnabled(false);
        ((MainActivity) activity).showCart(false);
        ((MainActivity) activity).showDrawer(false);
        no_data_mssg = v.findViewById(R.id.no_data_mssg);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        searchView = v.findViewById(R.id.search);
        searchView.setOnQueryTextListener(this);
        searchView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchView.setIconified(false);
            }
        });
        AppUtils.showProgressDialog(getActivity());
        orderHistoryViewModel = ViewModelProviders.of(getActivity()).get(OrderHistoryViewModel.class);
        orderHistoryViewModel.init(null, null, null);
        observeViewModel(orderHistoryViewModel);
        return v;
    }

    private void observeViewModel(OrderHistoryViewModel viewModel) {
        viewModel.getHistoryData().observe(this, orderDataHistories -> {
            AppUtils.hideProgressDialog();
            if (orderDataHistories != null && orderDataHistories.size() > 0) {
                orderHistoryList.clear();
                no_data_mssg.setVisibility(View.GONE);
                orderHistoryList.addAll(orderDataHistories);
                printData();
            } else {
                no_data_mssg.setVisibility(View.VISIBLE);
            }
        });
    }

    private void printData() {
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerAdapterOrderHistory = new RecyclerAdapterOrderHistory(
                getActivity(), orderHistoryList);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(recyclerAdapterOrderHistory);
    }

    @Override
    public void onPause() {
        super.onPause();
        Bundle params = new Bundle();
        SimpleDateFormat s = new SimpleDateFormat("hh:mm:ss");
        format2 = s.format(new Date());
        diffTime = AppUtils.showTimeDiff(format1, format2, diffTime);
        params.putString("OrderHistory", diffTime + "   " + AppPreference.getInstance().getDEHAATI_MOBILE());
        mFirebaseAnalytics.setUserProperty("Mobile", AppPreference.getInstance().getDEHAATI_MOBILE());
        mFirebaseAnalytics.logEvent("OrderHistory", params);

    }

    @Override
    public void onResume() {
        super.onResume();
        SimpleDateFormat s = new SimpleDateFormat("hh:mm:ss");
        format1 = s.format(new Date());
    }


    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if (orderDataHistories.size() != 0) {
            recyclerAdapterOrderHistory.getFilter().filter(newText);
        }
        return false;
    }

}
