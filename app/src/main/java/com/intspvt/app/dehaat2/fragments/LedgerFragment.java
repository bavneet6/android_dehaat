package com.intspvt.app.dehaat2.fragments;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.graphics.Color;
import android.os.Bundle;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.intspvt.app.dehaat2.R;
import com.intspvt.app.dehaat2.activity.MainActivity;
import com.intspvt.app.dehaat2.adapter.RecyclerAdapterLedgerPurchaseHistory;
import com.intspvt.app.dehaat2.adapter.RecyclerAdapterLedgerSaleHistory;
import com.intspvt.app.dehaat2.rest.response.InputSellingResponse;
import com.intspvt.app.dehaat2.rest.response.OrderDataHistory;
import com.intspvt.app.dehaat2.rest.response.PnLModel;
import com.intspvt.app.dehaat2.utilities.AppPreference;
import com.intspvt.app.dehaat2.utilities.AppUtils;
import com.intspvt.app.dehaat2.viewmodel.FarmerSaleHistoryViewModel;
import com.intspvt.app.dehaat2.viewmodel.OrderHistoryViewModel;
import com.intspvt.app.dehaat2.viewmodel.PnLViewModel;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class LedgerFragment extends BaseFragment implements View.OnClickListener {
    public static final String TAG = AboutDehaatFragment.class.getSimpleName();

    private FirebaseAnalytics mFirebaseAnalytics;
    private TextView purchase_price, sale_price, pnl_price, no_data_mssg, no_ledger_data_mssg,
            customDate, yesterdayBack, mtdBack, ytdBack, inventory;
    private RecyclerView transactions_list;
    private Calendar myCalendar = Calendar.getInstance();
    private PnLViewModel pnLViewModel;
    private PnLModel.PnL pnLData;
    private String key = "yesterday";
    private ImageView back;
    private LinearLayout purchaseBack, salesBack;
    private TextView order_from, order_to;
    private OrderHistoryViewModel orderHistoryViewModel;
    private FarmerSaleHistoryViewModel farmerSaleHistoryViewModel;
    private RecyclerView.LayoutManager layoutManager;
    private List<OrderDataHistory> orderHistoryList = new ArrayList<>();
    private ArrayList<InputSellingResponse.Dehaat_farmer> inputSellingData = new ArrayList<>();
    private RecyclerAdapterLedgerPurchaseHistory recyclerAdapterLedgerPurchaseHistory;
    private RecyclerAdapterLedgerSaleHistory recyclerAdapterLedgerSaleHistory;


    public static LedgerFragment newInstance() {
        return new LedgerFragment();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_ledger, null);
        showActionBar(false);
        ((MainActivity) activity).showDrawer(false);
        ((MainActivity) activity).showCart(false);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        back = v.findViewById(R.id.back);
        no_data_mssg = v.findViewById(R.id.no_data_mssg);
        no_ledger_data_mssg = v.findViewById(R.id.no_ledger_data_mssg);
        customDate = v.findViewById(R.id.customDate);
        inventory = v.findViewById(R.id.inventory);

        purchase_price = v.findViewById(R.id.purchase_price);
        sale_price = v.findViewById(R.id.sale_price);
        pnl_price = v.findViewById(R.id.pnl_price);

        yesterdayBack = v.findViewById(R.id.yesterdayBack);
        mtdBack = v.findViewById(R.id.mtdBack);
        ytdBack = v.findViewById(R.id.ytdBack);

        purchaseBack = v.findViewById(R.id.purchaseBack);
        salesBack = v.findViewById(R.id.salesBack);

        purchaseBack.setOnClickListener(this);
        salesBack.setOnClickListener(this);
        yesterdayBack.setOnClickListener(this);
        mtdBack.setOnClickListener(this);
        ytdBack.setOnClickListener(this);

        inventory.setOnClickListener(this);
        customDate.setOnClickListener(this);
        back.setOnClickListener(this);
        transactions_list = v.findViewById(R.id.transactions_list);
        transactions_list.setNestedScrollingEnabled(false);
        AppUtils.showProgressDialog(getActivity());
        pnLViewModel = ViewModelProviders.of(getActivity()).get(PnLViewModel.class);
        pnLViewModel.init(null, null);
        observeViewModel(pnLViewModel);
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        Bundle params = new Bundle();
        params.putLong("Ledger", Long.parseLong(AppPreference.getInstance().getDEHAATI_MOBILE()));
        mFirebaseAnalytics.setUserProperty("Mobile", AppPreference.getInstance().getDEHAATI_MOBILE());
        mFirebaseAnalytics.logEvent("Ledger", params);
    }

    private void observeViewModel(PnLViewModel viewModel) {
        viewModel.getPnL()
                .observe(this, pnlInfo -> {
                    if (pnlInfo != null) {
                        pnLData = pnlInfo;
                        displayData(key, pnlInfo);
                        no_ledger_data_mssg.setVisibility(View.GONE);
                    } else
                        no_ledger_data_mssg.setVisibility(View.VISIBLE);

                    AppUtils.hideProgressDialog();
                    yesterdayBack.callOnClick();
                    purchaseBack.callOnClick();
                });
    }

    private void displayData(String key, PnLModel.PnL pnlInfo) {
        if (pnlInfo == null)
            return;
        if (key.equals("yesterday")) {
            if (pnlInfo.getPurchase() != null) {
                purchase_price.setText(getString(R.string.rs) + pnlInfo.getPurchase().getYd_value());
            }
            if (pnlInfo.getSale() != null) {
                sale_price.setText(getString(R.string.rs) + pnlInfo.getSale().getYd_value());
            }
            if (pnlInfo.getPnl() != null) {
                pnl_price.setText(getString(R.string.rs) + pnlInfo.getPnl().getYd_value());
                if (pnlInfo.getPnl().getYd_value().toString().startsWith("-")) {
                    pnl_price.setTextColor(Color.RED);
                } else {
                    pnl_price.setTextColor(getActivity().getResources().getColor(R.color.greendark));
                }
            }
        } else if (key.equals("mtd")) {
            if (pnlInfo.getPurchase() != null) {
                purchase_price.setText(getString(R.string.rs) + pnlInfo.getPurchase().getMtd_value());
            }
            if (pnlInfo.getSale() != null) {
                sale_price.setText(getString(R.string.rs) + pnlInfo.getSale().getMtd_value());
            }
            if (pnlInfo.getPnl() != null) {
                pnl_price.setText(getString(R.string.rs) + pnlInfo.getPnl().getMtd_value());
                if (pnlInfo.getPnl().getMtd_value().toString().startsWith("-")) {
                    pnl_price.setTextColor(Color.RED);
                } else {
                    pnl_price.setTextColor(getActivity().getResources().getColor(R.color.greendark));
                }
            }
        } else if (key.equals("ytd")) {
            if (pnlInfo.getPurchase() != null) {
                purchase_price.setText(getString(R.string.rs) + pnlInfo.getPurchase().getYtd_value());
            }
            if (pnlInfo.getSale() != null) {
                sale_price.setText(getString(R.string.rs) + pnlInfo.getSale().getYtd_value());
            }
            if (pnlInfo.getPnl() != null) {
                pnl_price.setText(getString(R.string.rs) + pnlInfo.getPnl().getYtd_value());
                if (pnlInfo.getPnl().getYtd_value().toString().startsWith("-")) {
                    pnl_price.setTextColor(Color.RED);
                } else {
                    pnl_price.setTextColor(getActivity().getResources().getColor(R.color.greendark));
                }
            }
        }
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.back:
                getActivity().onBackPressed();
                break;
            case R.id.customDate:
                showCalendarDialog();
                break;
            case R.id.yesterdayBack:
                key = "yesterday";
                displayData(key, pnLData);
                showLineSelected(yesterdayBack, mtdBack, ytdBack);
                purchaseBack.callOnClick();
                break;
            case R.id.mtdBack:
                key = "mtd";
                displayData(key, pnLData);
                showLineSelected(mtdBack, ytdBack, yesterdayBack);
                purchaseBack.callOnClick();
                break;
            case R.id.ytdBack:
                key = "ytd";
                displayData(key, pnLData);
                showLineSelected(ytdBack, mtdBack, yesterdayBack);
                purchaseBack.callOnClick();
                break;
            case R.id.purchaseBack:
                getPurchaseData(key);
                showBoxSelected(purchaseBack, salesBack);
                break;
            case R.id.salesBack:
                getSaleData(key);
                showBoxSelected(salesBack, purchaseBack);
                break;
            case R.id.inventory:
                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.frag_container, InventoryFragment.newInstance()).addToBackStack("").commit();
                break;
        }

    }

    private void showBoxSelected(LinearLayout l1, LinearLayout l2) {
        l1.setBackground(getActivity().getResources().getDrawable(R.drawable.card_back_selected));
        l2.setBackground(getActivity().getResources().getDrawable(R.drawable.card_back));
    }

    private void showLineSelected(TextView t1, TextView t2, TextView t3) {
        t1.setBackground(getActivity().getResources().getDrawable(R.drawable.line_back));
        t2.setBackgroundColor(getActivity().getResources().getColor(R.color.white));
        t3.setBackgroundColor(getActivity().getResources().getColor(R.color.white));
    }

    private void onText(final TextView textView) {
        int mday, mmonth, myear;
        if (textView.getText().toString().equals("")) {
            mday = myCalendar.get(Calendar.DAY_OF_MONTH);
            mmonth = myCalendar.get(Calendar.MONTH);
            myear = myCalendar.get(Calendar.YEAR);
        } else {
            ArrayList<String> list = new ArrayList<>(Arrays.asList(textView.getText().toString().split("/")));
            mday = Integer.parseInt(list.get(0));
            mmonth = Integer.parseInt(list.get(1));
            myear = Integer.parseInt(list.get(2));
        }
        DatePickerDialog StartTime = new DatePickerDialog(getActivity(), (view, year, monthOfYear, dayOfMonth) -> {
            int month = monthOfYear + 1;
            textView.setText(dayOfMonth + "/" + month + "/" + year);
        }, myear, mmonth - 1, mday);
        StartTime.show();

    }

    private void showCalendarDialog() {
        final Dialog dialog = new Dialog(new ContextThemeWrapper(getActivity(),
                R.style.DialogSlideAnim));
        final TextView submit;
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_calendar);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        order_from = dialog.findViewById(R.id.order_from);
        order_to = dialog.findViewById(R.id.order_to);
        submit = dialog.findViewById(R.id.submit);
        String date = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).format(new Date());
        order_from.setText(date);
        order_to.setText(date);
        order_from.setOnClickListener(view -> {
            onText(order_from);
        });
        order_to.setOnClickListener(view -> {
            onText(order_to);
        });
        submit.setOnClickListener(view -> {
            if (!AppUtils.isNullCase(order_from.getText().toString()) &&
                    !AppUtils.isNullCase(order_to.getText().toString())) {
                AppUtils.showProgressDialog(getActivity());
                pnLViewModel = ViewModelProviders.of(getActivity()).get(PnLViewModel.class);
                pnLViewModel.init(order_from.getText().toString(), order_to.getText().toString());
                observeCustomDateViewModel(pnLViewModel);
                dialog.dismiss();
            }
        });

        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.show();
    }

    private void observeCustomDateViewModel(PnLViewModel viewModel) {
        viewModel.getPnL()
                .observe(this, pnlInfo -> {
                    CustomDateLedgerFragment customDateLedgerFragment = CustomDateLedgerFragment.newInstance();
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("DATA", pnlInfo);
                    bundle.putString("DATE_FROM", order_from.getText().toString());
                    bundle.putString("DATE_TO", order_to.getText().toString());
                    customDateLedgerFragment.setArguments(bundle);

                    FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.frag_container, customDateLedgerFragment).addToBackStack("").commit();
                    AppUtils.hideProgressDialog();


                });
    }


    private void getPurchaseData(String key) {
        orderHistoryViewModel = ViewModelProviders.of(getActivity()).
                get(OrderHistoryViewModel.class);
        orderHistoryViewModel.init(key, null, null);
        observeHistoryViewModel(orderHistoryViewModel);
    }

    private void getSaleData(String key) {
        farmerSaleHistoryViewModel = ViewModelProviders.of(getActivity()).get
                (FarmerSaleHistoryViewModel.class);
        farmerSaleHistoryViewModel.init(key, null, null);
        observeSaleViewModel(farmerSaleHistoryViewModel);
    }

    private void observeSaleViewModel(FarmerSaleHistoryViewModel viewModel) {
        viewModel.getHistoryData().observe(this, inputData -> {
            if (inputData != null && inputData.size() > 0) {
                inputSellingData.clear();
                no_data_mssg.setVisibility(View.GONE);
                inputSellingData.addAll(inputData);
                transactions_list.setVisibility(View.VISIBLE);
                printSaleData();
            } else {
                transactions_list.setVisibility(View.GONE);
                no_data_mssg.setVisibility(View.VISIBLE);
            }
        });
    }

    private void observeHistoryViewModel(OrderHistoryViewModel viewModel) {
        viewModel.getHistoryData().observe(this, orderDataHistories -> {
            if (orderDataHistories != null && orderDataHistories.size() > 0) {
                orderHistoryList.clear();
                no_data_mssg.setVisibility(View.GONE);
                orderHistoryList.addAll(orderDataHistories);
                transactions_list.setVisibility(View.VISIBLE);
                printData();
            } else {
                no_data_mssg.setVisibility(View.VISIBLE);
                transactions_list.setVisibility(View.GONE);
            }
        });
    }

    private void printData() {
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerAdapterLedgerPurchaseHistory = new RecyclerAdapterLedgerPurchaseHistory(
                getActivity(), orderHistoryList);
        transactions_list.setLayoutManager(layoutManager);
        transactions_list.setAdapter(recyclerAdapterLedgerPurchaseHistory);
    }

    private void printSaleData() {
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerAdapterLedgerSaleHistory = new RecyclerAdapterLedgerSaleHistory(
                getActivity(), inputSellingData);
        transactions_list.setLayoutManager(layoutManager);
        transactions_list.setAdapter(recyclerAdapterLedgerSaleHistory);
    }
}