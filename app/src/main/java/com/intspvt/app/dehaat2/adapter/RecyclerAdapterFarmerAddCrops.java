package com.intspvt.app.dehaat2.adapter;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;

import androidx.recyclerview.widget.RecyclerView;

import com.intspvt.app.dehaat2.R;
import com.intspvt.app.dehaat2.rest.response.Crops;
import com.intspvt.app.dehaat2.rest.response.CropsData;
import com.intspvt.app.dehaat2.utilities.AppUtils;
import com.intspvt.app.dehaat2.utilities.CropsCallback;

import java.util.ArrayList;
import java.util.HashMap;

public class RecyclerAdapterFarmerAddCrops extends RecyclerView.Adapter<RecyclerAdapterFarmerAddCrops.ImageList> {
    private Context context;
    private ArrayList<CropsData.CropsInfo> productsList;
    private ArrayList<String> productNameList = new ArrayList<>();
    private CropsCallback cropsCallback;
    private ArrayList<String> unitList;
    private HashMap<Integer, Crops> productHashmap, temp;


    public RecyclerAdapterFarmerAddCrops(ArrayList<CropsData.CropsInfo> productsList, HashMap<Integer, Crops> productHashmap, CropsCallback cropsCallback, ArrayList unitList) {
        this.productsList = productsList;
        this.productHashmap = productHashmap;
        this.cropsCallback = cropsCallback;
        this.unitList = unitList;
        if (productsList != null)
            for (int i = 0; i < productsList.size(); i++) {
                productNameList.add(productsList.get(i).getName());
            }
    }

    @Override
    public RecyclerAdapterFarmerAddCrops.ImageList onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.template_farmer_add_crops, parent, false);
        context = parent.getContext();
        return new RecyclerAdapterFarmerAddCrops.ImageList(itemView);

    }

    @Override
    public void onBindViewHolder(final RecyclerAdapterFarmerAddCrops.ImageList holder, final int position) {
        holder.setIsRecyclable(false);
        final int pos = holder.getAdapterPosition();
        if (productHashmap.get(pos).getCropDel())
            holder.back.setVisibility(View.GONE);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, R.layout.template_spinner_text, productNameList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        holder.cropSpinner.setAdapter(adapter);

        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(context, R.layout.template_spinner_text, unitList);
        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        holder.unitSpinner.setAdapter(adapter1);


        if (productHashmap.get(pos) != null) {
            String name = null;
            if (!AppUtils.isNullCase(productHashmap.get(pos).getCropId())) {
                name = getNameFromId(Integer.parseInt(productHashmap.get(pos).getCropId()));
            }
            if (name == null) {
                holder.cropSpinner.setSelection(0);
            } else {
                int spinnerPosition = adapter.getPosition(name);
                holder.cropSpinner.setSelection(spinnerPosition);
            }

            if (!AppUtils.isNullCase(productHashmap.get(pos).getCropUnit())) {

                int unit = adapter1.getPosition(productHashmap.get(pos).getCropUnit());
                holder.unitSpinner.setSelection(unit);
            } else
                holder.unitSpinner.setSelection(0);

            holder.cropValue.setText(productHashmap.get(pos).getCropArea());
        }

        holder.cropSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                long selection = productsList.get(position).getID();
                Crops crops = new Crops();
                crops.setCropId("" + selection);
                crops.setCropArea(productHashmap.get(pos).getCropArea());
                crops.setCropUnit(productHashmap.get(pos).getCropUnit());
                crops.setCropDel(false);
                productHashmap.put(pos, crops);
                cropsCallback.onClick(productHashmap);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        holder.unitSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Crops crops = new Crops();
                crops.setCropId(productHashmap.get(pos).getCropId());
                crops.setCropArea(productHashmap.get(pos).getCropArea());
                crops.setCropUnit(unitList.get(position));
                crops.setCropDel(false);
                productHashmap.put(pos, crops);
                cropsCallback.onClick(productHashmap);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        holder.cropValue.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                Crops crops = new Crops();
                crops.setCropId(productHashmap.get(pos).getCropId());
                crops.setCropArea(holder.cropValue.getText().toString());
                crops.setCropUnit(unitList.get(position));
                crops.setCropDel(false);
                productHashmap.put(pos, crops);
                cropsCallback.onClick(productHashmap);
            }
        });

        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Crops crops = new Crops();
                crops.setCropId(productHashmap.get(pos).getCropId());
                crops.setCropArea(holder.cropValue.getText().toString());
                crops.setCropUnit(unitList.get(position));
                crops.setCropDel(true);
                productHashmap.put(pos, crops);
                cropsCallback.onClick(productHashmap);
                temp = productHashmap;
                update(temp);
            }
        });

    }

    private String getNameFromId(int selection) {

        for (int i = 0; i < productsList.size(); i++) {
            if (AppUtils.isNullCase("" + selection))
                return null;
            if (productsList.get(i).getID() == selection) {
                return productsList.get(i).getName();
            }
        }
        return null;
    }

    @Override
    public int getItemCount() {
        return productHashmap.size();
    }

    private void update(HashMap<Integer, Crops> temp) {
        this.productHashmap = temp;
        this.notifyDataSetChanged();
    }

    public class ImageList extends RecyclerView.ViewHolder {
        private Spinner cropSpinner, unitSpinner;
        private EditText cropValue;
        private ImageView delete;
        private LinearLayout back;

        public ImageList(View itemView) {
            super(itemView);
            back = itemView.findViewById(R.id.back);
            cropSpinner = itemView.findViewById(R.id.cropSpinner);
            unitSpinner = itemView.findViewById(R.id.unitSpinner);
            cropValue = itemView.findViewById(R.id.cropValue);
            delete = itemView.findViewById(R.id.delete);
        }


    }


}
