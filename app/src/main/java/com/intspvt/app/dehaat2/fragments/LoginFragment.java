package com.intspvt.app.dehaat2.fragments;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentTransaction;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.intspvt.app.dehaat2.Dehaat2;
import com.intspvt.app.dehaat2.R;
import com.intspvt.app.dehaat2.rest.AppRestClient;
import com.intspvt.app.dehaat2.rest.body.SendNumber;
import com.intspvt.app.dehaat2.rest.response.LoginResponse;
import com.intspvt.app.dehaat2.utilities.AppPreference;
import com.intspvt.app.dehaat2.utilities.AppUtils;
import com.intspvt.app.dehaat2.utilities.UrlConstant;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by DELL on 11/15/2017.
 */

public class LoginFragment extends BaseFragment implements View.OnClickListener, TextWatcher {
    public static final String TAG = LoginFragment.class.getSimpleName();
    private static final int PERMISSION_REQUEST_CODE = 1;
    private EditText mob;
    private TextView terms, call, proceed;
    private FirebaseAnalytics firebaseAnalytics;

    public static LoginFragment newInstance() {
        return new LoginFragment();
    }

    public static boolean checkPermission(Activity context) {
        boolean result = true;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(context,
                    Manifest.permission.READ_EXTERNAL_STORAGE) + ContextCompat
                    .checkSelfPermission(context,
                            Manifest.permission.CAMERA) + +ContextCompat
                    .checkSelfPermission(context,
                            Manifest.permission.INTERNET) + ContextCompat
                    .checkSelfPermission(context,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE) + ContextCompat
                    .checkSelfPermission(context,
                            Manifest.permission.RECORD_AUDIO) + ContextCompat
                    .checkSelfPermission(context,
                            Manifest.permission.ACCESS_COARSE_LOCATION) +
                    ContextCompat
                            .checkSelfPermission(context,
                                    Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale
                        (context, Manifest.permission.READ_EXTERNAL_STORAGE) ||
                        ActivityCompat.shouldShowRequestPermissionRationale
                                (context, Manifest.permission.INTERNET) || ActivityCompat.shouldShowRequestPermissionRationale
                        (context, Manifest.permission.RECORD_AUDIO) || ActivityCompat.shouldShowRequestPermissionRationale
                        (context, Manifest.permission.WRITE_EXTERNAL_STORAGE) ||
                        ActivityCompat.shouldShowRequestPermissionRationale
                                (context, Manifest.permission.ACCESS_FINE_LOCATION) ||
                        ActivityCompat.shouldShowRequestPermissionRationale
                                (context, Manifest.permission.CAMERA)) {
                } else {

                    ActivityCompat.requestPermissions(context, new String[]{Manifest.permission.CAMERA, Manifest.permission.INTERNET, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.RECORD_AUDIO, Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
                }
                result = false;
            }

        }
        return result;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_login, null);
        showActionBar(false);
        mob = v.findViewById(R.id.number);
        checkPermission(getActivity());
        AppPreference.getInstance().setLANGUAGE(UrlConstant.HINDI);
        terms = v.findViewById(R.id.terms);
        call = v.findViewById(R.id.call);
        proceed = v.findViewById(R.id.next);
        terms.setOnClickListener(this);
        call.setOnClickListener(this);
        proceed.setOnClickListener(this);
        mob.addTextChangedListener(this);
        firebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        call.setPaintFlags(call.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        terms.setPaintFlags(terms.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        AppPreference.getInstance().clearData();
        AppPreference.getInstance().setLANGUAGE(UrlConstant.HINDI);
        Dehaat2.setLocale(getActivity(), UrlConstant.HINDI);
        return v;
    }
    //cjYrtD4yx/e


    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.terms:
                if (AppUtils.haveNetworkConnection(getActivity()))
                    getFragmentManager().beginTransaction().replace(R.id.frag_cont, PrivacyFragment.newInstance()).addToBackStack("").commit();
                else
                    AppUtils.showInternetDialog(getActivity());
                break;
            case R.id.call:
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:" + UrlConstant.TOLL_FREE_NUMBER));
                startActivity(intent);
                break;
            case R.id.next:
                if (mob.getText().toString().length() != 10) {
                    mob.setError("Please enter 10 digit number");
                    mob.requestFocus();
                } else {
                    if (AppUtils.haveNetworkConnection(getActivity())) {
                        checkDehaatiNumber();
                    } else
                        AppUtils.showInternetDialog(getActivity());
                }
                break;

        }

    }

    private void checkDehaatiNumber() {
        AppUtils.showProgressDialog(getActivity());
        SendNumber sendNumber = new SendNumber(mob.getText().toString(), null);
        AppRestClient client = AppRestClient.getInstance();
        Call<LoginResponse> call = client.dehaatNumberCheck(sendNumber);

        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                AppUtils.hideProgressDialog();
                if (response.code() == 200) {
                    if (response.body().getMessage().equals("DeHaat Cordinator Tag")) {
                        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                        transaction.replace(R.id.frag_cont, OTPFragment.newInstance()).addToBackStack("").commit();
                        AppPreference.getInstance().setDEHAATI_MOBILE(mob.getText().toString());
                    } else if (response.body().getMessage().equals("Farmer Tag")) {
                        final Dialog dialog = new Dialog(getActivity());
                        final TextView link, callToll;
                        dialog.setCanceledOnTouchOutside(true);
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog.setContentView(R.layout.dialog_farmer_login);
                        link = dialog.findViewById(R.id.dehaati_link);
                        callToll = dialog.findViewById(R.id.call);
                        callToll.setPaintFlags(callToll.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
                        callToll.setOnClickListener(view ->
                        {
                            Intent intent = new Intent(Intent.ACTION_DIAL);
                            intent.setData(Uri.parse("tel:" + UrlConstant.TOLL_FREE_NUMBER));
                            startActivity(intent);
                        });
                        link.setOnClickListener(v -> {
                            startActivity(new Intent(Intent.ACTION_VIEW,
                                    Uri.parse("https://play.google.com/store/apps/details?id=app.intspvt.com.farmer")));
                            dialog.dismiss();
                        });

                        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                        dialog.getWindow().setGravity(Gravity.CENTER);
                        dialog.show();
                    } else {
                        showMessageDialog();
                    }
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                AppUtils.hideProgressDialog();
            }

        });
    }

    private void showMessageDialog() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.setCanceledOnTouchOutside(true);
        final TextView callToll;
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_show_message_call);
        callToll = dialog.findViewById(R.id.call_toll);

        callToll.setOnClickListener(view ->
        {
            Intent intent = new Intent(Intent.ACTION_DIAL);
            intent.setData(Uri.parse("tel:" + UrlConstant.TOLL_FREE_NUMBER));
            startActivity(intent);
        });

        dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.show();
    }

    @Override
    public void onResume() {
        super.onResume();
        AppUtils.getUpdateApp(getActivity());
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        if (mob.getText().length() == 10) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
            proceed.setTextColor(getActivity().getResources().getColor(R.color.greendark));
        } else {
            proceed.setTextColor(getActivity().getResources().getColor(R.color.greylight));
        }
    }
}
