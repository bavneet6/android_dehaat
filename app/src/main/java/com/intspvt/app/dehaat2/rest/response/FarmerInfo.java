package com.intspvt.app.dehaat2.rest.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by DELL on 8/29/2017.
 */

public class FarmerInfo {
    @SerializedName("state_id")
    private int state_id;
    @SerializedName("id")
    private int id;
    @SerializedName("image_small")
    private String image_small;
    @SerializedName("mobile")
    private String phone_number;
    @SerializedName("name")
    private String name;
    @SerializedName("district_id")
    private int district_id;
    @SerializedName("block_id")
    private int block_id;
    @SerializedName("panchayat_id")
    private int panchayat_id;
    @SerializedName("village_id")
    private int village_id;
    @SerializedName("title")
    private int title;

    public FarmerInfo(int state_id, int district_id, int block_id, int panchayat_id, int village_id, String name, String phone_number, String image_small, int title) {
        this.state_id = state_id;
        this.district_id = district_id;
        this.block_id = block_id;
        this.panchayat_id = panchayat_id;
        this.village_id = village_id;
        this.name = name;
        this.phone_number = phone_number;
        this.image_small = image_small;
        this.title = title;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTitle() {
        return title;
    }

    public void setTitle(int title) {
        this.title = title;
    }
}