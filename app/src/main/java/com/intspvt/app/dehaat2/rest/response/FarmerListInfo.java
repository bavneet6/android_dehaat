package com.intspvt.app.dehaat2.rest.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by DELL on 11/20/2017.
 */

public class FarmerListInfo {
    @SerializedName("id")
    private int id;
    @SerializedName("image_medium")
    private String image;

    @SerializedName("name")
    private String name;
    @SerializedName("mobile")
    private String mobile;

    @SerializedName("state")
    private String state;
    @SerializedName("district")
    private String district;
    @SerializedName("block")
    private String block;
    @SerializedName("panchayat")
    private String panchayat;
    @SerializedName("village")
    private String village;

    public String getName() {
        return name;
    }

    public String getMobile() {
        return mobile;
    }

    public String getState() {
        return state;
    }

    public String getBlock() {
        return block;
    }

    public String getPanchayat() {
        return panchayat;
    }

    public String getDistrict() {
        return district;
    }

    public String getImage() {
        return image;
    }

    public String getVillage() {
        return village;
    }

    public int getId() {
        return id;
    }

}
