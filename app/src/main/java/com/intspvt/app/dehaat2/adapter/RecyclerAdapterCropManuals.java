package com.intspvt.app.dehaat2.adapter;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.intspvt.app.dehaat2.R;
import com.intspvt.app.dehaat2.database.DatabaseHandler;
import com.intspvt.app.dehaat2.fragments.SingleCropManualFragment;
import com.intspvt.app.dehaat2.rest.PicassoCallback;
import com.intspvt.app.dehaat2.rest.response.CropManualData;
import com.intspvt.app.dehaat2.utilities.AppUtils;
import com.intspvt.app.dehaat2.utilities.RoundImageView;
import com.squareup.picasso.Picasso;

import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

import static com.intspvt.app.dehaat2.utilities.AppUtils.isNullCase;

public class RecyclerAdapterCropManuals extends RecyclerView.Adapter<RecyclerAdapterCropManuals.CropManual> implements Filterable {
    private Context context;
    private ArrayList<CropManualData> cropManuals = new ArrayList<>();
    private ArrayList<CropManualData> cropFilterManuals = new ArrayList<>();
    private String getPic;
    private HashMap<Long, ArrayList<String>> manualArticleMap = new HashMap<>();
    private DatabaseHandler databaseHandler;


    public RecyclerAdapterCropManuals(Context activity, ArrayList<CropManualData> cropManuals) {
        this.context = activity;
        this.cropManuals = cropManuals;
        this.cropFilterManuals = cropManuals;
        databaseHandler = new DatabaseHandler(context);
        manualArticleMap.clear();
        for (int i = 0; i < cropManuals.size(); i++) {
            ArrayList<String> articleNameIdList = new ArrayList<>();
            articleNameIdList.add("" + cropManuals.get(i).getId());
            articleNameIdList.add("" + cropManuals.get(i).getName());
            manualArticleMap.put(Long.valueOf(i), articleNameIdList);
        }
    }

    @Override
    public RecyclerAdapterCropManuals.CropManual onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.template_crop_manual, parent, false);
        context = parent.getContext();
        return new RecyclerAdapterCropManuals.CropManual(itemView);

    }

    @Override
    public void onBindViewHolder(final RecyclerAdapterCropManuals.CropManual holder, int position) {
        final int pos = holder.getAdapterPosition();
        holder.cropInfoType.setText(context.getString(R.string.category) + ": " + cropManuals.get(pos).getInfo_type());
        holder.cropStage.setText(context.getString(R.string.crop_stage) + ": " + cropManuals.get(pos).getStage());
        holder.cropTitle.setText(cropManuals.get(pos).getCrop() + " : " + cropManuals.get(pos).getName());
        if (!isNullCase(cropManuals.get(pos).getAttachments())) {
            showPicassoImage(pos, holder.cropImage);
        }

        holder.viewDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putLong("MANUAL_ID", cropManuals.get(pos).getId());
                bundle.putLong("MANUAL_POS", pos);
                bundle.putSerializable("MANUAL_MAP", manualArticleMap);
                SingleCropManualFragment fragment = SingleCropManualFragment.newInstance();
                fragment.setArguments(bundle);
                AppUtils.changeFragment((FragmentActivity) context, fragment);
            }
        });
    }

    private void showPicassoImage(int pos, final ImageView imageView) {
        AppUtils.generateAndCheckUrl(context, cropManuals.get(pos).getAttachments());
        String photo = cropManuals.get(pos).getAttachments();
        getPic = null;
        if (!isNullCase(photo)) {
            getPic = databaseHandler.getFileUrl(photo);
            if (getPic != null) {
                final Picasso picasso = Picasso.with(context.getApplicationContext());
                picasso.setLoggingEnabled(true);
                picasso.load(getPic).into(imageView, new PicassoCallback(photo) {

                    @Override
                    public void onErrorPic(URL url) {
                        picasso.load("" + url).error(R.drawable.no_crop_image).into(imageView);
                    }
                });

            } else {
                imageView.setImageResource(R.drawable.no_crop_image);
            }
        }
    }

    @Override
    public Filter getFilter() {

        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {

                String charString = charSequence.toString();

                if (charString.isEmpty()) {

                    cropManuals = cropFilterManuals;
                } else {

                    ArrayList<CropManualData> filteredList = new ArrayList<>();

                    for (CropManualData list : cropFilterManuals)
                        if (list.getName().toLowerCase().contains(charString.toLowerCase()))
                            filteredList.add(list);

                    cropManuals = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = cropManuals;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                cropManuals = (ArrayList<CropManualData>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    @Override
    public int getItemCount() {
        return cropManuals.size();
    }

    public class CropManual extends RecyclerView.ViewHolder {
        private TextView cropTitle, cropInfoType, cropStage;
        private LinearLayout viewDetails;
        private RoundImageView cropImage;

        public CropManual(View itemView) {
            super(itemView);
            cropTitle = itemView.findViewById(R.id.cropTitle);
            cropInfoType = itemView.findViewById(R.id.cropInfoType);
            viewDetails = itemView.findViewById(R.id.viewDetails);
            cropStage = itemView.findViewById(R.id.cropStage);
            cropImage = itemView.findViewById(R.id.cropImage);
        }

    }
}
