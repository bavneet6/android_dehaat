package com.intspvt.app.dehaat2.rest.response;

public class Crops {

    private String cropId, cropArea, cropUnit;
    private boolean cropDel;


    public String getCropArea() {
        return cropArea;
    }

    public void setCropArea(String cropArea) {
        this.cropArea = cropArea;
    }

    public boolean getCropDel() {
        return cropDel;
    }

    public void setCropDel(boolean cropDel) {
        this.cropDel = cropDel;
    }

    public String getCropId() {
        return cropId;
    }

    public void setCropId(String cropId) {
        this.cropId = cropId;
    }

    public String getCropUnit() {
        return cropUnit;
    }

    public void setCropUnit(String cropUnit) {
        this.cropUnit = cropUnit;
    }
}
