package com.intspvt.app.dehaat2.rest.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class CropManualData implements Serializable {

    @SerializedName("image")
    private String attachments;

    @SerializedName("content")
    private String content;
    @SerializedName("action_step")
    private String action_step;
    @SerializedName("action_benefit")
    private String action_benefit;
    @SerializedName("government_scheme")
    private String government_scheme;
    @SerializedName("crop")
    private String crop;
    @SerializedName("info_type")
    private String info_type;
    @SerializedName("name")
    private String name;
    @SerializedName("stage")
    private String stage;
    @SerializedName("id")
    private Long id;
    @SerializedName("product_ids")
    private List<Long> product_ids;
    @SerializedName("intercropping")
    private List<String> intercropping;
    @SerializedName("disease")
    private List<String> disease;

    public List<String> getDisease() {
        return disease;
    }

    public List<String> getIntercropping() {
        return intercropping;
    }

    public List<Long> getProduct_ids() {
        return product_ids;
    }

    public String getAttachments() {
        return attachments;
    }

    public String getAction_benefit() {
        return action_benefit;
    }

    public String getAction_step() {
        return action_step;
    }

    public String getGovernment_scheme() {
        return government_scheme;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public String getCrop() {
        return crop;
    }

    public String getInfo_type() {
        return info_type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStage() {
        return stage;
    }
}
