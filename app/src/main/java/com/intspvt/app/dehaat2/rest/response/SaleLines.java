package com.intspvt.app.dehaat2.rest.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SaleLines implements Serializable {
    @SerializedName("product_id")
    private int product_id;

    @SerializedName("product_qty")
    private int product_qty;
    @SerializedName("id")
    private int id;
    @SerializedName("price_unit")
    private float product_price;
    @SerializedName("product_name")
    private String product_name;
    @SerializedName("is_deleted")
    private Boolean is_deleted;

    public Boolean getIs_deleted() {
        return is_deleted;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setIs_deleted(Boolean is_deleted) {
        this.is_deleted = is_deleted;
    }

    public float getProduct_price() {
        return product_price;
    }

    public void setProduct_price(float product_price) {
        this.product_price = product_price;
    }

    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }

    public int getProduct_id() {
        return product_id;
    }

    public int getProduct_qty() {
        return product_qty;
    }

    public void setProduct_qty(int product_qty) {
        this.product_qty = product_qty;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }
}
