package com.intspvt.app.dehaat2.rest.body;

import com.google.gson.annotations.SerializedName;

public class UpdateDehaatInfo {
    @SerializedName("data")
    private UpdateInfo data;

    public UpdateDehaatInfo(UpdateInfo updateInfo) {
        this.data = updateInfo;
    }


    public UpdateInfo getData() {
        return data;
    }

    public static class UpdateInfo {
        @SerializedName("name")
        public String name;
        @SerializedName("dehaat_info")
        public DehaatInfo dehaatInfo;
        @SerializedName("dehaat_id")
        public Long dehaat_id;

        public UpdateInfo(Long dehaat_id, String name, DehaatInfo dehaatInfo) {

            this.dehaat_id = dehaat_id;
            this.name = name;
            this.dehaatInfo = dehaatInfo;

        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public static class DehaatInfo {
            @SerializedName("x_name")
            public String x_name;
            @SerializedName("address_line")
            public String address_line;
            @SerializedName("block_id")
            public int block_id;
            @SerializedName("district_id")
            public int district_id;
            @SerializedName("state_id")
            public int state_id;


            public DehaatInfo(String x_name, String address_line, int state_id, int district_id, int block_id) {
                this.x_name = x_name;
                this.address_line = address_line;
                this.state_id = state_id;
                this.district_id = district_id;
                this.block_id = block_id;

            }

        }
    }
}
