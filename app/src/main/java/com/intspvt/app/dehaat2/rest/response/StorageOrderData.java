package com.intspvt.app.dehaat2.rest.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class StorageOrderData implements Serializable {

    @SerializedName("data")
    private List<StorageLoan> data;

    public List<StorageLoan> getData() {
        return data;
    }

    public static class StorageLoan implements Serializable {

        @SerializedName("storage")
        private Storage storage;

        @SerializedName("loan")
        private Loan loan;

        public Loan getLoan() {
            return loan;
        }

        public Storage getStorage() {
            return storage;
        }

        public class Storage implements Serializable {
            @SerializedName("date_order")
            private String date_order;

            @SerializedName("picking_type_id")
            private String picking_type_id;

            @SerializedName("name")
            private String name;

            @SerializedName("product_id")
            private String product_id;

            @SerializedName("id")
            private int id;

            @SerializedName("quantity")
            private int quantity;

            @SerializedName("quantity_unit")
            private String quantity_unit;

            @SerializedName("duration")
            private int duration;

            @SerializedName("duration_unit")
            private String duration_unit;

            public String getDate_order() {
                return date_order;
            }

            public String getName() {
                return name;
            }

            public String getProduct_id() {
                return product_id;
            }

            public int getId() {
                return id;
            }

            public int getQuantity() {
                return quantity;
            }

            public String getQuantity_unit() {
                return quantity_unit;
            }

            public int getDuration() {
                return duration;
            }

            public String getDuration_unit() {
                return duration_unit;
            }

            public String getPicking_type_id() {
                return picking_type_id;
            }
        }

        public class Loan implements Serializable {

            @SerializedName("loan_application_date")
            private String loan_application_date;

            @SerializedName("loan_approval_date")
            private String loan_approval_date;

            @SerializedName("start_date")
            private String start_date;

            @SerializedName("payment_due_date")
            private String payment_due_date;

            @SerializedName("outstanding_loan_amount")
            private int outstanding_loan_amount;

            @SerializedName("name")
            private String name;

            @SerializedName("type")
            private String type;

            @SerializedName("id")
            private int id;

            @SerializedName("approved_loan_amount")
            private int approved_loan_amount;

            @SerializedName("rate_of_interest")
            private int rate_of_interest;

            @SerializedName("duration")
            private int duration;

            @SerializedName("requested_loan_amount")
            private int requested_loan_amount;

            public String getLoan_application_date() {
                return loan_application_date;
            }

            public String getLoan_approval_date() {
                return loan_approval_date;
            }

            public String getName() {
                return name;
            }

            public int getId() {
                return id;
            }

            public int getApproved_loan_amount() {
                return approved_loan_amount;
            }

            public int getRate_of_interest() {
                return rate_of_interest;
            }

            public int getDuration() {
                return duration;
            }

            public String getType() {
                return type;
            }

            public int getRequested_loan_amount() {
                return requested_loan_amount;
            }

            public String getStart_date() {
                return start_date;
            }

            public String getPayment_due_date() {
                return payment_due_date;
            }

            public int getOutstanding_loan_amount() {
                return outstanding_loan_amount;
            }
        }
    }
}
