package com.intspvt.app.dehaat2.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SearchView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.intspvt.app.dehaat2.R;
import com.intspvt.app.dehaat2.activity.MainActivity;
import com.intspvt.app.dehaat2.adapter.RecyclerAdapterProductSelling;
import com.intspvt.app.dehaat2.database.DatabaseHandler;
import com.intspvt.app.dehaat2.rest.ApiCallback;
import com.intspvt.app.dehaat2.rest.AppRestClient;
import com.intspvt.app.dehaat2.rest.response.InputSellingResponse;
import com.intspvt.app.dehaat2.rest.response.ProductsDehaatFarmerSale;
import com.intspvt.app.dehaat2.rest.response.SaleLines;
import com.intspvt.app.dehaat2.utilities.AppPreference;
import com.intspvt.app.dehaat2.utilities.AppUtils;
import com.intspvt.app.dehaat2.utilities.InputSellingUpdate;
import com.intspvt.app.dehaat2.utilities.ItemClickListener;
import com.intspvt.app.dehaat2.utilities.UrlConstant;

import org.json.JSONException;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import in.myinnos.alphabetsindexfastscrollrecycler.IndexFastScrollRecyclerView;
import retrofit2.Call;
import retrofit2.Response;

public class InputSellingProductFragment extends BaseFragment implements View.OnClickListener,
        SearchView.OnQueryTextListener, AdapterView.OnItemSelectedListener, InputSellingUpdate,
        ItemClickListener {
    public static final String TAG = InputSellingProductFragment.class.getSimpleName();
    private TextView itemCount, clearItems;
    private IndexFastScrollRecyclerView recyclerView;
    private LinearLayout countBack;
    private ArrayList<SaleLines> inputSellingList = new ArrayList<>();
    private FirebaseAnalytics firebaseAnalytics;
    private RecyclerAdapterProductSelling recyclerAdapterProductSelling;
    private DatabaseHandler databaseHandler;
    private ArrayList<ProductsDehaatFarmerSale.Products> sPro = new ArrayList<>();
    private ArrayList<ProductsDehaatFarmerSale.Products> fPro = new ArrayList<>();
    private ArrayList<ProductsDehaatFarmerSale.Products> cPro = new ArrayList<>();
    private ArrayList<ProductsDehaatFarmerSale.Products> data = new ArrayList<>();
    private InputSellingResponse.Dehaat_farmer dehaat_farmer_data;

    private ArrayList<String> categoriesList = new ArrayList<>();
    private boolean checkPic;
    private Spinner category;
    private ImageView back;
    private String format2, format1, diffTime, selected;
    private SearchView searchView;

    public static InputSellingProductFragment newInstance() {
        return new InputSellingProductFragment();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_input_selling, null);
        showActionBar(false);
        ((MainActivity) activity).showDrawer(false);
        ((MainActivity) activity).showCart(false);
        databaseHandler = new DatabaseHandler(getActivity());
        firebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        itemCount = v.findViewById(R.id.itemSelectedCount);
        category = v.findViewById(R.id.category);
        recyclerView = v.findViewById(R.id.products_list);
        clearItems = v.findViewById(R.id.clearItems);
        clearItems.setVisibility(View.GONE);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setIndexBarColor("#ffffff");
        recyclerView.setIndexBarTextColor("#447d22");
        recyclerView.setIndexTextSize(14);
        countBack = v.findViewById(R.id.nextBtnBack);
        searchView = v.findViewById(R.id.search);
        back = v.findViewById(R.id.back);
        searchView.setOnQueryTextListener(this);
        countBack.setOnClickListener(this);
        back.setOnClickListener(this);
        category.setOnItemSelectedListener(this);
        Bundle bundle = getArguments();
        if (bundle != null) {
            dehaat_farmer_data = (InputSellingResponse.Dehaat_farmer) bundle.getSerializable("DATA");
            inputSellingList = dehaat_farmer_data.getSale_lines();
        }
        getProductList();
        categoriesList.clear();
        categoriesList.add(getString(R.string.all));
        categoriesList.add(getString(R.string.seed));
        categoriesList.add(getString(R.string.fertilizer));
        categoriesList.add(getString(R.string.protection));
        ArrayAdapter<String> adp = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, categoriesList);
        adp.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        category.setAdapter(adp);
        searchView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchView.setIconified(false);
            }
        });
        return v;


    }

    private void getProductList() {
        AppUtils.showProgressDialog(activity);
        AppRestClient client = AppRestClient.getInstance();
        Call<ProductsDehaatFarmerSale> call = client.getDehaatFarmerProductList();
        call.enqueue(new ApiCallback<ProductsDehaatFarmerSale>() {
            @Override
            public void onResponse(Response<ProductsDehaatFarmerSale> response) {
                sPro = new ArrayList<>();
                fPro = new ArrayList<>();
                cPro = new ArrayList<>();
                if (response.body() == null)
                    return;
                if (response.code() == 200) {
                    if (response.body().getProducts() == null)
                        return;
                    for (int i = 0; i < response.body().getProducts().size(); i++) {
                        if (!AppUtils.isNullCase(response.body().getProducts().get(i).getImage())) {
                            checkPic = databaseHandler.checkImage(response.body().getProducts().get(i).getImage());
                            if (!checkPic)
                                ((MainActivity) activity).generateAndCheckUrl(response.body().getProducts().get(i).getImage());

                        }
                        if (response.body().getProducts().get(i).getCategory() != null) {
                            if (response.body().getProducts().get(i).getCategory().equals(UrlConstant.SEED)) {
                                sPro.add(response.body().getProducts().get(i));
                            } else if (response.body().getProducts().get(i).getCategory().equals(UrlConstant.FERT)) {
                                fPro.add(response.body().getProducts().get(i));
                            } else if (response.body().getProducts().get(i).getCategory().equals(UrlConstant.CROP)) {
                                cPro.add(response.body().getProducts().get(i));
                            }
                        }
                    }
                    data = response.body().getProducts();
                    changeProductlist(selected);

                }
                AppUtils.hideProgressDialog();
                showItemSelected();

            }


            @Override
            public void onResponse401(Response<ProductsDehaatFarmerSale> response) throws JSONException {
                AppUtils.showSessionExpiredDialog(getActivity());

            }

        });
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.nextBtnBack:
                Bundle bundle = new Bundle();
                bundle.putSerializable("DATA", dehaat_farmer_data);
                ModifyDetailsFragment fragment = ModifyDetailsFragment.newInstance();
                fragment.setArguments(bundle);
                FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.frag_container, fragment).commit();
                break;
            case R.id.back:
                getActivity().onBackPressed();
                break;
        }
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if (data.size() != 0) {
            recyclerAdapterProductSelling.getFilter().filter(newText);
        }
        return false;
    }


    private void showItemSelected() {
        if (inputSellingList.size() == 0) {
            countBack.setVisibility(View.GONE);
        } else {
            countBack.setVisibility(View.VISIBLE);
            SaleLines saleLines;
            int c = 0;
            for (int i = 0; i < inputSellingList.size(); i++) {
                saleLines = inputSellingList.get(i);
                if (!saleLines.getIs_deleted())
                    c++;
            }
            itemCount.setText(getString(R.string.total_items) + " - " + c + "");
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch (parent.getId()) {

            case R.id.category:
                selected = parent.getItemAtPosition(position).toString();
                changeProductlist(selected);
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private void changeProductlist(String category) {
        if (getActivity() == null)
            return;
        if (category.equals(""))
            return;
        if (category.equals(getString(R.string.all))) {
            recyclerAdapterProductSelling = new RecyclerAdapterProductSelling(getActivity(), data,
                    inputSellingList, this, this);
        } else if (category.equals(getString(R.string.seed))) {
            recyclerAdapterProductSelling = new RecyclerAdapterProductSelling(getActivity(), sPro,
                    inputSellingList, this, this);

        } else if (category.equals(getString(R.string.fertilizer))) {
            recyclerAdapterProductSelling = new RecyclerAdapterProductSelling(getActivity(), fPro,
                    inputSellingList, this, this);

        } else if (category.equals(getString(R.string.protection))) {
            recyclerAdapterProductSelling = new RecyclerAdapterProductSelling(getActivity(), cPro,
                    inputSellingList, this, this);

        }
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 3));
        recyclerView.setAdapter(recyclerAdapterProductSelling);
        showItemSelected();
        if (!searchView.isIconified()) {
            searchView.setQuery("", false);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
        Bundle params = new Bundle();
        SimpleDateFormat s = new SimpleDateFormat("hh:mm:ss");
        format2 = s.format(new Date());
        diffTime = AppUtils.showTimeDiff(format1, format2, diffTime);
        params.putString("Dehaat_Farmer", diffTime + "   " + AppPreference.getInstance().getDEHAATI_MOBILE());
        firebaseAnalytics.setUserProperty("Mobile", AppPreference.getInstance().getDEHAATI_MOBILE());
        firebaseAnalytics.logEvent("Dehaat_Farmer", params);
    }

    @Override
    public void onResume() {
        super.onResume();
        SimpleDateFormat s = new SimpleDateFormat("hh:mm:ss");
        format1 = s.format(new Date());

    }

    @Override
    public void onItemClick(Integer pos, String s) {
        if (s.equals("true"))
            inputSellingList.get(pos).setIs_deleted(true);
        else
            inputSellingList.get(pos).setIs_deleted(false);
        showItemSelected();
    }

    @Override
    public void onItemClick(ArrayList<String> arrayList) {
        SaleLines saleLines = new SaleLines();
        saleLines.setIs_deleted(false);
        saleLines.setProduct_qty(1);
        saleLines.setProduct_id(Integer.parseInt(arrayList.get(0)));
        saleLines.setProduct_price(Float.parseFloat(arrayList.get(1)));
        saleLines.setProduct_name(arrayList.get(2));
        inputSellingList.add(saleLines);

        showItemSelected();
        changeProductlist(selected);

    }
}
