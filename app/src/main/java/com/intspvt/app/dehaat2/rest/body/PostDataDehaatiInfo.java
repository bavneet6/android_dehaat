package com.intspvt.app.dehaat2.rest.body;

import com.google.gson.annotations.SerializedName;

public class PostDataDehaatiInfo {

    @SerializedName("data")
    public PostdehaatiInfoRegister postdehaatiInfoRegister;

    public PostDataDehaatiInfo(PostdehaatiInfoRegister postdehaatiInfoRegister) {
        this.postdehaatiInfoRegister = postdehaatiInfoRegister;
    }

    public PostdehaatiInfoRegister getPostdehaatiInfoRegister() {
        return postdehaatiInfoRegister;
    }

    public void setPostdehaatiInfoRegister(PostdehaatiInfoRegister postdehaatiInfoRegister) {
        this.postdehaatiInfoRegister = postdehaatiInfoRegister;
    }

    public static class PostdehaatiInfoRegister {

        @SerializedName("name")
        public String name;
        @SerializedName("x_dehaat")
        public int dehaat_id;

        @SerializedName("mobile")
        public String mobile;


        public PostdehaatiInfoRegister(String mobile, String name, int dehaat_id) {
            this.mobile = mobile;
            this.name = name;
            this.dehaat_id = dehaat_id;

        }

        public String getName() {
            return name;
        }

        public String getMobile() {
            return mobile;
        }
    }

}
