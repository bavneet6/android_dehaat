package com.intspvt.app.dehaat2.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.intspvt.app.dehaat2.R;
import com.intspvt.app.dehaat2.rest.response.InputSellingResponse;
import com.intspvt.app.dehaat2.utilities.AppUtils;

import java.util.ArrayList;


/**
 * Created by DELL on 10/5/2017.
 */

public class RecyclerAdapterLedgerSaleHistory extends RecyclerView.Adapter<RecyclerAdapterLedgerSaleHistory.Order> {
    private Context context;
    private ArrayList<InputSellingResponse.Dehaat_farmer> data;
    private RecyclerView.LayoutManager layoutManager;

    public RecyclerAdapterLedgerSaleHistory(Activity activity, ArrayList<InputSellingResponse.Dehaat_farmer> data) {
        this.context = activity;
        this.data = data;
    }

    @Override
    public RecyclerAdapterLedgerSaleHistory.Order onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.template_ledger_purchase, parent, false);
        context = parent.getContext();
        return new RecyclerAdapterLedgerSaleHistory.Order(itemView);

    }

    @Override
    public void onBindViewHolder(final RecyclerAdapterLedgerSaleHistory.Order holder, int position) {
        final int pos = holder.getAdapterPosition();

        holder.orderAmt.setText(context.getString(R.string.rs) + data.get(pos).getAmount_total());
        holder.orderDate.setText(data.get(pos).getCreate_date());
        if (!AppUtils.isNullCase(data.get(pos).getCustomer_mobile()))
            holder.orderStatus.setText(data.get(pos).getCustomer_mobile());
        holder.orderId.setText(context.getString(R.string.orderId) + data.get(pos).getId());
        holder.totalItem.setText(context.getString(R.string.total_items) + data.get(pos).getSale_lines().size());
        holder.back.setOnClickListener(v -> {
            if (holder.detailsBack.getVisibility() != View.VISIBLE) {
                holder.detailsBack.setVisibility(View.VISIBLE);
                holder.details.setAdapter(new RecyclerAdapterSalesOrderLines((Activity) context, data.get(pos).getSale_lines()));
                layoutManager = new LinearLayoutManager(context);
                holder.details.setLayoutManager(layoutManager);
            } else {
                holder.detailsBack.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    public class Order extends RecyclerView.ViewHolder {
        private TextView orderStatus, orderId, orderDate, orderAmt, totalItem;
        private RecyclerView details;
        private LinearLayout detailsBack, back;


        public Order(View itemView) {
            super(itemView);
            orderStatus = itemView.findViewById(R.id.orderStatus);
            orderDate = itemView.findViewById(R.id.orderDate);
            orderId = itemView.findViewById(R.id.orderId);
            orderAmt = itemView.findViewById(R.id.orderAmt);
            totalItem = itemView.findViewById(R.id.totalItem);
            details = itemView.findViewById(R.id.details);
            detailsBack = itemView.findViewById(R.id.details_back);
            back = itemView.findViewById(R.id.back);
        }
    }

}

