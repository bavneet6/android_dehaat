package com.intspvt.app.dehaat2.rest.response;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;


/**
 * Created by DELL on 9/26/2017.
 */

public class OrderDataHistory {

    @SerializedName("amount_total")
    private float amount_total;

    @SerializedName("date_order")
    private String date_order;
    @SerializedName("note")
    private String note;
    @SerializedName("id")
    private int id;

    @SerializedName("name")
    private String name;
    @SerializedName("order_lines")
    private ArrayList<OrderLines> order_lines;

    @SerializedName("state")
    private String state;

    public String getNote() {
        return note;
    }

    public String getState() {
        return state;
    }

    public int getId() {
        return id;
    }

    public float getAmount_total() {
        return amount_total;
    }

    public ArrayList<OrderLines> getOrder_lines() {
        return order_lines;
    }

    public String getDate_order() {
        return date_order;
    }

    public String getName() {
        return name;
    }
}
