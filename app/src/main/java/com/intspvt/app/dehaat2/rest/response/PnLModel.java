package com.intspvt.app.dehaat2.rest.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class PnLModel {
    @SerializedName("data")
    private PnL data;

    public PnL getData() {
        return data;
    }

    public static class PnL implements Serializable {
        @SerializedName("purchase")
        private Data purchase;
        @SerializedName("sale")
        private Data sale;
        @SerializedName("pnl")
        private Data pnl;

        public Data getPnl() {
            return pnl;
        }

        public Data getPurchase() {
            return purchase;
        }

        public Data getSale() {
            return sale;
        }

        public class Data {
            @SerializedName("ytd_value")
            private Float ytd_value;
            @SerializedName("total_purchase_amount")
            private Float total_purchase_amount;
            @SerializedName("total_sale_amount")
            private Float total_sale_amount;
            @SerializedName("total_pnl_amount")
            private Float total_pnl_amount;
            @SerializedName("mtd_value")
            private Float mtd_value;
            @SerializedName("yd_value")
            private Float yd_value;

            public Float getTotal_sale_amount() {
                return total_sale_amount;
            }

            public Float getTotal_pnl_amount() {
                return total_pnl_amount;
            }

            public Float getTotal_purchase_amount() {
                return total_purchase_amount;
            }

            public Float getMtd_value() {
                return mtd_value;
            }

            public Float getYd_value() {
                return yd_value;
            }

            public Float getYtd_value() {
                return ytd_value;
            }
        }
    }
}
