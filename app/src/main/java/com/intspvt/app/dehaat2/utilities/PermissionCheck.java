package com.intspvt.app.dehaat2.utilities;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;

import static androidx.core.content.PermissionChecker.checkSelfPermission;

/**
 * Created by ba.kaur on 20/12/16.
 */

public class PermissionCheck {
    public static boolean checkCameraPermission(Context context) {
        return checkSelfPermission(context, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED;
    }

    public static boolean checkStoragePermission(Context context) {
        return checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
    }


    public static boolean checkAudioPermissiom(Context context) {
        return checkSelfPermission(context, Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED;
    }

    public static boolean checkFLocationPermission(Context context) {
        return checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }
}
