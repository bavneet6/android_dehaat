package com.intspvt.app.dehaat2.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.intspvt.app.dehaat2.R;
import com.intspvt.app.dehaat2.rest.response.OrderDataHistory;
import com.intspvt.app.dehaat2.utilities.AppUtils;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by DELL on 10/5/2017.
 */

public class RecyclerAdapterOrderHistory extends RecyclerView.Adapter<RecyclerAdapterOrderHistory.Order> {
    private Context context;
    private List<OrderDataHistory> data;
    private List<OrderDataHistory> filteredDataList;
    private RecyclerView.LayoutManager layoutManager;
    private boolean newListAdd = false;

    public RecyclerAdapterOrderHistory(Activity activity, List<OrderDataHistory> data) {
        this.context = activity;
        this.data = data;
        this.filteredDataList = data;
    }

    @Override
    public RecyclerAdapterOrderHistory.Order onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.order_history_layout, parent, false);
        context = parent.getContext();
        return new RecyclerAdapterOrderHistory.Order(itemView);

    }

    @Override
    public void onBindViewHolder(final RecyclerAdapterOrderHistory.Order holder, int position) {
        final int pos = holder.getAdapterPosition();

        if (data != null) {
            holder.orderStatus.setText(data.get(pos).getState());
            holder.orderAmt.setText(context.getString(R.string.rs) + data.get(pos).getAmount_total());
            holder.orderDate.setText(data.get(pos).getDate_order());
            holder.orderId.setText(context.getString(R.string.orderId) + data.get(pos).getName());
            holder.totalItem.setText(context.getString(R.string.total_items) + data.get(pos).getOrder_lines().size());
            if (!AppUtils.isNullCase(data.get(pos).getNote()))
                holder.notes.setText(context.getString(R.string.notes) + data.get(pos).getNote());
            // view more details of a particular order
            // view more details of a particular order
            holder.viewAll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (holder.viewAll.getText().toString().equals(context.getString(R.string.view_details))) {
                        holder.detailsBack.setVisibility(View.VISIBLE);
                        holder.details.setAdapter(new RecyclerAdapterOrderLines((Activity) context, data.get(pos).getOrder_lines()));
                        layoutManager = new LinearLayoutManager(context);
                        holder.details.setLayoutManager(layoutManager);
                        holder.viewAll.setText(context.getString(R.string.hideALL));
                    } else {
                        holder.detailsBack.setVisibility(View.GONE);
                        holder.viewAll.setText(context.getString(R.string.view_details));
                    }
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public Filter getFilter() {

        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {

                String charString = charSequence.toString();

                if (charString.isEmpty()) {
                    data = filteredDataList;
                } else {
                    List<OrderDataHistory> filteredList = new ArrayList<>();
                    for (int i = 0; i < filteredDataList.size(); i++) {
                        newListAdd = (String.valueOf(filteredDataList.get(i).getAmount_total()).contains(charString));
                        for (int j = 0; j < filteredDataList.get(i).getOrder_lines().size(); j++) {
                            if ((filteredDataList.get(i).getOrder_lines().get(j).getEngName().toLowerCase().contains(charString.toLowerCase())))
                                newListAdd = true;
                        }
                        if (newListAdd)
                            filteredList.add(filteredDataList.get(i));

                    }

                    data = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = data;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                data = (List<OrderDataHistory>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public class Order extends RecyclerView.ViewHolder {
        private TextView orderStatus, orderId, orderDate, orderAmt, totalItem, viewAll, orderAmtText, notes;
        private RecyclerView details;
        private LinearLayout detailsBack;

        public Order(View itemView) {
            super(itemView);
            orderStatus = itemView.findViewById(R.id.orderStatus);
            orderDate = itemView.findViewById(R.id.orderDate);
            orderId = itemView.findViewById(R.id.orderId);
            orderAmt = itemView.findViewById(R.id.orderAmt);
            totalItem = itemView.findViewById(R.id.totalItem);
            viewAll = itemView.findViewById(R.id.viewAll);
            orderAmtText = itemView.findViewById(R.id.total_amt_txt);
            notes = itemView.findViewById(R.id.notes);
            details = itemView.findViewById(R.id.details);
            detailsBack = itemView.findViewById(R.id.details_back);
        }
    }

}

