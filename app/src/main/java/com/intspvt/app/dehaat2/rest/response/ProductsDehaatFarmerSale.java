package com.intspvt.app.dehaat2.rest.response;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ProductsDehaatFarmerSale {

    @SerializedName("data")
    private ArrayList<Products> products;

    public ArrayList<Products> getProducts() {
        return products;
    }

    public class Products {
        @SerializedName("id")
        private Long productId;

        @SerializedName("name")
        private String productName;
        @SerializedName("price")
        private Float price;

        @SerializedName("image")
        private String image;
        @SerializedName("categ_id")
        private String category;

        public String getProductName() {
            return productName;
        }

        public Long getProductId() {
            return productId;
        }

        public String getImage() {
            return image;
        }

        public Float getPrice() {
            return price;
        }

        public String getCategory() {
            return category;
        }
    }
}
