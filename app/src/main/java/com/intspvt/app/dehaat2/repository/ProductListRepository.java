package com.intspvt.app.dehaat2.repository;

import androidx.lifecycle.MutableLiveData;

import com.intspvt.app.dehaat2.Dehaat2;
import com.intspvt.app.dehaat2.R;
import com.intspvt.app.dehaat2.rest.AppRestClient;
import com.intspvt.app.dehaat2.rest.response.Products;
import com.intspvt.app.dehaat2.rest.response.ProductsList;
import com.intspvt.app.dehaat2.utilities.AppUtils;

import java.net.ConnectException;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductListRepository {
    private static ProductListRepository mInstance;
    private ArrayList<Products> productsArrayList = new ArrayList<>();

    public static ProductListRepository getInstance() {
        if (mInstance == null)
            mInstance = new ProductListRepository();
        return mInstance;
    }

    public MutableLiveData<ArrayList<Products>> getProductData() {
        final MutableLiveData<ArrayList<Products>> data = new MutableLiveData<>();
        AppRestClient client = AppRestClient.getInstance();
        Call<ProductsList> call = client.productList(null);
        call.enqueue(new Callback<ProductsList>() {
            @Override
            public void onResponse(Call<ProductsList> call, Response<ProductsList> response) {
                if (response.body() == null)
                    return;
                productsArrayList = new ArrayList<>();
                if (response.code() == 200) {
                    if (response.body().getProducts() != null) {
                        productsArrayList = response.body().getProducts();
                    }
                } else if (response.code() == 500) {
                    AppUtils.showToast(Dehaat2.getInstance().getApplicationContext().getString(R.string.tech_prob));
                }
                data.setValue(productsArrayList);
            }

            @Override
            public void onFailure(Call<ProductsList> call, Throwable t) {
                if (t instanceof ConnectException) {
                    if (!AppUtils.haveNetworkConnection(Dehaat2.getInstance().getApplicationContext())) {
                        AppUtils.showToast(
                                Dehaat2.getInstance().getApplicationContext().getString(R.string.no_internet));
                    } else {
                        AppUtils.showToast(Dehaat2.getInstance().getApplicationContext().getString(R.string.server_no_respond));

                    }
                }
                data.setValue(new ArrayList<>());
            }


        });
        return data;
    }
}
