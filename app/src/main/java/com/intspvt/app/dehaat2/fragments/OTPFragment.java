package com.intspvt.app.dehaat2.fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentTransaction;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.auth.api.phone.SmsRetrieverClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.intspvt.app.dehaat2.R;
import com.intspvt.app.dehaat2.rest.AppRestClient;
import com.intspvt.app.dehaat2.rest.body.SendNumber;
import com.intspvt.app.dehaat2.rest.response.GetPersonalInfo;
import com.intspvt.app.dehaat2.rest.response.LoginResponse;
import com.intspvt.app.dehaat2.utilities.AppPreference;
import com.intspvt.app.dehaat2.utilities.AppUtils;
import com.intspvt.app.dehaat2.utilities.MySMSBroadcastReceiver;
import com.intspvt.app.dehaat2.utilities.UrlConstant;

import java.net.ConnectException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OTPFragment extends BaseFragment implements View.OnFocusChangeListener, TextWatcher, MySMSBroadcastReceiver.OTPReceiveListener, View.OnClickListener {
    public static final String TAG = OTPFragment.class.getSimpleName();
    private EditText otp1, otp2, otp3, otp4;
    private int focus = 0;
    private String otp, mob;
    private int app_view;
    private TextView msgLine, next, resendOTP;
    private MySMSBroadcastReceiver broadcastReceiver = new MySMSBroadcastReceiver();
    private MySMSBroadcastReceiver.OTPReceiveListener otpReceiveListener = this;

    public static OTPFragment newInstance() {
        return new OTPFragment();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_otp, null);
        showActionBar(false);
        otp1 = v.findViewById(R.id.otp1);
        otp2 = v.findViewById(R.id.otp2);
        otp3 = v.findViewById(R.id.otp3);
        otp4 = v.findViewById(R.id.otp4);
        next = v.findViewById(R.id.next);
        resendOTP = v.findViewById(R.id.resendOTP);
        msgLine = v.findViewById(R.id.msgLine);
        otp1.setOnFocusChangeListener(this);
        otp1.addTextChangedListener(this);
        otp2.setOnFocusChangeListener(this);
        otp2.addTextChangedListener(this);
        otp3.setOnFocusChangeListener(this);
        otp3.addTextChangedListener(this);
        otp4.setOnFocusChangeListener(this);
        otp4.addTextChangedListener(this);
        resendOTP.setOnClickListener(this);
        next.setOnClickListener(this);
        mob = AppPreference.getInstance().getDEHAATI_MOBILE();
        msgLine.setText("Please type the verification code sent to" + " +91" + mob);
        initiateSmsRetriever();
        broadcastReceiver.initOTPListener(otpReceiveListener);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(SmsRetriever.SMS_RETRIEVED_ACTION);
        getActivity().registerReceiver(broadcastReceiver, intentFilter);
        sendMobileNumber(AppPreference.getInstance().getDEHAATI_MOBILE());

        return v;
    }

    private void sendMobileNumber(final String mobile) {
        AppUtils.showProgressDialog(getActivity());
        SendNumber sendNumber = new SendNumber(mobile, null);
        AppRestClient client = AppRestClient.getInstance();
        Call<Void> call = client.sendNumber(sendNumber);

        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                AppUtils.hideProgressDialog();

            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
            }

        });
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private void verifyOTP() {
        otp = otp1.getText().toString() + otp2.getText().toString() + otp3.getText().toString() + otp4.getText().toString();
        AppUtils.showProgressDialog(getActivity());
        SendNumber sendNumber = new SendNumber(AppPreference.getInstance().getDEHAATI_MOBILE(), otp);
        AppRestClient client = AppRestClient.getInstance();
        Call<LoginResponse> call = client.verifyNumber(sendNumber);

        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                AppUtils.hideProgressDialog();
                if (response.code() == 200) {
                    AppPreference.getInstance().setAuthToken(response.body().getAuth_token());
                    getDehaatiInfo();
                } else if (response.code() == 401)
                    AppUtils.showToast("कृप्या सही OTP दर्ज करें");
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
            }

        });

    }

    private void initiateSmsRetriever() {
        SmsRetrieverClient client = SmsRetriever.getClient(getActivity());
        Task<Void> task = client.startSmsRetriever();
        task.addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                // Successfully started retriever, expect broadcast intent
                // ...
            }
        });
        task.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                // Failed to start retriever, inspect Exception for more details
                // ...
            }
        });
    }

    @Override
    public void onPause() {
        super.onPause();
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
    }

    private void getDehaatiInfo() {
        AppUtils.showProgressDialog(getActivity());
        AppRestClient client = AppRestClient.getInstance();
        Call<GetPersonalInfo> call = client.getInfo();
        call.enqueue(new Callback<GetPersonalInfo>() {
            @Override
            public void onResponse(Call<GetPersonalInfo> call, Response<GetPersonalInfo> response) {
                if (response.code() == 200) {
                    if (response.body() == null)
                        return;
                    if (response.body().getData() == null)
                        return;
                    if (response.body().getData().getDehaati_info() != null) {
                        AppUtils.storeDehaatiInfo(response.body().getData());
                        AppPreference.getInstance().setAPP_LOGIN(true);
                        if (getActivity() != null && isAdded()) {
                            FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                            transaction.replace(R.id.frag_cont, HomeIntroFragment.newInstance()).commit();
                        }
                    } else {
                        Bundle bundle = new Bundle();
                        bundle.putString("MOBILE", AppPreference.getInstance().getDEHAATI_MOBILE());
                        bundle.putString("APP_VIEW", "" + app_view);
                        bundle.putSerializable("HAS_DATA", response.body().getData());
                        addFragment(bundle);
                    }
                } else if (response.code() == 404) {//if data is not in oddoo then navigate that into the registration form
                    Bundle bundle = new Bundle();
                    bundle.putString("MOBILE", AppPreference.getInstance().getDEHAATI_MOBILE());
                    bundle.putString("APP_VIEW", "" + app_view);
                    bundle.putSerializable("HAS_DATA", null);
                    addFragment(bundle);

                } else if (response.code() == 403) {
                    final Dialog dialog = new Dialog(getActivity());
                    final TextView link, callToll;
                    dialog.setCanceledOnTouchOutside(false);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.dialog_farmer_login);
                    link = dialog.findViewById(R.id.dehaati_link);
                    callToll = dialog.findViewById(R.id.call);
                    callToll.setPaintFlags(callToll.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
                    callToll.setOnClickListener(view ->
                    {
                        Intent intent = new Intent(Intent.ACTION_DIAL);
                        intent.setData(Uri.parse("tel:" + UrlConstant.TOLL_FREE_NUMBER));
                        startActivity(intent);
                    });
                    link.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=app.intspvt.com.farmer")));
                            dialog.dismiss();
                        }
                    });

                    dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    dialog.getWindow().setGravity(Gravity.CENTER);
                    dialog.show();
                }
                AppUtils.hideProgressDialog();
            }

            @Override
            public void onFailure(Call<GetPersonalInfo> call, Throwable t) {
                onFailureMethod(t);
            }
        });
    }

    private void addFragment(Bundle bundle) {
        if (getActivity() == null)
            return;
        DehaatiInformationFragment fragment = DehaatiInformationFragment.newInstance();
        fragment.setArguments(bundle);
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frag_cont, fragment).commitAllowingStateLoss();
    }

    private void onFailureMethod(Throwable t) {
        AppUtils.hideProgressDialog();
        if (t instanceof ConnectException) {
            if (!AppUtils.haveNetworkConnection(getActivity()))
                AppUtils.showToast(getString(R.string.no_internet));
            else
                AppUtils.showToast(getString(R.string.server_no_respond));
        }
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {

        switch (v.getId()) {
            case R.id.otp1:
                focus = 1;
                break;
            case R.id.otp2:
                focus = 2;
                break;
            case R.id.otp3:
                focus = 3;
                break;
            case R.id.otp4:
                focus = 4;
                break;

        }
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        switch (focus) {
            case 1:
                if (otp1.getText().toString().length() == 1) {
                    otp2.requestFocus();

                } else {
                    otp1.requestFocus();

                }
            case 2:
                if (otp2.getText().toString().length() == 1) {
                    otp3.requestFocus();

                } else {
                    otp2.requestFocus();
                }
                break;

            case 3:
                if (otp3.getText().toString().length() == 1) {
                    otp4.requestFocus();

                } else {
                    otp3.requestFocus();
                }
                break;

            case 4:
                if (otp4.getText().toString().length() == 1) {
                    otp4.requestFocus();
                    verifyOTP();
                }
                break;
        }
    }

    @Override
    public void onOTPReceived(String otp) {
        Log.e("OTP", otp);
        String otpString = otp.substring(18, 22);
        if (!AppUtils.isNullCase(otpString)) {
            otp1.setText(otpString.substring(0, 1));
            otp2.setText(otpString.substring(1, 2));
            otp3.setText(otpString.substring(2, 3));
            otp4.setText(otpString.substring(3, 4));
            verifyOTP();
        }
        if (getActivity() == null)
            return;
        if (broadcastReceiver != null) {
            LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(broadcastReceiver);
        }
    }

    @Override
    public void onOTPTimeOut() {

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.resendOTP:
                sendMobileNumber(AppPreference.getInstance().getDEHAATI_MOBILE());
                break;
            case R.id.next:
                if (!TextUtils.isEmpty(otp))
                    if (otp.length() != 4)
                        AppUtils.showToast("कृप्या 4 अंक ओटीपी दर्ज करें");
                    else
                        verifyOTP();
                break;
        }
    }
}