package com.intspvt.app.dehaat2.rest.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class GetAddressMapping {

    @SerializedName("data")
    private ArrayList<GetData> getAddress;


    public ArrayList<GetData> getGetAddress() {
        return getAddress;
    }

    public static class GetData implements Serializable {

        @SerializedName("id")
        private int id;
        @SerializedName("name")
        private String name;


        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}
