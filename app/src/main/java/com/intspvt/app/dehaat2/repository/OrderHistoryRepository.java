package com.intspvt.app.dehaat2.repository;

import androidx.lifecycle.MutableLiveData;

import com.intspvt.app.dehaat2.Dehaat2;
import com.intspvt.app.dehaat2.R;
import com.intspvt.app.dehaat2.rest.AppRestClient;
import com.intspvt.app.dehaat2.rest.response.OrderDataHistory;
import com.intspvt.app.dehaat2.rest.response.OrderHistory;
import com.intspvt.app.dehaat2.utilities.AppUtils;

import java.net.ConnectException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderHistoryRepository {

    private static OrderHistoryRepository mInstance;
    private List<OrderDataHistory> orderDataHistories = new ArrayList<>();

    public static OrderHistoryRepository getInstance() {
        if (mInstance == null)
            mInstance = new OrderHistoryRepository();
        return mInstance;
    }

    public MutableLiveData<List<OrderDataHistory>> getOrderData(String key, String from_date, String to_date) {
        MutableLiveData<List<OrderDataHistory>> data = new MutableLiveData<>();
        AppRestClient client = AppRestClient.getInstance();
        Call<OrderHistory> call = client.orderHistory(key, from_date, to_date);
        call.enqueue(new Callback<OrderHistory>() {
            @Override
            public void onResponse(Call<OrderHistory> call, Response<OrderHistory> response) {
                if (response.body() == null)
                    return;
                orderDataHistories = new ArrayList<>();
                if (response.code() == 200) {
                    if (response.body().getData() != null) {
                        orderDataHistories = response.body().getData();
                    }
                } else if (response.code() == 500) {
                    AppUtils.showToast(Dehaat2.getInstance().getApplicationContext().getString(R.string.tech_prob));
                }
                data.setValue(orderDataHistories);

            }

            @Override
            public void onFailure(Call<OrderHistory> call, Throwable t) {
                if (t instanceof ConnectException) {
                    if (!AppUtils.haveNetworkConnection(Dehaat2.getInstance().getApplicationContext())) {
                        AppUtils.showToast(
                                Dehaat2.getInstance().getApplicationContext().getString(R.string.no_internet));
                    } else {
                        AppUtils.showToast(Dehaat2.getInstance().getApplicationContext().getString(R.string.server_no_respond));

                    }
                }
                data.setValue(new ArrayList<>());
            }

        });
        return data;
    }
}
