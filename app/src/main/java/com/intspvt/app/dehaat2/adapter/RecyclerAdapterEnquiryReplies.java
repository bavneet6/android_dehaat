package com.intspvt.app.dehaat2.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.intspvt.app.dehaat2.R;
import com.intspvt.app.dehaat2.activity.MainActivity;
import com.intspvt.app.dehaat2.database.DatabaseHandler;
import com.intspvt.app.dehaat2.fragments.EnquiryDetailHistoryFragment;
import com.intspvt.app.dehaat2.rest.response.EnquiryReply;
import com.intspvt.app.dehaat2.utilities.AppUtils;

import java.net.URL;
import java.util.ArrayList;


/**
 * Created by DELL on 3/26/2018.
 */

public class RecyclerAdapterEnquiryReplies extends RecyclerView.Adapter<RecyclerAdapterEnquiryReplies.Replies> {
    Context context;
    private ArrayList<EnquiryReply> enquiryReply;
    private boolean checkPic;
    private String getVoiceUrl;
    private DatabaseHandler databaseHandler;

    public RecyclerAdapterEnquiryReplies(ArrayList<EnquiryReply> enquiryReply) {
        this.enquiryReply = enquiryReply;
    }

    @Override
    public RecyclerAdapterEnquiryReplies.Replies onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.template_enquiry_replies, parent, false);
        context = parent.getContext();
        return new RecyclerAdapterEnquiryReplies.Replies(itemView);

    }

    @Override
    public void onBindViewHolder(final RecyclerAdapterEnquiryReplies.Replies holder, final int position) {
        databaseHandler = new DatabaseHandler(context);
        final int pos = holder.getAdapterPosition();
        URL url = null;
        if (!AppUtils.isNullCase(enquiryReply.get(pos).getBody())) {
            holder.heading.loadDataWithBaseURL(null, enquiryReply.get(pos).getBody(), "text/html", "UTF-8", null);
        } else {
            holder.heading.loadDataWithBaseURL(null, "", "text/html", "UTF-8", null);
        }

        if (enquiryReply.get(pos).getAuthor() != null)
            if (enquiryReply.get(pos).getAuthor().equals("user"))
                holder.reply.setText(context.getString(R.string.yourReply));
            else
                holder.reply.setText(context.getString(R.string.dehaatReply));

        holder.heading.setBackgroundColor(Color.parseColor("#fafafa"));
        holder.write_date.setText(enquiryReply.get(pos).getWrite_date().substring(0, 10));

        if (enquiryReply.get(pos).getAttachments() != null) {
            if (enquiryReply.get(pos).getAttachments().getImages() != null) {
                holder.imagesRec.setVisibility(View.VISIBLE);
                holder.imagesRec.setAdapter(new RecyclerAdapterEnquiryImageList(enquiryReply.get(pos).getAttachments().getImages()));
                holder.imagesRec.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
            } else {
                holder.imagesRec.setVisibility(View.GONE);
            }
            if (!AppUtils.isNullCase(enquiryReply.get(pos).getAttachments().getVoice())) {
                holder.play.setVisibility(View.VISIBLE);

                checkPic = databaseHandler.checkImage(enquiryReply.get(pos).getAttachments().getVoice());

                if (!checkPic) {
                    ((MainActivity) context).generateAndCheckUrl(enquiryReply.get(pos).getAttachments().getVoice());

                }
                getVoiceUrl = databaseHandler.getFileUrl(enquiryReply.get(pos).getAttachments().getVoice());
            } else {
                holder.play.setVisibility(View.GONE);
            }

        }

        holder.play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (AppUtils.haveNetworkConnection(context)) {
                    getVoiceUrl = databaseHandler.getFileUrl(enquiryReply.get(pos).getAttachments().getVoice());
                    EnquiryDetailHistoryFragment.newInstance().playAudio(holder.play, "" + getVoiceUrl, R.drawable.play, R.drawable.pause, enquiryReply.get(pos).getAttachments().getVoice(), context);
                } else {
                    AppUtils.showToast(
                            context.getString(R.string.no_internet));
                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return enquiryReply.size();
    }

    public class Replies extends RecyclerView.ViewHolder {
        TextView write_date, reply;
        WebView heading;
        RecyclerView imagesRec;
        LinearLayout back;
        private ImageView play;

        public Replies(View itemView) {
            super(itemView);

            heading = itemView.findViewById(R.id.enHead);
            write_date = itemView.findViewById(R.id.write_date);
            reply = itemView.findViewById(R.id.reply);
            imagesRec = itemView.findViewById(R.id.images_rec);
            play = itemView.findViewById(R.id.play);
            back = itemView.findViewById(R.id.back);
        }


    }

}
