package com.intspvt.app.dehaat2.rest.body;

import com.google.gson.annotations.SerializedName;
import com.intspvt.app.dehaat2.rest.response.CropData;

/**
 * Created by DELL on 11/23/2017.
 */

public class FarmProduce {

    @SerializedName("data")
    private CropData data;

    public FarmProduce(CropData data) {
        this.data = data;

    }

    public CropData getData() {
        return data;
    }
}
