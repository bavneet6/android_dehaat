package com.intspvt.app.dehaat2.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.intspvt.app.dehaat2.R;
import com.intspvt.app.dehaat2.rest.response.InventoryStock;

import java.util.ArrayList;

public class RecyclerAdapterInventoryStock extends RecyclerView.Adapter<RecyclerAdapterInventoryStock.Inventory> {
    private Context context;
    private ArrayList<InventoryStock.Data> inventoryList;
    private String getPic;

    public RecyclerAdapterInventoryStock(FragmentActivity activity, ArrayList<InventoryStock.Data> data) {
        this.context = activity;
        this.inventoryList = data;

    }

    @Override
    public RecyclerAdapterInventoryStock.Inventory onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.template_inventory_stock, parent, false);
        context = parent.getContext();
        return new RecyclerAdapterInventoryStock.Inventory(itemView);

    }

    @Override
    public void onBindViewHolder(final RecyclerAdapterInventoryStock.Inventory holder, final int position) {
        final int pos = holder.getAdapterPosition();
        holder.commodityName.setText(inventoryList.get(pos).getName());
        holder.qty.setText("" + inventoryList.get(pos).getQty());
    }

    @Override
    public int getItemCount() {
        return inventoryList.size();
    }

    public class Inventory extends RecyclerView.ViewHolder {
        private TextView commodityName, qty;

        public Inventory(View itemView) {
            super(itemView);
            qty = itemView.findViewById(R.id.qty);
            commodityName = itemView.findViewById(R.id.commodityName);
        }
    }
}
