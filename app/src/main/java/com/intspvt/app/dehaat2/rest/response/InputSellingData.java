package com.intspvt.app.dehaat2.rest.response;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class InputSellingData {

    @SerializedName("total_amt")
    private float total_amt;
    @SerializedName("id")
    private Long id;
    @SerializedName("farmer_no")
    private String farmer_no;
    @SerializedName("state")
    private String state;
    @SerializedName("inventory_status")
    private String inventory_status;
    @SerializedName("name")
    private String name;
    @SerializedName("sale_lines")
    private ArrayList<SaleLines> sale_lines;


    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public void setInventory_status(String inventory_status) {
        this.inventory_status = inventory_status;
    }

    public void setTotal_amt(float total_amt) {
        this.total_amt = total_amt;
    }

    public void setSale_lines(ArrayList<SaleLines> sale_lines) {
        this.sale_lines = sale_lines;
    }

    public void setFarmer_no(String farmer_no) {
        this.farmer_no = farmer_no;
    }
}
