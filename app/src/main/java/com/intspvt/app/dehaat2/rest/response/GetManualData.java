package com.intspvt.app.dehaat2.rest.response;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by DELL on 11/28/2017.
 */

public class GetManualData {
    @SerializedName("data")
    private ArrayList<ManualData> data;

    @SerializedName("message")
    private String message;

    @SerializedName("status")
    private String status;

    public ArrayList<ManualData> getData() {
        return data;
    }

    public String getMessage() {
        return message;
    }

    public String getStatus() {
        return status;
    }

    public class ManualData {
        @SerializedName("id")
        public int id;

        @SerializedName("image")
        public String image;
        @SerializedName("name")
        public String name;
        @SerializedName("manuals")
        public ArrayList<Manual> manuals;

        public String getName() {
            return name;
        }

        public int getId() {
            return id;
        }

        public ArrayList<Manual> getManuals() {
            return manuals;
        }

        public String getImage() {
            return image;
        }

    }

}
