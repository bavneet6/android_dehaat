package com.intspvt.app.dehaat2.rest.body;

import com.google.gson.annotations.SerializedName;
import com.intspvt.app.dehaat2.rest.response.InputSellingData;

public class InputSelling {
    @SerializedName("data")
    private InputSellingData inputSellingData;

    public InputSelling(InputSellingData inputSellingData) {
        this.inputSellingData = inputSellingData;
    }

    public InputSellingData getInputSellingData() {
        return inputSellingData;
    }

    public void setInputSellingData(InputSellingData inputSellingData) {
        this.inputSellingData = inputSellingData;
    }
}
