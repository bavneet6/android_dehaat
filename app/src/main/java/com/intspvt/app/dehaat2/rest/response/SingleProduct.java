package com.intspvt.app.dehaat2.rest.response;

import com.google.gson.annotations.SerializedName;

public class SingleProduct {


    @SerializedName("data")
    private Products products;

    public Products getProducts() {
        return products;
    }
}
