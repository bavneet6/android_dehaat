package com.intspvt.app.dehaat2.rest.response;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by DELL on 9/8/2017.
 */

public class Trending {
    @SerializedName("data")
    private ArrayList<TrendingData> datas;

    public ArrayList<TrendingData> getDatas() {
        return datas;
    }


    public class TrendingData {
        @SerializedName("content")
        public String content;

        @SerializedName("image")
        public String image;
        @SerializedName("name")
        public String name;

        public String getContent() {
            return content;
        }

        public String getImage() {
            return image;
        }

        public String getName() {
            return name;
        }
    }

}
