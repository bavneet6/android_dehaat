package com.intspvt.app.dehaat2.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.intspvt.app.dehaat2.R;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

public class RecyclerAdapterStorageLoanDetail extends RecyclerView.Adapter<RecyclerAdapterStorageLoanDetail.CreditData> {
    private LinkedHashMap<String, String> data;
    private Context context;
    private ArrayList<String> keyList = new ArrayList<>();
    private ArrayList<String> valueList = new ArrayList<>();


    public RecyclerAdapterStorageLoanDetail(Activity activity, LinkedHashMap<String, String> data) {
        this.context = activity;
        this.data = data;

        for (Map.Entry<String, String> entry : data.entrySet()) {
            keyList.add(entry.getKey());
            valueList.add(entry.getValue());
        }
    }

    @Override
    public RecyclerAdapterStorageLoanDetail.CreditData onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.template_storage_loan_detail, parent, false);
        context = parent.getContext();
        return new RecyclerAdapterStorageLoanDetail.CreditData(itemView);

    }

    @Override
    public void onBindViewHolder(final RecyclerAdapterStorageLoanDetail.CreditData holder, final int position) {
        final int pos = holder.getAdapterPosition();
        holder.key.setText(keyList.get(pos));
        holder.value.setText(valueList.get(pos));
    }


    @Override
    public int getItemCount() {
        return data.size();
    }

    public class CreditData extends RecyclerView.ViewHolder {
        TextView key, value;

        public CreditData(View itemView) {
            super(itemView);
            key = itemView.findViewById(R.id.key);
            value = itemView.findViewById(R.id.value);

        }


    }

}

