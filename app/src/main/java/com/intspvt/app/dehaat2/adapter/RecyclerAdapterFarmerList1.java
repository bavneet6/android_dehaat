package com.intspvt.app.dehaat2.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SectionIndexer;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.github.ivbaranov.mli.MaterialLetterIcon;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.intspvt.app.dehaat2.R;
import com.intspvt.app.dehaat2.activity.MainActivity;
import com.intspvt.app.dehaat2.database.DatabaseHandler;
import com.intspvt.app.dehaat2.rest.PicassoCallback;
import com.intspvt.app.dehaat2.rest.response.FarmerListInfo;
import com.intspvt.app.dehaat2.utilities.AppUtils;
import com.intspvt.app.dehaat2.utilities.RoundImageView;
import com.squareup.picasso.Picasso;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

public class RecyclerAdapterFarmerList1 extends RecyclerView.Adapter<RecyclerAdapterFarmerList1.FarmerList> implements Filterable, SectionIndexer {
    private static final Random RANDOM = new Random();
    private Context context;
    private ArrayList<FarmerListInfo> farmerListInfos;
    private ArrayList<FarmerListInfo> farmerFilterList;
    private String firstLet, getPic;
    private int[] mMaterialColors;
    private ArrayList<Integer> mSectionPositions;
    private boolean checkPic;
    private FirebaseAnalytics mFirebaseAnalytics;
    private DatabaseHandler databaseHandler;

    public RecyclerAdapterFarmerList1(ArrayList<FarmerListInfo> farmerListInfo) {
        this.farmerListInfos = farmerListInfo;
        this.farmerFilterList = farmerListInfo;
    }

    @Override
    public RecyclerAdapterFarmerList1.FarmerList onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.template_farmer_list1, parent, false);
        context = parent.getContext();
        return new RecyclerAdapterFarmerList1.FarmerList(itemView);

    }

    @Override
    public Object[] getSections() {
        List<String> sections = new ArrayList<>();
        mSectionPositions = new ArrayList<>();
        for (int i = 0, size = farmerListInfos.size(); i < size; i++) {
            String input = farmerListInfos.get(i).getName();
            input = input.replace("\n", "");
            input = input.trim();
            if (AppUtils.isNullCase(input)) {
                firstLet = "#";
            } else if (!String.valueOf(input.charAt(0)).toUpperCase().matches(".*[A-Z].*")) {
                firstLet = "#";
            } else {
                firstLet = String.valueOf(input.charAt(0)).toUpperCase();
            }
            if (!sections.contains(firstLet)) {
                sections.add(firstLet);
                mSectionPositions.add(i);
            }
        }
        return sections.toArray(new String[0]);
    }

    @Override
    public int getPositionForSection(int i) {
        return mSectionPositions.get(i);
    }

    @Override
    public int getSectionForPosition(int i) {
        return 0;
    }


    @Override
    public void onBindViewHolder(final RecyclerAdapterFarmerList1.FarmerList holder, final int position) {
        mMaterialColors = context.getResources().getIntArray(R.array.colors);
        databaseHandler = new DatabaseHandler(context);
        holder.setIsRecyclable(false);
        final FarmerListInfo farmerListInfo = farmerListInfos.get(position);
        final String photo = farmerListInfo.getImage();
        //getPic finds the url from db and if it returns the url then picasso print that
        getPic = null;
        if (!AppUtils.isNullCase(photo)) {
            holder.icon.setVisibility(View.GONE);
            holder.faramerPic.setVisibility(View.VISIBLE);
            checkPic = databaseHandler.checkImage(photo);
            if (!checkPic) {
                ((MainActivity) context).generateAndCheckUrl(photo);
            }
            getPic = databaseHandler.getFileUrl(photo);
            if (getPic != null) {
                final Picasso picasso = Picasso.with(context.getApplicationContext());
                picasso.setLoggingEnabled(true);
                picasso.load(getPic).fit().into(holder.faramerPic, new PicassoCallback(photo) {

                    @Override
                    public void onErrorPic(URL url) {
                        picasso.load("" + url).error(R.drawable.no_image).into(holder.faramerPic);
                    }
                });
            }
        } else {
            holder.icon.setShapeColor(mMaterialColors[RANDOM.nextInt(mMaterialColors.length)]);
            holder.faramerPic.setVisibility(View.GONE);
            holder.icon.setVisibility(View.VISIBLE);
            holder.icon.setLetter(farmerListInfo.getName().substring(0, 1));

        }
        if (!AppUtils.isNullCase(farmerListInfo.getName()))
            holder.name.setText(farmerListInfo.getName());


        holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog dialog = new Dialog(new ContextThemeWrapper(context, R.style.DialogSlideAnim));
                final TextView name, state, distrcit, block, village, mobile, panchayat;
                final ImageView message, back, call;
                final LinearLayout cardView;
                dialog.setCanceledOnTouchOutside(true);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.fragment_farmer_details);
                name = dialog.findViewById(R.id.txt_name);
                state = dialog.findViewById(R.id.txt_state);
                distrcit = dialog.findViewById(R.id.txt_district);
                block = dialog.findViewById(R.id.txt_block);
                panchayat = dialog.findViewById(R.id.txt_panchayat);
                village = dialog.findViewById(R.id.txt_village);
                mobile = dialog.findViewById(R.id.txt_mobile);
                cardView = dialog.findViewById(R.id.backCard);
                call = dialog.findViewById(R.id.txtcall);
                message = dialog.findViewById(R.id.txt_message);
                back = dialog.findViewById(R.id.back);
                name.setText(holder.name.getText());
                if (!AppUtils.isNullCase(farmerListInfo.getState())) {
                    state.setText(context.getString(R.string.state) + " - " + farmerListInfo.getState());
                    state.setVisibility(View.VISIBLE);
                } else {
                    state.setVisibility(View.GONE);
                }
                if (!AppUtils.isNullCase(farmerListInfo.getDistrict())) {
                    distrcit.setText(context.getString(R.string.district) + " - " + farmerListInfo.getDistrict());
                    distrcit.setVisibility(View.VISIBLE);
                } else {
                    distrcit.setVisibility(View.GONE);
                }
                if (!AppUtils.isNullCase(farmerListInfo.getPanchayat())) {
                    panchayat.setText(context.getString(R.string.panchayat) + " - " + farmerListInfo.getPanchayat());
                    panchayat.setVisibility(View.VISIBLE);
                } else {
                    block.setVisibility(View.GONE);
                }
                if (!AppUtils.isNullCase(farmerListInfo.getBlock())) {
                    block.setText(context.getString(R.string.block) + " - " + farmerListInfo.getBlock());
                    block.setVisibility(View.VISIBLE);
                } else {
                    block.setVisibility(View.GONE);
                }
                if (!AppUtils.isNullCase(farmerListInfo.getVillage())) {
                    village.setText(context.getString(R.string.village) + " - " + farmerListInfo.getVillage());
                    village.setVisibility(View.VISIBLE);
                } else {
                    village.setVisibility(View.GONE);
                }
                if (!AppUtils.isNullCase(farmerListInfo.getMobile())) {
                    mobile.setText(farmerListInfo.getMobile());
                    cardView.setVisibility(View.VISIBLE);
                } else {
                    cardView.setVisibility(View.GONE);
                }
                back.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });
                //getPic finds the url from db and if it returns the url then picasso print that
                call.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(Intent.ACTION_DIAL);
                        intent.setData(Uri.parse("tel:" + mobile.getText().toString()));
                        context.startActivity(intent);
                    }
                });
                message.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", mobile.getText().toString(), null)));

                    }
                });
                Drawable d = new ColorDrawable(context.getResources().getColor(R.color.boxcolor));
                d.setAlpha(100);
                dialog.getWindow().setBackgroundDrawable(d);
                dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
                dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                dialog.getWindow().setGravity(Gravity.BOTTOM);
                dialog.show();
                mFirebaseAnalytics = FirebaseAnalytics.getInstance(context);
                Bundle params = new Bundle();
                SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                String format = s.format(new Date());
                params.putString("FarmerDetail", format);
                mFirebaseAnalytics.logEvent("FarmerDetail", params);
            }
        });


    }

    @Override
    public Filter getFilter() {

        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {

                String charString = charSequence.toString();

                if (charString.isEmpty()) {

                    farmerListInfos = farmerFilterList;
                } else {

                    ArrayList<FarmerListInfo> filteredList = new ArrayList<>();

                    for (FarmerListInfo list : farmerFilterList) {

                        if (list.getName().toLowerCase().contains(charString.toLowerCase()) || (list.getMobile().contains(charString))) {
                            filteredList.add(list);
                        }
                    }

                    farmerListInfos = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = farmerListInfos;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                farmerListInfos = (ArrayList<FarmerListInfo>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    @Override
    public int getItemCount() {
        return farmerListInfos.size();
    }

    public class FarmerList extends RecyclerView.ViewHolder {
        private TextView name;
        private LinearLayout relativeLayout;
        private MaterialLetterIcon icon;
        private RoundImageView faramerPic;

        public FarmerList(View itemView) {
            super(itemView);
            relativeLayout = itemView.findViewById(R.id.back);
            name = itemView.findViewById(R.id.name);
            icon = itemView.findViewById(R.id.icon);
            faramerPic = itemView.findViewById(R.id.image);

        }


    }


}
