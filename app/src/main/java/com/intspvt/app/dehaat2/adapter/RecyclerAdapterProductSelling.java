package com.intspvt.app.dehaat2.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SectionIndexer;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.intspvt.app.dehaat2.R;
import com.intspvt.app.dehaat2.database.DatabaseHandler;
import com.intspvt.app.dehaat2.rest.PicassoCallback;
import com.intspvt.app.dehaat2.rest.response.ProductsDehaatFarmerSale;
import com.intspvt.app.dehaat2.rest.response.SaleLines;
import com.intspvt.app.dehaat2.utilities.AppUtils;
import com.intspvt.app.dehaat2.utilities.InputSellingUpdate;
import com.intspvt.app.dehaat2.utilities.ItemClickListener;
import com.squareup.picasso.Picasso;

import java.net.URL;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class RecyclerAdapterProductSelling extends RecyclerView.Adapter<RecyclerAdapterProductSelling.Products> implements SectionIndexer {
    List<String> indexes;
    private Context context;
    private DatabaseHandler databaseHandler;
    private ArrayList<ProductsDehaatFarmerSale.Products> products;
    private ArrayList<ProductsDehaatFarmerSale.Products> filteredProducts;
    private String firstLet, getPic;
    private ArrayList<Integer> mSectionPositions;
    private ArrayList<SaleLines> saleLinesList;
    private InputSellingUpdate inputSellingUpdate;
    private ItemClickListener itemClickListener;
    private LinkedHashMap<String, SaleLines> salesLinesMap = new LinkedHashMap<>();


    public RecyclerAdapterProductSelling(Context context, ArrayList<ProductsDehaatFarmerSale.Products> data, ArrayList<SaleLines> saleLinesList, InputSellingUpdate inputSellingUpdate, ItemClickListener itemClickListener) {
        this.context = context;
        this.products = data;
        this.saleLinesList = saleLinesList;
        this.filteredProducts = products;
        this.inputSellingUpdate = inputSellingUpdate;
        this.itemClickListener = itemClickListener;
        for (int i = 0; i < saleLinesList.size(); i++)
            salesLinesMap.put("" + saleLinesList.get(i).getProduct_id(), saleLinesList.get(i));

        indexes = new ArrayList<String>(salesLinesMap.keySet());

        databaseHandler = new DatabaseHandler(context);

    }

    @Override
    public RecyclerAdapterProductSelling.Products onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.template_input_selling_product, parent, false);
        context = parent.getContext();
        return new RecyclerAdapterProductSelling.Products(itemView);

    }

    @Override
    public void onBindViewHolder(final RecyclerAdapterProductSelling.Products holder, final int position) {
        holder.setIsRecyclable(false);
        final int pos = holder.getAdapterPosition();
        holder.productMrp.setText(context.getString(R.string.rs) + "" + products.get(pos).getPrice());
        holder.productName.setText(products.get(pos).getProductName());
        if ((salesLinesMap.containsKey("" + products.get(pos).getProductId())) &&
                (!salesLinesMap.get("" + products.get(pos).getProductId()).getIs_deleted())) {
            holder.tick.setVisibility(View.VISIBLE);
        } else
            holder.tick.setVisibility(View.INVISIBLE);
        getPic = null;
        if (!AppUtils.isNullCase(products.get(pos).getImage())) {

            getPic = databaseHandler.getFileUrl(products.get(pos).getImage());
            if (getPic != null) {
                final Picasso picasso = Picasso.with(context.getApplicationContext());
                picasso.setLoggingEnabled(true);
                picasso.load(getPic).into(holder.productImg, new PicassoCallback(products.get(pos).getImage()) {

                    @Override
                    public void onErrorPic(URL url) {
                        picasso.load("" + url).error(R.drawable.no_product_image).into(holder.productImg);
                    }
                });
            }

        }
        holder.back.setOnClickListener(v -> {
            if (salesLinesMap.containsKey("" + products.get(pos).getProductId())) {
                if (salesLinesMap.get("" + products.get(pos).getProductId()).getIs_deleted()) {
                    holder.tick.setVisibility(View.VISIBLE);
                    inputSellingUpdate.onItemClick(indexes.indexOf("" + products.get(pos).getProductId()), "false");
                } else {
                    holder.tick.setVisibility(View.INVISIBLE);
                    inputSellingUpdate.onItemClick(indexes.indexOf("" + products.get(pos).getProductId()), "true");
                }
            } else {
                holder.tick.setVisibility(View.VISIBLE);
                ArrayList<String> salesLinesList = new ArrayList<>();
                salesLinesList.add("" + products.get(pos).getProductId());
                salesLinesList.add("" + products.get(pos).getPrice());
                salesLinesList.add("" + products.get(pos).getProductName());
                itemClickListener.onItemClick(salesLinesList);
            }
        });

    }


    public Filter getFilter() {

        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {

                String charString = charSequence.toString();

                if (charString.isEmpty()) {

                    products = filteredProducts;
                } else {

                    ArrayList<ProductsDehaatFarmerSale.Products> filteredList = new ArrayList<>();

                    for (ProductsDehaatFarmerSale.Products list : filteredProducts) {

                        if (list.getProductName().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(list);
                        }
                    }

                    products = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = products;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                products = (ArrayList<ProductsDehaatFarmerSale.Products>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }


    @Override
    public int getItemCount() {
        return products.size();
    }

    @Override
    public Object[] getSections() {
        List<String> sections = new ArrayList<>();
        mSectionPositions = new ArrayList<>();
        for (int i = 0; i < products.size(); i++) {
            String input;
            input = products.get(i).getProductName();
            input = input.replace("\n", "");
            input = input.trim();
            if (AppUtils.isNullCase(input)) {
                firstLet = "#";
            } else if (!String.valueOf(input.charAt(0)).toUpperCase().matches(".*[A-Z].*")) {
                firstLet = "#";
            } else {
                firstLet = String.valueOf(input.charAt(0)).toUpperCase();
            }
            if (!sections.contains(firstLet)) {
                sections.add(firstLet);
                mSectionPositions.add(i);
            }
        }
        return sections.toArray(new String[0]);
    }

    @Override
    public int getPositionForSection(int i) {
        return mSectionPositions.get(i);
    }

    @Override
    public int getSectionForPosition(int i) {
        return 0;
    }


    public class Products extends RecyclerView.ViewHolder {
        private TextView productName, productMrp;
        private ImageView productImg, tick;
        private LinearLayout back;


        public Products(View itemView) {
            super(itemView);
            productName = itemView.findViewById(R.id.product_name);
            productMrp = itemView.findViewById(R.id.product_mrp);
            productImg = itemView.findViewById(R.id.product_image);
            back = itemView.findViewById(R.id.back);
            tick = itemView.findViewById(R.id.tick);
        }
    }

}
