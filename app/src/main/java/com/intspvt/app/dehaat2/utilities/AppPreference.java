package com.intspvt.app.dehaat2.utilities;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.util.Base64;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.intspvt.app.dehaat2.Dehaat2;

import java.io.ByteArrayOutputStream;
import java.lang.reflect.Type;
import java.util.List;


/**
 * Created by DELL on 6/22/2017.
 */

public class AppPreference {

    private static AppPreference mInstance;
    private final String APP_VIEW = "app_view";
    private final String PREF_NAME = "pref";
    private final String APP_LOGIN = "app_login";

    private final String FCM_TOKEN = "fcm_token";
    private final String AUTH_TOKEN = "auth_token";
    private final String PIN = "pin";

    private final String DEHAATI_MOBILE = "dehaati_mobile";
    private final String DEHAATI_NAME = "dehaati_name";
    private final String DEHAATI_NODE_NO = "dehaati_node_no";
    private final String DEHAATI_NODE_NAME = "dehaati_node_name";
    private final String DEHAATI_IMAGE = "dehaati_image";

    private final String DEHAAT_ADD = "dehaat_add";
    private final String DEHAAT_STATE = "dehaat_state";
    private final String DEHAAT_DISTRICT = "dehaat_district";
    private final String DEHAAT_BLOCK = "dehaat_block";
    private final String DEHAAT_NAME = "dehaat_name";

    private final String NOTES = "notes";

    private final String LANGUAGE = "language";
    private SharedPreferences sharedPref;
    private SharedPreferences.Editor mEditor;

    public AppPreference() {
        sharedPref = Dehaat2.getInstance().getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
    }

    public static AppPreference getInstance() {
        if (mInstance == null)
            mInstance = new AppPreference();

        return mInstance;
    }

    public static String encodeTobase64(Bitmap image) {
        if (image == null)
            return null;

        Bitmap bitmap_image = image;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap_image.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] b = baos.toByteArray();
        String imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);
        return imageEncoded;
    }

    public void clearData() {
        SharedPreferences.Editor mEditor = sharedPref.edit();
        mEditor.clear();
        mEditor.commit();
    }

    private void saveData(String key, String val) {
        SharedPreferences.Editor mEditor = sharedPref.edit();
        mEditor.putString(key, val);
        mEditor.commit();
    }


    private void saveData(String key, boolean val) {
        SharedPreferences.Editor mEditor = sharedPref.edit();
        mEditor.putBoolean(key, val);
        mEditor.commit();
    }


    private String getStringData(String key, String defValue) {
        return sharedPref.getString(key, defValue);
    }


    private boolean getBooleanData(String key, boolean defValue) {
        return sharedPref.getBoolean(key, defValue);
    }

    public String getAuthToken() {
        return getStringData(AUTH_TOKEN, null);
    }
    //****************************************************************888

    public void setAuthToken(String token) {
        saveData(AUTH_TOKEN, token);
    }


    public String getLANGUAGE() {
        return getStringData(LANGUAGE, null);
    }

    public void setLANGUAGE(String language) {
        saveData(LANGUAGE, language);
    }

    public String getFCM_TOKEN() {
        return getStringData(FCM_TOKEN, null);
    }

    public void setFCM_TOKEN(String token) {
        saveData(FCM_TOKEN, token);
    }

    /**
     * DEHAATI INFO
     **/

    public String getDEHAATI_NAME() {
        return getStringData(DEHAATI_NAME, null);
    }

    public void setDEHAATI_NAME(String token) {
        saveData(DEHAATI_NAME, token);
    }


    public String getDEHAATI_NODE_NO() {
        return getStringData(DEHAATI_NODE_NO, null);
    }

    public void setDEHAATI_NODE_NO(String token) {
        saveData(DEHAATI_NODE_NO, token);
    }

    public String getDEHAATI_NODE_NAME() {
        return getStringData(DEHAATI_NODE_NAME, null);
    }

    public void setDEHAATI_NODE_NAME(String token) {
        saveData(DEHAATI_NODE_NAME, token);
    }

    public String getDEHAATI_IMAGE() {
        return getStringData(DEHAATI_IMAGE, null);
    }

    public void setDEHAATI_IMAGE(String token) {
        saveData(DEHAATI_IMAGE, token);
    }

    public String getDEHAATI_MOBILE() {
        return getStringData(DEHAATI_MOBILE, null);
    }

    public void setDEHAATI_MOBILE(String token) {
        saveData(DEHAATI_MOBILE, token);
    }

    /**
     * DEHAAT INFO
     **/

    public String getDEHAAT_ADD() {
        return getStringData(DEHAAT_ADD, null);
    }

    public void setDEHAAT_ADD(String token) {
        saveData(DEHAAT_ADD, token);
    }

    public String getDEHAAT_NAME() {
        return getStringData(DEHAAT_NAME, null);
    }

    public void setDEHAAT_NAME(String token) {
        saveData(DEHAAT_NAME, token);
    }

    public List<String> getDEHAAT_STATE() {
        Gson gson = new Gson();
        String signInJson = getStringData(DEHAAT_STATE, null);
        if (signInJson != null) {
            Type type = new TypeToken<List<String>>() {
            }.getType();
            List<String> allCardResponse = gson.fromJson(signInJson, type);
            return allCardResponse;
        }
        return null;
    }

    public void setDEHAAT_STATE(List<String> token) {
        Gson gson = new Gson();
        String signInJson = null;
        if (token != null) {
            signInJson = gson.toJson(token);
        }
        saveData(DEHAAT_STATE, signInJson);
    }

    public List<String> getDEHAAT_DISTRICT() {
        Gson gson = new Gson();
        String signInJson = getStringData(DEHAAT_DISTRICT, null);
        if (signInJson != null) {
            Type type = new TypeToken<List<String>>() {
            }.getType();
            List<String> allCardResponse = gson.fromJson(signInJson, type);
            return allCardResponse;
        }
        return null;
    }

    public void setDEHAAT_DISTRICT(List token) {
        Gson gson = new Gson();
        String signInJson = null;
        if (token != null) {
            signInJson = gson.toJson(token);
        }
        saveData(DEHAAT_DISTRICT, signInJson);
    }

    public List<String> getDEHAAT_BLOCK() {
        Gson gson = new Gson();
        String signInJson = getStringData(DEHAAT_BLOCK, null);
        if (signInJson != null) {
            Type type = new TypeToken<List<String>>() {
            }.getType();
            List<String> allCardResponse = gson.fromJson(signInJson, type);
            return allCardResponse;
        }
        return null;
    }

    public void setDEHAAT_BLOCK(List<String> token) {
        Gson gson = new Gson();
        String signInJson = null;
        if (token != null) {
            signInJson = gson.toJson(token);
        }
        saveData(DEHAAT_BLOCK, signInJson);
    }


    public String getNOTES() {
        return getStringData(NOTES, null);
    }

    public void setNOTES(String pin) {
        saveData(NOTES, pin);
    }

    public boolean getAPP_LOGIN() {
        return getBooleanData(APP_LOGIN, false);
    }

    public void setAPP_LOGIN(boolean b) {
        saveData(APP_LOGIN, b);
    }
}


