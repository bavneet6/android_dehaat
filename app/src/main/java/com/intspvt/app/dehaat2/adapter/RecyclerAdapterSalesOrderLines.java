package com.intspvt.app.dehaat2.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.intspvt.app.dehaat2.R;
import com.intspvt.app.dehaat2.rest.response.SaleLines;

import java.util.ArrayList;

/**
 * Created by DELL on 10/5/2017.
 */

public class RecyclerAdapterSalesOrderLines extends RecyclerView.Adapter<RecyclerAdapterSalesOrderLines.Order> {
    private Context context;
    private ArrayList<SaleLines> data;

    public RecyclerAdapterSalesOrderLines(Activity activity, ArrayList<SaleLines> data) {
        this.context = activity;
        this.data = data;
    }

    @Override
    public RecyclerAdapterSalesOrderLines.Order onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.order_lines_layout, parent, false);
        context = parent.getContext();
        return new RecyclerAdapterSalesOrderLines.Order(itemView);

    }

    @Override
    public void onBindViewHolder(final RecyclerAdapterSalesOrderLines.Order holder, int position) {
        // print information recieved from the bundle
        holder.productName.setText(data.get(holder.getAdapterPosition()).getProduct_name());
        holder.product_qty.setText("x" + data.get(holder.getAdapterPosition()).getProduct_qty());
        holder.product_mrp.setText(context.getString(R.string.rs) + data.get(holder.getAdapterPosition()).getProduct_price());
        float i = data.get(holder.getAdapterPosition()).getProduct_price() * data.get(holder.getAdapterPosition()).getProduct_qty();
        holder.product_total.setText(context.getString(R.string.rs) + i);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class Order extends RecyclerView.ViewHolder {
        private TextView productName, product_mrp, product_qty, product_total;

        public Order(View itemView) {
            super(itemView);
            productName = itemView.findViewById(R.id.productName);
            product_mrp = itemView.findViewById(R.id.product_mrp);
            product_qty = itemView.findViewById(R.id.product_qty);
            product_total = itemView.findViewById(R.id.product_total);

        }
    }


}
