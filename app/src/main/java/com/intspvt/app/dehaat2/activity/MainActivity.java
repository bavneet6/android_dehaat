package com.intspvt.app.dehaat2.activity;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.amazonaws.mobile.client.AWSMobileClient;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.intspvt.app.dehaat2.Dehaat2;
import com.intspvt.app.dehaat2.R;
import com.intspvt.app.dehaat2.database.DatabaseHandler;
import com.intspvt.app.dehaat2.fragments.CartFragment;
import com.intspvt.app.dehaat2.fragments.CropManualListFragment;
import com.intspvt.app.dehaat2.fragments.EnquiryDetailHistoryFragment;
import com.intspvt.app.dehaat2.fragments.FarmProduceNewFragment;
import com.intspvt.app.dehaat2.fragments.HomeFragment;
import com.intspvt.app.dehaat2.fragments.InputSellingHistroryFragment;
import com.intspvt.app.dehaat2.fragments.OrderHistoryFragment;
import com.intspvt.app.dehaat2.fragments.ProductInfoFragment;
import com.intspvt.app.dehaat2.fragments.ProductListFragment;
import com.intspvt.app.dehaat2.fragments.TrendingFragment;
import com.intspvt.app.dehaat2.rest.response.AddToCart;
import com.intspvt.app.dehaat2.rest.response.ProductFileTable;
import com.intspvt.app.dehaat2.utilities.AppPreference;
import com.intspvt.app.dehaat2.utilities.AppUtils;
import com.intspvt.app.dehaat2.utilities.BottomNavigationViewHelper;
import com.intspvt.app.dehaat2.utilities.UrlConstant;
import com.skydoves.powermenu.MenuAnimation;
import com.skydoves.powermenu.PowerMenu;

import java.net.URL;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

public class MainActivity extends AppCompatActivity
        implements View.OnClickListener, BottomNavigationView.OnNavigationItemSelectedListener {
    private TextView heading, cart_count;
    private Toolbar toolbar;
    private View viewLine;
    private LinearLayout cart, bottomBar;
    private FrameLayout frag_container;
    private DatabaseHandler databaseHandler;
    private MenuItem home, manual, article;
    private ImageView dehaati_icon;

    private BottomNavigationView bottomNavigationView;
    private PowerMenu powerMenu;

    @Override
    protected void onResume() {
        super.onResume();
        AppUtils.getUpdateApp(this);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        AWSMobileClient.getInstance().initialize(this).execute();
        frag_container = toolbar.findViewById(R.id.frag_container);
        heading = toolbar.findViewById(R.id.title);
        cart = toolbar.findViewById(R.id.cart);
        cart_count = toolbar.findViewById(R.id.cart_item_count);
        toolbar.setNavigationIcon(R.drawable.menu);
        bottomNavigationView = findViewById(R.id.navigationView);
        Menu menu = bottomNavigationView.getMenu();
        home = menu.findItem(R.id.home);
        manual = menu.findItem(R.id.manuals);
        article = menu.findItem(R.id.articles);
        bottomBar = findViewById(R.id.bottomBar);
        dehaati_icon = findViewById(R.id.dehaati_icon);
        viewLine = findViewById(R.id.line);
        BottomNavigationViewHelper.removeShiftMode(bottomNavigationView);
        cart.setOnClickListener(this);
        dehaati_icon.setOnClickListener(this);
        databaseHandler = new DatabaseHandler(this);

        FragmentManager fragManager = getSupportFragmentManager();
        Fragment fragment = fragManager.findFragmentByTag(HomeFragment.TAG);
        if (fragment != null) {
            fragManager.beginTransaction().remove(fragManager.findFragmentByTag(HomeFragment.TAG)).commitAllowingStateLoss();
            getSupportFragmentManager().popBackStack();
        }
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.frag_container, HomeFragment.newInstance(),
                        HomeFragment.TAG).commitAllowingStateLoss();

        getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @SuppressLint("RestrictedApi")
            @Override
            public void onBackStackChanged() {
                if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                    getSupportActionBar().setHomeAsUpIndicator(R.drawable.back_white);
                    // show back button
                    toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            onBackPressed();
                        }
                    });

                }
            }
        });

        if (AppPreference.getInstance().getLANGUAGE() != null && AppPreference.getInstance().getLANGUAGE().length() > 0) {
            Dehaat2.setLocale(MainActivity.this, AppPreference.getInstance().getLANGUAGE());
        } else {
            Dehaat2.setLocale(MainActivity.this, UrlConstant.HINDI);
            AppPreference.getInstance().setLANGUAGE(UrlConstant.HINDI);
        }

        home.setTitle(R.string.home);
        manual.setTitle(R.string.home_manual);
        article.setTitle(R.string.farm_info);
        bottomNavigationView.setOnNavigationItemSelectedListener(this);
        onNewIntent(getIntent());
    }

    /*
   method is for push notification , to navigate to a particular page
     */
    private void checkNotification(Intent intent) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        Bundle getData = intent.getExtras();
        Fragment currentFrag = this.getSupportFragmentManager().findFragmentById(R.id.frag_container);
        if (getData == null)
            return;

        if (getData.getString("screen") == null)
            return;
        if (getData.getString("screen").equals(UrlConstant.AGR_IN_HI)) {
            if (currentFrag instanceof OrderHistoryFragment)
                transaction.replace(R.id.frag_container, OrderHistoryFragment.newInstance()).commit();
            else
                transaction.replace(R.id.frag_container, OrderHistoryFragment.newInstance()).addToBackStack("").commit();
        } else if (getData.getString("screen").equals(UrlConstant.TREN_ART)) {
            if (currentFrag instanceof TrendingFragment)
                transaction.replace(R.id.frag_container, TrendingFragment.newInstance()).commit();
            else
                transaction.replace(R.id.frag_container, TrendingFragment.newInstance()).addToBackStack("").commit();
        } else if (getData.getString("screen").equals(UrlConstant.PROM_ART)) {
            if (currentFrag instanceof HomeFragment) {
                transaction.replace(R.id.frag_container, HomeFragment.newInstance()).commit();
            } else if (currentFrag != null)
                transaction.replace(R.id.frag_container, HomeFragment.newInstance()).addToBackStack("").commit();
        } else if (getData.getString("screen").equals(UrlConstant.ENQ_HI)) {

            String data = getData.getString("ISSUE_ID");
            if (data == null)
                return;
            Bundle bundle = new Bundle();
            bundle.putInt("ISSUE_ID", Integer.parseInt(data));
            if (currentFrag instanceof EnquiryDetailHistoryFragment) {
                EnquiryDetailHistoryFragment fragment = EnquiryDetailHistoryFragment.newInstance();
                fragment.setArguments(bundle);
                transaction.replace(R.id.frag_container, fragment).commit();
            } else {
                EnquiryDetailHistoryFragment fragment = EnquiryDetailHistoryFragment.newInstance();
                fragment.setArguments(bundle);
                transaction.replace(R.id.frag_container, fragment).addToBackStack("").commit();
            }
        } else if (getData.getString("screen").equals(UrlConstant.DEHAAT_FARMER_HI)) {
            if (currentFrag instanceof InputSellingHistroryFragment)
                transaction.replace(R.id.frag_container, InputSellingHistroryFragment.newInstance()).commit();
            else
                transaction.replace(R.id.frag_container, InputSellingHistroryFragment.newInstance()).addToBackStack("").commit();
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        checkNotification(intent);
    }

    @Override
    public void onBackPressed() {
        if ((powerMenu != null) && (powerMenu.isShowing())) {
            powerMenu.dismiss();
        } else {
            super.onBackPressed();
        }
    }

    /*
    set heading in all fragments
     */
    public void setTitle(String s) {
        heading.setText(s);
    }

    public void showToolbar(boolean b) {
        if (b) {
            toolbar.setVisibility(View.VISIBLE);
        } else {
            toolbar.setVisibility(View.GONE);
        }
    }

    /*
     to show bottom drawer
     */
    public void showDrawer(boolean b) {
        if (b) {
            bottomBar.setVisibility(View.VISIBLE);
            viewLine.setVisibility(View.VISIBLE);

        } else {
            bottomBar.setVisibility(View.GONE);
            viewLine.setVisibility(View.GONE);
        }
    }

    public void refreshUI() {
        home.setTitle(R.string.home);
        manual.setTitle(R.string.home_manual);
        article.setTitle(R.string.farm_info);
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    /*
      to show cart count
     */
    public void showCartCount(boolean blink) {
        if (blink) {
            Animation animBlink = AnimationUtils.loadAnimation(this,
                    R.anim.blinktext);
            cart_count.setAnimation(animBlink);
        }
        final ArrayList<AddToCart> list;
        list = databaseHandler.getAllCartData();
        cart_count.setText("" + list.size());


    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onClick(View view) {
        FragmentTransaction transaction;
        transaction = getSupportFragmentManager().beginTransaction();
        int id = view.getId();
        switch (id) {
            case R.id.cart:
                Fragment currentFrag = this.getSupportFragmentManager().findFragmentById(R.id.frag_container);
                if (currentFrag instanceof ProductInfoFragment) {
                    transaction.replace(R.id.frag_container, CartFragment.newInstance()).commit();
                } else {
                    transaction.replace(R.id.frag_container, CartFragment.newInstance()).addToBackStack("").commit();
                }
                break;
            case R.id.dehaati_icon:
                initializeCustomDialogMenu();
                powerMenu.showAsDropDown(view);
                break;
        }
    }

    private PowerMenu showBottomDialog() {
        return new PowerMenu.Builder(this)
                .setHeaderView(R.layout.dialog_upper_menu) // header used for title
                .setFooterView(R.layout.dialog_bottom_menu) // header used for title
                .setAnimation(MenuAnimation.SHOWUP_BOTTOM_RIGHT)
                .setMenuRadius(10f)
                .setMenuShadow(10f)
                .setWidth(550)
                .setSelectedMenuColor(this.getResources().getColor(R.color.greendark)) //
                .setBackgroundColor(R.color.boxcolor)
                .setBackgroundAlpha(100)
                .build();
    }

    private void initializeCustomDialogMenu() {
        final FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        powerMenu = showBottomDialog();
        View footerView = powerMenu.getFooterView();
        LinearLayout inputBack = footerView.findViewById(R.id.input);
        LinearLayout produceBack = footerView.findViewById(R.id.produce);
        inputBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                transaction.replace(R.id.frag_container, ProductListFragment.newInstance()).addToBackStack("").commitAllowingStateLoss();
                powerMenu.dismiss();

            }
        });

        produceBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                transaction.replace(R.id.frag_container, FarmProduceNewFragment.newInstance()).addToBackStack("").commitAllowingStateLoss();
                powerMenu.dismiss();

            }
        });
    }

    public void showCart(boolean b) {
        if (b) {
            cart.setVisibility(View.VISIBLE);
        } else {
            cart.setVisibility(View.GONE);
        }
    }

    /*
    this method is used to store the path od a file and generate the url from s3 and store that into db
     */
    public void generateAndCheckUrl(String picFilePath) {
        try {
            ProductFileTable productFileTable = new ProductFileTable();
            productFileTable.setProd_file(picFilePath);
            URL url = null;
            try {
                url = new AppUtils.FetchPicture(picFilePath).execute().get();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }

            productFileTable.setProd_url("" + url);

            databaseHandler.inserPicturesFile(productFileTable);
        } catch (OutOfMemoryError E) {
            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
            builder.setTitle("सूचना")
                    .setMessage("फ़ोन में जगह नहीं")
                    .setPositiveButton("हाँ", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    })
                    .show();
        }
    }

    public void showHomeSelected() {
        bottomNavigationView.setSelectedItemId(R.id.home);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        // if we make it publick it will crash because of commit already used
        // always take this in on click

        int id = item.getItemId();
        Fragment currentFrag = this.getSupportFragmentManager().findFragmentById(R.id.frag_container);
        if (id == R.id.home) {
            if (currentFrag instanceof HomeFragment) {

            } else {
                FragmentManager fragManager = getSupportFragmentManager();
                Fragment fragment = fragManager.findFragmentByTag(HomeFragment.TAG);
                if (fragment == null)
                    fragment = HomeFragment.newInstance();
                getSupportFragmentManager().popBackStack(null, android.app.FragmentManager.POP_BACK_STACK_INCLUSIVE);
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.frag_container, fragment,
                                HomeFragment.TAG).commitAllowingStateLoss();
            }
        } else if (id == R.id.manuals) {
            if (currentFrag instanceof HomeFragment) {
                transaction.replace(R.id.frag_container, CropManualListFragment.newInstance()).addToBackStack("").commit();
            } else {
                transaction.replace(R.id.frag_container, CropManualListFragment.newInstance()).commit();
            }
        } else if (id == R.id.articles) {
            if (currentFrag instanceof HomeFragment) {
                transaction.replace(R.id.frag_container, TrendingFragment.newInstance()).addToBackStack("").commit();
            } else {
                transaction.replace(R.id.frag_container, TrendingFragment.newInstance()).commit();
            }
        }

        return true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
