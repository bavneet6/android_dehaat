package com.intspvt.app.dehaat2.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.intspvt.app.dehaat2.R;
import com.intspvt.app.dehaat2.database.DatabaseHandler;
import com.intspvt.app.dehaat2.rest.PicassoCallback;
import com.intspvt.app.dehaat2.utilities.AppUtils;
import com.squareup.picasso.Picasso;

import java.net.URL;
import java.util.ArrayList;

/**
 * Created by DELL on 11/22/2017.
 */

public class RecyclerAdapterTrendingArticles extends RecyclerView.Adapter<RecyclerAdapterTrendingArticles.Trending> {
    private ArrayList<com.intspvt.app.dehaat2.rest.response.Trending.TrendingData> trending;
    private Context context;
    private String getPic;
    private DatabaseHandler databaseHandler;

    public RecyclerAdapterTrendingArticles(Activity context, ArrayList<com.intspvt.app.dehaat2.rest.response.Trending.TrendingData> products) {
        this.context = context;
        this.trending = products;
        databaseHandler = new DatabaseHandler(context);
    }

    @Override
    public RecyclerAdapterTrendingArticles.Trending onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.template_trending_articles, parent, false);
        context = parent.getContext();
        return new RecyclerAdapterTrendingArticles.Trending(itemView);

    }

    @Override
    public void onBindViewHolder(final RecyclerAdapterTrendingArticles.Trending holder, final int position) {
        holder.heading.setText(trending.get(holder.getAdapterPosition()).getName());
        String photo = trending.get(holder.getAdapterPosition()).getImage();
        getPic = null;
        if (!AppUtils.isNullCase(photo)) {
            getPic = databaseHandler.getFileUrl(photo);
            if (getPic != null) {
                final Picasso picasso = Picasso.with(context.getApplicationContext());
                picasso.setLoggingEnabled(true);
                picasso.load(getPic).into(holder.image, new PicassoCallback(photo) {

                    @Override
                    public void onErrorPic(URL url) {
                        picasso.load("" + url).into(holder.image);
                    }
                });

            } else {
                holder.image.setImageResource(0);
            }
        }

        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog dialog = new Dialog(new ContextThemeWrapper(context, R.style.DialogSlideAnim));
                final TextView heading, content;
                final ImageView trendPic;
                dialog.setCanceledOnTouchOutside(true);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.dialog_trending_article);
                heading = dialog.findViewById(R.id.trend_head);
                content = dialog.findViewById(R.id.trend_des);
                trendPic = dialog.findViewById(R.id.trendImg);
                String photo = trending.get(holder.getAdapterPosition()).getImage();
                getPic = null;
                String input = trending.get(holder.getAdapterPosition()).getContent();
                input = input.replace("\n", "");
                input = input.trim();
                if (!AppUtils.isNullCase(photo)) {
                    getPic = databaseHandler.getFileUrl(photo);
                    if (getPic != null) {
                        final Picasso picasso = Picasso.with(context.getApplicationContext());
                        picasso.setLoggingEnabled(true);
                        picasso.load(getPic).into(trendPic, new PicassoCallback(photo) {

                            @Override
                            public void onErrorPic(URL url) {
                                picasso.load("" + url).into(trendPic);
                            }
                        });

                    } else {
                        holder.image.setImageResource(0);
                    }
                }
                heading.setText(holder.heading.getText().toString());
                content.setText(input);
                Drawable d = new ColorDrawable(context.getResources().getColor(R.color.greytext));
                d.setAlpha(100);
                dialog.getWindow().setBackgroundDrawable(d);
                dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
                int width = context.getResources().getDisplayMetrics().widthPixels;
                dialog.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                dialog.getWindow().setGravity(Gravity.CENTER);
                dialog.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return trending.size();
    }

    public class Trending extends RecyclerView.ViewHolder {
        private TextView heading;
        private ImageView image;
        private RelativeLayout linearLayout;

        public Trending(View itemView) {
            super(itemView);
            linearLayout = itemView.findViewById(R.id.back);
            heading = itemView.findViewById(R.id.heading);
            image = itemView.findViewById(R.id.trend_img);
        }
    }
}
