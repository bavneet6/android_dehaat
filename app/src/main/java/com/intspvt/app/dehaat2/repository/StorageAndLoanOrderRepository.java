package com.intspvt.app.dehaat2.repository;

import androidx.lifecycle.MutableLiveData;

import com.intspvt.app.dehaat2.Dehaat2;
import com.intspvt.app.dehaat2.R;
import com.intspvt.app.dehaat2.rest.AppRestClient;
import com.intspvt.app.dehaat2.rest.response.StorageOrderData;
import com.intspvt.app.dehaat2.utilities.AppUtils;

import java.net.ConnectException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StorageAndLoanOrderRepository {
    private static StorageAndLoanOrderRepository mInstance;
    private List<StorageOrderData.StorageLoan> orderDataHistories;

    public static StorageAndLoanOrderRepository getInstance() {
        if (mInstance == null)
            mInstance = new StorageAndLoanOrderRepository();
        return mInstance;
    }

    public MutableLiveData<List<StorageOrderData.StorageLoan>> getOrderData() {
        MutableLiveData<List<StorageOrderData.StorageLoan>> data = new MutableLiveData<>();
        AppRestClient client = AppRestClient.getInstance();
        Call<StorageOrderData> call = client.storageandloanOrderHistory();
        call.enqueue(new Callback<StorageOrderData>() {
            @Override
            public void onResponse(Call<StorageOrderData> call, Response<StorageOrderData> response) {
                orderDataHistories = new ArrayList<StorageOrderData.StorageLoan>();
                if (response.body() == null) return;
                if (response.code() == 200 && response.body().getData() != null)
                    orderDataHistories = response.body().getData();
                else if (response.code() == 500)
                    AppUtils.showToast(
                            Dehaat2.getInstance().getApplicationContext().getString(R.string.tech_prob));
                data.setValue(orderDataHistories);
            }

            @Override
            public void onFailure(Call<StorageOrderData> call, Throwable t) {
                if (t instanceof ConnectException) {
                    if (!AppUtils.haveNetworkConnection(
                            Dehaat2.getInstance().getApplicationContext())) {
                        AppUtils.showToast(
                                Dehaat2.getInstance().getApplicationContext().getString(
                                        R.string.no_internet));
                    } else {
                        AppUtils.showToast(
                                Dehaat2.getInstance().getApplicationContext().getString(
                                        R.string.server_no_respond));
                    }
                }
                data.setValue(new ArrayList<>());
            }
        });

        return data;
    }
}
