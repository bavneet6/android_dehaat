package com.intspvt.app.dehaat2.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.intspvt.app.dehaat2.R;
import com.intspvt.app.dehaat2.fragments.StorageAndLoanDetailFragment;
import com.intspvt.app.dehaat2.rest.ApiCallback;
import com.intspvt.app.dehaat2.rest.AppRestClient;
import com.intspvt.app.dehaat2.rest.body.Issue;
import com.intspvt.app.dehaat2.rest.response.IssueData;
import com.intspvt.app.dehaat2.rest.response.StorageOrderData;
import com.intspvt.app.dehaat2.utilities.AppUtils;
import com.intspvt.app.dehaat2.utilities.UrlConstant;

import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

public class RecyclerAdapterStorageAndLoanOrderHistory extends RecyclerView.Adapter<RecyclerAdapterStorageAndLoanOrderHistory.Order> {
    private Context context;
    private List<StorageOrderData.StorageLoan> storageOrderData;

    public RecyclerAdapterStorageAndLoanOrderHistory(Context context, List<StorageOrderData.StorageLoan> storageOrderData
    ) {
        this.context = context;
        this.storageOrderData = storageOrderData;
    }


    @Override
    public RecyclerAdapterStorageAndLoanOrderHistory.Order onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.template_fragment_output_credit, parent, false);

        context = parent.getContext();
        return new RecyclerAdapterStorageAndLoanOrderHistory.Order(itemView);
    }

    @Override
    public void onBindViewHolder(final RecyclerAdapterStorageAndLoanOrderHistory.Order holder, int position) {
        final int pos = holder.getAdapterPosition();

        if (storageOrderData != null) {
            if (storageOrderData.get(pos).getLoan() != null) {
                holder.ll_loan_order.setVisibility(View.VISIBLE);
                holder.ll_no_loan_order.setVisibility(View.GONE);
                holder.approved_loan_amount.setText("" + context.getString(R.string.approved_loan_amount) + context.getString(R.string.rs) + " " + storageOrderData.get(pos).getLoan().getApproved_loan_amount());
                holder.loan_start_date.setText(" " + storageOrderData.get(pos).getLoan().getStart_date());
                holder.loan_payment_due_date.setText(" " + storageOrderData.get(pos).getLoan().getPayment_due_date());
                holder.loan_outstanding_amount.setText(" " + context.getString(R.string.rs) + " " + storageOrderData.get(pos).getLoan().getOutstanding_loan_amount());
            } else {
                holder.ll_loan_order.setVisibility(View.GONE);
                holder.ll_no_loan_order.setVisibility(View.VISIBLE);
                holder.yes_loan.setOnClickListener(v -> sendData("Request For Loan against storage order - " + storageOrderData.get(pos).getStorage().getName()));
            }
            holder.viewDetails.setOnClickListener(v -> {
                StorageOrderData.StorageLoan storageLoan = storageOrderData.get(pos);
                StorageAndLoanDetailFragment storageAndLoanDetailFragment = StorageAndLoanDetailFragment.newInstance();
                AppCompatActivity activity = (AppCompatActivity) v.getContext();
                Bundle bundle = new Bundle();
                bundle.putSerializable("storage_data", storageLoan);
                storageAndLoanDetailFragment.setArguments(bundle);
                FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.frag_container, storageAndLoanDetailFragment).addToBackStack("").commitAllowingStateLoss();
            });
            holder.crop_id.setText(storageOrderData.get(pos).getStorage().getProduct_id());
            holder.storage_date.setText(storageOrderData.get(pos).getStorage().getDate_order());
            holder.storage_id3.setText(context.getString(R.string.order_no) + ": " + storageOrderData.get(pos).getStorage().getName());
            holder.crop_quantity.setText("" + storageOrderData.get(pos).getStorage().getQuantity());
            holder.storage_duration_unit.setText("" + storageOrderData.get(pos).getStorage().getDuration_unit());
            holder.storage_duration.setText("" + storageOrderData.get(pos).getStorage().getDuration());
            holder.tvLocation.setText("" + storageOrderData.get(pos).getStorage().getPicking_type_id());
            holder.crop_quantity_unit.setText("" + storageOrderData.get(pos).getStorage().getQuantity_unit());

        }
    }

    @Override
    public int getItemCount() {
        return storageOrderData.size();
    }

    private void sendData(String text) {
        AppUtils.showProgressDialog(context);
        IssueData issueData = new IssueData();
        if (AppUtils.isNullCase(text)) {
            issueData.setDescription(null);

        } else {
            issueData.setDescription(text);
        }
        issueData.setCategory(UrlConstant.OUTPUT);
        Issue issue = new Issue(issueData);
        Call<Void> call;
        AppRestClient client = AppRestClient.getInstance();
        call = client.sendEnquiryData(issue, null, null);
        call.enqueue(new ApiCallback<Void>() {
            @Override
            public void onResponse(Response<Void> response) {

                AppUtils.hideProgressDialog();
                if (response.code() == 200) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setTitle(R.string.information);
                    builder.setMessage(R.string.taken_request_line);
                    builder.setCancelable(false);
                    builder.setPositiveButton(R.string.ok, (dialog, which) -> {
                        dialog.dismiss();

                    });
                    AlertDialog alert = builder.create();
                    alert.show();
                }

            }

            @Override
            public void onResponse401(Response<Void> response) {

            }


        });
    }

    public class Order extends RecyclerView.ViewHolder {
        private TextView crop_id, crop_quantity, storage_date, crop_quantity_unit, storage_id3,
                storage_duration_unit, storage_duration, approved_loan_amount, loan_start_date, loan_payment_due_date,
                loan_outstanding_amount, tvLocation;
        private Button yes_loan;

        private LinearLayout ll_loan_order, ll_no_loan_order, ll_storage_order, viewDetails;

        public Order(View itemView) {
            super(itemView);
            crop_id = itemView.findViewById(R.id.crop_id);
            storage_date = itemView.findViewById(R.id.storage_date);
            storage_id3 = itemView.findViewById(R.id.storage_id3);
            crop_quantity = itemView.findViewById(R.id.crop_quantity);
            crop_quantity_unit = itemView.findViewById(R.id.crop_quantity_unit);
            storage_duration_unit = itemView.findViewById(R.id.storage_duration_unit);
            approved_loan_amount = itemView.findViewById(R.id.approved_loan_amount);
            loan_start_date = itemView.findViewById(R.id.loan_start_date);
            loan_payment_due_date = itemView.findViewById(R.id.loan_payment_due_date);
            loan_outstanding_amount = itemView.findViewById(R.id.loan_outstanding_amount);
            ll_loan_order = itemView.findViewById(R.id.ll_loan_order);
            ll_no_loan_order = itemView.findViewById(R.id.ll_no_loan_order);
            yes_loan = itemView.findViewById(R.id.yes_loan);
            ll_storage_order = itemView.findViewById(R.id.ll_storage_order);
            viewDetails = itemView.findViewById(R.id.viewDetails);


        }
    }

}

