package com.intspvt.app.dehaat2.rest.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by DELL on 9/14/2017.
 */

public class LandInfo {

    @SerializedName("x_land_unit")
    private String x_land_unit;

    @SerializedName("x_total_land")
    private float x_total_land;

    @SerializedName("x_up_land")
    private float x_up_land;

    @SerializedName("x_low_land")
    private float x_low_land;

    @SerializedName("x_mid_land")
    private float x_mid_land;

    @SerializedName("x_leased_in")
    private float x_leased_in;

    @SerializedName("x_leased_out")
    private float x_leased_out;

    public LandInfo(String x_land_unit, float x_total_land, float x_up_land, float x_low_land, float x_mid_land, float x_leased_in, float x_leased_out) {
        this.x_land_unit = x_land_unit;
        this.x_total_land = x_total_land;
        this.x_up_land = x_up_land;
        this.x_low_land = x_low_land;
        this.x_mid_land = x_mid_land;
        this.x_leased_in = x_leased_in;
        this.x_leased_out = x_leased_out;
    }

}