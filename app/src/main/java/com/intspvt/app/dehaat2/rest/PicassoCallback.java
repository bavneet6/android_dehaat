package com.intspvt.app.dehaat2.rest;

import android.content.Context;
import android.content.DialogInterface;

import androidx.appcompat.app.AlertDialog;

import com.intspvt.app.dehaat2.Dehaat2;
import com.intspvt.app.dehaat2.database.DatabaseHandler;
import com.intspvt.app.dehaat2.rest.response.ProductFileTable;
import com.intspvt.app.dehaat2.utilities.AppUtils;
import com.squareup.picasso.Callback;

import java.net.URL;
import java.util.concurrent.ExecutionException;

/**
 * Created by DELL on 3/23/2018.
 */

public abstract class PicassoCallback implements Callback {
    String imagePath;
    Context context = Dehaat2.getInstance();
    DatabaseHandler databaseHandler = new DatabaseHandler(context);

    public PicassoCallback(String imagePath) {
        this.imagePath = imagePath;
    }

    @Override
    public void onSuccess() {


    }

    @Override
    public void onError() {
        if (AppUtils.haveNetworkConnection(context)) {
            try {
                ProductFileTable productFileTable = new ProductFileTable();
                productFileTable.setProd_file(imagePath);
                URL url = null;
                try {
                    url = new AppUtils.FetchPicture(imagePath).execute().get();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }

                productFileTable.setProd_url("" + url);

                databaseHandler.inserPicturesFile(productFileTable);
                onErrorPic(url);

            } catch (OutOfMemoryError E) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("सूचना")
                        .setMessage("फ़ोन में जगह नहीं")
                        .setPositiveButton("हाँ", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        })
                        .show();
            }
        }
    }

    public abstract void onErrorPic(URL url);


}
