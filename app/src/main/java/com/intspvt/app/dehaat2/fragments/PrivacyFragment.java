package com.intspvt.app.dehaat2.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.intspvt.app.dehaat2.R;

/**
 * Created by DELL on 1/3/2018.
 */

public class PrivacyFragment extends BaseFragment {
    public static final String TAG = PrivacyFragment.class.getSimpleName();
    private WebView webView;

    public static PrivacyFragment newInstance() {
        return new PrivacyFragment();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.privacy_terms, null);
        webView = v.findViewById(R.id.privacy);
        String yourhtmlpage =
                "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\"><html><head><META http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"></head><body>\n" +
                        "<div>\n" +
                        "<h2>Privacy Policy</h2>\n" +
                        "<p>Dehaat team built the Dehaat app as a free app. This SERVICE is provided by Dehaat at no cost and is intended\n" +
                        "    for use as is.</p>\n" +
                        "<p>This page is used to inform website visitors regarding our policies with the collection, use, and\n" +
                        "    disclosure of Personal Information if anyone decided to use our Service.</p>\n" +
                        "<p>If you choose to use our Service, then you agree to the collection and use of information in\n" +
                        "    relation with this policy. The Personal Information that we collect are used for providing and\n" +
                        "    improving the Service. We will not use or share your information with anyone except as described\n" +
                        "    in this Privacy Policy.</p>\n" +
                        "<p>The terms used in this Privacy Policy have the same meanings as in our Terms and Conditions,\n" +
                        "    which is accessible at Dehaat, unless otherwise defined in this Privacy Policy.</p>\n" +
                        "\n" +
                        "<p><strong>Information Collection and Use</strong></p>\n" +
                        "<p>For a better experience while using our Service, we may require you to provide us with certain\n" +
                        "    personally identifiable information, including but not limited to users name, address, location, pictures.\n" +
                        "\tThe information that we request will be retained by us and used as described in this privacy policy.</p>\n" +
                        "<p>The app does use third party services that may collect information used to identify you.\n" +
                        "<p><strong>Account</strong></p>\n" +
                        "We collect, and associate with your account, information like your name, email address, phone number, crop information, age, gender, physical address, and account activity. Some of our services let you access your accounts and your information with other service providers.\n" +
                        "<p><strong>Services</strong></p>\n" +
                        "Our services our designed to make it simple for you to access all your agricultural needs and collaborate with others in doing so. To make that possible, we store, process and transmit your data - such as precise location, photos, voice recordings - as well as information related to it.\n" +
                        "<p> You may choose to give us access to your phone application to make calls to our toll free number and contacts to make it easy for you to invite others to use our services. <p>\n" +
                        "<p><strong>Usage</strong></p>\n" +
                        "We collect information related to how you use the Services, including actions you take in your account (like reading articles, browsing and purchasing products, raising queries). This helps us provide you with features like product recommendations and crop specific information.\n" +
                        "We also collect information from and about the devices you use to access the Services. This includes things like IP addresses, the type of browser and device you use and identifiers associated with your devices. Your devices (depending on their settings) may also transmit location information to the Services.\n" +
                        "\n" +
                        "<p><strong>Log Data</strong></p>\n" +
                        "<p>We want to inform you that whenever you use our Service, in case of an error in the app we collect\n" +
                        "    data and information (through third party products) on your phone called Log Data. This Log Data\n" +
                        "    may include information such as your devicesâ\u0080\u0099s Internet Protocol (â\u0080\u009CIPâ\u0080\u009D) address, device name,\n" +
                        "    operating system version, configuration of the app when utilising our Service, the time and date\n" +
                        "    of your use of the Service, and other statistics.</p>\n" +
                        "\n" +
                        "<p><strong>Cookies</strong></p>\n" +
                        "<p>Cookies are files with small amount of data that is commonly used an anonymous unique identifier.\n" +
                        "    These are sent to your browser from the website that you visit and are stored on your devicesâ\u0080\u0099s\n" +
                        "    internal memory.</p>\n" +
                        "<p>This Services uses these â\u0080\u009Ccookiesâ\u0080\u009D explicitly. However, the app may use third party code\n" +
                        "    and libraries that use â\u0080\u009Ccookiesâ\u0080\u009D to collection information and to improve their services. You\n" +
                        "    have the option to either accept or refuse these cookies, and know when a cookie is being sent\n" +
                        "    to your device. If you choose to refuse our cookies, you may not be able to use some portions of\n" +
                        "    this Service.</p>\n" +
                        "\n" +
                        "<p><strong>Service Providers</strong></p>\n" +
                        "<p>We may employ third-party companies and individuals due to the following reasons:</p>\n" +
                        "<ul>\n" +
                        "    <li>To facilitate our Service;</li>\n" +
                        "    <li>To provide the Service on our behalf;</li>\n" +
                        "    <li>To perform Service-related services; or</li>\n" +
                        "    <li>To assist us in analyzing how our Service is used.</li>\n" +
                        "</ul>\n" +
                        "<p>We want to inform users of this Service that these third parties have access to your Personal\n" +
                        "    Information. The reason is to perform the tasks assigned to them on our behalf. However, they\n" +
                        "    are obligated not to disclose or use the information for any other purpose.</p>\n" +
                        "\n" +
                        "<p><strong>Security</strong></p>\n" +
                        "<p>We value your trust in providing us your Personal Information, thus we are striving to use\n" +
                        "    commercially acceptable means of protecting it. But remember that no method of transmission over\n" +
                        "    the internet, or method of electronic storage is 100% secure and reliable, and we cannot\n" +
                        "    guarantee its absolute security.</p>\n" +
                        "\n" +
                        "<p><strong>Links to Other Sites</strong></p>\n" +
                        "<p>This Service may contain links to other sites. If you click on a third-party link, you will be\n" +
                        "    directed to that site. Note that these external sites are not operated by us. Therefore, I\n" +
                        "    strongly advise you to review the Privacy Policy of these websites. We have no control over, and\n" +
                        "    assume no responsibility for the content, privacy policies, or practices of any third-party\n" +
                        "    sites or services.</p>\n" +
                        "\n" +
                        "<p><strong>Childrenâ\u0080\u0099s Privacy</strong></p>\n" +
                        "<p>This Services do not address anyone under the age of 13. We do not knowingly collect personal\n" +
                        "    identifiable information from children under 13. In the case we discover that a child under 13\n" +
                        "    has provided us with personal information, we immediately delete this from our servers. If you\n" +
                        "    are a parent or guardian and you are aware that your child has provided us with personal\n" +
                        "    information, please contact us so that we will be able to do necessary actions.</p>\n" +
                        "\n" +
                        "<p><strong>Changes to This Privacy Policy</strong></p>\n" +
                        "<p>We may update our Privacy Policy from time to time. Thus, you are advised to review this page\n" +
                        "    periodically for any changes. We will notify you of any changes by posting the new Privacy Policy\n" +
                        "    on this page. These changes are effective immediately, after they are posted on this page.</p>\n" +
                        "\n" +
                        "<p><strong>Contact Us</strong></p>\n" +
                        "<p>If you have any questions or suggestions about our Privacy Policy, do not hesitate to contact\n" +
                        "    Us at our toll free number - 1800 1036 110</p>\n" +
                        "</p></p></p></div>\n" +
                        "\n" +
                        "</body></html>";
        webView.loadDataWithBaseURL(null, yourhtmlpage, "text/html", "UTF-8", null);

        return v;
    }
}
