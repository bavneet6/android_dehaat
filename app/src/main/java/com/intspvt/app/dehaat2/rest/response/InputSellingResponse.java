package com.intspvt.app.dehaat2.rest.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class InputSellingResponse implements Serializable {

    @SerializedName("data")
    private ArrayList<Dehaat_farmer> dehaat_farmer;

    public ArrayList<Dehaat_farmer> getDehaat_farmer() {
        return dehaat_farmer;
    }

    public class Dehaat_farmer implements Serializable {
        @SerializedName("amount_total")
        public float amount_total;
        @SerializedName("create_date")
        public String create_date;
        @SerializedName("customer_mobile")
        public String customer_mobile;
        @SerializedName("state")
        public String state;
        @SerializedName("name")
        public String name;
        @SerializedName("delivery_pass_code")
        public String delivery_pass_code;
        @SerializedName("inventory_status")
        public String inventory_status;
        @SerializedName("id")
        public int id;
        @SerializedName("is_deleted")
        public boolean deleted;
        @SerializedName("sale_lines")
        public ArrayList<SaleLines> sale_lines;

        public String getDelivery_pass_code() {
            return delivery_pass_code;
        }

        public boolean isDeleted() {
            return deleted;
        }

        public String getState() {
            return state;
        }

        public String getInventory_status() {
            return inventory_status;
        }

        public ArrayList<SaleLines> getSale_lines() {
            return sale_lines;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public float getAmount_total() {
            return amount_total;
        }

        public String getCreate_date() {
            return create_date;
        }

        public String getName() {
            return name;
        }

        public String getCustomer_mobile() {
            return customer_mobile;
        }

    }
}
