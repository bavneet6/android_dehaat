package com.intspvt.app.dehaat2.rest.response;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class CropManuals {
    @SerializedName("data")
    private ArrayList<CropManualData> cropManual;

    public ArrayList<CropManualData> getCropManual() {
        return cropManual;
    }


}
