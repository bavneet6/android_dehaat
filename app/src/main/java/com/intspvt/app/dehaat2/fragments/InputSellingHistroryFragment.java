package com.intspvt.app.dehaat2.fragments;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.intspvt.app.dehaat2.R;
import com.intspvt.app.dehaat2.activity.MainActivity;
import com.intspvt.app.dehaat2.rest.ApiCallback;
import com.intspvt.app.dehaat2.rest.AppRestClient;
import com.intspvt.app.dehaat2.rest.response.InputSellingResponse;
import com.intspvt.app.dehaat2.utilities.AppPreference;
import com.intspvt.app.dehaat2.utilities.AppUtils;

import org.json.JSONException;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

public class InputSellingHistroryFragment extends BaseFragment {
    public static final String TAG = InputSellingHistroryFragment.class.getSimpleName();
    public static final String ORDER_LIST = "order list";

    private FirebaseAnalytics mFirebaseAnalytics;
    private String format2, format1, diffTime;
    private ViewPager viewPager;
    private TabLayout tabLayout;
    private TextView no_data_mssg;
    private FragmentManager fragmentManager;
    private ImageView backButton;

    public static InputSellingHistroryFragment newInstance() {
        return new InputSellingHistroryFragment();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_input_selling_history, null);
        showActionBar(true);
        ((MainActivity) activity).showDrawer(false);
        ((MainActivity) activity).showCart(false);
        ((MainActivity) activity).showToolbar(false);
        no_data_mssg = v.findViewById(R.id.no_data_mssg);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        backButton = v.findViewById(R.id.input_history_back);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });
        viewPager = v.findViewById(R.id.order_history_viewpager);
        tabLayout = v.findViewById(R.id.order_history_tabs);
        fragmentManager = getChildFragmentManager();

        getSaleData();

        return v;
    }

    private void setupViewPager(ArrayList<InputSellingResponse.Dehaat_farmer> dehaat_farmer) {
        InputSellingViewPagerAdapter adapter = new InputSellingViewPagerAdapter(getChildFragmentManager());
        ArrayList<InputSellingResponse.Dehaat_farmer> delivered = new ArrayList<>();
        ArrayList<InputSellingResponse.Dehaat_farmer> pending = new ArrayList<>();
        Bundle bundleDelivered = new Bundle();
        Bundle bundlePending = new Bundle();
        for (int i = 0; i < dehaat_farmer.size(); i++) {
            if (dehaat_farmer.get(i).getState() != null) {

                if (dehaat_farmer.get(i).getState().equals("delivered")) {
                    Log.d("TAG", dehaat_farmer.get(i).getState());
                    delivered.add(dehaat_farmer.get(i));
                } else {
                    Log.d("TAG", dehaat_farmer.get(i).getState());
                    pending.add(dehaat_farmer.get(i));
                }
            }
        }
        bundleDelivered.putSerializable(ORDER_LIST, delivered);
        bundlePending.putSerializable(ORDER_LIST, pending);
        adapter.addFragment(new InputSellingStatusHistoryFragment(), getString(R.string.status_pending), bundlePending);
        adapter.addFragment(new InputSellingStatusHistoryFragment(), getString(R.string.status_delivered), bundleDelivered);
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
    }

    private void getSaleData() {
        AppUtils.showProgressDialog(activity);
        AppRestClient client = AppRestClient.getInstance();
        Call<InputSellingResponse> call = client.dehaatFarmerSaleHistory(null, null, null);
        call.enqueue(new ApiCallback<InputSellingResponse>() {
            @Override
            public void onResponse(Response<InputSellingResponse> response) {

                if (response.body() == null)
                    return;
                if (response.body().getDehaat_farmer() != null && response.body().getDehaat_farmer().size() > 0) {
                    Log.d("TAG", response.body().getDehaat_farmer().size() + "");
                    setupViewPager(response.body().getDehaat_farmer());
                } else {
                    no_data_mssg.setVisibility(View.VISIBLE);
                }
                AppUtils.hideProgressDialog();
            }

            @Override
            public void onResponse401(Response<InputSellingResponse> response) throws JSONException {
                AppUtils.showSessionExpiredDialog(getActivity());
            }
        });
    }

    @Override
    public void onPause() {
        super.onPause();
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
        Bundle params = new Bundle();
        SimpleDateFormat s = new SimpleDateFormat("hh:mm:ss");
        format2 = s.format(new Date());
        diffTime = AppUtils.showTimeDiff(format1, format2, diffTime);
        params.putString("Dehaat_Farmer_history", diffTime + "   " + AppPreference.getInstance().getDEHAATI_MOBILE());
        mFirebaseAnalytics.setUserProperty("Mobile", AppPreference.getInstance().getDEHAATI_MOBILE());
        mFirebaseAnalytics.logEvent("Dehaat_Farmer_history", params);
    }

    @Override
    public void onResume() {
        super.onResume();
        SimpleDateFormat s = new SimpleDateFormat("hh:mm:ss");
        format1 = s.format(new Date());
    }

    public void clear() {
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        List<Fragment> fragmentList = fragmentManager.getFragments();
        for (Fragment fragment : fragmentList) {
            transaction.remove(fragment);
        }
        fragmentList.clear();
        transaction.commitAllowingStateLoss();
        getSaleData();
    }

    class InputSellingViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public InputSellingViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title, Bundle bundle) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
            fragment.setArguments(bundle);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}