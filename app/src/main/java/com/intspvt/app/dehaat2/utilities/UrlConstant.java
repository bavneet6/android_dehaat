package com.intspvt.app.dehaat2.utilities;

import com.intspvt.app.dehaat2.BuildConfig;

/**
 * Created by DELL on 7/3/2017.
 */

public interface UrlConstant {
    String BASE_URL = BuildConfig.BASE_URL;
    String ENGLISH = "en";
    String HINDI = "hi";
    String SEED = "Seeds";
    String FERT = "Fertilizers";
    String CROP = "Crop Protection";
    String ENQ_HI = "" +
            "enquiry/history";
    String AGR_IN_HI = "agri_input/history";
    String TREN_ART = "trending/article";
    String PROM_ART = "trending/promotion";
    String INPUT = "input";
    String OUTPUT = "output";
    String ADVISORY = "advisory";
    String OTHERS = "other";
    String SELECT = "Select";
    String TEMP_FNF_DB = BuildConfig.TEMP_FNF_DB;
    String TOLL_FREE_NUMBER = "18001036110";
    String DEHAAT_FARMER_HI = "dehaat/farmaer_sale";


}
