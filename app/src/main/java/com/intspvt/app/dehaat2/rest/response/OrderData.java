package com.intspvt.app.dehaat2.rest.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by DELL on 9/1/2017.
 */

public class OrderData {
    @SerializedName("product_id")
    private int product_id;

    @SerializedName("product_uom_qty")
    private int product_uom_qty;
    @SerializedName("price_unit")
    private float product_price;

    public void setProduct_price(float product_price) {
        this.product_price = product_price;
    }

    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }

    public void setProduct_uom_qty(int product_uom_qty) {
        this.product_uom_qty = product_uom_qty;
    }
}
