package com.intspvt.app.dehaat2.adapter;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.intspvt.app.dehaat2.R;
import com.intspvt.app.dehaat2.rest.response.SaleLines;
import com.intspvt.app.dehaat2.utilities.InputSellingUpdate;
import com.intspvt.app.dehaat2.utilities.ValueUpdate;

import java.util.ArrayList;

public class RecyclerAdapterInputSellingModify extends
        RecyclerView.Adapter<RecyclerAdapterInputSellingModify.Products> {
    private Context context;
    private ArrayList<SaleLines> salesLinesData = new ArrayList<>();
    private int productQtyValue;
    private InputSellingUpdate inputSellingUpdate;
    private ValueUpdate valueUpdate;

    public RecyclerAdapterInputSellingModify(ArrayList<SaleLines> salesLinesData,
                                             InputSellingUpdate inputSellingUpdate,
                                             ValueUpdate valueUpdate) {
        this.salesLinesData = salesLinesData;
        this.inputSellingUpdate = inputSellingUpdate;
        this.valueUpdate = valueUpdate;
    }

    @Override
    public RecyclerAdapterInputSellingModify.Products onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.template_input_selling_cart, parent, false);
        context = parent.getContext();
        return new RecyclerAdapterInputSellingModify.Products(itemView);

    }

    @Override
    public void onBindViewHolder(final RecyclerAdapterInputSellingModify.Products holder, final int position) {
        final int pos = holder.getAdapterPosition();
        if (salesLinesData.get(pos).getIs_deleted() != null && salesLinesData.get(pos).getIs_deleted()) {
            holder.back.setVisibility(View.GONE);
        } else {
            holder.back.setVisibility(View.VISIBLE);
            holder.priceText.setEnabled(false);
            holder.productName.setText(salesLinesData.get(pos).getProduct_name());
            holder.productQty.setText("" + salesLinesData.get(pos).getProduct_qty());
            holder.priceText.setText("" + salesLinesData.get(pos).getProduct_price());

            holder.delete.setOnClickListener(view -> {
                inputSellingUpdate.onItemClick(pos, "true");

            });

            holder.plus.setOnClickListener(view -> {
                if (holder.productQty.getText().toString().equals("")) {
                    holder.productQty.setText("1");
                } else {
                    productQtyValue = Integer.parseInt(holder.productQty.getText().toString());
                    ++productQtyValue;
                    holder.productQty.setText("" + productQtyValue);
                }
            });
            holder.minus.setOnClickListener(view -> {
                if (holder.productQty.getText().toString().equals("")) {
                    holder.productQty.setText("0");
                } else {
                    productQtyValue = Integer.parseInt(holder.productQty.getText().toString());
                    if (productQtyValue == 1) {
                        inputSellingUpdate.onItemClick(pos, "true");
                    } else {
                        --productQtyValue;
                        holder.productQty.setText("" + productQtyValue);
                    }
                }
            });
        }
        holder.productQty.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!holder.productQty.getText().toString().equals("")) {
                    valueUpdate.onUpdate(pos, holder.productQty.getText().toString());
                }
            }
        });

    }


    @Override
    public int getItemCount() {
        return salesLinesData.size();
    }


    public class Products extends RecyclerView.ViewHolder {
        private TextView productName;
        private ImageView delete, plus, minus;
        private EditText productQty, priceText;
        private LinearLayout back;


        public Products(View itemView) {
            super(itemView);
            productName = itemView.findViewById(R.id.pro_name);
            priceText = itemView.findViewById(R.id.price);
            plus = itemView.findViewById(R.id.plus);
            minus = itemView.findViewById(R.id.minus);
            delete = itemView.findViewById(R.id.delete);
            productQty = itemView.findViewById(R.id.pro_qty);
            back = itemView.findViewById(R.id.back);
        }
    }
}
