package com.intspvt.app.dehaat2.adapter;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.intspvt.app.dehaat2.R;
import com.intspvt.app.dehaat2.database.DatabaseHandler;
import com.intspvt.app.dehaat2.fragments.EnquiryDetailHistoryFragment;
import com.intspvt.app.dehaat2.rest.response.EnquiryDataHistory;
import com.intspvt.app.dehaat2.rest.response.IssueDbStatus;
import com.intspvt.app.dehaat2.utilities.AppUtils;
import com.intspvt.app.dehaat2.utilities.UrlConstant;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by DELL on 1/11/2018.
 */

public class RecyclerAdapterEnquiryHistory extends RecyclerView.Adapter<RecyclerAdapterEnquiryHistory.Order> {
    private Context context;
    private List<EnquiryDataHistory> data;
    private DatabaseHandler databaseHandler;
    private ArrayList<IssueDbStatus> issueArrayList = new ArrayList<>();

    public RecyclerAdapterEnquiryHistory(List<EnquiryDataHistory> data) {
        this.data = data;
    }

    @Override
    public RecyclerAdapterEnquiryHistory.Order onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.template_enquiry_history, parent, false);
        context = parent.getContext();
        return new RecyclerAdapterEnquiryHistory.Order(itemView);

    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onBindViewHolder(final RecyclerAdapterEnquiryHistory.Order holder, int position) {
        databaseHandler = new DatabaseHandler(context);
        final int pos = holder.getAdapterPosition();
        issueArrayList = databaseHandler.getIssueSeenData();
        for (int i = 0; i < issueArrayList.size(); i++) {
            if (issueArrayList.get(i).getIssue_id() == data.get(pos).getId()) {
                if (issueArrayList.get(i).getIssue_unseen() == 1) {
                    holder.new_reply.setVisibility(View.VISIBLE);
                    holder.back.setBackground(context.getResources().getDrawable(R.drawable.card_back_selected));
                } else {
                    holder.new_reply.setVisibility(View.GONE);
                    holder.back.setBackground(context.getResources().getDrawable(R.drawable.dehaat_reply_back));
                }
            }
        }
        if (!AppUtils.isNullCase(data.get(pos).getDescription())) {
            holder.enHeading.setText(data.get(pos).getDescription());
        }
        holder.enStatus.setText(data.get(pos).getStage_id());
        holder.enDate.setText(data.get(pos).getCreate_date());
        holder.enId.setText(context.getString(R.string.issue_no) + data.get(pos).getId());

        if ((!data.get(pos).

                isHas_images()) && (!data.get(pos).

                isHas_audio())) {
            holder.image_no.setText(context.getString(R.string.no_pho_au_at));
            holder.image_no.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        } else if (data.get(pos).

                isHas_images()) {
            if (data.get(pos).isHas_audio()) {
                holder.image_no.setText(context.getString(R.string.photo_au_at));
                holder.image_no.setCompoundDrawablesWithIntrinsicBounds(R.drawable.attachment, 0, 0, 0);
            } else {
                holder.image_no.setText(context.getString(R.string.photo_at));
                holder.image_no.setCompoundDrawablesWithIntrinsicBounds(R.drawable.attachment, 0, 0, 0);
            }
        } else if (data.get(pos).

                isHas_audio()) {
            holder.image_no.setText(context.getString(R.string.audio_at));
            holder.image_no.setCompoundDrawablesWithIntrinsicBounds(R.drawable.attachment, 0, 0, 0);

        }
        if (!AppUtils.isNullCase(data.get(pos).

                getCategory())) {
            if (data.get(pos).getCategory().equals(UrlConstant.INPUT)) {
                holder.enCate.setText(context.getString(R.string.input));
            } else if (data.get(pos).getCategory().equals(UrlConstant.OUTPUT)) {
                holder.enCate.setText(context.getString(R.string.output));
            } else if (data.get(pos).getCategory().equals(UrlConstant.ADVISORY)) {
                holder.enCate.setText(context.getString(R.string.advisory));
            } else if (data.get(pos).getCategory().equals(UrlConstant.OTHERS)) {
                holder.enCate.setText(context.getString(R.string.others));
            }
        }

        holder.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putInt("ISSUE_ID", data.get(pos).getId());
                EnquiryDetailHistoryFragment fragment = EnquiryDetailHistoryFragment.newInstance();
                fragment.setArguments(bundle);
                AppUtils.changeFragment((FragmentActivity) context, fragment);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class Order extends RecyclerView.ViewHolder {
        private TextView enHeading, enId, enDate, image_no, enStatus, enCate;
        private LinearLayout back, new_reply;

        public Order(View itemView) {
            super(itemView);
            enHeading = itemView.findViewById(R.id.enHeading);
            enStatus = itemView.findViewById(R.id.enStatus);
            enId = itemView.findViewById(R.id.enId);
            enDate = itemView.findViewById(R.id.enDate);
            image_no = itemView.findViewById(R.id.images_no);
            enCate = itemView.findViewById(R.id.enCate);
            back = itemView.findViewById(R.id.back);
            new_reply = itemView.findViewById(R.id.new_reply);


        }
    }
}



