package com.intspvt.app.dehaat2.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.intspvt.app.dehaat2.R;
import com.intspvt.app.dehaat2.activity.MainActivity;
import com.intspvt.app.dehaat2.adapter.RecyclerAdapterEnquiryHistory;
import com.intspvt.app.dehaat2.database.DatabaseHandler;
import com.intspvt.app.dehaat2.rest.ApiCallback;
import com.intspvt.app.dehaat2.rest.AppRestClient;
import com.intspvt.app.dehaat2.rest.response.EnquiryHistory;
import com.intspvt.app.dehaat2.rest.response.IssueDbStatus;
import com.intspvt.app.dehaat2.utilities.AppPreference;
import com.intspvt.app.dehaat2.utilities.AppUtils;

import org.json.JSONException;

import java.text.SimpleDateFormat;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by DELL on 1/11/2018.
 */

public class EnquiryHistoryFragment extends BaseFragment {
    public static final String TAG = EnquiryHistoryFragment.class.getSimpleName();
    private FirebaseAnalytics mFirebaseAnalytics;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private TextView no_data_mssg;
    private String format2, format1, diffTime;
    private DatabaseHandler databaseHandler;

    public static EnquiryHistoryFragment newInstance() {
        return new EnquiryHistoryFragment();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_enquriy_history, null);
        showActionBar(true);
        recyclerView = v.findViewById(R.id.recy_list);
        recyclerView.setNestedScrollingEnabled(false);
        ((MainActivity) activity).setTitle(getString(R.string.enhistory));
        ((MainActivity) activity).showCart(false);
        ((MainActivity) activity).showDrawer(false);
        no_data_mssg = v.findViewById(R.id.no_data_mssg);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        databaseHandler = new DatabaseHandler(getActivity());
        getInfo();
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        SimpleDateFormat s = new SimpleDateFormat("hh:mm:ss");
        format1 = s.format(new Date());
    }

    @Override
    public void onPause() {
        super.onPause();
        Bundle params = new Bundle();
        SimpleDateFormat s = new SimpleDateFormat("hh:mm:ss");
        format2 = s.format(new Date());
        diffTime = AppUtils.showTimeDiff(format1, format2, diffTime);
        params.putString("EnquiryHistory", diffTime + "   " + AppPreference.getInstance().getDEHAATI_MOBILE());
        mFirebaseAnalytics.setUserProperty("Mobile", AppPreference.getInstance().getDEHAATI_MOBILE());
        mFirebaseAnalytics.logEvent("EnquiryHistory", params);
    }

    private void getInfo() {
        AppUtils.showProgressDialog(getActivity());
        AppRestClient client = AppRestClient.getInstance();
        Call<EnquiryHistory> call = client.getEnquiryHistory();
        call.enqueue(new ApiCallback<EnquiryHistory>() {
            @Override
            public void onResponse(Response<EnquiryHistory> response) {
                if (response.body() == null)
                    return;
                if (AppUtils.haveNetworkConnection(getActivity()))
                    if (response.raw().networkResponse().code() != 304)
                        if (response.body().getData() != null)
// check if a reply is added now then store into database with true value
                            for (int i = 0; i < response.body().getData().size(); i++) {
                                if (response.body().getData().get(i).isHas_new_replies()) {
                                    IssueDbStatus issueDbStatus = new IssueDbStatus();
                                    issueDbStatus.setIssue_id(response.body().getData().get(i).getId());
                                    // 1 for true
                                    issueDbStatus.setIssue_unseen(1);
                                    databaseHandler.insertIssueStatusSeen(issueDbStatus);
                                    databaseHandler.getIssueSeenData();
                                }
                            }

                if (response.body().getData() != null) {
                    no_data_mssg.setVisibility(View.GONE);
                    recyclerView.setAdapter(new RecyclerAdapterEnquiryHistory(response.body().getData()));
                    layoutManager = new LinearLayoutManager(getActivity());
                    recyclerView.setLayoutManager(layoutManager);
                } else {
                    no_data_mssg.setVisibility(View.VISIBLE);
                }
                AppUtils.hideProgressDialog();
            }

            @Override
            public void onResponse401(Response<EnquiryHistory> response) throws JSONException {
                AppUtils.showSessionExpiredDialog(getActivity());
            }

        });
    }
}
