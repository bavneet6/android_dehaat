package com.intspvt.app.dehaat2.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.intspvt.app.dehaat2.R;
import com.intspvt.app.dehaat2.rest.response.OrderDataHistory;
import com.intspvt.app.dehaat2.utilities.AppUtils;

import java.util.List;


/**
 * Created by DELL on 10/5/2017.
 */

public class RecyclerAdapterLedgerPurchaseHistory extends RecyclerView.Adapter<RecyclerAdapterLedgerPurchaseHistory.Order> {
    private Context context;
    private List<OrderDataHistory> data;
    private RecyclerView.LayoutManager layoutManager;

    public RecyclerAdapterLedgerPurchaseHistory(Activity activity, List<OrderDataHistory> data) {
        this.context = activity;
        this.data = data;
    }

    @Override
    public RecyclerAdapterLedgerPurchaseHistory.Order onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.template_ledger_purchase, parent, false);
        context = parent.getContext();
        return new RecyclerAdapterLedgerPurchaseHistory.Order(itemView);

    }

    @Override
    public void onBindViewHolder(final RecyclerAdapterLedgerPurchaseHistory.Order holder, int position) {
        final int pos = holder.getAdapterPosition();

        holder.orderStatus.setText(data.get(pos).getState());
        holder.orderAmt.setText(context.getString(R.string.rs) + data.get(pos).getAmount_total());
        holder.orderDate.setText(data.get(pos).getDate_order());
        holder.orderId.setText(context.getString(R.string.orderId) + data.get(pos).getName());
        holder.totalItem.setText(context.getString(R.string.total_items) + data.get(pos).getOrder_lines().size());
        if (!AppUtils.isNullCase(data.get(pos).getNote()))
            holder.notes.setText(context.getString(R.string.notes) + data.get(pos).getNote());
        holder.back.setOnClickListener(v -> {
            if (holder.detailsBack.getVisibility() != View.VISIBLE) {
                holder.detailsBack.setVisibility(View.VISIBLE);
                holder.details.setAdapter(new RecyclerAdapterOrderLines((Activity) context, data.get(pos).getOrder_lines()));
                layoutManager = new LinearLayoutManager(context);
                holder.details.setLayoutManager(layoutManager);
            } else {
                holder.detailsBack.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    public class Order extends RecyclerView.ViewHolder {
        private TextView orderStatus, orderId, orderDate, orderAmt, totalItem, orderAmtText, notes;
        private RecyclerView details;
        private LinearLayout detailsBack, back;


        public Order(View itemView) {
            super(itemView);
            orderStatus = itemView.findViewById(R.id.orderStatus);
            orderDate = itemView.findViewById(R.id.orderDate);
            orderId = itemView.findViewById(R.id.orderId);
            orderAmt = itemView.findViewById(R.id.orderAmt);
            totalItem = itemView.findViewById(R.id.totalItem);
            orderAmtText = itemView.findViewById(R.id.total_amt_txt);
            notes = itemView.findViewById(R.id.notes);
            details = itemView.findViewById(R.id.details);
            detailsBack = itemView.findViewById(R.id.details_back);
            back = itemView.findViewById(R.id.back);
        }
    }

}

