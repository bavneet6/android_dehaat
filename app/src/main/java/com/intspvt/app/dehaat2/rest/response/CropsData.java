package com.intspvt.app.dehaat2.rest.response;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by DELL on 9/14/2017.
 */

public class CropsData {

    @SerializedName("data")
    private ArrayList<CropsInfo> cropsInfo;

    public ArrayList<CropsInfo> getCropsInfo() {
        return cropsInfo;
    }

    public static class CropsInfo {

        @SerializedName("id")
        private Long id;
        @SerializedName("crop_id")
        private Long crop_id;
        @SerializedName("cultivated_area")
        private float cultivated_area;
        @SerializedName("land_unit")
        private String land_unit;
        @SerializedName("name")
        private String name;
        @SerializedName("image")
        private String image;
        @SerializedName("deleted")
        private boolean deleted;
        @SerializedName("partner_crop_rel_record_id")
        private Long partner_crop_rel_record_id;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }


        public Long getID() {
            return id;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }


        public void setId(Long id) {
            this.id = id;
        }
    }
}
