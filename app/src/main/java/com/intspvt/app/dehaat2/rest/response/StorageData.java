package com.intspvt.app.dehaat2.rest.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by DELL on 9/1/2017.
 */

public class StorageData {

    @SerializedName("data")
    private boolean data;


    public boolean isData() {
        return data;
    }
}
