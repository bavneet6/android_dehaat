package com.intspvt.app.dehaat2.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.intspvt.app.dehaat2.repository.OrderHistoryRepository;
import com.intspvt.app.dehaat2.rest.response.OrderDataHistory;

import java.util.List;


public class OrderHistoryViewModel extends ViewModel {
    private MutableLiveData<List<OrderDataHistory>> orderList;
    private OrderHistoryRepository orderHistoryRepository;

    public void init(String key, String from_date, String to_date) {
        if (orderHistoryRepository == null)
            orderHistoryRepository = OrderHistoryRepository.getInstance();
        orderList = orderHistoryRepository.getOrderData(key, from_date, to_date);
    }

    public LiveData<List<OrderDataHistory>> getHistoryData() {
        return orderList;
    }
}
