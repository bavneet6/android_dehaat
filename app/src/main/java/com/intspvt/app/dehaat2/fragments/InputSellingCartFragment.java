package com.intspvt.app.dehaat2.fragments;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.intspvt.app.dehaat2.R;
import com.intspvt.app.dehaat2.activity.MainActivity;
import com.intspvt.app.dehaat2.adapter.RecyclerAdapterInputSellingCart;
import com.intspvt.app.dehaat2.database.DatabaseHandler;
import com.intspvt.app.dehaat2.rest.ApiCallback;
import com.intspvt.app.dehaat2.rest.AppRestClient;
import com.intspvt.app.dehaat2.rest.response.InputSelling;
import com.intspvt.app.dehaat2.rest.response.InputSellingData;
import com.intspvt.app.dehaat2.rest.response.SaleLines;
import com.intspvt.app.dehaat2.utilities.AppPreference;
import com.intspvt.app.dehaat2.utilities.AppUtils;
import com.intspvt.app.dehaat2.utilities.DeleteInputCart;
import com.intspvt.app.dehaat2.utilities.OnItemClick;

import org.json.JSONException;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import nl.dionsegijn.konfetti.KonfettiView;
import nl.dionsegijn.konfetti.models.Shape;
import retrofit2.Call;
import retrofit2.Response;

public class InputSellingCartFragment extends BaseFragment implements View.OnClickListener, OnItemClick, DeleteInputCart, TextWatcher {
    public static final String TAG = InputSellingCartFragment.class.getSimpleName();
    private RecyclerView recyclerView;
    private FirebaseAnalytics firebaseAnalytics;
    private DatabaseHandler databaseHandler;
    private ArrayList<InputSelling> arrayList = new ArrayList<>();
    private ArrayList<SaleLines> inputSellingList = new ArrayList<>();
    private RecyclerView.LayoutManager layoutManager;
    private TextView totalItems, totalPrice;
    private LinearLayout proceedToSell, sellingBack;
    private EditText farmerNo;
    private float total_amt;
    private String productNames, format2, format1, diffTime;
    private ImageView proceed;

    public static InputSellingCartFragment newInstance() {
        return new InputSellingCartFragment();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_input_selling_cart, null);
        showActionBar(true);
        ((MainActivity) activity).showDrawer(false);
        ((MainActivity) activity).showCart(false);
        ((MainActivity) activity).setTitle(getString(R.string.agri_input));
        databaseHandler = new DatabaseHandler(getActivity());
        firebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        recyclerView = v.findViewById(R.id.cart_list);
        totalItems = v.findViewById(R.id.totalItems);
        totalPrice = v.findViewById(R.id.totalPrice);
        proceedToSell = v.findViewById(R.id.proceedToSell);
        sellingBack = v.findViewById(R.id.selling_back);
        recyclerView.setNestedScrollingEnabled(false);
        arrayList = databaseHandler.getInputSellingData();
        displaySellingCartData();
        proceedToSell.setOnClickListener(this);
        printPriceAndItems();
        return v;

    }

    private void printPriceAndItems() {
        total_amt = 0;
        for (int i = 0; i < arrayList.size(); i++) {
            float total = arrayList.get(i).getProductMrp() * arrayList.get(i).getProductQty();
            total_amt = total_amt + total;
        }
        totalPrice.setText(getString(R.string.rs) + total_amt);
        totalItems.setText("   " + getString(R.string.total_items) + arrayList.size());
    }

    private void displaySellingCartData() {
        recyclerView.setAdapter(new RecyclerAdapterInputSellingCart(arrayList, this, this));
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.proceedToSell:
                productNames = "";
                inputSellingList.clear();
                if (arrayList.size() != 0) {
                    for (int i = 0; i < arrayList.size(); i++) {
                        SaleLines saleLines = new SaleLines();
                        saleLines.setProduct_id(Integer.parseInt(arrayList.get(i).getRequestId()));
                        saleLines.setProduct_price(arrayList.get(i).getProductMrp());
                        saleLines.setProduct_qty(arrayList.get(i).getProductQty());
                        inputSellingList.add(saleLines);
                        if (productNames.equals(""))
                            productNames = arrayList.get(i).getProductName();
                        else
                            productNames = productNames + " , " + arrayList.get(i).getProductName();

                    }
                }
                openFarmerNumberDialog();
                break;

        }
    }

    private void openFarmerNumberDialog() {
        final Dialog dialog = new Dialog(new ContextThemeWrapper(getActivity(), R.style.DialogSlideAnim));
        final TextView skip, offerLine;
        dialog.setCanceledOnTouchOutside(true);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_farmer_number);
        skip = dialog.findViewById(R.id.skip);
        farmerNo = dialog.findViewById(R.id.ed2);
        farmerNo.addTextChangedListener(this);
        proceed = dialog.findViewById(R.id.proceed);
        proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (farmerNo.getText().toString().length() != 10) {
                    farmerNo.requestFocus();
                    farmerNo.setError(getString(R.string.enter_correct_number));
                } else if (farmerNo.getText().toString().equals(AppPreference.getInstance().getDEHAATI_MOBILE())) {
                    farmerNo.requestFocus();
                    farmerNo.setError(getString(R.string.dont_enter_your_number));
                } else {
                    dialog.dismiss();
                    sendData();
                }
            }
        });
        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                farmerNo.setText("");
                dialog.dismiss();
                sendData();
            }
        });
        Drawable d = new ColorDrawable(getResources().getColor(R.color.greytext));
        d.setAlpha(100);
        dialog.getWindow().setBackgroundDrawable(d);
        dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        int width = getResources().getDisplayMetrics().widthPixels;
        dialog.getWindow().setLayout(width, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setGravity(Gravity.BOTTOM);
        dialog.show();
        showKeyboard(farmerNo);
    }

    private void sendData() {
        AppUtils.showProgressDialog(activity);
        AppRestClient client = AppRestClient.getInstance();
        InputSellingData inputSellingData = new InputSellingData();
        inputSellingData.setInventory_status("available");
        inputSellingData.setFarmer_no(farmerNo.getText().toString());
        inputSellingData.setSale_lines(inputSellingList);
        inputSellingData.setTotal_amt(total_amt);
        inputSellingData.setState("delivered");
        com.intspvt.app.dehaat2.rest.body.InputSelling inputSelling = new com.intspvt.app.dehaat2.rest.body.InputSelling(inputSellingData);
        Call<Void> call = client.dehaatFarmerSale(inputSelling);
        call.enqueue(new ApiCallback<Void>() {
            @Override
            public void onResponse(Response<Void> response) {
                AppUtils.hideProgressDialog();
                if (response.code() == 200) {
                    databaseHandler.clearInputSellingData();
                    showOrderPlacedScreen();
                    Bundle params = new Bundle();
                    SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                    String format = s.format(new Date());
                    params.putString("Success", format + "   " + AppPreference.getInstance().getDEHAATI_MOBILE());
                    firebaseAnalytics.logEvent("DehaatFarmerSale", params);
                }

            }

            @Override
            public void onResponse401(Response<Void> response) throws JSONException {
                AppUtils.showSessionExpiredDialog(getActivity());
            }
        });

    }

    private void showOrderPlacedScreen() {
        final Dialog dialog = new Dialog(getActivity());
        TextView salesHistory, exit;
        KonfettiView view;
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_input_selling_order_placed);
        salesHistory = dialog.findViewById(R.id.salesHistory);
        exit = dialog.findViewById(R.id.exit);
        view = dialog.findViewById(R.id.view);
        view.build()
                .addColors(Color.parseColor("#f2d422"), Color.parseColor("#ff8828"), Color.parseColor("#ff492c"), Color.parseColor("#ea473b"))
                .setDirection(0.0, 359.0)
                .setSpeed(1f, 5f)
                .setFadeOutEnabled(true)
                .setTimeToLive(2000L)
                .addShapes(Shape.RECT, Shape.CIRCLE)
                .setPosition(700f, view.getX() + 50f, 700f, -50f)
                .stream(300, 300L);
        salesHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                getFragmentManager().beginTransaction().add(R.id.frag_container, InputSellingHistroryFragment.newInstance()).commit();

            }
        });

        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                getActivity().onBackPressed();
            }
        });
        Drawable d = new ColorDrawable(getResources().getColor(R.color.greytext));
        d.setAlpha(100);
        dialog.getWindow().setBackgroundDrawable(d);
        dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        int width = getResources().getDisplayMetrics().widthPixels;
        dialog.getWindow().setLayout(width, ViewGroup.LayoutParams.MATCH_PARENT);
        dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.show();
    }

    public void showKeyboard(final EditText ettext) {
        ettext.requestFocus();
        ettext.postDelayed(new Runnable() {
                               @Override
                               public void run() {
                                   InputMethodManager keyboard = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                                   keyboard.showSoftInput(ettext, 0);
                               }
                           }
                , 20);
    }


    @Override
    public void onClick(HashMap<Integer, Float> hashMap) {
        arrayList = databaseHandler.getInputSellingData();
        printPriceAndItems();
        showBottomBanner();
    }

    private void showBottomBanner() {
        arrayList = databaseHandler.getInputSellingData();
        if (arrayList.size() == 0)
            sellingBack.setVisibility(View.GONE);
        else
            sellingBack.setVisibility(View.VISIBLE);

    }

    @Override
    public void onItemClick(String s) {
        if (s.equals("Yes"))
            getActivity().onBackPressed();
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        if (farmerNo.getText().toString().length() == 10) {
            proceed.setVisibility(View.VISIBLE);
        } else {
            proceed.setVisibility(View.INVISIBLE);
        }

    }

    @Override
    public void onPause() {
        super.onPause();
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
        Bundle params = new Bundle();
        SimpleDateFormat s = new SimpleDateFormat("hh:mm:ss");
        format2 = s.format(new Date());
        diffTime = AppUtils.showTimeDiff(format1, format2, diffTime);
        params.putString("Dehaat_Farmer_cart", diffTime + "   " + AppPreference.getInstance().getDEHAATI_MOBILE());
        firebaseAnalytics.setUserProperty("Mobile", AppPreference.getInstance().getDEHAATI_MOBILE());
        firebaseAnalytics.logEvent("Dehaat_Farmer_cart", params);
    }

    @Override
    public void onResume() {
        super.onResume();
        SimpleDateFormat s = new SimpleDateFormat("hh:mm:ss");
        format1 = s.format(new Date());

    }
}
