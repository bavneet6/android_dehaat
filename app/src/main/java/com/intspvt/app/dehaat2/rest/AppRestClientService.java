package com.intspvt.app.dehaat2.rest;


import com.intspvt.app.dehaat2.rest.body.InputSelling;
import com.intspvt.app.dehaat2.rest.body.PostDataDehaat;
import com.intspvt.app.dehaat2.rest.body.PostDataDehaatiInfo;
import com.intspvt.app.dehaat2.rest.body.PostFarmerPersonalInfo;
import com.intspvt.app.dehaat2.rest.body.SendCartDataMain;
import com.intspvt.app.dehaat2.rest.body.SendNumber;
import com.intspvt.app.dehaat2.rest.response.CropManuals;
import com.intspvt.app.dehaat2.rest.response.CropsData;
import com.intspvt.app.dehaat2.rest.response.EnquiryHistory;
import com.intspvt.app.dehaat2.rest.response.FarmerList;
import com.intspvt.app.dehaat2.rest.response.FcmToken;
import com.intspvt.app.dehaat2.rest.response.GetAddressMapping;
import com.intspvt.app.dehaat2.rest.response.GetManualData;
import com.intspvt.app.dehaat2.rest.response.GetOrderResponse;
import com.intspvt.app.dehaat2.rest.response.GetPersonalInfo;
import com.intspvt.app.dehaat2.rest.response.InputSellingResponse;
import com.intspvt.app.dehaat2.rest.response.Inventory;
import com.intspvt.app.dehaat2.rest.response.InventoryStock;
import com.intspvt.app.dehaat2.rest.response.LandUnits;
import com.intspvt.app.dehaat2.rest.response.LoginResponse;
import com.intspvt.app.dehaat2.rest.response.OrderHistory;
import com.intspvt.app.dehaat2.rest.response.PnLModel;
import com.intspvt.app.dehaat2.rest.response.ProductsDehaatFarmerSale;
import com.intspvt.app.dehaat2.rest.response.ProductsList;
import com.intspvt.app.dehaat2.rest.response.RegisterResponse;
import com.intspvt.app.dehaat2.rest.response.SingleCropManual;
import com.intspvt.app.dehaat2.rest.response.SingleIssueHistory;
import com.intspvt.app.dehaat2.rest.response.SingleProduct;
import com.intspvt.app.dehaat2.rest.response.StorageOrderData;
import com.intspvt.app.dehaat2.rest.response.TopProducts;
import com.intspvt.app.dehaat2.rest.response.Trending;
import com.intspvt.app.dehaat2.rest.response.VersionName;

import java.util.List;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by DELL on 6/22/2017.
 */

public interface AppRestClientService {

    //2
    @GET("/dehaat/user/info")
    Call<GetPersonalInfo> getInfo();

    //3
    @GET("/dehaat/crop/list")
    Call<CropsData> getCropList();

    //4
    @GET("/dehaat/crop/manual")
    Call<GetManualData> getManualData();

    //5
    @GET("/dehaat/issue/history")
    Call<EnquiryHistory> getEnquiryHistory();

    //6
    @POST("/dehaat/register_farmer")
    Call<PostFarmerPersonalInfo> registerFarmer(@Body PostFarmerPersonalInfo postFarmerPersonalInfo);

    //10
    @GET("/dehaat/product/list")
    Call<ProductsList> productList(@Header("product_ids") String product_ids);

    //11

    @GET("/dehaat/farmer_list")
    Call<FarmerList> getFarmerList();

    //12
    @GET("dehaat/sale_order/history")
    Call<OrderHistory> orderHistory(@Query("duration") String key, @Query("from_date")
            String from_date, @Query("to_date") String to_date);


    //14
    @GET("/dehaat/trend/articles")
    Call<Trending> trendingData();

    //16
    @Multipart
    @POST("/dehaat/issue")
    Call<Void> sendEnquiryData(@PartMap() Map<String, RequestBody> partMap, @Part MultipartBody.Part audio, @Part List<MultipartBody.Part> files);

    //17
    @POST("/dehaat/sale_order/add")
    Call<GetOrderResponse> sendCartData(@Body SendCartDataMain body);

    //18
    @GET("/dehaat/version")
    Call<VersionName> getVersionName();

    //20
    @POST("/dehaat/user/info")
    Call<Void> sendDehaatInfo(@Body PostDataDehaatiInfo postdehaatiInfoRegister);

    //21
    @POST("/dehaat")
    Call<RegisterResponse> sendDehaat(@Body PostDataDehaat postDataDehaati);

    @POST("/dehaat/user/update/FCM_token")
    Call<Void> fcmToken(@Body FcmToken fcmToken);

    //24
    @POST("/dehaat/farmer_sale/update")
    Call<Void> updateDehaatFarmerSale(@Body InputSelling body);

    //24
    @POST("/dehaat/farmer_sale")
    Call<Void> dehaatFarmerSale(@Body InputSelling body);

    //25
    @GET("/dehaat/farmer_sale/history")
    Call<InputSellingResponse> dehaatFarmerSaleHistory(@Query("duration") String key,
                                                       @Query("from_date")
                                                               String from_date, @Query("to_date") String to_date);

    //26
    @GET("/dehaat/promotion/articles")
    Call<Trending> promotionArticles();

    //27
    @Multipart
    @POST("/dehaat/issue/reply")
    Call<Void> sendReply(@PartMap() Map<String, RequestBody> partMap, @Part MultipartBody.Part audio,
                         @Part List<MultipartBody.Part> files);

    //28
    @GET("/dehaat/issue/{issue_id}")
    Call<SingleIssueHistory> getSingleHistoryData(@Path("issue_id") int id); //28

    //29
    @GET("/dehaat/state")
    Call<GetAddressMapping> getState();

    //30
    @GET("/dehaat/district/{state_id}")
    Call<GetAddressMapping> getDistrict(@Path("state_id") int id);

    //31
    @GET("/dehaat/block/{district_id}")
    Call<GetAddressMapping> getBlock(@Path("district_id") int id);

    //34
    @Multipart
    @POST("/dehaat/user/update")
    Call<Void> updateDehaatInfo(@PartMap() Map<String, RequestBody> partMap, @Part MultipartBody.Part files);

    //35
    @GET("/dehaat/product/top")
    Call<TopProducts> productListTop();

    //36
    @GET("/dehaat/register_farmer/state")
    Call<GetAddressMapping> getStateRegister();
    //37

    @GET("/dehaat/register_farmer/district/{state_id}")
    Call<GetAddressMapping> getDistrictRegister(@Path("state_id") int id);
    //38

    @GET("/dehaat/register_farmer/block/{district_id}")
    Call<GetAddressMapping> getBlockRegister(@Path("district_id") int id);
    //39

    @GET("/dehaat/register_farmer/village/{panchayat_id}")
    Call<GetAddressMapping> getVillageRegister(@Path("panchayat_id") int id);

    //40
    @GET("/dehaat/register_farmer/panchayat/{block_id}")
    Call<GetAddressMapping> getPanchayatRegister(@Path("block_id") int id);

    @POST("/dehaat/verify/otp")
    Call<LoginResponse> verifyNumber(@Body SendNumber body);

    //1
    @POST("/dehaat/generate/otp")
    Call<Void> sendNumber(@Body SendNumber body);

    //37
    @GET("/dehaat/land/uom/list")
    Call<LandUnits> getLandUnits();

    @GET("/dehaat/product/{id}")
    Call<SingleProduct> getSingleProduct(@Path("id") Long id);


    //10
    @GET("/dehaat/product/variants")
    Call<ProductsDehaatFarmerSale> getDehaatFarmerProductList();


    @GET("/dehaat/crop/manual/crops/list")
    Call<CropManuals> getCropManualList();

    //33
    @GET("/dehaat/crop/manual/{id}")
    Call<SingleCropManual> getSingleCropManual(@Path("id") Long id);

    //32
    @GET("/dehaat/crop/manual/list")
    Call<CropManuals> getCropManuals(@Query("crop_id") Long id);   //32

    @GET("/dehaat/sale/purchase/ledger/balance")
    Call<PnLModel> getPnL();

    @GET("/dehaat/sale/order/custom_date/balance")
    Call<PnLModel> getDehaatSaleOrder(@Query("from_date") String from, @Query("to_date") String to);

    //14
    @GET("/dehaat/sale/purchase/inventory/value")
    Call<Inventory> inventoryValue();

    //14
    @GET("/dehaat/remaining_inventory")
    Call<InventoryStock> inventoryStock();

    //14
    @POST("/dehaat/number/check")
    Call<LoginResponse> dehaatNumberCheck(@Body SendNumber sendNumber);

    @GET("/dehaat/storage_loan/list")
    Call<StorageOrderData> storageandloanOrderHistory();


}
