package com.intspvt.app.dehaat2.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.intspvt.app.dehaat2.R;
import com.intspvt.app.dehaat2.activity.MainActivity;

/**
 * Created by DELL on 7/14/2017.
 */

public class HomeIntroFragment extends BaseFragment {
    public static final String TAG = HomeIntroFragment.class.getSimpleName();
    private int[] images = {R.drawable.img1, R.drawable.img2};     // array of images
    private ImageView image;
    private Thread thread, newthread;
    private ProgressBar progressBar;
    private int progress = 0, images_no;
    private boolean run = true;

    public static HomeIntroFragment newInstance() {
        return new HomeIntroFragment();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_home_intro, null);
        showActionBar(false);
        image = v.findViewById(R.id.image);
        progressBar = v.findViewById(R.id.progress);
        setProgressValue(progress);
        image.setScaleType(ImageView.ScaleType.FIT_XY);
        // this shows 2 images and a progress bar
        thread = new Thread() {
            @Override
            public void run() {
                for (images_no = 0; images_no < images.length; images_no++) {
                    {
                        image.post(new Runnable() {
                            @Override
                            public void run() {
                                image.setImageResource(images[images_no]);
                                Animation animation = AnimationUtils.loadAnimation(getActivity(),
                                        R.anim.scale_home);
                                image.startAnimation(animation);

                            }
                        });


                        try {
                            sleep(3000);
                        } catch (InterruptedException e) {
                        }
                    }
                }
                if (images_no == images.length) {

                }
            }
        };
        thread.start();
        return v;
    }

    private void setProgressValue(final int progress) {
        if (run) {
            if (progressBar.getProgress() == 100) {
                // set the progress
                Intent intent = new Intent(getActivity(), MainActivity.class);
                startActivity(intent);
                getActivity().finish();

            } else {
                progressBar.setProgress(progress);
                // thread is used to change the progress value
                newthread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Thread.sleep(150);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        setProgressValue(progress + 2);
                    }
                });
                newthread.start();
            }
        } else {

        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if (getView() == null) {
            return;
        }
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {

                if (keyEvent.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    getActivity().finish();

                    return true;
                }
                return false;
            }
        });
    }

    @Override
    public void onPause() {
        super.onPause();
        run = false;
        if (thread != null) {
            Thread moribund = thread;
            thread = null;
            moribund.interrupt();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
    }
}
