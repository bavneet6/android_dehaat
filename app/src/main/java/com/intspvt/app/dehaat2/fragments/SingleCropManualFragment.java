package com.intspvt.app.dehaat2.fragments;

import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.intspvt.app.dehaat2.R;
import com.intspvt.app.dehaat2.activity.MainActivity;
import com.intspvt.app.dehaat2.rest.ApiCallback;
import com.intspvt.app.dehaat2.rest.AppRestClient;
import com.intspvt.app.dehaat2.rest.response.CropManualData;
import com.intspvt.app.dehaat2.rest.response.SingleCropManual;
import com.intspvt.app.dehaat2.utilities.AppUtils;
import com.intspvt.app.dehaat2.utilities.ShowCartManual;

import org.json.JSONException;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Response;

public class SingleCropManualFragment extends BaseFragment implements View.OnClickListener, ShowCartManual {
    public static final String TAG = SingleCropManualFragment.class.getSimpleName();
    private ViewPager viewPager;
    private LinearLayout nextTopicBack;
    private TabLayout tabLayout;
    private Bundle nextBundle;
    private TextView nextTopicHead;
    private List<Long> productIds = new ArrayList<>();
    private Long manual_id = 0L, manual_pos;
    private FirebaseAnalytics firebaseAnalytics;

    private HashMap<Long, ArrayList<String>> manualListMap = new HashMap<>();

    public static SingleCropManualFragment newInstance() {
        return new SingleCropManualFragment();
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_single_crop_manual, null);
        showActionBar(true);
        ((MainActivity) activity).showDrawer(true);
        ((MainActivity) activity).showCart(true);
        viewPager = v.findViewById(R.id.viewpager);
        nextTopicBack = v.findViewById(R.id.nextTopicBack);
        nextTopicHead = v.findViewById(R.id.nextTopicHead);
        tabLayout = v.findViewById(R.id.tabs);
        firebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        nextTopicBack.setOnClickListener(this);
        Bundle bundle = getArguments();
        if (bundle != null) {
            manual_id = bundle.getLong("MANUAL_ID");
            manual_pos = bundle.getLong("MANUAL_POS");
            manualListMap = (HashMap<Long, ArrayList<String>>) bundle.getSerializable("MANUAL_MAP");
        }
        if (manual_id != 0L)
            getSingleCropManual();


        return v;
    }

    private void getSingleCropManual() {
        AppRestClient client = AppRestClient.getInstance();
        Call<SingleCropManual> call = client.getSingleCropManual(manual_id);
        call.enqueue(new ApiCallback<SingleCropManual>() {
            @Override
            public void onResponse(Response<SingleCropManual> response) {
                if (getActivity() != null && isAdded()) {
                    if (response.body() == null)
                        return;
                    if (response.body().getCropManual() == null)
                        return;
                    productIds = response.body().getCropManual().getProduct_ids();
                    displayManualData(response.body().getCropManual());

                }
            }

            @Override
            public void onResponse401(Response<SingleCropManual> response) throws
                    JSONException {
                AppUtils.showSessionExpiredDialog(getActivity());
            }


        });
    }

    private void displayManualData(CropManualData cropManual) {
        setupViewPager(cropManual);
        tabLayout.setupWithViewPager(viewPager);
        ((MainActivity) activity).setTitle("Crop" + " : " + cropManual.getCrop());

        Long nextpos;
        if (manualListMap.size() - 1 == manual_pos) {
            nextpos = 0L;
        } else
            nextpos = manual_pos + 1;

        if (!manualListMap.isEmpty())
            for (Map.Entry<Long, ArrayList<String>> entry : manualListMap.entrySet()) {
                if (nextpos.equals(entry.getKey())) {
                    nextBundle = new Bundle();
                    nextBundle.putLong("MANUAL_ID", Long.parseLong(entry.getValue().get(0)));
                    nextBundle.putLong("MANUAL_POS", nextpos);
                    nextBundle.putSerializable("MANUAL_MAP", manualListMap);
                    nextTopicHead.setText(getString(R.string.next_information) + cropManual.getCrop() + " : " + entry.getValue().get(1));
                }
            }
    }

    private void setupViewPager(CropManualData cropManual) {
        SingleCropManualFragment.ViewPagerAdapter adapter = new SingleCropManualFragment.ViewPagerAdapter(getChildFragmentManager());
        Fragment fragment = new SingleManualContentFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("MANUAL_DATA", cropManual);
        fragment.setArguments(bundle);
        adapter.addFragment(fragment, getString(R.string.Guide));
        if (productIds != null && productIds.size() != 0) {
            Fragment fragment1 = new ManualProductsFragment();
            Bundle bundle1 = new Bundle();
            bundle1.putSerializable("PRODUCT_IDS", (Serializable) productIds);
            fragment1.setArguments(bundle1);
            adapter.addFragment(fragment1, getString(R.string.recommended_products));
            ((MainActivity) activity).showCartCount(true);
        }
        this.viewPager.setAdapter(adapter);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onClick(View view) {
        FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();
        int id = view.getId();
        switch (id) {
            case R.id.back:
                getActivity().onBackPressed();
                break;
            case R.id.cart_back:
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.frag_container, CartFragment.newInstance()).commitAllowingStateLoss();
                break;
            case R.id.nextTopicBack:
                SingleCropManualFragment fragment = SingleCropManualFragment.newInstance();
                fragment.setArguments(nextBundle);
                transaction.replace(R.id.frag_container, fragment).commitAllowingStateLoss();
                break;
        }
    }


    @Override
    public void showCartManual(Activity activity) {
        if (activity != null)
            ((MainActivity) activity).showCartCount(true);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }


}
