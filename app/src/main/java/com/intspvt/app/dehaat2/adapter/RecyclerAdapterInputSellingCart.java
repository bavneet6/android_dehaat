package com.intspvt.app.dehaat2.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.intspvt.app.dehaat2.R;
import com.intspvt.app.dehaat2.database.DatabaseHandler;
import com.intspvt.app.dehaat2.rest.PicassoCallback;
import com.intspvt.app.dehaat2.rest.response.InputSelling;
import com.intspvt.app.dehaat2.utilities.AppUtils;
import com.intspvt.app.dehaat2.utilities.DeleteInputCart;
import com.intspvt.app.dehaat2.utilities.OnItemClick;
import com.squareup.picasso.Picasso;

import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

public class RecyclerAdapterInputSellingCart extends RecyclerView.Adapter<RecyclerAdapterInputSellingCart.Products> {
    private Context context;
    private ArrayList<InputSelling> arrayList = new ArrayList<>();
    private ArrayList<InputSelling> inputList = new ArrayList<>();
    private ArrayList<InputSelling> newList = new ArrayList<>();
    private DatabaseHandler databaseHandler;
    private String getPic;
    private int productQtyValue;
    private OnItemClick onItemClick;
    private DeleteInputCart deleteInputCart;
    private HashMap<Integer, Float> hashMap = new HashMap<>();

    public RecyclerAdapterInputSellingCart(ArrayList<InputSelling> arrayList, OnItemClick onItemClick, DeleteInputCart deleteInputCart) {
        this.arrayList = arrayList;
        this.onItemClick = onItemClick;
        this.deleteInputCart = deleteInputCart;
    }

    @Override
    public RecyclerAdapterInputSellingCart.Products onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.template_input_selling_cart, parent, false);
        context = parent.getContext();
        return new RecyclerAdapterInputSellingCart.Products(itemView);

    }

    @Override
    public void onBindViewHolder(final RecyclerAdapterInputSellingCart.Products holder, final int position) {
        if (arrayList.size() == 0)
            return;
        databaseHandler = new DatabaseHandler(context);
        final int pos = holder.getAdapterPosition();

        holder.setIsRecyclable(false);
        inputList = databaseHandler.getInputSellingData();
        for (int currentpos = 0; currentpos < inputList.size(); currentpos++) {
            if (inputList.get(currentpos).getRequestId().equals(String.valueOf(arrayList.get(pos).getRequestId()))) {
                holder.productQty.setText("" + inputList.get(currentpos).getProductQty());
                holder.priceText.setText("" + inputList.get(currentpos).getProductMrp());
            } else {
                holder.priceText.setText("" + inputList.get(pos).getProductMrp());
            }
        }
        if (!TextUtils.isEmpty(holder.productQty.getText().toString()) && !TextUtils.isEmpty(holder.priceText.getText().toString()))
            totalPrice(holder.productQty.getText().toString(), Float.parseFloat(holder.priceText.getText().toString()), pos);
        holder.productName.setText(arrayList.get(pos).getProductName());
        //getPic finds the url from db and if it returns the url then picasso print that
        getPic = null;
        if (!AppUtils.isNullCase(arrayList.get(pos).getProductImage())) {

            getPic = databaseHandler.getFileUrl(arrayList.get(pos).getProductImage());
            if (getPic != null) {
                final Picasso picasso = Picasso.with(context.getApplicationContext());
                picasso.setLoggingEnabled(true);
                picasso.load(getPic).into(holder.productImg, new PicassoCallback(arrayList.get(pos).getProductImage()) {

                    @Override
                    public void onErrorPic(URL url) {
                        picasso.load("" + url).error(R.drawable.no_product_image).into(holder.productImg);
                    }
                });
            }
        }
        holder.minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (holder.productQty.getText().toString().equals("")) {
                    holder.productQty.setText("0");
                } else {
                    productQtyValue = Integer.parseInt(holder.productQty.getText().toString());
                    if (productQtyValue == 1) {
                        if (databaseHandler.getInputSellingData().size() != 1) {
                            deleteProduct("" + arrayList.get(pos).getRequestId());
                        } else {
                            showEmptyCartDialog("" + arrayList.get(pos).getRequestId());
                        }
                    } else {
                        --productQtyValue;
                        holder.productQty.setText("" + productQtyValue);
                    }
                }
            }
        });
        holder.plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (holder.productQty.getText().toString().equals("")) {
                    holder.productQty.setText("1");
                } else {
                    productQtyValue = Integer.parseInt(holder.productQty.getText().toString());
                    ++productQtyValue;
                    holder.productQty.setText("" + productQtyValue);
                }


            }
        });

        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (databaseHandler.getInputSellingData().size() != 1) {
                    deleteProduct("" + arrayList.get(pos).getRequestId());
                } else {
                    showEmptyCartDialog("" + arrayList.get(pos).getRequestId());
                }
            }
        });

        holder.productQty.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!holder.productQty.getText().toString().equals("")) {
                    addToDB(pos, holder.productQty.getText().toString(), holder.priceText.getText().toString());
                    totalPrice(holder.productQty.getText().toString(), Float.parseFloat(holder.priceText.getText().toString()), pos);
                }
            }
        });
        holder.priceText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!holder.priceText.getText().toString().equals("")) {
                    addToDB(pos, holder.productQty.getText().toString(), holder.priceText.getText().toString());
                    totalPrice(holder.productQty.getText().toString(), Float.parseFloat(holder.priceText.getText().toString()), pos);
                }
            }
        });

    }

    private void totalPrice(String priceQty, float mrp, int pos) {
        int m = Integer.parseInt(priceQty);
        final float total = m * mrp;
        hashMap.put(pos, total);
        onItemClick.onClick(hashMap);
    }

    private void addToDB(int pos, String value, String price) {
        InputSelling contact = new InputSelling();
        contact.setRequestId(arrayList.get(pos).getRequestId());
        contact.setProductName(arrayList.get(pos).getProductName());
        contact.setProductMrp(Float.parseFloat(price));
        contact.setProductQty(Integer.parseInt(value));
        contact.setProductImage(arrayList.get(pos).getProductImage());
        databaseHandler.insertSellingInputRecord(contact);
    }

    private void deleteProduct(final String id) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(context.getString(R.string.delete))
                .setMessage(context.getString(R.string.delete_text))
                .setCancelable(false)
                .setPositiveButton(context.getString(R.string.okText), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        databaseHandler.deleteInputData(id);
                        updateList();
                    }
                })
                .setNegativeButton(context.getString(R.string.cancelText), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .setIcon(android.R.drawable.ic_menu_delete)
                .show();


    }

    private void showEmptyCartDialog(final String id) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(context.getString(R.string.delete))
                .setMessage(context.getString(R.string.delete_text))
                .setPositiveButton(context.getString(R.string.okText), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete

                        deleteInputCart.onItemClick("Yes");
                        databaseHandler.deleteInputData(id);

                    }
                })
                .setNegativeButton(context.getString(R.string.cancelText), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .setIcon(android.R.drawable.ic_menu_delete)
                .show();

    }

    private void updateList() {
        newList = databaseHandler.getInputSellingData();
        this.arrayList = newList;
        notifyDataSetChanged();
        hashMap.clear();
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }


    public class Products extends RecyclerView.ViewHolder {
        private TextView productName;
        private ImageView productImg, delete, plus, minus;
        private EditText productQty, priceText;


        public Products(View itemView) {
            super(itemView);
            productName = itemView.findViewById(R.id.pro_name);
            priceText = itemView.findViewById(R.id.price);
            productImg = itemView.findViewById(R.id.pro_img);
            plus = itemView.findViewById(R.id.plus);
            minus = itemView.findViewById(R.id.minus);
            delete = itemView.findViewById(R.id.delete);
            productQty = itemView.findViewById(R.id.pro_qty);
        }
    }
}
