package com.intspvt.app.dehaat2.utilities;

public interface ValueUpdate {
    void onUpdate(Integer pos, String s);
}
