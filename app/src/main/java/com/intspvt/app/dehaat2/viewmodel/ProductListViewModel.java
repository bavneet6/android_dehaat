package com.intspvt.app.dehaat2.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.intspvt.app.dehaat2.repository.ProductListRepository;
import com.intspvt.app.dehaat2.rest.response.Products;

import java.util.ArrayList;


public class ProductListViewModel extends ViewModel {
    private MutableLiveData<ArrayList<Products>> productList;
    private ProductListRepository productListRepository;

    public void init() {
        if (productListRepository == null)
            productListRepository = ProductListRepository.getInstance();
        productList = productListRepository.getProductData();
    }

    public LiveData<ArrayList<Products>> getProductData() {
        return productList;
    }
}
