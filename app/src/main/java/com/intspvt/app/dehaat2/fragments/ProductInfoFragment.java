package com.intspvt.app.dehaat2.fragments;

import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.RequiresApi;

import com.intspvt.app.dehaat2.R;
import com.intspvt.app.dehaat2.activity.MainActivity;
import com.intspvt.app.dehaat2.database.DatabaseHandler;
import com.intspvt.app.dehaat2.rest.ApiCallback;
import com.intspvt.app.dehaat2.rest.AppRestClient;
import com.intspvt.app.dehaat2.rest.PicassoCallback;
import com.intspvt.app.dehaat2.rest.response.AddToCart;
import com.intspvt.app.dehaat2.rest.response.Products;
import com.intspvt.app.dehaat2.rest.response.SingleProduct;
import com.intspvt.app.dehaat2.utilities.AppUtils;
import com.squareup.picasso.Picasso;

import org.json.JSONException;

import java.net.URL;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by DELL on 11/16/2017.
 */

public class ProductInfoFragment extends BaseFragment implements View.OnClickListener, TextWatcher, AdapterView.OnItemSelectedListener {
    public static final String TAG = ProductInfoFragment.class.getSimpleName();
    private ImageView productImg, plus, minus;
    private TextView productName, pro_mrp, about, add_to_cart;
    private EditText pro_qty;
    private int productQty, variantSelectedPos;
    private LinearLayout variantBack;
    private DatabaseHandler databaseHandler;
    private Spinner variants;
    private ArrayList<Products.Variants> variantList;
    private ArrayList<String> variantsNames = new ArrayList<>();
    private String getPic = null;
    private Long productId = 0L;
    private ProgressBar progressBar;
    private Products productsData;

    public static ProductInfoFragment newInstance() {
        return new ProductInfoFragment();
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_product_details, null);
        showActionBar(true);
        ((MainActivity) activity).setTitle(getString(R.string.agri_input));
        ((MainActivity) activity).showCart(true);
        ((MainActivity) activity).showDrawer(false);
        ((MainActivity) activity).showCartCount(false);
        databaseHandler = new DatabaseHandler(getActivity());
        variantBack = v.findViewById(R.id.variantBack);
        productImg = v.findViewById(R.id.productImg);
        plus = v.findViewById(R.id.plus);
        minus = v.findViewById(R.id.minus);
        productName = v.findViewById(R.id.productName);
        pro_mrp = v.findViewById(R.id.pro_mrp);
        about = v.findViewById(R.id.aboutProduct);
        add_to_cart = v.findViewById(R.id.add_to_cart);
        pro_qty = v.findViewById(R.id.pro_qty);
        variants = v.findViewById(R.id.variants);
        progressBar = v.findViewById(R.id.progress_bar);

        variants.setOnItemSelectedListener(this);
        plus.setOnClickListener(this);
        minus.setOnClickListener(this);
        add_to_cart.setOnClickListener(this);
        pro_qty.addTextChangedListener(this);
        Bundle bundle = getArguments();
        if (bundle != null) {
            productId = bundle.getLong("PRODUCT_ID");
        }
        getSingleProductData(productId);
        return v;
    }

    private void getSingleProductData(Long productId) {
        AppUtils.showProgressDialog(activity);
        AppRestClient client = AppRestClient.getInstance();
        Call<SingleProduct> call = client.getSingleProduct(productId);
        call.enqueue(new ApiCallback<SingleProduct>() {
            @Override
            public void onResponse(Response<SingleProduct> response) {
                AppUtils.hideProgressDialog();
                if (response.body() == null)
                    return;
                if (response.body().getProducts() == null)
                    return;
                productsData = response.body().getProducts();
                displayData(productsData);
            }

            @Override
            public void onResponse401(Response<SingleProduct> response) throws JSONException {

            }


        });
    }

    private void displayData(Products products) {
        if (AppUtils.isNullCase(products.getProductHindiName()))
            productName.setText(products.getProductName());
        else
            productName.setText(products.getProductHindiName());

        if (!AppUtils.isNullCase(products.getImage()))
            showPicassoImage(products.getImage());

        //display Variants
        variantList = new ArrayList<>();
        variantsNames = new ArrayList<>();
        if (products.getVariants() != null && products.getVariants().size() != 0)
            variantList = products.getVariants();
        for (int i = 0; i < variantList.size(); i++) {
            if (!AppUtils.isNullCase(variantList.get(i).getAttribute()))
                variantsNames.add(variantList.get(i).getAttribute());
        }
        if (variantsNames.size() != 0) {
            variantBack.setVisibility(View.VISIBLE);
            ArrayAdapter<String> stateAdapter = new ArrayAdapter<String>(getContext(), R.layout.template_spinner_text, variantsNames);
            stateAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            variants.setAdapter(stateAdapter);
        } else {
            variantBack.setVisibility(View.INVISIBLE);
            pro_mrp.setText(getString(R.string.rs) + products.getVariants().get(0).getFixed_price());
        }
        if (!AppUtils.isNullCase(products.getDescription()))
            about.setText(products.getDescription());

    }

    private void showPicassoImage(String image) {
        AppUtils.generateAndCheckUrl(getContext(), image);
        getPic = null;
        if (!AppUtils.isNullCase(image)) {
            getPic = databaseHandler.getFileUrl(image);
            if (getPic != null) {
                final Picasso picasso = Picasso.with(getActivity().getApplicationContext());
                picasso.setLoggingEnabled(true);
                picasso.load(getPic).into(productImg, new PicassoCallback(image) {

                    @Override
                    public void onErrorPic(URL url) {
                        picasso.load("" + url).error(R.drawable.no_product_image).into(productImg);
                    }
                });

            } else {
                productImg.setImageResource(R.drawable.no_product_image);
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.plus:
                if (pro_qty.getText().toString().equals("")) {
                    pro_qty.setText("1");
                } else {
                    productQty = Integer.parseInt(pro_qty.getText().toString());
                    ++productQty;
                    pro_qty.setText("" + productQty);
                }
                break;
            case R.id.minus:
                if (pro_qty.getText().toString().equals("")) {
                    pro_qty.setText("0");
                } else {
                    productQty = Integer.parseInt(pro_qty.getText().toString());
                    if (productQty == 0) {
                    } else {
                        --productQty;
                    }
                    pro_qty.setText("" + productQty);
                }
                break;
            case R.id.add_to_cart:
                if (AppUtils.haveNetworkConnection(getActivity())) {
                    if (AppUtils.isNullCase(pro_qty.getText().toString())) {
                        AppUtils.showToast(getString(R.string.enter_qty));
                    } else {
                        progressBar.setVisibility(View.VISIBLE);
                        AddToCart contact = new AddToCart();
                        contact.setProductId("" + productsData.getVariants().get(variantSelectedPos).getId());
                        contact.setProductName(productName.getText().toString());
                        contact.setProductMrp(Float.valueOf(productsData.getVariants().get(variantSelectedPos).getFixed_price()));
                        contact.setProductQty(Integer.parseInt(pro_qty.getText().toString()));
                        float a = Integer.parseInt(pro_qty.getText().toString()) * Float.valueOf(productsData.getVariants().get(variantSelectedPos).getFixed_price());
                        contact.setTotalPrice(a);
                        contact.setProductImage(productsData.getImage());
                        contact.setVariant(productsData.getVariants().get(variantSelectedPos).getAttribute());
                        databaseHandler.insertCartData(true, contact);
                        ((MainActivity) activity).showCartCount(true);
                        stopProgressBar();
                    }
                } else {
                    AppUtils.showToast(
                            getString(R.string.no_internet));
                }
                break;
        }
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {
        if (AppUtils.isNullCase(pro_qty.getText().toString()))
            add_to_cart.setBackgroundColor(getActivity().getResources().getColor(R.color.grey));
        else
            add_to_cart.setBackgroundColor(getActivity().getResources().getColor(R.color.greendark));
    }

    public void onPause() {
        super.onPause();
        ((InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE))
                .hideSoftInputFromWindow(pro_qty.getWindowToken(), 0);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        variantSelectedPos = position;
        pro_mrp.setText(getString(R.string.rs) + productsData.getVariants().get(position).getFixed_price());
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private void stopProgressBar() {
        if (progressBar.getVisibility() == View.VISIBLE) {
            Thread thread = new Thread() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                    }

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            AppUtils.showToast(getString(R.string.added_to_cart));
                            progressBar.setVisibility(View.GONE);
                        }
                    });
                }
            };
            thread.start();
        }
    }
}
