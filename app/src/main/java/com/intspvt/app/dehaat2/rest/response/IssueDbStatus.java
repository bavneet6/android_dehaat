package com.intspvt.app.dehaat2.rest.response;

/**
 * Created by DELL on 3/29/2018.
 */

public class IssueDbStatus {

    private int issue_id;
    private int issue_unseen;

    public int getIssue_id() {
        return issue_id;
    }

    public void setIssue_id(int issue_id) {
        this.issue_id = issue_id;
    }

    public int getIssue_unseen() {
        return issue_unseen;
    }

    public void setIssue_unseen(int issue_unseen) {
        this.issue_unseen = issue_unseen;
    }
}
