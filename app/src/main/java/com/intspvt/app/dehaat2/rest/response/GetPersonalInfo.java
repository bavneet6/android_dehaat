package com.intspvt.app.dehaat2.rest.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by DELL on 8/30/2017.
 */

public class GetPersonalInfo {
    @SerializedName("data")
    private GetDehaatiInfo data;

    @SerializedName("message")
    private String message;

    @SerializedName("status")
    private String status;

    public GetDehaatiInfo getData() {
        return data;
    }

    public String getMessage() {
        return message;
    }

    public String getStatus() {
        return status;
    }

    public class GetDehaatiInfo implements Serializable {

        @SerializedName("dehaat_info")
        public DehaatInfo dehaati_info;
        @SerializedName("image_small")
        public String image_small;
        @SerializedName("name")
        public String name;
        @SerializedName("node_mobile")
        public String node_mobile;
        @SerializedName("node_name")
        public String node_name;
        @SerializedName("account_no")
        private String account_no;

        public String getAccount_no() {
            return account_no;
        }

        public DehaatInfo getDehaati_info() {
            return dehaati_info;
        }

        public String getName() {
            return name;
        }

        public String getImage_small() {
            return image_small;
        }

        public String getNode_mobile() {
            return node_mobile;
        }

        public String getNode_name() {
            return node_name;
        }

    }


}
