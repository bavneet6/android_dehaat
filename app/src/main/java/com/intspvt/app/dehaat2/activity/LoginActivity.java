package com.intspvt.app.dehaat2.activity;

import android.os.Bundle;
import android.widget.FrameLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.intspvt.app.dehaat2.R;
import com.intspvt.app.dehaat2.fragments.LoginFragment;
import com.intspvt.app.dehaat2.fragments.SplashScreenFragment;

public class LoginActivity extends AppCompatActivity {
    private FrameLayout frameLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        frameLayout = findViewById(R.id.frag_cont);
        FragmentManager fragManager = getSupportFragmentManager();

        // if bundle is recievd from logout show the number screen
        // else show the into screen
        Bundle getData = getIntent().getExtras();
        if (getData != null) {
            if (getData.getString("logout") != null) {
                Fragment fragment = fragManager.findFragmentByTag(LoginFragment.TAG);
                if (fragment == null)
                    fragment = LoginFragment.newInstance();
                getSupportFragmentManager()
                        .beginTransaction()
                        .add(R.id.frag_cont, fragment,
                                LoginFragment.TAG).commitAllowingStateLoss();
            } else {
                Fragment fragment = fragManager.findFragmentByTag(SplashScreenFragment.TAG);
                if (fragment == null)
                    fragment = SplashScreenFragment.newInstance();
                getSupportFragmentManager()
                        .beginTransaction()
                        .add(R.id.frag_cont, fragment,
                                SplashScreenFragment.TAG).commitAllowingStateLoss();
            }
        } else {
            Fragment fragment = fragManager.findFragmentByTag(SplashScreenFragment.TAG);
            if (fragment == null)
                fragment = SplashScreenFragment.newInstance();
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.frag_cont, fragment,
                            SplashScreenFragment.TAG).commitAllowingStateLoss();

        }

    }
}
