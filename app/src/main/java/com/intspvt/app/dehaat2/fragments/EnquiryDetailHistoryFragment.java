package com.intspvt.app.dehaat2.fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.intspvt.app.dehaat2.R;
import com.intspvt.app.dehaat2.activity.MainActivity;
import com.intspvt.app.dehaat2.adapter.RecyclerAdapterEnquiryImageList;
import com.intspvt.app.dehaat2.adapter.RecyclerAdapterEnquiryReplies;
import com.intspvt.app.dehaat2.database.DatabaseHandler;
import com.intspvt.app.dehaat2.rest.ApiCallback;
import com.intspvt.app.dehaat2.rest.AppRestClient;
import com.intspvt.app.dehaat2.rest.response.EnquiryDataHistory;
import com.intspvt.app.dehaat2.rest.response.EnquiryReply;
import com.intspvt.app.dehaat2.rest.response.IssueDbStatus;
import com.intspvt.app.dehaat2.rest.response.SingleIssueHistory;
import com.intspvt.app.dehaat2.utilities.AppUtils;
import com.intspvt.app.dehaat2.utilities.UrlConstant;

import org.json.JSONException;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by DELL on 3/23/2018.
 */

public class EnquiryDetailHistoryFragment extends BaseFragment implements View.OnClickListener {
    public static final String TAG = EnquiryDetailHistoryFragment.class.getSimpleName();
    private static MediaPlayer mediaPlayer = null;
    private static ImageView previous = null;
    private static int previousImg;
    private static DatabaseHandler databaseHandler;
    private FirebaseAnalytics mFirebaseAnalytics;
    private ArrayList<EnquiryReply> enquiryReply = new ArrayList<>();
    private ArrayList<String> images = new ArrayList<>();
    private ImageView back, play;
    private LinearLayoutManager layoutManager;
    private TextView category, status, id, description, create_date, addMore, callUs, farmer_number, farmer_text, solution;
    private RecyclerView imagesRecy, repliesRecy;
    private int issue_id = 0;
    private String picUrlDb;
    private LinearLayout solutionBack;
    private boolean checkPic;
    private EnquiryDataHistory enquiryDataHistory;

    public static EnquiryDetailHistoryFragment newInstance() {
        return new EnquiryDetailHistoryFragment();
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_single_enquiry, null);
        showActionBar(false);
        ((MainActivity) activity).setTitle("");
        ((MainActivity) activity).showDrawer(false);
        ((MainActivity) activity).showCart(false);
        databaseHandler = new DatabaseHandler(getActivity());
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        Bundle bundle = getArguments();
        if (bundle != null) {
            issue_id = bundle.getInt("ISSUE_ID");
        }
        mediaPlayer = new MediaPlayer();

        back = v.findViewById(R.id.back);
        play = v.findViewById(R.id.play);
        category = v.findViewById(R.id.enCate);
        status = v.findViewById(R.id.enStatus);
        id = v.findViewById(R.id.enId);
        description = v.findViewById(R.id.enHeading);
        solution = v.findViewById(R.id.solution);
        create_date = v.findViewById(R.id.enDate);
        farmer_number = v.findViewById(R.id.farmer_number);
        farmer_text = v.findViewById(R.id.farmer_text);
        farmer_number.setPaintFlags(farmer_number.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        callUs = v.findViewById(R.id.callUs);
        imagesRecy = v.findViewById(R.id.images_rec);
        imagesRecy.setNestedScrollingEnabled(false);
        repliesRecy = v.findViewById(R.id.replies_rec);
        repliesRecy.setNestedScrollingEnabled(false);
        addMore = v.findViewById(R.id.addMore);
        solutionBack = v.findViewById(R.id.solutionBack);
        getSingleEnquiryHistory(issue_id);
        back.setOnClickListener(this);
        play.setOnClickListener(this);
        addMore.setOnClickListener(this);
        farmer_number.setOnClickListener(this);
        callUs.setOnClickListener(this);
        // now a user opens a enquiry with a new reply change its state to false
        IssueDbStatus issueDbStatus = new IssueDbStatus();
        issueDbStatus.setIssue_id(issue_id);
        issueDbStatus.setIssue_unseen(0);
        databaseHandler.insertIssueStatusSeen(issueDbStatus);
        return v;
    }


    private void displayImages() {
        if (images != null) {
            imagesRecy.setVisibility(View.VISIBLE);
            imagesRecy.setAdapter(new RecyclerAdapterEnquiryImageList(images));
            imagesRecy.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        } else {
            imagesRecy.setVisibility(View.GONE);
        }
    }

    private void displayReplies() {
        if (enquiryReply != null) {
            if (enquiryReply.size() != 0) {
                repliesRecy.setVisibility(View.VISIBLE);
                layoutManager = new LinearLayoutManager(getActivity());
                layoutManager.setReverseLayout(true);
                repliesRecy.setAdapter(new RecyclerAdapterEnquiryReplies(enquiryReply));
                repliesRecy.setLayoutManager(layoutManager);
            }

        } else
            repliesRecy.setVisibility(View.GONE);

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id) {
            case R.id.back:
                getActivity().onBackPressed();
                break;
            case R.id.callUs:
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:" + UrlConstant.TOLL_FREE_NUMBER));
                startActivity(intent);

                break;
            case R.id.play:
                if (AppUtils.haveNetworkConnection(getActivity())) {
                    playAudio(play, picUrlDb, R.drawable.play_white, R.drawable.pause_white, enquiryDataHistory.getAudio(), getActivity());
                } else {
                    AppUtils.showToast(
                            getString(R.string.no_internet));
                }
                break;
            case R.id.farmer_number:
                if (!AppUtils.isNullCase(enquiryDataHistory.getFarmer_number())) {
                    Intent intent1 = new Intent(Intent.ACTION_DIAL);
                    intent1.setData(Uri.parse("tel:" + enquiryDataHistory.getFarmer_number()));
                    startActivity(intent1);

                }
                break;
            case R.id.addMore:
                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                Bundle main = new Bundle();
                Bundle bundle = new Bundle();
                bundle.putBoolean("NEWISSUE", false);
                bundle.putString("ID", String.valueOf(enquiryDataHistory.getId()));
                bundle.putString("STATE", String.valueOf(enquiryDataHistory.getStage_id()));
                main.putBundle("B1", bundle);
                EnquiryFormFragment fragment = EnquiryFormFragment.newInstance();
                fragment.setArguments(main);
                transaction.replace(R.id.frag_container, fragment).addToBackStack("").commit();
                break;
        }
    }

    public void playAudio(final ImageView current, String url, final int playImg, final int pauseImg, String imagePath, Context context) {
        if (context == null)
            return;
        // if media player is not playing then set the media player object
        if (!mediaPlayer.isPlaying()) {
            try {
//
                mediaPlayer.reset();
                mediaPlayer.setDataSource(url);
                mediaPlayer.prepare();
                mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                    @Override
                    public void onPrepared(MediaPlayer mp) {
                        current.setImageResource(pauseImg);
                        mediaPlayer.start();
                    }
                });
                mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        current.setImageResource(playImg);
                        mp.seekTo(0);
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
            previousImg = playImg;

        } else {
            //if media player is playing then stop the media player , and set previous image to play
            // and check if previous and current image are not same make cureent play the souund
            mediaPlayer.stop();
            previous.setImageResource(previousImg);
            if (!previous.equals(current)) {
                playAudio(current, url, playImg, pauseImg, imagePath, getActivity());
            }
        }
        previous = current;
    }


    @Override
    public void onPause() {
        super.onPause();
        if (mediaPlayer.isPlaying()) {
            mediaPlayer.stop();
            mediaPlayer.release();
        }
    }


    private void getSingleEnquiryHistory(int issue_id) {
        AppUtils.showProgressDialog(getActivity());
        AppRestClient client = AppRestClient.getInstance();
        Call<SingleIssueHistory> call = client.getSingleHistoryData(issue_id);
        call.enqueue(new ApiCallback<SingleIssueHistory>() {
            @Override
            public void onResponse(Response<SingleIssueHistory> response) {
                AppUtils.hideProgressDialog();

                if (response.body() == null)
                    return;
                if (response.body().getData() == null)
                    return;
                images = response.body().getData().getImages();
                enquiryReply = response.body().getData().getReplies();
                enquiryDataHistory = response.body().getData();
                addMore.setVisibility(View.VISIBLE);
                callUs.setVisibility(View.VISIBLE);
                displayImages();
                displayReplies();
                displayData();

            }

            @Override
            public void onResponse401(Response<SingleIssueHistory> response) throws
                    JSONException {
                AppUtils.showSessionExpiredDialog(getActivity());
            }

        });
    }

    private void displayData() {
        if (!AppUtils.isNullCase(enquiryDataHistory.getStage_id())) {
            status.setText(enquiryDataHistory.getStage_id());
        }
        category.setText(enquiryDataHistory.getCategory());

        if (!AppUtils.isNullCase(enquiryDataHistory.getSolution())) {
            solutionBack.setVisibility(View.VISIBLE);
            solution.setText(enquiryDataHistory.getSolution());
        }
        if (!AppUtils.isNullCase(enquiryDataHistory.getFarmer_number())) {
            farmer_number.setText(enquiryDataHistory.getFarmer_number());
        } else {
            farmer_number.setVisibility(View.GONE);
            farmer_text.setVisibility(View.GONE);
        }
        id.setText(getString(R.string.issue_no) + enquiryDataHistory.getId());
        if (!AppUtils.isNullCase(enquiryDataHistory.getDescription())) {
            description.setText(enquiryDataHistory.getDescription());
        }
        create_date.setText(enquiryDataHistory.getCreate_date());
        if (!AppUtils.isNullCase(enquiryDataHistory.getAudio())) {
            play.setVisibility(View.VISIBLE);
            checkPic = databaseHandler.checkImage(enquiryDataHistory.getAudio());

            if (!checkPic)
                ((MainActivity) activity).generateAndCheckUrl(enquiryDataHistory.getAudio());

            picUrlDb = databaseHandler.getFileUrl(enquiryDataHistory.getAudio());

        } else {
            play.setVisibility(View.GONE);
        }

    }
}