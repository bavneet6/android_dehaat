package com.intspvt.app.dehaat2.rest.response;

/**
 * Created by DELL on 3/20/2018.
 */

public class ProductFileTable {
    private String prod_file, prod_url;


    public String getProd_file() {
        return prod_file;
    }

    public void setProd_file(String prod_file) {
        this.prod_file = prod_file;
    }

    public String getProd_url() {
        return prod_url;
    }

    public void setProd_url(String prod_url) {
        this.prod_url = prod_url;
    }
}
