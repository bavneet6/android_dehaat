package com.intspvt.app.dehaat2.fragments;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.SearchView;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.intspvt.app.dehaat2.R;
import com.intspvt.app.dehaat2.activity.MainActivity;
import com.intspvt.app.dehaat2.adapter.RecyclerAdapterCropManualList;
import com.intspvt.app.dehaat2.rest.ApiCallback;
import com.intspvt.app.dehaat2.rest.AppRestClient;
import com.intspvt.app.dehaat2.rest.response.CropManualData;
import com.intspvt.app.dehaat2.rest.response.CropManuals;
import com.intspvt.app.dehaat2.utilities.AppUtils;

import org.json.JSONException;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Response;

public class CropManualListFragment extends BaseFragment implements SearchView.OnQueryTextListener {
    public static final String TAG = CropManualListFragment.class.getSimpleName();
    private RecyclerView crop_manuals_list;
    private RecyclerAdapterCropManualList recyclerAdapterCropManualList;
    private SearchView search;
    private FirebaseAnalytics firebaseAnalytics;
    private ArrayList<CropManualData> cropManualData = new ArrayList<>();

    public static CropManualListFragment newInstance() {
        return new CropManualListFragment();
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_crop_manual_list, null);
        showActionBar(true);
        ((MainActivity) activity).setTitle(getString(R.string.manual));
        ((MainActivity) activity).showDrawer(true);
        ((MainActivity) activity).showCart(false);
        crop_manuals_list = v.findViewById(R.id.crop_manuals_list);
        search = v.findViewById(R.id.search);
        crop_manuals_list.setNestedScrollingEnabled(false);
        search.setOnQueryTextListener(this);
        firebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        getCropManualsList();
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                search.setIconified(false);
            }
        });
        return v;
    }

    private void getCropManualsList() {

        AppUtils.showProgressDialog(getActivity());
        AppRestClient client = AppRestClient.getInstance();
        Call<CropManuals> call = client.getCropManualList();
        call.enqueue(new ApiCallback<CropManuals>() {
            @Override
            public void onResponse(Response<CropManuals> response) {
                if (getActivity() != null && isAdded()) {
                    AppUtils.hideProgressDialog();

                    if (response.body() == null)
                        return;
                    if (response.body().getCropManual() == null)
                        return;
                    cropManualData = response.body().getCropManual();
                    displayManualData(cropManualData);
                }
            }

            @Override
            public void onResponse401(Response<CropManuals> response) throws
                    JSONException {
                AppUtils.showSessionExpiredDialog(getActivity());
            }


        });

    }

    private void displayManualData(ArrayList<CropManualData> cropManuals) {
        if (getActivity() != null && isAdded()) {
            recyclerAdapterCropManualList = new RecyclerAdapterCropManualList(getActivity(), cropManuals);
            crop_manuals_list.setAdapter(recyclerAdapterCropManualList);
            crop_manuals_list.setLayoutManager(new GridLayoutManager(getActivity(), 3));
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);

    }


    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if (cropManualData.size() != 0) {
            recyclerAdapterCropManualList.getFilter().filter(newText);
        }
        return false;
    }

}
