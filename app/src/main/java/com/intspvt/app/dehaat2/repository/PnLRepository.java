package com.intspvt.app.dehaat2.repository;

import androidx.lifecycle.MutableLiveData;

import com.intspvt.app.dehaat2.Dehaat2;
import com.intspvt.app.dehaat2.R;
import com.intspvt.app.dehaat2.rest.AppRestClient;
import com.intspvt.app.dehaat2.rest.response.PnLModel;
import com.intspvt.app.dehaat2.utilities.AppUtils;

import java.net.ConnectException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PnLRepository {
    private static PnLRepository mInstance;
    private PnLModel.PnL pnLInfos = new PnLModel.PnL();

    public static PnLRepository getInstance() {
        if (mInstance == null)
            mInstance = new PnLRepository();
        return mInstance;
    }

    public MutableLiveData<PnLModel.PnL> getPnL(String fromDate, String toDate) {
        MutableLiveData<PnLModel.PnL> data = new MutableLiveData<>();
        AppRestClient client = AppRestClient.getInstance();
        Call<PnLModel> call;
        if (AppUtils.isNullCase(fromDate))
            call = client.getPnL();
        else
            call = client.getDehaatSaleOrder(fromDate, toDate);
        call.enqueue(new Callback<PnLModel>() {
            @Override
            public void onResponse(Call<PnLModel> call, Response<PnLModel> response) {
                if (response.body() == null)
                    return;
                pnLInfos = null;
                if (response.code() == 200) {
                    if (response.body().getData() != null) {
                        pnLInfos = response.body().getData();
                    }
                } else if (response.code() == 500) {
                    AppUtils.showToast(Dehaat2.getInstance().getApplicationContext().getString(R.string.tech_prob));
                }
                data.setValue(pnLInfos);

            }

            @Override
            public void onFailure(Call<PnLModel> call, Throwable t) {
                if (t instanceof ConnectException) {
                    if (!AppUtils.haveNetworkConnection(Dehaat2.getInstance().getApplicationContext())) {
                        AppUtils.showToast(
                                Dehaat2.getInstance().getApplicationContext().getString(R.string.no_internet));
                    } else {
                        AppUtils.showToast(Dehaat2.getInstance().getApplicationContext().getString(R.string.server_no_respond));

                    }
                }
                data.setValue(null);
            }

        });
        return data;
    }
}
