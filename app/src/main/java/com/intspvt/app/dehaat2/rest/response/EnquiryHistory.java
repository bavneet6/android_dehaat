package com.intspvt.app.dehaat2.rest.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by DELL on 1/11/2018.
 */

public class EnquiryHistory {
    @SerializedName("data")
    private List<EnquiryDataHistory> data;
    @SerializedName("message")
    private String message;
    @SerializedName("status")
    private String status;

    public List<EnquiryDataHistory> getData() {
        return data;
    }

    public String getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }
}
