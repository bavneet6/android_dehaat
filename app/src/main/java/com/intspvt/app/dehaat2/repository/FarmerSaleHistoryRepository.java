package com.intspvt.app.dehaat2.repository;

import androidx.lifecycle.MutableLiveData;

import com.intspvt.app.dehaat2.Dehaat2;
import com.intspvt.app.dehaat2.R;
import com.intspvt.app.dehaat2.rest.AppRestClient;
import com.intspvt.app.dehaat2.rest.response.InputSellingResponse;
import com.intspvt.app.dehaat2.utilities.AppUtils;

import java.net.ConnectException;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FarmerSaleHistoryRepository {

    private static FarmerSaleHistoryRepository mInstance;
    private ArrayList<InputSellingResponse.Dehaat_farmer> orderDataHistories = new ArrayList<>();

    public static FarmerSaleHistoryRepository getInstance() {
        if (mInstance == null)
            mInstance = new FarmerSaleHistoryRepository();
        return mInstance;
    }

    public MutableLiveData<ArrayList<InputSellingResponse.Dehaat_farmer>> getOrderData(String key,
                                                                                       String order_from,
                                                                                       String order_to) {
        MutableLiveData<ArrayList<InputSellingResponse.Dehaat_farmer>> data = new MutableLiveData<>();
        AppRestClient client = AppRestClient.getInstance();
        Call<InputSellingResponse> call = client.dehaatFarmerSaleHistory(key, order_from, order_to);
        call.enqueue(new Callback<InputSellingResponse>() {
            @Override
            public void onResponse(Call<InputSellingResponse> call, Response<InputSellingResponse> response) {
                if (response.body() == null)
                    return;
                orderDataHistories = new ArrayList<>();
                if (response.code() == 200) {
                    if (response.body().getDehaat_farmer() != null) {
                        orderDataHistories = response.body().getDehaat_farmer();
                    }
                } else if (response.code() == 500) {
                    AppUtils.showToast(Dehaat2.getInstance().getApplicationContext().getString(R.string.tech_prob));
                }
                data.setValue(orderDataHistories);

            }

            @Override
            public void onFailure(Call<InputSellingResponse> call, Throwable t) {
                if (t instanceof ConnectException) {
                    if (!AppUtils.haveNetworkConnection(Dehaat2.getInstance().getApplicationContext())) {
                        AppUtils.showToast(
                                Dehaat2.getInstance().getApplicationContext().getString(R.string.no_internet));
                    } else {
                        AppUtils.showToast(Dehaat2.getInstance().getApplicationContext().getString(R.string.server_no_respond));

                    }
                }
                data.setValue(new ArrayList<>());
            }

        });
        return data;
    }
}
