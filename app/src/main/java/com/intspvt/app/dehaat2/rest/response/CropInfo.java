package com.intspvt.app.dehaat2.rest.response;

import com.google.gson.annotations.SerializedName;

public class CropInfo {


    @SerializedName("crop_id")
    private int crop_id;
    @SerializedName("land_unit")
    private String land_unit;
    @SerializedName("cultivated_area")
    private float cultivated_area;


    public void setLand_unit(String land_unit) {
        this.land_unit = land_unit;
    }

    public void setCultivated_area(float cultivated_area) {
        this.cultivated_area = cultivated_area;
    }

    public void setCrop_id(int crop_id) {
        this.crop_id = crop_id;
    }
}
