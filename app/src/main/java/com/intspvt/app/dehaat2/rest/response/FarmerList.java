package com.intspvt.app.dehaat2.rest.response;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by DELL on 11/20/2017.
 */

public class FarmerList {
    @SerializedName("data")
    private GetDehaatiFarmerList data;
    @SerializedName("message")
    private String message;
    @SerializedName("status")
    private String status;

    public GetDehaatiFarmerList getData() {
        return data;
    }

    public String getMessage() {
        return message;
    }

    public String getStatus() {
        return status;
    }

    public class GetDehaatiFarmerList {
        @SerializedName("farmers")
        public ArrayList<FarmerListInfo> farmerListInfos;

        public ArrayList<FarmerListInfo> getFarmerListInfos() {
            return farmerListInfos;
        }

    }
}
