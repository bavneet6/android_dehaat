package com.intspvt.app.dehaat2.adapter;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.intspvt.app.dehaat2.R;
import com.intspvt.app.dehaat2.activity.MainActivity;
import com.intspvt.app.dehaat2.database.DatabaseHandler;
import com.intspvt.app.dehaat2.fragments.ProductInfoFragment;
import com.intspvt.app.dehaat2.rest.PicassoCallback;
import com.intspvt.app.dehaat2.rest.response.AddToCart;
import com.intspvt.app.dehaat2.rest.response.Products;
import com.intspvt.app.dehaat2.utilities.AppUtils;
import com.squareup.picasso.Picasso;

import java.net.URL;
import java.util.ArrayList;

import static com.amazonaws.mobile.auth.core.internal.util.ThreadUtils.runOnUiThread;
import static com.intspvt.app.dehaat2.utilities.AppUtils.isNullCase;

/**
 * Created by DELL on 11/16/2017.
 */

public class RecyclerAdapterProductList extends RecyclerView.Adapter<RecyclerAdapterProductList.ProductList> implements Filterable {
    private ArrayList<Products> products;
    private Context context;
    private ArrayList<Products> productFilterList;
    private DatabaseHandler databaseHandler;
    private int productQty, variantSelectedPos;
    private String getPic;
    private ArrayList<Products.Variants> variants;
    private ArrayList<String> variantsNames = new ArrayList<>();


    public RecyclerAdapterProductList(Context context, ArrayList<Products> products) {
        this.context = context;
        this.products = products;
        this.productFilterList = products;
        databaseHandler = new DatabaseHandler(context);
    }

    @Override
    public RecyclerAdapterProductList.ProductList onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.template_product_list, parent, false);
        context = parent.getContext();
        return new RecyclerAdapterProductList.ProductList(itemView);
    }

    @Override
    public void onBindViewHolder(final RecyclerAdapterProductList.ProductList holder, final int position) {
        holder.setIsRecyclable(false);
        final int pos = holder.getAdapterPosition();
        showPicassoImage(pos, holder.pro_image);
        final String tempName = products.get(pos).getProductHindiName();
        // if hindi name is not present then print the english one
        if (isNullCase(tempName)) {
            holder.pro_name.setText(products.get(pos).getProductName());
        } else {
            holder.pro_name.setText(tempName);
        }

        //display Variants
        variants = new ArrayList<>();
        variantsNames = new ArrayList<>();
        if (products.get(pos).getVariants() != null && products.get(pos).getVariants().size() != 0)
            variants = products.get(pos).getVariants();
        for (int i = 0; i < variants.size(); i++) {
            if (!isNullCase(variants.get(i).getAttribute()))
                variantsNames.add(variants.get(i).getAttribute());
        }
        if (variantsNames.size() != 0) {
            holder.variantBack.setVisibility(View.VISIBLE);
            try {
                ArrayAdapter<String> stateAdapter = new ArrayAdapter<String>(context, R.layout.template_product_spinner_text, variantsNames);
                stateAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                holder.variants.setAdapter(stateAdapter);
            } catch (ActivityNotFoundException ac) {
            }
        } else {
            holder.variantBack.setVisibility(View.INVISIBLE);
            holder.pro_mrp.setText(context.getString(R.string.rs) + " " + products.get(pos).getVariants().get(0).getFixed_price());
        }
        holder.variants.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                variantSelectedPos = position;
                holder.pro_mrp.setText(context.getString(R.string.rs) + " " + products.get(pos).getVariants().get(position).getFixed_price());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        holder.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putLong("PRODUCT_ID", products.get(pos).getProductId());
                ProductInfoFragment fragment = ProductInfoFragment.newInstance();
                fragment.setArguments(bundle);
                AppUtils.changeFragment((FragmentActivity) context, fragment);
            }

        });
        holder.minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (holder.pro_qty.getText().toString().equals("")) {
                    holder.pro_qty.setText("0");
                } else {
                    productQty = Integer.parseInt(holder.pro_qty.getText().toString());
                    if (productQty == 0) {
                    } else {
                        --productQty;
                    }
                    holder.pro_qty.setText("" + productQty);
                }
            }
        });
        holder.plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (holder.pro_qty.getText().toString().equals("")) {
                    holder.pro_qty.setText("1");
                } else {
                    productQty = Integer.parseInt(holder.pro_qty.getText().toString());
                    ++productQty;
                    holder.pro_qty.setText("" + productQty);
                }
            }
        });
        holder.pro_qty.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (isNullCase(holder.pro_qty.getText().toString()))
                    holder.add_to_cart.setBackgroundColor(context.getResources().getColor(R.color.grey));
                else
                    holder.add_to_cart.setBackgroundColor(context.getResources().getColor(R.color.greendark));
            }
        });
        holder.add_to_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isNullCase(holder.pro_qty.getText().toString())) {
                    AppUtils.showToast(context.getString(R.string.enter_qty));
                } else {
                    holder.progressBar.setVisibility(View.VISIBLE);
                    AddToCart contact = new AddToCart();
                    contact.setProductId("" + products.get(pos).getVariants().get(variantSelectedPos).getId());
                    String temp = products.get(pos).getProductHindiName();
                    if (isNullCase(temp))
                        contact.setProductName(products.get(pos).getProductName());
                    else
                        contact.setProductName(temp);
                    contact.setProductMrp(products.get(pos).getVariants().get(variantSelectedPos).getFixed_price());
                    contact.setProductQty(Integer.parseInt(holder.pro_qty.getText().toString()));
                    float a = Integer.parseInt(holder.pro_qty.getText().toString()) * products.get(pos).getVariants().get(variantSelectedPos).getFixed_price();
                    contact.setTotalPrice(a);
                    contact.setProductImage(products.get(pos).getImage());
                    contact.setVariant(products.get(pos).getVariants().get(variantSelectedPos).getAttribute());
                    databaseHandler.insertCartData(true, contact);
                    ((MainActivity) context).showCartCount(true);
                }
                if (holder.progressBar.getVisibility() == View.VISIBLE) {
                    Thread thread = new Thread() {
                        @Override
                        public void run() {
                            try {
                                Thread.sleep(1000);
                            } catch (InterruptedException e) {
                            }

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    AppUtils.showToast(context.getString(R.string.added_to_cart));
                                    holder.progressBar.setVisibility(View.GONE);
                                }
                            });
                        }
                    };
                    thread.start();
                }
            }
        });
    }

    @Override
    public Filter getFilter() {

        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {

                String charString = charSequence.toString();

                if (charString.isEmpty()) {

                    products = productFilterList;
                } else {

                    ArrayList<Products> filteredList = new ArrayList<>();

                    for (Products list : productFilterList)
                        if (((!isNullCase(list.getProductName())) && list.getProductName().toLowerCase().contains(charString.toLowerCase())) || (!isNullCase(list.getProductHindiName()) && list.getProductHindiName().toLowerCase().contains(charString.toLowerCase())))
                            filteredList.add(list);


                    products = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = products;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                products = (ArrayList<Products>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    private void showPicassoImage(int pos, final ImageView imageView) {
        AppUtils.generateAndCheckUrl(context, products.get(pos).getImage());
        String photo = products.get(pos).getImage();
        getPic = null;
        if (!isNullCase(photo)) {
            getPic = databaseHandler.getFileUrl(photo);
            if (getPic != null) {
                final Picasso picasso = Picasso.with(context.getApplicationContext());
                picasso.setLoggingEnabled(true);
                picasso.load(getPic).into(imageView, new PicassoCallback(photo) {

                    @Override
                    public void onErrorPic(URL url) {
                        picasso.load("" + url).error(R.drawable.no_product_image).into(imageView);
                    }
                });

            } else
                imageView.setImageResource(R.drawable.no_product_image);
        }
    }

    @Override
    public int getItemCount() {
        return products.size();
    }


    public class ProductList extends RecyclerView.ViewHolder {
        private TextView pro_name, pro_mrp, add_to_cart;
        private ImageView pro_image, plus, minus;
        private LinearLayout back, variantBack;
        private Spinner variants;
        private EditText pro_qty;
        private ProgressBar progressBar;

        public ProductList(View itemView) {
            super(itemView);
            back = itemView.findViewById(R.id.back);
            variantBack = itemView.findViewById(R.id.variantBack);
            minus = itemView.findViewById(R.id.minus);
            plus = itemView.findViewById(R.id.plus);
            pro_image = itemView.findViewById(R.id.pro_image);
            pro_name = itemView.findViewById(R.id.pro_name);
            pro_mrp = itemView.findViewById(R.id.pro_mrp);
            add_to_cart = itemView.findViewById(R.id.add_to_cart);
            pro_qty = itemView.findViewById(R.id.pro_qty);
            variants = itemView.findViewById(R.id.variants);
            progressBar = itemView.findViewById(R.id.progress_bar);
        }
    }
}
