package com.intspvt.app.dehaat2.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.intspvt.app.dehaat2.R;
import com.intspvt.app.dehaat2.rest.response.SaleLines;

import java.util.ArrayList;

public class RecyclerAdapterSaleLines extends RecyclerView.Adapter<RecyclerAdapterSaleLines.HistoryData> {
    private Context context;
    private ArrayList<SaleLines> saleLines = new ArrayList<>();

    public RecyclerAdapterSaleLines(ArrayList<SaleLines> saleLines) {
        this.saleLines = saleLines;
    }

    @Override
    public RecyclerAdapterSaleLines.HistoryData onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.template_sale_lines, parent, false);
        context = parent.getContext();
        return new RecyclerAdapterSaleLines.HistoryData(itemView);

    }

    @Override
    public void onBindViewHolder(final RecyclerAdapterSaleLines.HistoryData holder, final int position) {
        holder.setIsRecyclable(false);
        holder.pro_name.setText(saleLines.get(holder.getAdapterPosition()).getProduct_name());
        holder.pro_qty.setText("X" + saleLines.get(holder.getAdapterPosition()).getProduct_qty());
        holder.pro_mrp.setText(context.getString(R.string.rs) + saleLines.get(holder.getAdapterPosition()).getProduct_price());
    }


    @Override
    public int getItemCount() {
        return saleLines.size();
    }


    public class HistoryData extends RecyclerView.ViewHolder {
        private TextView pro_name, pro_qty, pro_mrp;


        public HistoryData(View itemView) {
            super(itemView);
            pro_name = itemView.findViewById(R.id.pro_name);
            pro_qty = itemView.findViewById(R.id.pro_qty);
            pro_mrp = itemView.findViewById(R.id.pro_mrp);
        }
    }

}