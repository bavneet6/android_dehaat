package com.intspvt.app.dehaat2.fragments;

import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.intspvt.app.dehaat2.R;
import com.intspvt.app.dehaat2.activity.MainActivity;
import com.intspvt.app.dehaat2.adapter.RecyclerAdapterOutputCredit;
import com.intspvt.app.dehaat2.adapter.RecyclerAdapterStorageAndLoanOrderHistory;
import com.intspvt.app.dehaat2.rest.response.OutputCreditHome;
import com.intspvt.app.dehaat2.rest.response.StorageOrderData;
import com.intspvt.app.dehaat2.utilities.AppUtils;
import com.intspvt.app.dehaat2.viewmodel.StorageAndLoanOrderViewModel;

import java.util.ArrayList;
import java.util.List;


public class StorageAndLoanFragment extends BaseFragment implements View.OnClickListener {
    public static final String TAG = StorageAndLoanFragment.class.getSimpleName();
    private ImageView back, help;
    private FirebaseAnalytics mFirebaseAnalytics;
    private TextView no_record;
    private RecyclerView output_credit_list;
    private LinearLayoutManager creditLinearLayout;
    private ArrayList<OutputCreditHome> outputCreditHomesList = new ArrayList<>();
    private RecyclerAdapterOutputCredit recyclerAdapterOutputCredit;
    private RecyclerView recyclerView;
    private List<StorageOrderData.StorageLoan> storageOrderData = new ArrayList<>();
    private StorageAndLoanOrderViewModel storageAndLoanOrderViewModel;
    private RecyclerAdapterStorageAndLoanOrderHistory recyclerAdapterStorageAndLoanOrderHistory;

    public static StorageAndLoanFragment newInstance() {
        return new StorageAndLoanFragment();
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_storage_and_loan, null);
        showActionBar(false);
        ((MainActivity) activity).showDrawer(false);
        ((MainActivity) activity).showCart(false);
        back = v.findViewById(R.id.back);
        help = v.findViewById(R.id.help);
        recyclerView = v.findViewById(R.id.storage_recycler);
        no_record = v.findViewById(R.id.no_record);
        back.setOnClickListener(this);
        help.setOnClickListener(this);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        AppUtils.showProgressDialog(getActivity());
        storageAndLoanOrderViewModel = ViewModelProviders.of(getActivity()).get(StorageAndLoanOrderViewModel.class);
        storageAndLoanOrderViewModel.init();
        observeViewModel(storageAndLoanOrderViewModel);

        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();

    }

    private void observeViewModel(StorageAndLoanOrderViewModel viewModel) {
        viewModel.getHistoryData()
                .observe(this, manualInfo -> {
                    AppUtils.hideProgressDialog();
                    if (manualInfo != null && manualInfo.size() != 0) {
                        no_record.setVisibility(View.GONE);
                        storageOrderData = manualInfo;
                        displayData(storageOrderData);
                    } else
                        no_record.setVisibility(View.VISIBLE);
                });
    }

    private void displayData(List<StorageOrderData.StorageLoan> storageOrderData) {

        recyclerAdapterStorageAndLoanOrderHistory = new RecyclerAdapterStorageAndLoanOrderHistory(getActivity(), storageOrderData);
        recyclerView.setAdapter(recyclerAdapterStorageAndLoanOrderHistory);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

    }


    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.back:
                getActivity().onBackPressed();
                break;
            case R.id.help:
                showHelpDialog();
                break;

        }
    }

    private void showHelpDialog() {

//        final Dialog dialog = new Dialog(getActivity());
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.setContentView(R.layout.dialog_storage_loan_help_data);
//        dialog.setCanceledOnTouchOutside(true);
//        output_credit_list = dialog.findViewById(R.id.output_credit_list);
//        output_credit_list.setNestedScrollingEnabled(false);
//        prepareOutputCredit();
//        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT);
//        dialog.getWindow().setGravity(Gravity.CENTER);
//        dialog.show();
    }

    private void prepareOutputCredit() {
//        OutputCreditHome outputCreditHome;
//        outputCreditHomesList.clear();
//        outputCreditHome = new OutputCreditHome();
//        outputCreditHome.setImageId(R.drawable.output_water);
//        outputCreditHome.setText1("ख़राब मौसम व बारिश से आपने अनाज को बचायें ");
//        outputCreditHome.setText2("यह मुख्यतः नर्सरी में लगने वाली भयंकर बीमारी है, जिसमें पौधे के जड़ के पास या बीच वाला भाग गल जाता है. जिसके कारण से पौध के ऊपरी…");
//        outputCreditHomesList.add(outputCreditHome);
//        outputCreditHome = new OutputCreditHome();
//        outputCreditHome.setImageId(R.drawable.output_truck);
//        outputCreditHome.setText1("अपने फ़सल का सही वज़न धर्म काँटा पर क़राये");
//        outputCreditHome.setText2("यह मुख्यतः नर्सरी में लगने वाली भयंकर बीमारी है, जिसमें पौधे के जड़ के पास या बीच वाला भाग गल जाता है. जिसके कारण से पौध के ऊपरी…");
//        outputCreditHomesList.add(outputCreditHome);
//        outputCreditHome = new OutputCreditHome();
//        outputCreditHome.setImageId(R.drawable.output_rat);
//        outputCreditHome.setText1("चूहों के आतंक से अपने अनाज को बचायें ");
//        outputCreditHome.setText2("यह मुख्यतः नर्सरी में लगने वाली भयंकर बीमारी है, जिसमें पौधे के जड़ के पास या बीच वाला भाग गल जाता है. जिसके कारण से पौध के ऊपरी…");
//        outputCreditHomesList.add(outputCreditHome);
//        if (getActivity() != null && isAdded()) {
//            output_credit_list.addItemDecoration(new CirclePagerIndicatorDecoration());
//            SnapHelper snapHelper = new PagerSnapHelper();
//            snapHelper.attachToRecyclerView(output_credit_list);
//            creditLinearLayout = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
//            recyclerAdapterOutputCredit = new RecyclerAdapterOutputCredit(getActivity(), outputCreditHomesList);
//            output_credit_list.setAdapter(recyclerAdapterOutputCredit);
//            output_credit_list.setLayoutManager(creditLinearLayout);
//        }

    }
}
