package com.intspvt.app.dehaat2.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.SearchView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.intspvt.app.dehaat2.R;
import com.intspvt.app.dehaat2.adapter.RecyclerAdapterInputSellingHistory;
import com.intspvt.app.dehaat2.rest.ApiCallback;
import com.intspvt.app.dehaat2.rest.AppRestClient;
import com.intspvt.app.dehaat2.rest.response.InputSellingData;
import com.intspvt.app.dehaat2.rest.response.InputSellingResponse;
import com.intspvt.app.dehaat2.utilities.AppUtils;
import com.intspvt.app.dehaat2.utilities.InputSellingUpdate;

import org.json.JSONException;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Response;

import static com.intspvt.app.dehaat2.fragments.InputSellingHistroryFragment.ORDER_LIST;

public class InputSellingStatusHistoryFragment extends BaseFragment implements InputSellingUpdate, SearchView.OnQueryTextListener {
    private TextView no_data_mssg;
    private RecyclerView recyclerView;
    private ArrayList<InputSellingResponse.Dehaat_farmer> dataList = new ArrayList<>();
    private RecyclerAdapterInputSellingHistory recyclerAdapterInputSellingHistory;
    private RecyclerView.LayoutManager layoutManager;
    private FirebaseAnalytics mFirebaseAnalytics;
    private FragmentManager fragmentManager;
    private SearchView searchView;
    private LinearLayout searchBack;

    public InputSellingStatusHistoryFragment() {

    }

    public static InputSellingStatusHistoryFragment newInstance() {
        return new InputSellingStatusHistoryFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_input_status_history, null);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        no_data_mssg = v.findViewById(R.id.no_data_mssg);
        recyclerView = v.findViewById(R.id.order_history_recycler_view);
        recyclerView.setNestedScrollingEnabled(false);
        searchView = v.findViewById(R.id.search);
        searchBack = v.findViewById(R.id.searchBack);

        fragmentManager = getActivity().getSupportFragmentManager();
        Bundle bundle = getArguments();
        ArrayList<InputSellingResponse.Dehaat_farmer> dehaat_farmer = (ArrayList<InputSellingResponse.Dehaat_farmer>) bundle.getSerializable(ORDER_LIST);
        printData(dehaat_farmer);
        searchView.setOnQueryTextListener(this);
        searchView.setOnClickListener(v1 -> searchView.setIconified(false));
        return v;
    }

    private void printData(ArrayList<InputSellingResponse.Dehaat_farmer> dehaat_farmer) {
        dataList = new ArrayList<>();
        searchBack.setVisibility(View.VISIBLE);
        if (dehaat_farmer != null && dehaat_farmer.size() > 0) {
            no_data_mssg.setVisibility(View.GONE);
            dataList = dehaat_farmer;
            recyclerAdapterInputSellingHistory = new RecyclerAdapterInputSellingHistory(getActivity(), dataList, this);
            recyclerView.setAdapter(recyclerAdapterInputSellingHistory);
            layoutManager = new LinearLayoutManager(getActivity());
            recyclerView.setLayoutManager(layoutManager);
        } else {
            no_data_mssg.setVisibility(View.VISIBLE);
        }
    }

    private void callDeHaatFarmerSaleUpdate(Integer pos, String s) {
        AppUtils.showProgressDialog(activity);
        AppRestClient client = AppRestClient.getInstance();
        InputSellingData inputSellingData = new InputSellingData();
        if (s.equals("delivered")) {
            inputSellingData.setInventory_status(dataList.get(pos).getInventory_status());
            inputSellingData.setState(s);
        } else {
            inputSellingData.setInventory_status(s);
            inputSellingData.setState(dataList.get(pos).getState());
        }
        inputSellingData.setFarmer_no(dataList.get(pos).getCustomer_mobile());
        inputSellingData.setSale_lines(dataList.get(pos).getSale_lines());
        inputSellingData.setTotal_amt(dataList.get(pos).getAmount_total());
        inputSellingData.setId(Long.valueOf(dataList.get(pos).getId()));
        inputSellingData.setName(dataList.get(pos).getName());
        com.intspvt.app.dehaat2.rest.body.InputSelling inputSelling = new com.intspvt.app.dehaat2.rest.body.InputSelling(inputSellingData);
        Call<Void> call = client.updateDehaatFarmerSale(inputSelling);
        call.enqueue(new ApiCallback<Void>() {
            @Override
            public void onResponse(Response<Void> response) {
                AppUtils.hideProgressDialog();
                if (response.code() == 200) {
                    ((InputSellingHistroryFragment) getParentFragment()).clear();
                }
            }

            @Override
            public void onResponse401(Response<Void> response) throws JSONException {
                AppUtils.showSessionExpiredDialog(getActivity());
            }
        });

    }

    @Override
    public void onItemClick(Integer pos, String s) {
        callDeHaatFarmerSaleUpdate(pos, s);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if (dataList.size() != 0) {
            recyclerAdapterInputSellingHistory.getFilter().filter(newText);
        }
        return false;
    }
}
