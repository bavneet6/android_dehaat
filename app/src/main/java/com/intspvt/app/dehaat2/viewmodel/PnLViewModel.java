package com.intspvt.app.dehaat2.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.intspvt.app.dehaat2.repository.PnLRepository;
import com.intspvt.app.dehaat2.rest.response.PnLModel;

public class PnLViewModel extends ViewModel {
    private MutableLiveData<PnLModel.PnL> pnLList;
    private PnLRepository pnLRepository;

    public void init(String fromDate, String toDate) {
        if (pnLRepository == null)
            pnLRepository = PnLRepository.getInstance();
        pnLList = pnLRepository.getPnL(fromDate, toDate);
    }

    public LiveData<PnLModel.PnL> getPnL() {
        return pnLList;
    }
}
