package com.intspvt.app.dehaat2.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SectionIndexer;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.intspvt.app.dehaat2.R;
import com.intspvt.app.dehaat2.database.DatabaseHandler;
import com.intspvt.app.dehaat2.rest.PicassoCallback;
import com.intspvt.app.dehaat2.rest.response.InputSelling;
import com.intspvt.app.dehaat2.rest.response.ProductsDehaatFarmerSale;
import com.intspvt.app.dehaat2.utilities.AppUtils;
import com.intspvt.app.dehaat2.utilities.ItemClickListener;
import com.squareup.picasso.Picasso;

import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class RecyclerAdapterInputSelling extends RecyclerView.Adapter<RecyclerAdapterInputSelling.Products> implements SectionIndexer {
    private Context context;
    private DatabaseHandler databaseHandler;
    private ArrayList<ProductsDehaatFarmerSale.Products> products = new ArrayList<>();
    private ArrayList<ProductsDehaatFarmerSale.Products> filteredProducts = new ArrayList<>();
    private String firstLet, getPic;
    private ArrayList<Integer> mSectionPositions;
    private HashMap<Integer, ArrayList<String>> inputSellingSelection = new HashMap<>();
    private ItemClickListener itemClickListener;
    private ArrayList<InputSelling> arrayList = new ArrayList<>();


    public RecyclerAdapterInputSelling(ArrayList<ProductsDehaatFarmerSale.Products> data, ItemClickListener itemClickListener) {
        this.products = data;
        this.itemClickListener = itemClickListener;
        this.filteredProducts = products;
    }

    @Override
    public RecyclerAdapterInputSelling.Products onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.template_input_selling_product, parent, false);
        context = parent.getContext();
        return new RecyclerAdapterInputSelling.Products(itemView);

    }

    @Override
    public void onBindViewHolder(final RecyclerAdapterInputSelling.Products holder, final int position) {
        databaseHandler = new DatabaseHandler(context);
        final int pos = holder.getAdapterPosition();

        holder.setIsRecyclable(false);
        arrayList = databaseHandler.getInputSellingData();
        final ProductsDehaatFarmerSale.Products pro = products.get(position);
        if (arrayList.size() != 0) {
            for (int i = 0; i < arrayList.size(); i++) {
                if (arrayList.get(i).getRequestId().equals("" + pro.getProductId())) {
                    holder.tick.setVisibility(View.VISIBLE);
                    ArrayList<String> arrayListString = new ArrayList<>();
                    arrayListString.add("" + arrayList.get(i).getRequestId());
                    arrayListString.add(arrayList.get(i).getProductName());
                    arrayListString.add("" + arrayList.get(i).getProductMrp());
                    arrayListString.add("" + arrayList.get(i).getProductQty());
                    arrayListString.add(arrayList.get(i).getProductImage());
                    inputSellingSelection.put(pos, arrayListString);
                    itemClickListener.onItemClick(arrayListString);
                }
            }
        }
        // if hindi name is not present then print the english one
        holder.productName.setText(pro.getProductName());
        holder.productMrp.setText(context.getString(R.string.rs) + ":" + pro.getPrice());
        //getPic finds the url from db and if it returns the url then picasso print that
        getPic = null;
        if (!AppUtils.isNullCase(pro.getImage())) {

            getPic = databaseHandler.getFileUrl(pro.getImage());
            if (getPic != null) {
                final Picasso picasso = Picasso.with(context.getApplicationContext());
                picasso.setLoggingEnabled(true);
                picasso.load(getPic).into(holder.productImg, new PicassoCallback(pro.getImage()) {

                    @Override
                    public void onErrorPic(URL url) {
                        picasso.load("" + url).error(R.drawable.no_product_image).into(holder.productImg);
                    }
                });
            }

        }
        holder.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<String> arrayList = new ArrayList<>();
                arrayList.add("" + pro.getProductId());
                arrayList.add(pro.getProductName());
                arrayList.add("" + pro.getPrice());
                if (inputSellingSelection.containsKey(pos)) {
                    holder.tick.setVisibility(View.INVISIBLE);
                    inputSellingSelection.remove(pos);
                    arrayList.add("0");
                } else {
                    holder.tick.setVisibility(View.VISIBLE);
                    inputSellingSelection.put(pos, arrayList);
                    arrayList.add("1");
                }
                arrayList.add(pro.getImage());
                itemClickListener.onItemClick(arrayList);

            }
        });


    }


    public Filter getFilter() {

        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {

                String charString = charSequence.toString();

                if (charString.isEmpty()) {

                    products = filteredProducts;
                } else {

                    ArrayList<ProductsDehaatFarmerSale.Products> filteredList = new ArrayList<>();

                    for (ProductsDehaatFarmerSale.Products list : filteredProducts) {

                        if (list.getProductName().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(list);
                        }
                    }

                    products = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = products;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                products = (ArrayList<ProductsDehaatFarmerSale.Products>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }


    @Override
    public int getItemCount() {
        return products.size();
    }

    @Override
    public Object[] getSections() {
        List<String> sections = new ArrayList<>();
        mSectionPositions = new ArrayList<>();
        for (int i = 0; i < products.size(); i++) {
            String input;
            input = products.get(i).getProductName();
            input = input.replace("\n", "");
            input = input.trim();
            if (AppUtils.isNullCase(input)) {
                firstLet = "#";
            } else if (!String.valueOf(input.charAt(0)).toUpperCase().matches(".*[A-Z].*")) {
                firstLet = "#";
            } else {
                firstLet = String.valueOf(input.charAt(0)).toUpperCase();
            }
            if (!sections.contains(firstLet)) {
                sections.add(firstLet);
                mSectionPositions.add(i);
            }
        }
        return sections.toArray(new String[0]);
    }

    @Override
    public int getPositionForSection(int i) {
        return mSectionPositions.get(i);
    }

    @Override
    public int getSectionForPosition(int i) {
        return 0;
    }


    public class Products extends RecyclerView.ViewHolder {
        private TextView productName, productMrp;
        private ImageView productImg, tick;
        private LinearLayout back;


        public Products(View itemView) {
            super(itemView);
            productName = itemView.findViewById(R.id.product_name);
            productMrp = itemView.findViewById(R.id.product_mrp);
            productImg = itemView.findViewById(R.id.product_image);
            back = itemView.findViewById(R.id.back);
            tick = itemView.findViewById(R.id.tick);
        }
    }

}
