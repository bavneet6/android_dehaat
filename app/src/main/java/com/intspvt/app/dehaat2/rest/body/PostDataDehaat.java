package com.intspvt.app.dehaat2.rest.body;

import com.google.gson.annotations.SerializedName;

public class PostDataDehaat {
    @SerializedName("data")
    public PostDehaat postDehaat;


    public PostDataDehaat(PostDehaat postDehaati) {
        this.postDehaat = postDehaati;
    }

    public PostDehaat getPostDehaati() {
        return postDehaat;
    }

    public void setPostDehaati(PostDehaat postDehaati) {
        this.postDehaat = postDehaati;
    }


    public static class PostDehaat {
        @SerializedName("x_name")
        public String x_name;
        @SerializedName("address_line")
        public String address_line;
        @SerializedName("state_id")
        public int state_id;
        @SerializedName("district_id")
        public int district_id;
        @SerializedName("block_id")
        public int block_id;


        public PostDehaat(String x_name, String address_line, int state_id, int district_id, int block_id) {
            this.x_name = x_name;
            this.address_line = address_line;
            this.state_id = state_id;
            this.block_id = block_id;
            this.district_id = district_id;
        }

        public String getAddress_line() {
            return address_line;
        }

        public void setAddress_line(String address_line) {
            this.address_line = address_line;
        }

        public int getState_id() {
            return state_id;
        }

        public void setState_id(int state_id) {
            this.state_id = state_id;
        }

        public int getDistrict_id() {
            return district_id;
        }

        public void setDistrict_id(int district_id) {
            this.district_id = district_id;
        }

        public int getBlock_id() {
            return block_id;
        }

        public void setBlock_id(int block_id) {
            this.block_id = block_id;
        }

        public String getX_name() {
            return x_name;
        }

        public void setX_name(String x_name) {
            this.x_name = x_name;
        }
    }

}
