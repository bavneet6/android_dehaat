package com.intspvt.app.dehaat2.rest.response;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class InventoryStock {
    @SerializedName("data")
    public ArrayList<Data> data;

    public ArrayList<Data> getData() {
        return data;
    }

    public class Data {
        @SerializedName("name")
        private String name;
        @SerializedName("qty")
        private Float qty;

        public Float getQty() {
            return qty;
        }

        public String getName() {
            return name;
        }
    }
}
