package com.intspvt.app.dehaat2.rest.response;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by DELL on 1/11/2018.
 */

public class EnquiryDataHistory {
    @SerializedName("create_date")
    private String create_date;
    @SerializedName("description")
    private String description;
    @SerializedName("kanban_state")
    private String stage_id;
    @SerializedName("x_audio")
    private String audio;
    @SerializedName("has_audio")
    private boolean has_audio;
    @SerializedName("has_new_replies")
    private boolean has_new_replies;
    @SerializedName("id")
    private int id;
    @SerializedName("category")
    private String category;
    @SerializedName("farmer_number")
    private String farmer_number;
    @SerializedName("has_images")
    private boolean has_images;
    @SerializedName("images")
    private ArrayList<String> images;
    @SerializedName("replies")
    private ArrayList<EnquiryReply> replies;
    @SerializedName("solution")
    private String solution;

    public String getCategory() {
        return category;
    }


    public String getCreate_date() {
        return create_date;
    }

    public String getFarmer_number() {
        return farmer_number;
    }

    public void setFarmer_number(String farmer_number) {
        this.farmer_number = farmer_number;
    }

    public String getDescription() {
        return description;
    }

    public String getStage_id() {
        return stage_id;
    }


    public int getId() {
        return id;
    }

    public String getAudio() {
        return audio;
    }

    public boolean isHas_images() {
        return has_images;
    }

    public boolean isHas_audio() {
        return has_audio;
    }

    public ArrayList<String> getImages() {
        return images;
    }

    public void setImages(ArrayList<String> images) {
        this.images = images;
    }

    public ArrayList<EnquiryReply> getReplies() {
        return replies;
    }

    public boolean isHas_new_replies() {
        return has_new_replies;
    }

    public String getSolution() {
        return solution;
    }

}
