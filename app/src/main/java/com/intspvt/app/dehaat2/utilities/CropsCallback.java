package com.intspvt.app.dehaat2.utilities;

import com.intspvt.app.dehaat2.rest.response.Crops;

import java.util.HashMap;

public interface CropsCallback {

    void onClick(HashMap<Integer, Crops> value);
}
