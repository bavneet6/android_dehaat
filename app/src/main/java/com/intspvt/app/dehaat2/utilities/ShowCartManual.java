package com.intspvt.app.dehaat2.utilities;

import android.app.Activity;

public interface ShowCartManual {
    void showCartManual(Activity activity);
}
