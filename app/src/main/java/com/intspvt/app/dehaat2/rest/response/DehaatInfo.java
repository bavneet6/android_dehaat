package com.intspvt.app.dehaat2.rest.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by DELL on 8/29/2017.
 */

public class DehaatInfo {
    @SerializedName("address_line")
    private String address_line;
    @SerializedName("state_id")
    private List<String> state_id;
    @SerializedName("block_id")
    private List<String> block_id;
    @SerializedName("district_id")
    private List<String> district_id;
    @SerializedName("name")
    private String name;

    public String getName() {
        return name;
    }

    public List<String> getBlock_id() {
        return block_id;
    }

    public List<String> getDistrict_id() {
        return district_id;
    }

    public List<String> getState_id() {
        return state_id;
    }

    public String getAddress_line() {
        return address_line;
    }
}
