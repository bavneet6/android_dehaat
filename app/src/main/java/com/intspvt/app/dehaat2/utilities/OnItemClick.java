package com.intspvt.app.dehaat2.utilities;

import java.util.HashMap;

/**
 * Created by DELL on 8/3/2017.
 */
public interface OnItemClick {
    void onClick(HashMap<Integer, Float> value);
}