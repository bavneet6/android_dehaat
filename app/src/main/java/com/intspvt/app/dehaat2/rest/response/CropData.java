package com.intspvt.app.dehaat2.rest.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by DELL on 11/23/2017.
 */

public class CropData {

    @SerializedName("product_id")
    private int product_id;

    @SerializedName("product_qty")
    private float product_qty;

    public float getProduct_qty() {
        return product_qty;
    }

    public void setProduct_qty(float product_qty) {
        this.product_qty = product_qty;
    }
}
