package com.intspvt.app.dehaat2.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.intspvt.app.dehaat2.R;
import com.intspvt.app.dehaat2.activity.MainActivity;
import com.intspvt.app.dehaat2.utilities.AppPreference;

/**
 * Created by DELL on 11/15/2017.
 */

public class SplashScreenFragment extends BaseFragment {
    public static final String TAG = SplashScreenFragment.class.getSimpleName();
    private final long SPLASH_TIME = 1000;
    private Handler handler;
    private Runnable splash = new Runnable() {
        @Override
        public void run() {
            if (!AppPreference.getInstance().getAPP_LOGIN()) {
                getFragmentManager().beginTransaction().replace(R.id.frag_cont, LoginFragment.newInstance()).commit();
            } else {
                Intent intent = new Intent(getActivity(), MainActivity.class);
                getActivity().startActivity(intent);
                getActivity().finish();
            }
        }
    };

    public static SplashScreenFragment newInstance() {
        return new SplashScreenFragment();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_splash_screen, null);
        handler = new Handler();
        showActionBar(false);
        return v;
    }

    public void onResume() {
        super.onResume();
        handler.postDelayed(splash, SPLASH_TIME);
    }

    public void onPause() {
        super.onPause();
        handler.removeCallbacks(splash);
    }

}
