package com.intspvt.app.dehaat2.fragments;

import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.intspvt.app.dehaat2.R;
import com.intspvt.app.dehaat2.adapter.RecyclerAdapterProductList;
import com.intspvt.app.dehaat2.rest.ApiCallback;
import com.intspvt.app.dehaat2.rest.AppRestClient;
import com.intspvt.app.dehaat2.rest.response.Products;
import com.intspvt.app.dehaat2.rest.response.ProductsList;
import com.intspvt.app.dehaat2.utilities.AppUtils;
import com.intspvt.app.dehaat2.utilities.ShowCartCount;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

public class ManualProductsFragment extends BaseFragment implements ShowCartCount {
    public static final String TAG = ManualProductsFragment.class.getSimpleName();
    private RecyclerView productsView;
    private List<Long> productIds;

    public static ManualProductsFragment newInstance() {
        return new ManualProductsFragment();
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_manual_products, null);
        productsView = v.findViewById(R.id.productsView);
        Bundle bundle = getArguments();
        if (bundle != null) {
            productIds = (List<Long>) bundle.getSerializable("PRODUCT_IDS");
            getRelevantProductList(productIds);
        }

        return v;
    }

    private void getRelevantProductList(List<Long> productIds) {
        String iDs = "";
        for (int i = 0; i < productIds.size(); i++) {
            if (i == productIds.size() - 1)
                iDs = iDs + productIds.get(i);
            else
                iDs = iDs + productIds.get(i) + ",";
        }
        AppRestClient client = AppRestClient.getInstance();
        Call<ProductsList> call = client.productList(iDs);
        call.enqueue(new ApiCallback<ProductsList>() {
            @Override
            public void onResponse(Response<ProductsList> response) {
                if (response.body() == null)
                    return;
                if (response.code() == 200)
                    if (response.body().getProducts() == null)
                        return;
                displayProductList(response.body().getProducts());
            }


            @Override
            public void onResponse401(Response<ProductsList> response) throws JSONException {
                AppUtils.showSessionExpiredDialog(getActivity());

            }


        });
    }

    private void displayProductList(ArrayList<Products> products) {
        if (getActivity() != null && isAdded()) {
            productsView.setLayoutManager(new LinearLayoutManager(getActivity()));
            productsView.setAdapter(new RecyclerAdapterProductList(getActivity(), products));
        }
    }

    @Override
    public void onPause() {
        super.onPause();

    }

    @Override
    public void showCart(boolean showCart) {
        SingleCropManualFragment.newInstance().showCartManual(getActivity());
    }
}
