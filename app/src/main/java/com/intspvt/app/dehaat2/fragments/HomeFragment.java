package com.intspvt.app.dehaat2.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.iid.FirebaseInstanceId;
import com.intspvt.app.dehaat2.R;
import com.intspvt.app.dehaat2.activity.MainActivity;
import com.intspvt.app.dehaat2.adapter.RecyclerAdapterProductsHome;
import com.intspvt.app.dehaat2.adapter.RecyclerAdapterPromotionArticles;
import com.intspvt.app.dehaat2.database.DatabaseHandler;
import com.intspvt.app.dehaat2.rest.ApiCallback;
import com.intspvt.app.dehaat2.rest.AppRestClient;
import com.intspvt.app.dehaat2.rest.response.AddToCart;
import com.intspvt.app.dehaat2.rest.response.FcmToken;
import com.intspvt.app.dehaat2.rest.response.Token;
import com.intspvt.app.dehaat2.rest.response.TopProducts;
import com.intspvt.app.dehaat2.rest.response.Trending;
import com.intspvt.app.dehaat2.utilities.AppPreference;
import com.intspvt.app.dehaat2.utilities.AppUtils;

import org.json.JSONException;

import java.net.ConnectException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by DELL on 11/15/2017.
 */

public class HomeFragment extends BaseFragment implements View.OnClickListener {
    public static final String TAG = HomeFragment.class.getSimpleName();
    private TextView moreProd, dehaati_name, cart_count, dehaati_dehaat;
    private LinearLayout register_farmer, enquiry, sell_input, buy_produce, cart_back, promotion_back,
            ledger_back, storage_loan, dehaat_farmer_back;
    private RecyclerView promotion, product_list;
    private DatabaseHandler databaseHandler;
    private String format2, format1, diffTime;
    private FirebaseAnalytics mFirebaseAnalytics;
    private RecyclerAdapterProductsHome recyclerAdapterProductsHome;
    private ImageView menu;
    private RecyclerAdapterPromotionArticles recyclerAdapterPromotionArticles;
    private boolean checkPic;
    private ProgressBar progressBar;

    public static HomeFragment newInstance() {
        return new HomeFragment();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_home, null);
        showActionBar(false);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        databaseHandler = new DatabaseHandler(getActivity());
        ((MainActivity) activity).showCart(false);
        ((MainActivity) activity).showDrawer(true);
        ((MainActivity) activity).showHomeSelected();
        promotion = v.findViewById(R.id.promotions);
        promotion.setNestedScrollingEnabled(false);
        product_list = v.findViewById(R.id.products);
        product_list.setNestedScrollingEnabled(false);
        moreProd = v.findViewById(R.id.moreProd);
        register_farmer = v.findViewById(R.id.register_farmer);
        enquiry = v.findViewById(R.id.raise_enq);
        sell_input = v.findViewById(R.id.sell_input);
        buy_produce = v.findViewById(R.id.buy_produce);
        promotion_back = v.findViewById(R.id.promotion_back);
        progressBar = v.findViewById(R.id.progress_prod);
        dehaati_name = v.findViewById(R.id.dehaati_name);
        cart_count = v.findViewById(R.id.cart_num);
        cart_back = v.findViewById(R.id.cart_back);
        ledger_back = v.findViewById(R.id.ledger_back);
        storage_loan = v.findViewById(R.id.storage_loan);
        dehaat_farmer_back = v.findViewById(R.id.dehaat_farmer_back);
        menu = v.findViewById(R.id.menu);
        dehaati_dehaat = v.findViewById(R.id.dehaati_dehaat);
        register_farmer.setOnClickListener(this);
        moreProd.setOnClickListener(this);
        enquiry.setOnClickListener(this);
        sell_input.setOnClickListener(this);
        buy_produce.setOnClickListener(this);
        cart_back.setOnClickListener(this);
        storage_loan.setOnClickListener(this);
        ledger_back.setOnClickListener(this);
        menu.setOnClickListener(this);
        dehaat_farmer_back.setOnClickListener(this);
        showDehaat();
        displayText(v);
        sendFCMToken();
        getProductList();
        getPromotionArticles();
        showCartCount(true);
        return v;
    }

    public void showDehaat() {
        if (!AppUtils.isNullCase(AppPreference.getInstance().getDEHAAT_NAME())) {
            dehaati_dehaat.setText(AppPreference.getInstance().getDEHAAT_NAME());
        }
    }

    private void displayText(View v) {
        dehaati_name.setText(AppPreference.getInstance().getDEHAATI_NAME());
    }

    public void showCartCount(boolean blink) {
        if (blink) {
            Animation animBlink = AnimationUtils.loadAnimation(getActivity(),
                    R.anim.blinktext);
            cart_count.setAnimation(animBlink);
        }
        final ArrayList<AddToCart> list;
        list = databaseHandler.getAllCartData();
        cart_count.setText("" + list.size());
    }

    @Override
    public void onPause() {
        super.onPause();
        Bundle params = new Bundle();
        SimpleDateFormat s = new SimpleDateFormat("hh:mm:ss");
        format2 = s.format(new Date());
        diffTime = AppUtils.showTimeDiff(format1, format2, diffTime);
        params.putString("mobile", diffTime + "   " + AppPreference.getInstance().getDEHAATI_MOBILE());
        mFirebaseAnalytics.setUserProperty("Mobile", AppPreference.getInstance().getDEHAATI_MOBILE());
        mFirebaseAnalytics.logEvent("HomePage", params);
        mFirebaseAnalytics.setAnalyticsCollectionEnabled(true);
        Bundle params1 = new Bundle();
        params1.putString(FirebaseAnalytics.Param.ITEM_CATEGORY, "screen");
        params1.putString(FirebaseAnalytics.Param.ITEM_NAME, "Home Screen");
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.VIEW_ITEM, params1);
    }

    @Override
    public void onResume() {
        super.onResume();
        SimpleDateFormat s = new SimpleDateFormat("hh:mm:ss");
        format1 = s.format(new Date());

    }

    /**
     * generate fcm token and send that
     */

    private void sendFCMToken() {
        final String token = FirebaseInstanceId.getInstance().getToken();
        if (token == null)
            return;
        if (!token.equals(AppPreference.getInstance().getFCM_TOKEN())) {
            // Add custom implementation, as needed.
            Token token1 = new Token();
            token1.setFcm_token(token);
            AppRestClient client = AppRestClient.getInstance();
            FcmToken fcmToken = new FcmToken(token1);
            Call<Void> call = client.fcmToken(fcmToken);
            call.enqueue(new Callback<Void>() {
                @Override
                public void onResponse(Call<Void> call, Response<Void> response) {
                    if (response.code() == 200) {
                        AppPreference.getInstance().setFCM_TOKEN(token);
                    }
                }

                @Override
                public void onFailure(Call<Void> call, Throwable t) {

                }
            });
        }

    }

    /**
     * get product list and store the images in db
     */

    private void getProductList() {
        AppRestClient client = AppRestClient.getInstance();
        Call<TopProducts> call = client.productListTop();
        call.enqueue(new Callback<TopProducts>() {
            @Override
            public void onResponse(Call<TopProducts> call, Response<TopProducts> response) {
                if (response.code() == 200) {
                    progressBar.setVisibility(View.GONE);
                    if (response.body() == null)
                        return;
                    if (response.body().getProducts() == null)
                        return;
                    for (int i = 0; i < response.body().getProducts().size(); i++) {
                        if (!AppUtils.isNullCase(response.body().getProducts().get(i).getImage())) {
                            checkPic = databaseHandler.checkImage(response.body().getProducts().get(i).getImage());

                            if (!checkPic)
                                ((MainActivity) activity).generateAndCheckUrl(response.body().getProducts().get(i).getImage());
                        }
                    }
                    recyclerAdapterProductsHome = new RecyclerAdapterProductsHome(response.body().getProducts());
                    product_list.setLayoutManager(new GridLayoutManager(getActivity(), 3));
                    product_list.setAdapter(recyclerAdapterProductsHome);
                } else {
                    progressBar.setVisibility(View.GONE);
                    if (response.code() == 500) {
                        AppUtils.showToast(getString(R.string.tech_prob));
                    } else if (response.code() == 401) {
                        AppUtils.showSessionExpiredDialog(getActivity());
                    } else {
                        AppUtils.showToast(getString(R.string.server_no_respond));
                    }
                }
            }

            @Override
            public void onFailure(Call<TopProducts> call, Throwable t) {
                if (getActivity() == null)
                    return;
                progressBar.setVisibility(View.GONE);
                if (t instanceof ConnectException) {
                    if (!AppUtils.haveNetworkConnection(getActivity())) {
                        AppUtils.showToast(
                                getString(R.string.no_internet));
                    } else {
                        AppUtils.showToast(getString(R.string.server_no_respond));
                    }
                }
            }
        });
    }

    @Override
    public void onClick(View view) {
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        int id = view.getId();
        switch (id) {
            case R.id.menu:
                transaction.replace(R.id.frag_container, MenuFragment.newInstance()).addToBackStack("").commit();
                break;
            case R.id.moreProd:
                transaction.replace(R.id.frag_container, ProductListFragment.newInstance()).addToBackStack("").commit();
                break;
            case R.id.buy_produce:
                transaction.replace(R.id.frag_container, BuyProduceFragment.newInstance()).addToBackStack("").commit();
                break;
            case R.id.sell_input:
                transaction.replace(R.id.frag_container, InputSellingFragment.newInstance()).addToBackStack("").commit();
                break;
            case R.id.register_farmer:
                transaction.replace(R.id.frag_container, FarmerListFragment.newInstance()).addToBackStack("").commit();
                break;
            case R.id.raise_enq:
                transaction.replace(R.id.frag_container, EnquiryFormFragment.newInstance()).addToBackStack("").commit();
                break;
            case R.id.cart_back:
                transaction.replace(R.id.frag_container, CartFragment.newInstance()).addToBackStack("").commit();
                break;
            case R.id.ledger_back:
                transaction.replace(R.id.frag_container, LedgerFragment.newInstance()).addToBackStack("").commit();
                break;
            case R.id.storage_loan:
                transaction.replace(R.id.frag_container, StorageAndLoanFragment.newInstance()).addToBackStack("").commit();
                break;
            case R.id.dehaat_farmer_back:
                transaction.replace(R.id.frag_container, InputSellingHistroryFragment.newInstance()).addToBackStack("InputSellingHistroryFragment").commit();
                break;
        }
    }


    private void getPromotionArticles() {
        AppRestClient client = AppRestClient.getInstance();
        Call<Trending> call = client.promotionArticles();
        call.enqueue(new ApiCallback<Trending>() {
            @Override
            public void onResponse(Response<Trending> response) {
                if (response.body() == null)
                    return;
                if (response.body().getDatas() == null)
                    return;
                promotion_back.setVisibility(View.VISIBLE);

                for (int i = 0; i < response.body().getDatas().size(); i++) {
                    if (!AppUtils.isNullCase(response.body().getDatas().get(i).getImage())) {
                        checkPic = databaseHandler.checkImage(response.body().getDatas().get(i).getImage());

                        if (!checkPic)
                            ((MainActivity) activity).generateAndCheckUrl(response.body().getDatas().get(i).getImage());

                    }
                }
                LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
                recyclerAdapterPromotionArticles = new RecyclerAdapterPromotionArticles(response.body().getDatas());
                promotion.setLayoutManager(layoutManager);
                promotion.setAdapter(recyclerAdapterPromotionArticles);
            }

            @Override
            public void onResponse401(Response<Trending> response) throws JSONException {
                AppUtils.showSessionExpiredDialog(getActivity());
            }


        });

    }

}
