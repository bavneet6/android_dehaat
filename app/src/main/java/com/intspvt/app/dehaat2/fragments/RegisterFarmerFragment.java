package com.intspvt.app.dehaat2.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Switch;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.fxn.pix.Pix;
import com.fxn.utility.PermUtil;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.intspvt.app.dehaat2.R;
import com.intspvt.app.dehaat2.activity.MainActivity;
import com.intspvt.app.dehaat2.adapter.RecyclerAdapterFarmerAddCrops;
import com.intspvt.app.dehaat2.rest.ApiCallback;
import com.intspvt.app.dehaat2.rest.AppRestClient;
import com.intspvt.app.dehaat2.rest.body.PostFarmerPersonalInfo;
import com.intspvt.app.dehaat2.rest.body.RegisterFarmer;
import com.intspvt.app.dehaat2.rest.response.CropInfo;
import com.intspvt.app.dehaat2.rest.response.Crops;
import com.intspvt.app.dehaat2.rest.response.CropsData;
import com.intspvt.app.dehaat2.rest.response.FarmerInfo;
import com.intspvt.app.dehaat2.rest.response.GetAddressMapping;
import com.intspvt.app.dehaat2.rest.response.LandInfo;
import com.intspvt.app.dehaat2.rest.response.LandUnits;
import com.intspvt.app.dehaat2.utilities.AppPreference;
import com.intspvt.app.dehaat2.utilities.AppUtils;
import com.intspvt.app.dehaat2.utilities.CropsCallback;
import com.intspvt.app.dehaat2.utilities.UrlConstant;

import org.json.JSONException;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by DELL on 11/15/2017.
 */

public class RegisterFarmerFragment extends BaseFragment implements View.OnClickListener, AdapterView.OnItemSelectedListener, CropsCallback {
    public static final String TAG = RegisterFarmerFragment.class.getSimpleName();
    ArrayList<String> returnValue = new ArrayList<>();
    private ImageView img_user;
    private Spinner block, district, unit, village, state, panchayat;
    private Switch gender;
    private RecyclerView cropsRecy;
    private ArrayList<CropsData.CropsInfo> cropProductList;
    private RecyclerView.LayoutManager layoutManager;
    private EditText name, mobile, tLand, uLand, mLand, lLand, liLand, loLand;
    private Button register, addCrops;
    private FirebaseAnalytics mFirebaseAnalytics;
    private int stateId, districtId, blockId, villageId, panchayatId, gen, cropCount = 0;
    private String format2, format1, diffTime, farmPic = null;
    private ArrayList<String> distrcitList = new ArrayList<>();
    private ArrayList<String> blockList = new ArrayList<>();
    private ArrayList<String> villageList = new ArrayList<>();
    private ArrayList<String> stateList = new ArrayList<>();
    private ArrayList<String> panchayatList = new ArrayList<>();
    private ArrayList<GetAddressMapping.GetData> districtNames = new ArrayList<>();
    private ArrayList<GetAddressMapping.GetData> blockNames = new ArrayList<>();
    private ArrayList<GetAddressMapping.GetData> stateNames = new ArrayList<>();
    private ArrayList<GetAddressMapping.GetData> villageNames = new ArrayList<>();
    private ArrayList<GetAddressMapping.GetData> panchayatNames = new ArrayList<>();
    private ArrayList<CropInfo> cropInfoList = new ArrayList<>();
    private ArrayList<String> landUnitsNames = new ArrayList<>();
    private ArrayList<File> imageFileList1 = new ArrayList<>();
    private HashMap<Integer, Crops> productHashmap = new HashMap();

    public static RegisterFarmerFragment newInstance() {
        return new RegisterFarmerFragment();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_register_farmer, null);
        showActionBar(true);
        activity.setTitle(R.string.register_farmer_text);
        ((MainActivity) activity).showDrawer(false);
        ((MainActivity) activity).showCart(false);
        img_user = v.findViewById(R.id.img_user);
        block = v.findViewById(R.id.ddl_block);
        district = v.findViewById(R.id.ddl_district);
        unit = v.findViewById(R.id.ddl_unit);
        gender = v.findViewById(R.id.tgl_gender);
        village = v.findViewById(R.id.ddl_village);
        state = v.findViewById(R.id.ddl_state);
        panchayat = v.findViewById(R.id.ddl_panchayat);
        name = v.findViewById(R.id.txt_first_name);
        mobile = v.findViewById(R.id.txt_mobile);
        tLand = v.findViewById(R.id.land_total);
        uLand = v.findViewById(R.id.land_up);
        mLand = v.findViewById(R.id.land_mid);
        lLand = v.findViewById(R.id.land_low);
        liLand = v.findViewById(R.id.land_leased_in);
        loLand = v.findViewById(R.id.land_leased_out);
        register = v.findViewById(R.id.btn_register);
        addCrops = v.findViewById(R.id.addCrops);
        cropsRecy = v.findViewById(R.id.cropsRecy);
        cropsRecy.setNestedScrollingEnabled(false);
        img_user.setOnClickListener(this);
        register.setOnClickListener(this);
        addCrops.setOnClickListener(this);

        district.setOnItemSelectedListener(this);
        block.setOnItemSelectedListener(this);
        village.setOnItemSelectedListener(this);
        state.setOnItemSelectedListener(this);
        panchayat.setOnItemSelectedListener(this);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        if (!AppUtils.haveNetworkConnection(getActivity())) {
            AppUtils.showToast(
                    getString(R.string.no_internet));
        }
        getLandUnits();
        getStateList();
        getCropList();
        return v;
    }

    private void getLandUnits() {
        AppRestClient client = AppRestClient.getInstance();
        Call<LandUnits> call = client.getLandUnits();
        call.enqueue(new ApiCallback<LandUnits>() {
            @Override
            public void onResponse(Response<LandUnits> response) {
                if (response.body() == null)
                    return;
                landUnitsNames = response.body().getLandUnits();
                setLandUnits(landUnitsNames);
            }

            @Override
            public void onResponse401(Response<LandUnits> response) throws
                    JSONException {
                AppUtils.showSessionExpiredDialog(activity);

            }


        });
    }

    private void setLandUnits(ArrayList<String> landUnits) {
        ArrayAdapter<String> stateAdapter = new ArrayAdapter<String>(getActivity(), R.layout.template_spinner_text, landUnits);
        stateAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        unit.setAdapter(stateAdapter);
    }

    private void getCropList() {
        AppUtils.showProgressDialog(getActivity());
        AppRestClient client = AppRestClient.getInstance();
        Call<CropsData> call = client.getCropList();
        call.enqueue(new ApiCallback<CropsData>() {

            @Override
            public void onResponse(Response<CropsData> response) throws JSONException {
                AppUtils.hideProgressDialog();
                if (response.body() == null)
                    return;
                if (response.body().getCropsInfo() == null)
                    return;
                cropProductList = response.body().getCropsInfo();
                displayCropData();


            }

            @Override
            public void onResponse401(Response<CropsData> response) throws JSONException {

            }


        });
    }

    private void displayCropData() {
        Crops crops = new Crops();
        crops.setCropId(null);
        crops.setCropArea("0.0");
        crops.setCropUnit(null);
        crops.setCropDel(false);
        productHashmap.put(cropCount, crops);
        RecyclerAdapterFarmerAddCrops recyclerAdapterFarmerAddCrops = new RecyclerAdapterFarmerAddCrops(cropProductList, productHashmap, this, landUnitsNames);
        cropsRecy.setAdapter(recyclerAdapterFarmerAddCrops);
        layoutManager = new LinearLayoutManager(getActivity());
        cropsRecy.setLayoutManager(layoutManager);

        ++cropCount;
    }

    private void getStateList() {
        stateList.clear();
        AppUtils.showProgressDialog(getActivity());
        AppRestClient client = AppRestClient.getInstance();
        Call<GetAddressMapping> call = client.getStateRegister();
        call.enqueue(new ApiCallback<GetAddressMapping>() {

            @Override
            public void onResponse(Response<GetAddressMapping> response) throws JSONException {
                AppUtils.hideProgressDialog();
                if (response.body() == null)
                    return;
                if (response.body().getGetAddress() == null)
                    return;
                GetAddressMapping.GetData getData = new GetAddressMapping.GetData();
                getData.setId(0);
                getData.setName(UrlConstant.SELECT);
                stateNames = response.body().getGetAddress();
                stateNames.add(0, getData);
                for (int i = 0; i < stateNames.size(); i++) {
                    stateList.add(stateNames.get(i).getName());
                }
                if (getActivity() == null)
                    return;
                ArrayAdapter<String> stateAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, stateList);
                stateAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                state.setAdapter(stateAdapter);
                if (AppPreference.getInstance().getDEHAAT_STATE().size() != 0) {
                    int spinnerPosition = stateAdapter.getPosition(AppPreference.getInstance().getDEHAAT_STATE().get(1));
                    state.setSelection(spinnerPosition);
                }
            }

            @Override
            public void onResponse401(Response<GetAddressMapping> response) throws JSONException {

            }


        });
    }

    private void getDistrictList(final int stateId) {
        distrcitList.clear();
        AppRestClient client = AppRestClient.getInstance();
        Call<GetAddressMapping> call = client.getDistrictRegister(stateId);
        call.enqueue(new ApiCallback<GetAddressMapping>() {

            @Override
            public void onResponse(Response<GetAddressMapping> response) throws JSONException {
                if (response.body() == null)
                    return;
                if (response.body().getGetAddress() == null)
                    return;
                GetAddressMapping.GetData getData = new GetAddressMapping.GetData();
                getData.setId(0);
                getData.setName(UrlConstant.SELECT);
                districtNames = response.body().getGetAddress();
                districtNames.add(0, getData);
                for (int i = 0; i < districtNames.size(); i++) {
                    distrcitList.add(districtNames.get(i).getName());
                }
                if (getActivity() == null)
                    return;
                ArrayAdapter<String> districtAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, distrcitList);
                districtAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                district.setAdapter(districtAdapter);
                if (AppPreference.getInstance().getDEHAAT_DISTRICT().size() != 0) {
                    if (AppPreference.getInstance().getDEHAAT_STATE().get(0).equals("" + stateId)) {
                        int spinnerPosition = districtAdapter.getPosition(AppPreference.getInstance().getDEHAAT_DISTRICT().get(1));
                        district.setSelection(spinnerPosition);
                    }
                }
            }

            @Override
            public void onResponse401(Response<GetAddressMapping> response) throws JSONException {

            }


        });
    }

    private void getBlockList(final int districtId) {
        blockList.clear();
        AppUtils.showProgressDialog(getActivity());
        AppRestClient client = AppRestClient.getInstance();
        Call<GetAddressMapping> call = client.getBlockRegister(districtId);
        call.enqueue(new ApiCallback<GetAddressMapping>() {

            @Override
            public void onResponse(Response<GetAddressMapping> response) throws JSONException {
                AppUtils.hideProgressDialog();
                if (response.body() == null)
                    return;
                if (response.body().getGetAddress() == null)
                    return;
                GetAddressMapping.GetData getData = new GetAddressMapping.GetData();
                getData.setId(0);
                getData.setName(UrlConstant.SELECT);
                blockNames = response.body().getGetAddress();
                blockNames.add(0, getData);
                for (int i = 0; i < blockNames.size(); i++) {
                    blockList.add(blockNames.get(i).getName());
                }
                if (getActivity() == null)
                    return;
                ArrayAdapter<String> blockAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, blockList);
                blockAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                block.setAdapter(blockAdapter);
                if (AppPreference.getInstance().getDEHAAT_BLOCK().size() != 0) {
                    if (AppPreference.getInstance().getDEHAAT_DISTRICT().get(0).equals("" + districtId)) {
                        int spinnerPosition = blockAdapter.getPosition(AppPreference.getInstance().getDEHAAT_BLOCK().get(1));
                        block.setSelection(spinnerPosition);
                    }
                }
            }

            @Override
            public void onResponse401(Response<GetAddressMapping> response) throws
                    JSONException {

            }


        });
    }

    private void getPanchayatList(int panchayatId) {
        panchayatList.clear();
        AppUtils.showProgressDialog(getActivity());
        AppRestClient client = AppRestClient.getInstance();
        Call<GetAddressMapping> call = client.getPanchayatRegister(panchayatId);
        call.enqueue(new ApiCallback<GetAddressMapping>() {

            @Override
            public void onResponse(Response<GetAddressMapping> response) throws JSONException {
                AppUtils.hideProgressDialog();
                if (response.body() == null)
                    return;
                if (response.body().getGetAddress() == null)
                    return;
                GetAddressMapping.GetData getData = new GetAddressMapping.GetData();
                getData.setId(0);
                getData.setName(UrlConstant.SELECT);
                panchayatNames = response.body().getGetAddress();
                panchayatNames.add(0, getData);
                for (int i = 0; i < panchayatNames.size(); i++) {
                    panchayatList.add(panchayatNames.get(i).getName());
                }
                if (getActivity() == null)
                    return;
                ArrayAdapter<String> panchayatAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, panchayatList);
                panchayatAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                panchayat.setAdapter(panchayatAdapter);
            }

            @Override
            public void onResponse401(Response<GetAddressMapping> response) throws JSONException {

            }


        });
    }

    private void getVillageList(int blockId) {
        villageList.clear();
        AppRestClient client = AppRestClient.getInstance();
        Call<GetAddressMapping> call = client.getVillageRegister(blockId);
        call.enqueue(new ApiCallback<GetAddressMapping>() {

            @Override
            public void onResponse(Response<GetAddressMapping> response) throws JSONException {
                if (response.body() == null)
                    return;
                if (response.body().getGetAddress() == null)
                    return;
                GetAddressMapping.GetData getData = new GetAddressMapping.GetData();
                getData.setId(0);
                getData.setName(UrlConstant.SELECT);
                villageNames = response.body().getGetAddress();
                villageNames.add(0, getData);
                for (int i = 0; i < villageNames.size(); i++) {
                    villageList.add(villageNames.get(i).getName());
                }
                if (getActivity() == null)
                    return;
                ArrayAdapter<String> villageAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, villageList);
                villageAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                village.setAdapter(villageAdapter);
            }

            @Override
            public void onResponse401(Response<GetAddressMapping> response) throws JSONException {

            }


        });
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.img_user:
                AppUtils.imageSelection(this, 1);
                break;
            case R.id.back:
                getActivity().onBackPressed();
                break;
            case R.id.addCrops:
                displayCropData();
                break;
            case R.id.btn_register:
                if (gender.isChecked() == true) {
                    gen = 7;
                } else {
                    gen = 6;
                }
                if (name.getText().toString().equals("")) {
                    name.setError(getString(R.string.name_req));
                    name.requestFocus();
                } else if (mobile.getText().toString().length() != 10) {
                    mobile.setError(getString(R.string.mobile_req));
                    mobile.requestFocus();
                } else {
                    if ((stateId != 0) && (districtId != 0) && (blockId != 0)) {
                        AppUtils.showProgressDialog(getActivity());
                        sendFarmerData();
                    } else {
                        AppUtils.showToast(getString(R.string.completeFarmerAdd));
                    }
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.e("val", "requestCode ->  " + requestCode + "  resultCode " + resultCode);
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case (100): {
                if (resultCode == Activity.RESULT_OK) {
                    returnValue = data.getStringArrayListExtra(Pix.IMAGE_RESULTS);
                    storeImagesInList(returnValue);
                }
            }
            break;
        }
    }

    private void storeImagesInList(ArrayList<String> returnValue) {
        imageFileList1.clear();
        for (int i = 0; i < returnValue.size(); i++) {
            File file = new File(returnValue.get(i));
            imageFileList1.add(file);
        }
        Bitmap bitmap = new BitmapDrawable(getActivity().getResources(), imageFileList1.get(0).getAbsolutePath()).getBitmap();
        img_user.setImageBitmap(bitmap);
        farmPic = AppPreference.encodeTobase64(bitmap);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case PermUtil.REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    AppUtils.imageSelection(this, 1);
                } else {
                }
                return;
            }
        }

    }

    private void sendFarmerData() {
        cropInfoList.clear();
        for (Map.Entry<Integer, Crops> entry : productHashmap.entrySet()) {
            CropInfo cropInfo = new CropInfo();
            cropInfo.setCrop_id(Integer.parseInt(entry.getValue().getCropId()));
            if (AppUtils.isNullCase(entry.getValue().getCropArea()))
                cropInfo.setCultivated_area(0f);
            else
                cropInfo.setCultivated_area(Float.parseFloat(entry.getValue().getCropArea()));
            cropInfo.setLand_unit(entry.getValue().getCropUnit());
            cropInfoList.add(cropInfo);
        }

        if (tLand.getText().toString().equals("")) {
            tLand.setText("0");
        }
        if (uLand.getText().toString().equals("")) {
            uLand.setText("0");
        }
        if (mLand.getText().toString().equals("")) {
            mLand.setText("0");
        }
        if (lLand.getText().toString().equals("")) {
            lLand.setText("0");
        }
        if (liLand.getText().toString().equals("")) {
            liLand.setText("0");
        }
        if (loLand.getText().toString().equals("")) {
            loLand.setText("0");
        }
        AppRestClient client = AppRestClient.getInstance();
        FarmerInfo farmerInfo = new FarmerInfo(stateId, districtId, blockId, panchayatId, villageId, name.getText().toString(), mobile.getText().toString(), farmPic, gen);
        LandInfo landInfo = new LandInfo(unit.getSelectedItem().toString(), Float.valueOf(tLand.getText().toString()), Float.valueOf(uLand.getText().toString()), Float.valueOf(lLand.getText().toString()),
                Float.valueOf(mLand.getText().toString()), Float.valueOf(liLand.getText().toString()), Float.valueOf(loLand.getText().toString()));
        RegisterFarmer getInfo = new RegisterFarmer(landInfo, farmerInfo, cropInfoList);
        PostFarmerPersonalInfo postFarmerPersonalInfo = new PostFarmerPersonalInfo(getInfo);
        Call<PostFarmerPersonalInfo> call = client.registerFarmer(postFarmerPersonalInfo);
        call.enqueue(new ApiCallback<PostFarmerPersonalInfo>() {

            @Override
            public void onResponse(Response<PostFarmerPersonalInfo> response) {
                AppUtils.hideProgressDialog();
                if (response.code() == 200) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setTitle(getActivity().getString(R.string.information));
                    builder.setMessage(getString(R.string.farmerAdded));
                    builder.setCancelable(false);
                    builder.setPositiveButton(getActivity().getString(R.string.okText), new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            getActivity().onBackPressed();
                        }
                    });
                    AlertDialog alert = builder.create();
                    alert.show();

                    mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
                    Bundle params = new Bundle();
                    SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                    String format = s.format(new Date());
                    mFirebaseAnalytics.setUserProperty("Mobile", AppPreference.getInstance().getDEHAATI_MOBILE());
                    params.putString("farmerRegistered", format + "   " + AppPreference.getInstance().getDEHAATI_MOBILE());
                    mFirebaseAnalytics.logEvent("farmerRegistered", params);
                } else if (response.code() == 202) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setTitle(getActivity().getString(R.string.information));
                    builder.setMessage(getActivity().getString(R.string.farmerDupLicateAdded));
                    builder.setCancelable(false);
                    builder.setPositiveButton(getActivity().getString(R.string.okText), new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            mobile.requestFocus();
                            mobile.setError("");
                        }
                    });
                    AlertDialog alert = builder.create();
                    alert.show();
                }
            }

            @Override
            public void onResponse401(Response<PostFarmerPersonalInfo> response) throws JSONException {

            }

        });


    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View v, int position,
                               long id) {
        String label = parent.getItemAtPosition(position).toString();
        switch (parent.getId()) {

            case R.id.ddl_state:
                stateId = stateNames.get(position).getId();
                if (stateId != 0)
                    getDistrictList(stateId);
                break;

            case R.id.ddl_district:
                districtId = districtNames.get(position).getId();
                if (districtId != 0)
                    getBlockList(districtId);
                break;
            case R.id.ddl_block:
                blockId = blockNames.get(position).getId();
                if (blockId != 0)
                    getPanchayatList(blockId);
                break;
            case R.id.ddl_panchayat:
                panchayatId = panchayatNames.get(position).getId();
                if (panchayatId != 0)
                    getVillageList(panchayatId);
                break;
            case R.id.ddl_village:
                villageId = villageNames.get(position).getId();
                break;
        }
    }


    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    @Override
    public void onResume() {
        super.onResume();
        SimpleDateFormat s = new SimpleDateFormat("hh:mm:ss");
        format1 = s.format(new Date());
    }

    public void onPause() {
        super.onPause();
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
        Bundle params = new Bundle();
        SimpleDateFormat s = new SimpleDateFormat("hh:mm:ss");
        format2 = s.format(new Date());
        diffTime = AppUtils.showTimeDiff(format1, format2, diffTime);
        params.putString("RegisterFarmer", diffTime + "   " + AppPreference.getInstance().getDEHAATI_MOBILE());
        mFirebaseAnalytics.setUserProperty("Mobile", AppPreference.getInstance().getDEHAATI_MOBILE());
        mFirebaseAnalytics.logEvent("RegisterFarmer", params);
    }

    @Override
    public void onClick(HashMap<Integer, Crops> value) {
        productHashmap.putAll(value);
    }
}
