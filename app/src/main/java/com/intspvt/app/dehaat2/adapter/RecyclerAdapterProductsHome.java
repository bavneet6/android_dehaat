package com.intspvt.app.dehaat2.adapter;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.intspvt.app.dehaat2.R;
import com.intspvt.app.dehaat2.database.DatabaseHandler;
import com.intspvt.app.dehaat2.fragments.ProductInfoFragment;
import com.intspvt.app.dehaat2.rest.PicassoCallback;
import com.intspvt.app.dehaat2.rest.response.AddToCart;
import com.intspvt.app.dehaat2.rest.response.TopProducts;
import com.intspvt.app.dehaat2.utilities.AppUtils;
import com.squareup.picasso.Picasso;

import java.net.URL;
import java.util.ArrayList;

public class RecyclerAdapterProductsHome extends RecyclerView.Adapter<RecyclerAdapterProductsHome.Products> {
    private Context context;
    private DatabaseHandler databaseHandler;
    private String getPic;
    private ArrayList<String> productList = new ArrayList<>();
    private ArrayList<TopProducts.Products> products = new ArrayList<>();
    private ArrayList<AddToCart> addToCarts = new ArrayList<>();

    public RecyclerAdapterProductsHome(ArrayList<TopProducts.Products> products) {
        this.products = products;
    }

    @Override
    public RecyclerAdapterProductsHome.Products onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.template_product_home, parent, false);
        context = parent.getContext();
        return new RecyclerAdapterProductsHome.Products(itemView);

    }

    @Override
    public void onBindViewHolder(final RecyclerAdapterProductsHome.Products holder, final int position) {
        final int pos = holder.getAdapterPosition();
        databaseHandler = new DatabaseHandler(context);
        addToCarts = databaseHandler.getAllCartData();
        // if hindi name is not present then print the english one
        holder.productName.setText(products.get(pos).getProductName());
        for (int currentpos = 0; currentpos < addToCarts.size(); currentpos++) {
            // display all the user crop list highlighted in the product list
            if (addToCarts.get(currentpos).getProductId().equals(String.valueOf(products.get(pos).getProductId()))) {
                holder.value.setText("" + addToCarts.get(currentpos).getProductQty());
            }
        }
        //getPic finds the url from db and if it returns the url then picasso print that
        getPic = null;
        if (!AppUtils.isNullCase(products.get(pos).getImage())) {

            getPic = databaseHandler.getFileUrl(products.get(pos).getImage());
            if (getPic != null) {
                final Picasso picasso = Picasso.with(context.getApplicationContext());
                picasso.setLoggingEnabled(true);
                picasso.load(getPic).into(holder.productImg, new PicassoCallback(products.get(pos).getImage()) {

                    @Override
                    public void onErrorPic(URL url) {
                        picasso.load("" + url).error(R.drawable.no_product_image).into(holder.productImg);
                    }
                });
            }

        }
        holder.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putLong("PRODUCT_ID", Long.parseLong(products.get(pos).getProduct_tmpl_id().get(0)));
                ProductInfoFragment fragment = ProductInfoFragment.newInstance();
                fragment.setArguments(bundle);
                AppUtils.changeFragment((FragmentActivity) context, fragment);
            }
        });
    }


    @Override
    public int getItemCount() {
        return products.size();
    }

    public class Products extends RecyclerView.ViewHolder {
        private TextView productName, value;
        private ImageView productImg;
        private LinearLayout back;


        public Products(View itemView) {
            super(itemView);
            productName = itemView.findViewById(R.id.product_name);
            value = itemView.findViewById(R.id.value);
            productImg = itemView.findViewById(R.id.product_image);
            back = itemView.findViewById(R.id.back);
        }
    }

}
