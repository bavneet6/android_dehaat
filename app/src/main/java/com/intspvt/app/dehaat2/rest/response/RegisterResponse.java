package com.intspvt.app.dehaat2.rest.response;

import com.google.gson.annotations.SerializedName;

public class RegisterResponse {
    @SerializedName("data")
    private GetId data;

    @SerializedName("message")
    private String message;
    @SerializedName("status")
    private String status;

    public GetId getData() {
        return data;
    }

    public void setData(GetId data) {
        this.data = data;
    }

    public class GetId {
        @SerializedName("id")
        public int id;


        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }
    }

}
