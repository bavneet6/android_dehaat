package com.intspvt.app.dehaat2.fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chauthai.swipereveallayout.SwipeRevealLayout;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.intspvt.app.dehaat2.R;
import com.intspvt.app.dehaat2.activity.MainActivity;
import com.intspvt.app.dehaat2.adapter.RecyclerAdapterCart;
import com.intspvt.app.dehaat2.database.DatabaseHandler;
import com.intspvt.app.dehaat2.rest.ApiCallback;
import com.intspvt.app.dehaat2.rest.AppRestClient;
import com.intspvt.app.dehaat2.rest.body.SendCartData;
import com.intspvt.app.dehaat2.rest.body.SendCartDataMain;
import com.intspvt.app.dehaat2.rest.response.AddToCart;
import com.intspvt.app.dehaat2.rest.response.GetOrderResponse;
import com.intspvt.app.dehaat2.rest.response.OrderData;
import com.intspvt.app.dehaat2.utilities.AppPreference;
import com.intspvt.app.dehaat2.utilities.AppUtils;
import com.intspvt.app.dehaat2.utilities.OnItemClick;
import com.intspvt.app.dehaat2.utilities.UrlConstant;

import org.json.JSONException;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import io.supercharge.shimmerlayout.ShimmerLayout;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by DELL on 11/16/2017.
 */

public class CartFragment extends BaseFragment implements OnItemClick {
    public static final String TAG = CartFragment.class.getSimpleName();
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private ImageView buy;
    private TextView total_price, totalItem;
    private float total_amt;
    private String productNames;
    private FirebaseAnalytics mFirebaseAnalytics;
    private LinearLayout bottomBuy, item, noCartItem;
    private String number;
    private DatabaseHandler sQLiteHelper;
    private List<OrderData> sendCartList = new ArrayList<>();
    private ArrayList<AddToCart> list;
    private SwipeRevealLayout swipeRevealLayout;
    private ShimmerLayout shimmerText;
    private String format2, format1, diffTime;

    public static CartFragment newInstance() {
        return new CartFragment();
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_cart, null);
        showActionBar(true);
        ((MainActivity) activity).showCart(false);
        ((MainActivity) activity).showDrawer(false);
        bottomBuy = v.findViewById(R.id.bottom_buy);
        item = v.findViewById(R.id.item);
        noCartItem = v.findViewById(R.id.tree);
        buy = v.findViewById(R.id.buy);
        recyclerView = v.findViewById(R.id.list_recyl);
        recyclerView.setNestedScrollingEnabled(false);
        shimmerText = v.findViewById(R.id.shimmer_text);
        sQLiteHelper = new DatabaseHandler(this.getActivity());
        // list returns all the data that are in the cart
        list = sQLiteHelper.getAllCartData();
        ((MainActivity) activity).setTitle(getString(R.string.dehaat_bag));
        showBottomBanner();
        total_price = v.findViewById(R.id.price);
        totalItem = v.findViewById(R.id.totalItem);
        buy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (AppUtils.haveNetworkConnection(getActivity())) {
                    showNotesDialog();
                } else {
                    AppUtils.showToast(
                            getString(R.string.no_internet));
                }
                ((MainActivity) activity).showCartCount(false);
            }
        });
        recyclerView.setAdapter(new RecyclerAdapterCart(list, this));
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        printPriceAndItem();
        return v;
    }

    private void printPriceAndItem() {
        total_amt = 0;
        for (int i = 0; i < list.size(); i++) {
            total_amt = total_amt + list.get(i).getTotalPrice();
        }
        total_price.setText("   " + getString(R.string.total_amount) + getString(R.string.rs) + total_amt);
        totalItem.setText("   " + getString(R.string.total_items) + list.size());
    }


    private void showNotesDialog() {
        final Dialog dialog = new Dialog(getActivity());
        final EditText notesText;
        Button saveNotes, skip;
        dialog.setCanceledOnTouchOutside(true);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_notes_cart);
        notesText = dialog.findViewById(R.id.notesText);
        saveNotes = dialog.findViewById(R.id.saveNotes);
        skip = dialog.findViewById(R.id.skip);
        if (!AppUtils.isNullCase(AppPreference.getInstance().getNOTES())) {
            notesText.setText(AppPreference.getInstance().getNOTES());
        }
        saveNotes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (notesText.getText().toString().equals("")) {
                    notesText.setError("");
                    notesText.requestFocus();
                } else {
                    dialog.dismiss();
                    AppPreference.getInstance().setNOTES(notesText.getText().toString());
                    showReviewDialog(notesText.getText().toString());
                }
            }
        });
        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                showReviewDialog(null);
            }
        });
        Drawable d = new ColorDrawable(getActivity().getResources().getColor(R.color.greytext));
        d.setAlpha(100);
        dialog.getWindow().setBackgroundDrawable(d);
        dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.show();

    }

    private void showBottomBanner() {
        if (list.size() == 0) {
            shimmerText.setVisibility(View.GONE);
            shimmerText.stopShimmerAnimation();
            bottomBuy.setVisibility(View.GONE);
            item.setVisibility(View.GONE);
            recyclerView.setVisibility(View.GONE);
            noCartItem.setVisibility(View.VISIBLE);

        } else {
            shimmerText.setVisibility(View.VISIBLE);
            shimmerText.startShimmerAnimation();
            bottomBuy.setVisibility(View.VISIBLE);
            item.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.VISIBLE);
            noCartItem.setVisibility(View.GONE);
        }
    }

    /**
     * method for sending the cart data i.e. order products
     *
     * @param notesdata
     */
    private void sendData(String notesdata) {
        AppUtils.showProgressDialog(activity);
        sendCartList.clear();
        productNames = "";
        list = sQLiteHelper.getAllCartData();
        if (list.isEmpty()) {
        } else {
            for (int i = 0; i < list.size(); i++) {
                OrderData orderData = new OrderData();
                orderData.setProduct_id(Integer.parseInt(list.get(i).getProductId()));
                orderData.setProduct_uom_qty(list.get(i).getProductQty());
                orderData.setProduct_price(list.get(i).getProductMrp());
                sendCartList.add(orderData);
                if (productNames.equals("")) {
                    productNames = list.get(i).getProductName();
                } else {
                    productNames = productNames + " , " + list.get(i).getProductName();
                }
                //sendCartList contains the cart data that is to be send
            }
        }
        AppRestClient client = AppRestClient.getInstance();
        SendCartData sendCartData = new SendCartData(sendCartList, notesdata);
        SendCartDataMain sendCartDataMain = new SendCartDataMain(sendCartData);
        Call<GetOrderResponse> call = client.sendCartData(sendCartDataMain);
        call.enqueue(new ApiCallback<GetOrderResponse>() {
            @Override
            public void onResponse(Response<GetOrderResponse> response) {
                AppUtils.hideProgressDialog();
                if (response.message().equals("OK")) {
                    AppPreference.getInstance().setNOTES(null);
                    showDialog();
                    Bundle params = new Bundle();
                    SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                    String format = s.format(new Date());
                    params.putString("Success", format + "   " + AppPreference.getInstance().getDEHAATI_MOBILE());
                    mFirebaseAnalytics.logEvent("OrderPlaced", params);
                }

            }

            @Override
            public void onResponse401(Response<GetOrderResponse> response) throws JSONException {
                AppUtils.showSessionExpiredDialog(getActivity());
            }
        });
    }

    /**
     * order placed dialog
     */
    private void showDialog() {
        final Dialog dialog = new Dialog(getActivity());
        final TextView num, textCount, textTotal;
        LinearLayout ok;
        ImageView cross;
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_confim);
        num = dialog.findViewById(R.id.num);
        textCount = dialog.findViewById(R.id.count);
        cross = dialog.findViewById(R.id.cross);
        textTotal = dialog.findViewById(R.id.totalAmt);
        textCount.setText("" + list.size());
        textTotal.setText(getString(R.string.rs) + total_amt);
        sQLiteHelper.clearCartTable();
        ((MainActivity) activity).showCartCount(false);
        //if number of dehaati is not there , toll free number will be printed
        number = UrlConstant.TOLL_FREE_NUMBER;
        num.setText(number);

        num.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:" + UrlConstant.TOLL_FREE_NUMBER));
                startActivity(intent);
            }
        });
        ok = dialog.findViewById(R.id.ok);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
                sQLiteHelper.clearCartTable();
                dialog.dismiss();
                ((MainActivity) activity).showCartCount(false);


            }
        });
        cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
                sQLiteHelper.clearCartTable();
                dialog.dismiss();
                ((MainActivity) activity).showCartCount(false);
            }
        });

        Drawable d = new ColorDrawable(getResources().getColor(R.color.white));
        dialog.getWindow().setBackgroundDrawable(d);
        dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        int width = getResources().getDisplayMetrics().widthPixels;
        dialog.getWindow().setLayout(width, ViewGroup.LayoutParams.MATCH_PARENT);
        dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.show();

    }

    /**
     * before order place , dialog for review your order
     */
    private void showReviewDialog(final String notesdata) {
        final Dialog dialog = new Dialog(new ContextThemeWrapper(getActivity(), R.style.DialogSlideAnim));
        final TextView textCount, textTotal, cancel, notes;
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(true);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_review_order);
        final ShimmerLayout shimmerText = dialog.findViewById(R.id.shimmer_text);

        cancel = dialog.findViewById(R.id.cancel);
        textCount = dialog.findViewById(R.id.count);
        textTotal = dialog.findViewById(R.id.totalAmt);
        notes = dialog.findViewById(R.id.notes);

        shimmerText.startShimmerAnimation();
        swipeRevealLayout = dialog.findViewById(R.id.swipe_layout_4);
        ((TextView) dialog.findViewById(R.id.textre5)).setText(UrlConstant.TOLL_FREE_NUMBER);
        if (!AppUtils.isNullCase(notesdata))
            notes.setText(getString(R.string.notes) + notesdata);
        dialog.findViewById(R.id.textre5).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:" + UrlConstant.TOLL_FREE_NUMBER));
                startActivity(intent);
            }
        });

        cancel.setText(getString(R.string.cancel));
        list = sQLiteHelper.getAllCartData();
        textCount.setText("" + list.size());
        textTotal.setText(getString(R.string.rs) + total_amt);
        swipeRevealLayout.setMinFlingVelocity(10000);
        swipeRevealLayout.setSwipeListener(new SwipeRevealLayout.SwipeListener() {
            @Override
            public void onClosed(SwipeRevealLayout view) {
            }

            @Override
            public void onOpened(SwipeRevealLayout view) {
                dialog.dismiss();
                shimmerText.stopShimmerAnimation();
                sendData(notesdata);
            }

            @Override
            public void onSlide(SwipeRevealLayout view, float slideOffset) {
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        Drawable d = new ColorDrawable(getResources().getColor(R.color.boxcolor));
        d.setAlpha(100);
        dialog.getWindow().setBackgroundDrawable(d);
        dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setGravity(Gravity.BOTTOM);
        dialog.show();
    }


    @Override
    public void onPause() {
        super.onPause();
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
        Bundle params = new Bundle();
        SimpleDateFormat s = new SimpleDateFormat("hh:mm:ss");
        format2 = s.format(new Date());
        diffTime = AppUtils.showTimeDiff(format1, format2, diffTime);
        params.putString("Cart", diffTime + "  " + AppPreference.getInstance().getDEHAATI_MOBILE());
        mFirebaseAnalytics.setUserProperty("Mobile", AppPreference.getInstance().getDEHAATI_MOBILE());
        mFirebaseAnalytics.logEvent("Cart", params);
    }

    @Override
    public void onClick(HashMap<Integer, Float> hashMap) {
        list = sQLiteHelper.getAllCartData();
        printPriceAndItem();
        showBottomBanner();
    }

    @Override
    public void onResume() {
        super.onResume();
        SimpleDateFormat s = new SimpleDateFormat("hh:mm:ss");
        format1 = s.format(new Date());
    }
}