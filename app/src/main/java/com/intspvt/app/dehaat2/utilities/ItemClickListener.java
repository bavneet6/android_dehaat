package com.intspvt.app.dehaat2.utilities;

import java.util.ArrayList;

/**
 * Created by DELL on 8/23/2017.
 */

public interface ItemClickListener {
    void onItemClick(ArrayList<String> arrayList);
}

