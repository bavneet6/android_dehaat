package com.intspvt.app.dehaat2.fragments;

import android.app.Dialog;
import android.os.Build;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.intspvt.app.dehaat2.R;
import com.intspvt.app.dehaat2.activity.MainActivity;
import com.intspvt.app.dehaat2.adapter.RecyclerAdapterStorageLoanDetail;
import com.intspvt.app.dehaat2.database.DatabaseHandler;
import com.intspvt.app.dehaat2.rest.ApiCallback;
import com.intspvt.app.dehaat2.rest.AppRestClient;
import com.intspvt.app.dehaat2.rest.PicassoCallback;
import com.intspvt.app.dehaat2.rest.response.GetPersonalInfo;
import com.intspvt.app.dehaat2.rest.response.StorageOrderData;
import com.intspvt.app.dehaat2.utilities.AppUtils;
import com.squareup.picasso.Picasso;

import org.json.JSONException;

import java.net.URL;
import java.util.LinkedHashMap;

import retrofit2.Call;
import retrofit2.Response;


public class StorageAndLoanDetailFragment extends BaseFragment implements View.OnClickListener {
    public static final String TAG = StorageAndLoanDetailFragment.class.getSimpleName();
    String cropName, unit, warehouse, storageDate, picUrlDb;
    int quantity;
    private ImageView back;
    private FirebaseAnalytics mFirebaseAnalytics;
    private TextView tvProfile, tvMaterialDeposit, tvInwardSlip, tvLoanAgreement, tvLoanApproval,
            btnInwardSlip, btnLoanAgreement, btnLoanApproval, heading1;
    private LinearLayout llLoan, llNoLoan, profile_back, material_deposit_back, inward_slip_back,
            loan_agreement_back, loan_approved_back;
    private StorageOrderData.StorageLoan storageLoan;
    private StorageOrderData.StorageLoan.Storage storage;
    private StorageOrderData.StorageLoan.Loan loan;
    private GetPersonalInfo.GetDehaatiInfo data;
    private DatabaseHandler databaseHandler;
    private ImageView imageP;
    private Boolean checkPic;


    public static StorageAndLoanDetailFragment newInstance() {
        return new StorageAndLoanDetailFragment();
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_storage_and_loan_detail, null);
        showActionBar(false);
        ((MainActivity) activity).showDrawer(false);
        ((MainActivity) activity).showCart(false);
        back = v.findViewById(R.id.back);
        tvProfile = v.findViewById(R.id.tv_profile);
        tvMaterialDeposit = v.findViewById(R.id.tv_material_deposit);
        tvInwardSlip = v.findViewById(R.id.tv_inward_slip);
        tvLoanAgreement = v.findViewById(R.id.tv_loan_agreement);
        tvLoanApproval = v.findViewById(R.id.tv_loan_approval);
        btnInwardSlip = v.findViewById(R.id.btn_inward_slip);
        btnLoanAgreement = v.findViewById(R.id.btn_loan_agreement);
        btnLoanApproval = v.findViewById(R.id.btn_loan_approval);
        heading1 = v.findViewById(R.id.heading1);
        llLoan = v.findViewById(R.id.ll_loan);
        llNoLoan = v.findViewById(R.id.ll_no_loan);
        profile_back = v.findViewById(R.id.profile_back);
        material_deposit_back = v.findViewById(R.id.material_deposit_back);
        inward_slip_back = v.findViewById(R.id.inward_slip_back);
        loan_agreement_back = v.findViewById(R.id.loan_agreement_back);
        loan_approved_back = v.findViewById(R.id.loan_approved_back);


        back.setOnClickListener(this);
        profile_back.setOnClickListener(this);
        material_deposit_back.setOnClickListener(this);
        inward_slip_back.setOnClickListener(this);
        loan_agreement_back.setOnClickListener(this);
        loan_approved_back.setOnClickListener(this);
        databaseHandler = new DatabaseHandler(getActivity());

        setData(v);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        AppUtils.showProgressDialog(getActivity());
        getDehaatiInfo();
        return v;
    }

    private void getDehaatiInfo() {
        AppRestClient client = AppRestClient.getInstance();
        Call<GetPersonalInfo> call = client.getInfo();
        call.enqueue(new ApiCallback<GetPersonalInfo>() {
            @Override
            public void onResponse(Response<GetPersonalInfo> response) {
                AppUtils.hideProgressDialog();
                if (response.body().getData() != null) {
                    data = response.body().getData();
                    AppUtils.storeDehaatiInfo(response.body().getData());
                    tvProfile.setText(data.getName() + "," +
                            data.getDehaati_info().getState_id().get(1) + "," +
                            data.getDehaati_info().getDistrict_id().get(1) + "," +
                            data.getDehaati_info().getBlock_id().get(1));
                }
            }

            @Override
            public void onResponse401(Response<GetPersonalInfo> response) throws
                    JSONException {

            }

        });
    }

    private void setData(View view) {
        storageLoan = (StorageOrderData.StorageLoan) getArguments().getSerializable("storage_data");
        storage = storageLoan.getStorage();
        loan = storageLoan.getLoan();
        heading1.setText(" " + storage.getName());
        loan = storageLoan.getLoan();
        if (loan != null) {
            llLoan.setVisibility(View.VISIBLE);
            llNoLoan.setVisibility(View.GONE);
            tvLoanAgreement.setText(loan.getType() + "," + getActivity().getString(R.string.rs) + " " + loan.getRequested_loan_amount());
            tvLoanApproval.setText(getActivity().getString(R.string.rs) + " " + loan.getApproved_loan_amount() + "@" + loan.getRate_of_interest() + "%");
            btnLoanApproval.setTextColor(getResources().getColor(R.color.white));
            btnLoanApproval.setText(getResources().getString(R.string.now));
            btnLoanApproval.setBackgroundDrawable(getResources().getDrawable(R.drawable.orange_back));
        } else {
            llLoan.setVisibility(View.GONE);
            llNoLoan.setVisibility(View.VISIBLE);
            btnInwardSlip.setTextColor(getResources().getColor(R.color.white));
            btnInwardSlip.setText(getResources().getString(R.string.now));
            btnInwardSlip.setBackgroundDrawable(getResources().getDrawable(R.drawable.orange_back));
        }
        cropName = storage.getProduct_id();
        quantity = storage.getQuantity();
        unit = storage.getQuantity_unit();
        warehouse = storage.getPicking_type_id();
        storageDate = storage.getDate_order();
        tvMaterialDeposit.setText(cropName + ":" + quantity + unit);
        tvInwardSlip.setText(warehouse + "," + storageDate);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();

    }


    @Override
    public void onClick(View view) {
        LinkedHashMap<String, String> dataList;
        int id = view.getId();
        switch (id) {
            case R.id.back:
                getActivity().onBackPressed();
                break;
            case R.id.profile_back:
                setDialogData();
                break;
            case R.id.material_deposit_back:
                dataList = new LinkedHashMap<>();
                dataList.put(getString(R.string.commodity), storage.getProduct_id());
                dataList.put(getString(R.string.date_of_storage), storage.getDate_order());
                dataList.put(getString(R.string.storage_quantity), "" + storage.getQuantity() + " " + storage.getQuantity_unit());
                showDialog(getString(R.string.material_deposit), dataList);
                break;
            case R.id.inward_slip_back:
                dataList = new LinkedHashMap<>();
                dataList.put(getString(R.string.commodity), storage.getProduct_id());
                dataList.put(getString(R.string.date_of_storage), storage.getDate_order());
                dataList.put(getString(R.string.storage_quantity), "" + storage.getQuantity() + " " + storage.getQuantity_unit());
                dataList.put(getString(R.string.duration), storage.getDuration() + " " + storage.getDuration_unit());
                dataList.put(getString(R.string.location), storage.getPicking_type_id());
                showDialog(getString(R.string.inward_slip), dataList);
                break;
            case R.id.loan_agreement_back:
                dataList = new LinkedHashMap<>();
                dataList.put(getString(R.string.loan_type), loan.getType());
                dataList.put(getString(R.string.loan_application_date), loan.getLoan_application_date());
                dataList.put(getString(R.string.req_loan_amount), "" + getActivity().getString(R.string.rs) + " " + loan.getRequested_loan_amount());
                showDialog(getString(R.string.loan_agreement), dataList);
                break;
            case R.id.loan_approved_back:
                dataList = new LinkedHashMap<>();
                dataList.put(getString(R.string.loan_type), loan.getType());
                dataList.put(getString(R.string.loan_approval_date), loan.getLoan_approval_date());
                dataList.put(getString(R.string.loan_amount), "" + getActivity().getString(R.string.rs) + " " + loan.getApproved_loan_amount());
                dataList.put(getString(R.string.rate_of_interest), "" + loan.getRate_of_interest() + "%");
                showDialog(getString(R.string.loan_approved), dataList);
                break;

        }
    }


    private void setDialogData() {
        if (data != null) {
            final Dialog dialog = new Dialog(getActivity());
            dialog.setCanceledOnTouchOutside(true);
            TextView farmerName, farmerAddress, farmerBankDetails;

            ImageView imageP;
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.template_profile_loan_detail);

            imageP = dialog.findViewById(R.id.image);
            farmerName = dialog.findViewById(R.id.farmerName);
            farmerAddress = dialog.findViewById(R.id.farmerAddress);
            farmerBankDetails = dialog.findViewById(R.id.farmerBankDetails);
            farmerName.setText(data.getName());
            farmerAddress.setText(data.getDehaati_info().getState_id().get(1) + "," +
                    data.getDehaati_info().getDistrict_id().get(1) + "," +
                    data.getDehaati_info().getBlock_id().get(1));

            if (data.getAccount_no() != null && !data.getAccount_no().equals("false") && data.getAccount_no() != " ")
                farmerBankDetails.setText(data.getAccount_no() + " ");
            else farmerBankDetails.setText("No bank account linked ");

            if (!AppUtils.isNullCase(data.getImage_small())) {
                checkPic = databaseHandler.checkImage(data.getImage_small());

                if (!checkPic)
                    AppUtils.generateAndCheckUrl(getActivity(), data.getImage_small());

                picUrlDb = databaseHandler.getFileUrl(data.getImage_small());
                if (picUrlDb == null)
                    return;
                if (getActivity() == null)
                    return;
                final Picasso picasso = Picasso.with(getActivity());
                picasso.setLoggingEnabled(true);
                picasso.load(picUrlDb).into(imageP, new PicassoCallback(data.getImage_small()) {

                    @Override
                    public void onErrorPic(URL url) {
                        picasso.load("" + url).error(R.drawable.user).into(imageP);
                    }
                });

            }
            dialog.getWindow().setGravity(Gravity.CENTER);
            dialog.show();
        }
    }

    private void showDialog(String heading_data, LinkedHashMap<String, String> data) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.setCanceledOnTouchOutside(true);
        final RecyclerView list_view;
        TextView heading;
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_storage_loan_data);

        list_view = dialog.findViewById(R.id.list_view);
        heading = dialog.findViewById(R.id.heading);
        heading.setText(heading_data);
        list_view.setLayoutManager(new LinearLayoutManager(getActivity()));
        list_view.setAdapter(new RecyclerAdapterStorageLoanDetail(getActivity(), data));


        dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.show();

    }


}
