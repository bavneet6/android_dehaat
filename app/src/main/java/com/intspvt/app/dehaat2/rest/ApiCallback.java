package com.intspvt.app.dehaat2.rest;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.TextUtils;
import android.util.Log;

import com.intspvt.app.dehaat2.Dehaat2;
import com.intspvt.app.dehaat2.R;
import com.intspvt.app.dehaat2.utilities.AppUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.ConnectException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by DELL on 6/22/2017.
 */

public abstract class ApiCallback<T> implements Callback<T> {
    Context context = Dehaat2.getInstance();

    @Override
    public void onFailure(Call<T> call, Throwable t) {// when no response is recieved
        AppUtils.hideProgressDialog();
        //TODO: Error
        Log.e("Error", " " + t.getCause());
        t.printStackTrace();
        String s = "Multipart body must have at least one part.";
        if (context == null)
            return;
        if (t instanceof ConnectException) {
            if (!haveNetworkConnection()) {
                AppUtils.showToast(
                        context.getString(R.string.no_internet));
            } else {
                AppUtils.showToast(context.getString(R.string.server_no_respond));
            }
        } else if (s.equals(t.getLocalizedMessage())) {
            AppUtils.showToast(" आपने कोई जानकारी नहीं भरी है");
        } else {
            AppUtils.showToast(t.getLocalizedMessage());
        }
    }

    @Override
    public void onResponse(Call<T> call, Response<T> response) {
        if (response.isSuccessful()) {
            try {
                onResponse(response);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if (response.code() == 401) {
            try {
                onResponse401(response);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            String error = null;
            try {
                error = response.errorBody().string();

                if (!TextUtils.isEmpty(error)) {
                    JSONObject obj = new JSONObject(error);
                    Log.e("Error: ", "" + error);
                    if (response.code() == 500) {
                        AppUtils.showToast(context.getString(R.string.tech_prob));
                    } else {
                        AppUtils.showToast(context.getString(R.string.server_no_respond));
                    }
                }
            } catch (Exception e) {
                Log.e("Error: ", "" + response.raw().message());
                AppUtils.showToast(context.getString(R.string.server_no_respond));
            }
        }
        AppUtils.hideProgressDialog();

    }

    private boolean haveNetworkConnection() {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }

    public abstract void onResponse(Response<T> response) throws JSONException;

    public abstract void onResponse401(Response<T> response) throws JSONException;
}
