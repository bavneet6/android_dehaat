package com.intspvt.app.dehaat2.fragments;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;

import androidx.fragment.app.FragmentTransaction;

import com.intspvt.app.dehaat2.R;
import com.intspvt.app.dehaat2.rest.ApiCallback;
import com.intspvt.app.dehaat2.rest.AppRestClient;
import com.intspvt.app.dehaat2.rest.body.PostDataDehaat;
import com.intspvt.app.dehaat2.rest.body.PostDataDehaatiInfo;
import com.intspvt.app.dehaat2.rest.body.UpdateDehaatInfo;
import com.intspvt.app.dehaat2.rest.response.GetAddressMapping;
import com.intspvt.app.dehaat2.rest.response.GetPersonalInfo;
import com.intspvt.app.dehaat2.rest.response.RegisterResponse;
import com.intspvt.app.dehaat2.utilities.AppPreference;
import com.intspvt.app.dehaat2.utilities.AppUtils;
import com.intspvt.app.dehaat2.utilities.GPSTracker;
import com.intspvt.app.dehaat2.utilities.UrlConstant;

import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DehaatiInformationFragment extends BaseFragment implements View.OnClickListener, AdapterView.OnItemSelectedListener {

    public static final String TAG = DehaatiInformationFragment.class.getSimpleName();
    private EditText name, shopName, locationAdd;
    private Spinner state;
    private Spinner district, block;
    private Button next;
    private String mobile, app_view, addressText, stateText, districtText;
    private double lat, longi;
    private int state_id, district_id, block_id;
    private GPSTracker gps;
    private ProgressBar progressBar;
    private ImageView back;
    private ArrayList<GetAddressMapping.GetData> districtNames = new ArrayList<>();
    private ArrayList<GetAddressMapping.GetData> blockNames = new ArrayList<>();
    private ArrayList<GetAddressMapping.GetData> stateNames = new ArrayList<>();
    private ArrayList<String> districtList = new ArrayList<>();
    private ArrayList<String> blockList = new ArrayList<>();
    private ArrayList<String> stateList = new ArrayList<>();
    private Geocoder geocoder;
    private List<Address> addresses;
    private GetPersonalInfo.GetDehaatiInfo dehaatiInfo;
    private ArrayAdapter<String> stateAdapter, districtAdapter;

    public static DehaatiInformationFragment newInstance() {
        return new DehaatiInformationFragment();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_dehaati_info, null);
        showActionBar(false);
        name = v.findViewById(R.id.name);
        state = v.findViewById(R.id.state);
        shopName = v.findViewById(R.id.shopName);
        locationAdd = v.findViewById(R.id.address);
        district = v.findViewById(R.id.district);
        block = v.findViewById(R.id.block);
        next = v.findViewById(R.id.create_account);
        back = v.findViewById(R.id.back);
        progressBar = v.findViewById(R.id.progress_bar);
        next.setOnClickListener(this);
        back.setOnClickListener(this);
        block.setOnItemSelectedListener(this);
        district.setOnItemSelectedListener(this);
        state.setOnItemSelectedListener(this);
        gps = new GPSTracker(getActivity());
        geocoder = new Geocoder(getActivity(), Locale.getDefault());
        checkGPS();
        Bundle bundle = getArguments();
        if (bundle != null) {
            mobile = bundle.getString("MOBILE");
            app_view = bundle.getString("APP_VIEW");
            dehaatiInfo = (GetPersonalInfo.GetDehaatiInfo) bundle.getSerializable("HAS_DATA");
        }
        if (dehaatiInfo != null)
            loadText();
        getStateList();
        return v;
    }

    private void loadText() {
        name.setText(dehaatiInfo.getName());
    }

    private void checkGPS() {
        if (gps.canGetLocation()) {
            double latitude = gps.getLatitude();
            double longitude = gps.getLongitude();
            if ((latitude != 0.0) && (longitude != 0.0)) {
                lat = latitude;
                longi = longitude;
                getAddressFromLatLong(lat, longi);
            }
        } else {
            if (gps.showSettingsAlert())
                checkGPS();
        }
    }

    private void getAddressFromLatLong(double lat, double longi) {
        try {
            addresses = geocoder.getFromLocation(lat, longi, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (addresses != null && addresses.size() > 0) {
            addressText = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            stateText = addresses.get(0).getAdminArea();
            districtText = addresses.get(0).getLocality();
            locationAdd.setText(addressText);
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.create_account:
                if (AppUtils.isNullCase(name.getText().toString())) {
                    name.setError("Please enter your name");
                    name.requestFocus();
                } else if (AppUtils.isNullCase(shopName.getText().toString())) {
                    shopName.setError("Please enter your DeHaat name");
                    shopName.requestFocus();
                } else {
                    if ((state_id != 0) && (district_id != 0) && (block_id != 0)) {
                        sendDehaat();
                    } else
                        AppUtils.showToast("Please fill your complete address");
                }
                break;
            case R.id.back:
                getActivity().onBackPressed();
                break;

        }
    }

    /**
     * method to create the dehaati info
     */
    private void postInfo(int id) {
        PostDataDehaatiInfo.PostdehaatiInfoRegister postdehaatiInfoRegister = new PostDataDehaatiInfo.PostdehaatiInfoRegister(mobile, name.getText().toString(), id);
        PostDataDehaatiInfo postDataDehaati = new PostDataDehaatiInfo(postdehaatiInfoRegister);
        AppRestClient client = AppRestClient.getInstance();
        Call<Void> call = client.sendDehaatiInfo(postDataDehaati);

        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (response.code() == 200) {
                    getDehaatiInfo();
                } else {
                    if (progressBar != null)
                        progressBar.setVisibility(View.GONE);
                    next.setEnabled(true);
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                next.setEnabled(true);
            }

        });
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch (parent.getId()) {

            case R.id.state:
                state_id = stateNames.get(position).getId();
                if (state_id != 0)
                    getDistrictList(state_id);
                break;

            case R.id.district:
                district_id = districtNames.get(position).getId();
                if (district_id != 0)
                    getBlockList(district_id);
                break;
            case R.id.block:
                block_id = blockNames.get(position).getId();
                break;
            default:
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }


    /**
     * method to create  a dehaat
     */
    private void sendDehaat() {
        progressBar.setVisibility(View.VISIBLE);
        next.setEnabled(false);
        PostDataDehaat.PostDehaat postDehaat = new PostDataDehaat.PostDehaat(shopName.getText().toString(), locationAdd.getText().toString(), state_id, district_id, block_id);
        PostDataDehaat postDataDehaat = new PostDataDehaat(postDehaat);
        AppRestClient client = AppRestClient.getInstance();
        Call<RegisterResponse> call = client.sendDehaat(postDataDehaat);

        call.enqueue(new Callback<RegisterResponse>() {

            @Override
            public void onResponse(Call<RegisterResponse> call, Response<RegisterResponse> response) {
                if (response.code() == 200) {
                    AppPreference.getInstance().setDEHAATI_MOBILE(mobile);
                    if (dehaatiInfo == null)
                        postInfo(response.body().getData().getId());
                    else
                        updateDehaatInfo(response.body().getData().getId());
                } else {
                    if (progressBar != null)
                        progressBar.setVisibility(View.GONE);
                    next.setEnabled(true);
                }
            }

            @Override
            public void onFailure(Call<RegisterResponse> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                next.setEnabled(true);
            }

        });
    }

    private void updateDehaatInfo(int id) {
        Call<Void> call;
        UpdateDehaatInfo.UpdateInfo.DehaatInfo dehaatInfo = new UpdateDehaatInfo.UpdateInfo.DehaatInfo(shopName.getText().toString(), locationAdd.getText().toString(), state_id, district_id, block_id);
        UpdateDehaatInfo.UpdateInfo updateInfo = new UpdateDehaatInfo.UpdateInfo(Long.parseLong(String.valueOf(id)), name.getText().toString(), dehaatInfo);
        UpdateDehaatInfo updateDehaatInfo = new UpdateDehaatInfo(updateInfo);

        AppRestClient client = AppRestClient.getInstance();
        call = client.updateDehaatInfo(updateDehaatInfo, null);
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (response.code() == 200)
                    getDehaatiInfo();
                else {
                    if (progressBar != null)
                        progressBar.setVisibility(View.GONE);
                    next.setEnabled(true);
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                next.setEnabled(true);
            }
        });
    }

    /**
     * after creating dehaati info is called
     */
    private void getDehaatiInfo() {
        AppRestClient client = AppRestClient.getInstance();
        Call<GetPersonalInfo> call = client.getInfo();
        call.enqueue(new Callback<GetPersonalInfo>() {
            @Override
            public void onResponse(Call<GetPersonalInfo> call, Response<GetPersonalInfo> response) {
                if (response.body() == null)
                    return;
                if (response.body().getData() == null)
                    return;
                AppUtils.storeDehaatiInfo(response.body().getData());
                progressBar.setVisibility(View.GONE);
                AppPreference.getInstance().setAPP_LOGIN(true);
                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.frag_cont, HomeIntroFragment.newInstance()).commit();
            }

            @Override
            public void onFailure(Call<GetPersonalInfo> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                next.setEnabled(true);
            }

        });

    }

    @Override
    public void onPause() {
        super.onPause();
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void getBlockList(int id) {
        blockList.clear();
        AppUtils.showProgressDialog(getActivity());
        AppRestClient client = AppRestClient.getInstance();
        Call<GetAddressMapping> call = client.getBlock(id);

        call.enqueue(new ApiCallback<GetAddressMapping>() {

            @Override
            public void onResponse(Response<GetAddressMapping> response) {
                AppUtils.hideProgressDialog();
                if (response.body() == null)
                    return;
                GetAddressMapping.GetData getData = new GetAddressMapping.GetData();
                getData.setId(0);
                getData.setName(UrlConstant.SELECT);
                blockNames = response.body().getGetAddress();
                blockNames.add(0, getData);
                for (int i = 0; i < blockNames.size(); i++) {
                    blockList.add(blockNames.get(i).getName());
                }
                if (getActivity() != null && isAdded()) {
                    ArrayAdapter<String> adp = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, blockList);
                    adp.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    block.setAdapter(adp);
                }
            }

            @Override
            public void onResponse401(Response<GetAddressMapping> response) throws JSONException {

            }


        });
    }

    private void getStateList() {
        stateList.clear();
        AppUtils.showProgressDialog(getActivity());
        AppRestClient client = AppRestClient.getInstance();
        Call<GetAddressMapping> call = client.getState();

        call.enqueue(new ApiCallback<GetAddressMapping>() {

            @Override
            public void onResponse(Response<GetAddressMapping> response) throws JSONException {
                AppUtils.hideProgressDialog();
                if (response.body() == null)
                    return;
                GetAddressMapping.GetData getData = new GetAddressMapping.GetData();
                getData.setId(0);
                getData.setName(UrlConstant.SELECT);
                stateNames = response.body().getGetAddress();
                stateNames.add(0, getData);
                for (int i = 0; i < stateNames.size(); i++) {
                    stateList.add(stateNames.get(i).getName());
                }
                if (getActivity() != null && isAdded()) {
                    stateAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, stateList);
                    stateAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    state.setAdapter(stateAdapter);
                    int spinnerPosition = stateAdapter.getPosition(stateText);
                    state.setSelection(spinnerPosition);
                }
            }

            @Override
            public void onResponse401(Response<GetAddressMapping> response) throws JSONException {

            }


        });
    }


    private void getDistrictList(int id) {
        districtList.clear();
        AppRestClient client = AppRestClient.getInstance();
        Call<GetAddressMapping> call = client.getDistrict(id);
        call.enqueue(new ApiCallback<GetAddressMapping>() {

            @Override
            public void onResponse(Response<GetAddressMapping> response) throws JSONException {
                if (response.body() == null)
                    return;
                GetAddressMapping.GetData getData = new GetAddressMapping.GetData();
                getData.setId(0);
                getData.setName(UrlConstant.SELECT);
                districtNames = response.body().getGetAddress();
                districtNames.add(0, getData);
                for (int i = 0; i < districtNames.size(); i++) {
                    districtList.add(districtNames.get(i).getName());
                }
                if (getActivity() != null && isAdded()) {
                    districtAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, districtList);
                    districtAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    district.setAdapter(districtAdapter);
                    int spinnerPosition = districtAdapter.getPosition(districtText);
                    district.setSelection(spinnerPosition);
                }
            }

            @Override
            public void onResponse401(Response<GetAddressMapping> response) throws JSONException {

            }


        });
    }

}