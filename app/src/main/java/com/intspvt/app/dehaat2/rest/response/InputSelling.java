package com.intspvt.app.dehaat2.rest.response;

public class InputSelling {
    private String requestId, productName, productImage;
    private float productMrp;
    private int productQty;


    public float getProductMrp() {
        return productMrp;
    }

    public void setProductMrp(float productMrp) {
        this.productMrp = productMrp;
    }

    public int getProductQty() {
        return productQty;
    }

    public void setProductQty(int productQty) {
        this.productQty = productQty;
    }

    public String getProductImage() {
        return productImage;
    }

    public void setProductImage(String productImage) {
        this.productImage = productImage;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

}
