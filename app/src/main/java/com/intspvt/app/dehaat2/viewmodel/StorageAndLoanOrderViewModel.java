package com.intspvt.app.dehaat2.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.intspvt.app.dehaat2.repository.StorageAndLoanOrderRepository;
import com.intspvt.app.dehaat2.rest.response.StorageOrderData;

import java.util.List;

public class StorageAndLoanOrderViewModel extends ViewModel {
    private MutableLiveData<List<StorageOrderData.StorageLoan>> orderList;
    private StorageAndLoanOrderRepository storageAndLoanOrderRepository;

    public void init() {
        if (storageAndLoanOrderRepository == null)
            storageAndLoanOrderRepository = StorageAndLoanOrderRepository.getInstance();
        orderList = storageAndLoanOrderRepository.getOrderData();
    }

    public LiveData<List<StorageOrderData.StorageLoan>> getHistoryData() {
        return orderList;
    }
}
