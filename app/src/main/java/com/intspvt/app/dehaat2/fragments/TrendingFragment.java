package com.intspvt.app.dehaat2.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.intspvt.app.dehaat2.R;
import com.intspvt.app.dehaat2.activity.MainActivity;
import com.intspvt.app.dehaat2.adapter.RecyclerAdapterTrendingArticles;
import com.intspvt.app.dehaat2.database.DatabaseHandler;
import com.intspvt.app.dehaat2.rest.ApiCallback;
import com.intspvt.app.dehaat2.rest.AppRestClient;
import com.intspvt.app.dehaat2.rest.response.Trending;
import com.intspvt.app.dehaat2.utilities.AppPreference;
import com.intspvt.app.dehaat2.utilities.AppUtils;

import org.json.JSONException;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by DELL on 11/15/2017.
 */

public class TrendingFragment extends BaseFragment {
    private FirebaseAnalytics mFirebaseAnalytics;
    private DatabaseHandler databaseHandler;
    private String format2, format1, diffTime;
    private boolean checkPic;
    private RecyclerAdapterTrendingArticles recyclerAdapterTrendingArticles;
    private RecyclerView trendRecy;
    private ArrayList<Trending.TrendingData> data = new ArrayList<>();

    public static TrendingFragment newInstance() {
        return new TrendingFragment();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_trending, null);
        showActionBar(true);
        ((MainActivity) activity).setTitle(getString(R.string.information));
        ((MainActivity) activity).showDrawer(true);
        ((MainActivity) activity).showCart(false);
        databaseHandler = new DatabaseHandler(getActivity());
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        trendRecy = v.findViewById(R.id.trend_recy);
        trendRecy.setNestedScrollingEnabled(false);
        getTrendingArticles();
        return v;
    }

    /**
     * function to get the content of the trending articles and show that into the card stack library
     */
    private void getTrendingArticles() {
        AppUtils.showProgressDialog(getActivity());
        AppRestClient client = AppRestClient.getInstance();
        Call<Trending> call = client.trendingData();
        call.enqueue(new ApiCallback<Trending>() {
            @Override
            public void onResponse(Response<Trending> response) {
                if (response.body() == null)
                    return;
                if (response.body().getDatas() == null)
                    return;
                for (int i = 0; i < response.body().getDatas().size(); i++) {
                    if (!AppUtils.isNullCase(response.body().getDatas().get(i).getImage())) {
                        checkPic = databaseHandler.checkImage(response.body().getDatas().get(i).getImage());

                        if (!checkPic) {
                            ((MainActivity) activity).generateAndCheckUrl(response.body().getDatas().get(i).getImage());
                        }
                    }
                }
                data = response.body().getDatas();
                recyclerAdapterTrendingArticles = new RecyclerAdapterTrendingArticles(getActivity(), data);
                trendRecy.setLayoutManager(new GridLayoutManager(getActivity(), 2));
                trendRecy.setAdapter(recyclerAdapterTrendingArticles);
                AppUtils.hideProgressDialog();

            }

            @Override
            public void onResponse401(Response<Trending> response) throws JSONException {
                AppUtils.showSessionExpiredDialog(getActivity());
            }

        });

    }

    @Override
    public void onResume() {
        super.onResume();
        SimpleDateFormat s = new SimpleDateFormat("hh:mm:ss");
        format1 = s.format(new Date());
    }

    @Override
    public void onPause() {
        super.onPause();
        Bundle params = new Bundle();
        SimpleDateFormat s = new SimpleDateFormat("hh:mm:ss");
        format2 = s.format(new Date());
        diffTime = AppUtils.showTimeDiff(format1, format2, diffTime);
        params.putString("trending", diffTime + "   " + AppPreference.getInstance().getDEHAATI_MOBILE());
        mFirebaseAnalytics.setUserProperty("Mobile", AppPreference.getInstance().getDEHAATI_MOBILE());
        mFirebaseAnalytics.logEvent("Trending", params);

    }
}
