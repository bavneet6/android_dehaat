package com.intspvt.app.dehaat2.utilities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;

import com.amazonaws.HttpMethod;
import com.amazonaws.mobile.client.AWSMobileClient;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
import com.fxn.pix.Options;
import com.fxn.pix.Pix;
import com.fxn.utility.ImageQuality;
import com.intspvt.app.dehaat2.Dehaat2;
import com.intspvt.app.dehaat2.R;
import com.intspvt.app.dehaat2.activity.LoginActivity;
import com.intspvt.app.dehaat2.database.DatabaseHandler;
import com.intspvt.app.dehaat2.rest.ApiCallback;
import com.intspvt.app.dehaat2.rest.AppRestClient;
import com.intspvt.app.dehaat2.rest.response.GetPersonalInfo;
import com.intspvt.app.dehaat2.rest.response.ProductFileTable;
import com.intspvt.app.dehaat2.rest.response.VersionName;

import org.json.JSONException;

import java.io.IOException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by DELL on 6/22/2017.
 */

public class AppUtils {

    private static ProgressDialog progressDialog;
    private static DatabaseHandler databaseHandler;
    private static String appPackageName, versionName;

    public static void showToast(String msg) {
        Context context = Dehaat2.getInstance();
        if (context != null && msg != null)
            Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
    }

    public static void changeFragment(FragmentActivity activity, Fragment fragment) {
        FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frag_container, fragment).addToBackStack("").commit();
    }

    public static String showTimeDiff(String format1, String format2, String diffTime) {
        SimpleDateFormat format = new SimpleDateFormat("hh:mm:ss");
        Date d1 = null;
        Date d2 = null;

        try {
            d1 = format.parse(format1);
            d2 = format.parse(format2);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        //in milliseconds
        long diff = d2.getTime() - d1.getTime();

        long diffSeconds = diff / 1000 % 60;
        long diffMinutes = diff / (60 * 1000) % 60;
        long diffHours = diff / (60 * 60 * 1000) % 24;

        Log.e("diffHours", "" + diffHours);
        Log.e("diffMinutes", " " + diffMinutes);
        Log.e("diffSecond", " " + diffSeconds);
        diffTime = diffHours + "hr" + diffMinutes + "min" + diffSeconds + "sec";
        return diffTime;
    }

    public static void showProgressDialog(Context context) {
        if (progressDialog != null)
            return;
        if (context == null)
            return;
        progressDialog = new ProgressDialog(context);
        progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progressDialog.setMessage(context.getString(R.string.please_wait));
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public static void hideProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
        progressDialog = null;
    }

    public static boolean haveNetworkConnection(Context activity) {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        if (activity == null) {
            activity = Dehaat2.getInstance().getApplicationContext();
        }
        ConnectivityManager cm = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {

            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }

    public static void showSessionExpiredDialog(final Activity context) {

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("Session Expired! \n \nPlease Login Again.");
        builder.setCancelable(false);

        builder.setPositiveButton(context.getString(R.string.okText), new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                databaseHandler = new DatabaseHandler(context);
                AppPreference.getInstance().clearData();
                Intent i = new Intent(context, LoginActivity.class);
                i.putExtra("logout", "logout");
                databaseHandler.clearCartTable();
                databaseHandler.clearInputSellingData();
                databaseHandler.clearPicTable();
                databaseHandler.clearIssueStatusSeen();
                Dehaat2.getInstance().clearApplicationData();
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(i);
                context.finish();
                dialog.dismiss();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();


    }

    public static void imageSelection(Fragment fragment, int count) {
        Options options = Options.init()
                .setRequestCode(100)
                .setCount(count)
                .setFrontfacing(false)
                .setImageQuality(ImageQuality.HIGH)
                .setImageResolution(1024, 800)
                .setScreenOrientation(Options.SCREEN_ORIENTATION_FULL_SENSOR)
                .setPath("/dehaat/new");
        Pix.start(fragment, options);
    }

    public static boolean isNullCase(String test) {
        return (test == null) || (test.equals("")) || (test.equals("false")) || (test.equals("0") || (test.equals("0.0")));
    }

    public static void showInternetDialog(Activity activity) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle("Information");
        builder.setMessage("Internet Connection is required.");
        builder.setCancelable(false);
        builder.setPositiveButton(activity.getString(R.string.okText), new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public static void generateAndCheckUrl(Context context, String picFilePath) {
        databaseHandler = new DatabaseHandler(context);
        ProductFileTable productFileTable = new ProductFileTable();
        productFileTable.setProd_file(picFilePath);
        URL url = null;
        try {
            url = new AppUtils.FetchPicture(picFilePath).execute().get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        productFileTable.setProd_url("" + url);
        databaseHandler.inserPicturesFile(productFileTable);
    }

    public static void storeDehaatiInfo(GetPersonalInfo.GetDehaatiInfo data) {
        AppPreference.getInstance().setDEHAATI_NAME(data.getName());
        AppPreference.getInstance().setDEHAATI_NODE_NO(data.getNode_mobile());
        AppPreference.getInstance().setDEHAATI_NODE_NAME(data.getNode_name());
        AppPreference.getInstance().setDEHAATI_IMAGE(data.getImage_small());
        //dehaat info
        if (data.getDehaati_info() != null) {
            AppPreference.getInstance().setDEHAAT_ADD(data.getDehaati_info().getAddress_line());
            AppPreference.getInstance().setDEHAAT_BLOCK(data.getDehaati_info().getBlock_id());
            AppPreference.getInstance().setDEHAAT_DISTRICT(data.getDehaati_info().getDistrict_id());
            AppPreference.getInstance().setDEHAAT_STATE(data.getDehaati_info().getState_id());
            AppPreference.getInstance().setDEHAAT_NAME(data.getDehaati_info().getName());
        }
    }

    public static void getUpdateApp(Context context) {
        appPackageName = context.getPackageName();
        try {
            versionName = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        getVersionName(context);
    }

    public static void getVersionName(final Context context) {
        AppRestClient client = AppRestClient.getInstance();
        Call<VersionName> call = client.getVersionName();
        call.enqueue(new ApiCallback<VersionName>() {
            @Override
            public void onResponse(Response<VersionName> response) {
                if (response.body() == null)
                    return;
                String version = response.body().getVersion();
                if (!version.equals(versionName))
                    openDialog(context);

            }

            @Override
            public void onResponse401(Response<VersionName> response) throws JSONException {

            }
        });
    }

    public static void openDialog(final Context
                                          context) {
        if (context == null)
            return;
        final Dialog dialog = new Dialog(context);
        TextView okay;
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_update);
        okay = dialog.findViewById(R.id.okay);
        okay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                dialog.dismiss();
            }
        });
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.show();
    }

    public static Address getLatLongFromPin(Activity activity, String pincode) {
        final Geocoder geocoder = new Geocoder(activity);
        try {
            List<Address> addresses = geocoder.getFromLocationName(pincode, 1);
            if (addresses != null && !addresses.isEmpty()) {
                Address address = addresses.get(0);
                return address;
            } else {
            }
        } catch (IOException e) {
        }
        return null;
    }

    public static class FetchPicture extends android.os.AsyncTask<String, String, URL> {
        URL url = null;
        String photo;

        public FetchPicture(String photo1) {
            photo = photo1;
        }

        @Override
        protected URL doInBackground(String... strings) {
            if (haveNetworkConnection(Dehaat2.getInstance().getApplicationContext())) {
                AmazonS3 s3Client = new AmazonS3Client(AWSMobileClient.getInstance().getCredentialsProvider());
                GeneratePresignedUrlRequest generatePresignedUrlRequest = new GeneratePresignedUrlRequest(UrlConstant.TEMP_FNF_DB, photo);
                generatePresignedUrlRequest.setMethod(HttpMethod.GET);
                url = s3Client.generatePresignedUrl(generatePresignedUrlRequest);
            }
            return url;
        }
    }
}
