package com.intspvt.app.dehaat2.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.intspvt.app.dehaat2.R;
import com.intspvt.app.dehaat2.activity.MainActivity;
import com.intspvt.app.dehaat2.adapter.RecyclerAdapterFarmerList1;
import com.intspvt.app.dehaat2.rest.ApiCallback;
import com.intspvt.app.dehaat2.rest.AppRestClient;
import com.intspvt.app.dehaat2.rest.response.FarmerList;
import com.intspvt.app.dehaat2.rest.response.FarmerListInfo;
import com.intspvt.app.dehaat2.utilities.AppPreference;
import com.intspvt.app.dehaat2.utilities.AppUtils;

import org.json.JSONException;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import in.myinnos.alphabetsindexfastscrollrecycler.IndexFastScrollRecyclerView;
import retrofit2.Call;
import retrofit2.Response;

public class FarmerListFragment extends BaseFragment implements SearchView.OnQueryTextListener, View.OnClickListener {
    public static final String TAG = FarmerListFragment.class.getSimpleName();
    private TextView no_data_mssg;
    private IndexFastScrollRecyclerView farmerList;
    private FirebaseAnalytics mFirebaseAnalytics;
    private String format2, format1, diffTime;
    private RecyclerView.LayoutManager layoutManager;
    private RecyclerAdapterFarmerList1 recyclerAdapterFarmerList;
    private SearchView searchView;
    private ArrayList<FarmerListInfo> farmerListData = new ArrayList<>();
    private FloatingActionButton register_farmer;

    public static FarmerListFragment newInstance() {
        return new FarmerListFragment();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_farmer_list, null);
        showActionBar(true);
        ((MainActivity) activity).showDrawer(false);
        ((MainActivity) activity).showCart(false);
        ((MainActivity) activity).setTitle(getString(R.string.farmer_list));
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        no_data_mssg = v.findViewById(R.id.no_data_mssg);
        farmerList = v.findViewById(R.id.farmerListView);
        searchView = v.findViewById(R.id.searchView);
        register_farmer = v.findViewById(R.id.register_farmer);
        farmerList.setNestedScrollingEnabled(false);
        farmerList.setIndexBarColor("#ffffff");
        farmerList.setIndexBarTextColor("#447d22");
        farmerList.setIndexTextSize(14);
        getFarmerList();
        searchView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchView.setIconified(false);
            }
        });
        searchView.setOnQueryTextListener(this);
        register_farmer.setOnClickListener(this);
        return v;

    }

    private void getFarmerList() {
        AppUtils.showProgressDialog(getActivity());
        AppRestClient client = AppRestClient.getInstance();
        Call<FarmerList> call = client.getFarmerList();
        call.enqueue(new ApiCallback<FarmerList>() {
            @Override
            public void onResponse(Response<FarmerList> response) {
                if (response.body() == null)
                    return;
                if (response.body().getData() == null) {
                    no_data_mssg.setVisibility(View.VISIBLE);
                } else {
                    if (response.body().getData().getFarmerListInfos() == null)
                        return;
                    farmerListData = response.body().getData().getFarmerListInfos();
                    displayFarmerData(farmerListData);
                }
                AppUtils.hideProgressDialog();

            }


            @Override
            public void onResponse401(Response<FarmerList> response) throws
                    JSONException {
                AppUtils.showSessionExpiredDialog(getActivity());
            }

        });
    }

    private void displayFarmerData(ArrayList<FarmerListInfo> data) {
        AppUtils.hideProgressDialog();
        recyclerAdapterFarmerList = new RecyclerAdapterFarmerList1(data);
        layoutManager = new LinearLayoutManager(getActivity());
        farmerList.setLayoutManager(layoutManager);
        farmerList.setAdapter(recyclerAdapterFarmerList);
        searchView.setVisibility(View.VISIBLE);
        no_data_mssg.setVisibility(View.GONE);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if (farmerListData.size() != 0) {
            recyclerAdapterFarmerList.getFilter().filter(newText);
        }
        return false;
    }

    @Override
    public void onResume() {
        super.onResume();
        SimpleDateFormat s = new SimpleDateFormat("hh:mm:ss");
        format1 = s.format(new Date());

    }

    @Override
    public void onPause() {
        super.onPause();
        Bundle params = new Bundle();
        SimpleDateFormat s = new SimpleDateFormat("hh:mm:ss");
        format2 = s.format(new Date());
        diffTime = AppUtils.showTimeDiff(format1, format2, diffTime);
        params.putString("farmerList", diffTime + "   " + AppPreference.getInstance().getDEHAATI_MOBILE());
        mFirebaseAnalytics.setUserProperty("Mobile", AppPreference.getInstance().getDEHAATI_MOBILE());
        mFirebaseAnalytics.logEvent("FarmerList", params);
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
    }

    @Override
    public void onClick(View v) {
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        int id = v.getId();
        switch (id) {
            case R.id.register_farmer:
                transaction.replace(R.id.frag_container, RegisterFarmerFragment.newInstance()).addToBackStack("").commit();
                break;
        }
    }
}
