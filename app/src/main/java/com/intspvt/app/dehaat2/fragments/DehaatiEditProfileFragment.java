package com.intspvt.app.dehaat2.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.fxn.pix.Pix;
import com.fxn.utility.PermUtil;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.intspvt.app.dehaat2.R;
import com.intspvt.app.dehaat2.activity.MainActivity;
import com.intspvt.app.dehaat2.database.DatabaseHandler;
import com.intspvt.app.dehaat2.rest.ApiCallback;
import com.intspvt.app.dehaat2.rest.AppRestClient;
import com.intspvt.app.dehaat2.rest.PicassoCallback;
import com.intspvt.app.dehaat2.rest.body.UpdateDehaatInfo;
import com.intspvt.app.dehaat2.rest.response.GetAddressMapping;
import com.intspvt.app.dehaat2.utilities.AppPreference;
import com.intspvt.app.dehaat2.utilities.AppUtils;
import com.intspvt.app.dehaat2.utilities.RoundImageView;
import com.intspvt.app.dehaat2.utilities.UrlConstant;
import com.squareup.picasso.Picasso;

import org.json.JSONException;

import java.io.File;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Response;

public class DehaatiEditProfileFragment extends BaseFragment implements View.OnClickListener, AdapterView.OnItemSelectedListener {
    public static final String TAG = DehaatiEditProfileFragment.class.getSimpleName();
    ArrayList<String> returnValue = new ArrayList<>();
    private RoundImageView dehaati_img;
    private Spinner block, district, state;
    private int state_id, district_id, block_id;
    private TextView save, mobile, node;
    private boolean checkPic;
    private ImageView back;
    private LinearLayout changeImageBack;
    private EditText name, dehaat, address;
    private String getPic;
    private File picFile;
    private DatabaseHandler databaseHandler;
    private ArrayList<GetAddressMapping.GetData> districtNames = new ArrayList<>();
    private ArrayList<GetAddressMapping.GetData> blockNames = new ArrayList<>();
    private ArrayList<GetAddressMapping.GetData> stateNames = new ArrayList<>();
    private ArrayList<String> districtList = new ArrayList<>();
    private ArrayList<String> blockList = new ArrayList<>();
    private ArrayList<String> stateList = new ArrayList<>();
    private FirebaseAnalytics firebaseAnalytics;
    private ArrayList<File> imageFileList1 = new ArrayList<>();

    public static DehaatiEditProfileFragment newInstance() {
        return new DehaatiEditProfileFragment();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_dehaati_edit_profile, null);
        showActionBar(false);
        ((MainActivity) activity).showDrawer(false);
        ((MainActivity) activity).showCart(false);
        databaseHandler = new DatabaseHandler(getActivity());
        firebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        back = v.findViewById(R.id.back);
        save = v.findViewById(R.id.save);
        mobile = v.findViewById(R.id.mobile);
        name = v.findViewById(R.id.name);
        address = v.findViewById(R.id.address);
        dehaat = v.findViewById(R.id.dehaat);
        node = v.findViewById(R.id.node);
        state = v.findViewById(R.id.state);
        block = v.findViewById(R.id.block);
        district = v.findViewById(R.id.district);
        changeImageBack = v.findViewById(R.id.changeImageBack);
        dehaati_img = v.findViewById(R.id.dehaati_img);
        save.setOnClickListener(this);
        changeImageBack.setOnClickListener(this);
        back.setOnClickListener(this);
        block.setOnItemSelectedListener(this);
        district.setOnItemSelectedListener(this);
        state.setOnItemSelectedListener(this);
        loadText(v);
        if (!AppUtils.isNullCase(AppPreference.getInstance().getDEHAATI_IMAGE())) {
            checkPic = databaseHandler.checkImage(AppPreference.getInstance().getDEHAATI_IMAGE());

            if (!checkPic) {
                ((MainActivity) activity).generateAndCheckUrl(AppPreference.getInstance().getDEHAATI_IMAGE());
            }
            getPic = databaseHandler.getFileUrl(AppPreference.getInstance().getDEHAATI_IMAGE());
            if (getPic != null) {
                final Picasso picasso = Picasso.with(getActivity());
                picasso.setLoggingEnabled(true);
                picasso.load(getPic).into(dehaati_img, new PicassoCallback(getPic) {

                    @Override
                    public void onErrorPic(URL url) {
                        picasso.load("" + url).error(R.drawable.user).into(dehaati_img);
                    }
                });
            }
        }
        getStateList();
        return v;
    }


    @Override
    public void onPause() {
        super.onPause();
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
    }

    private void loadText(View v) {
        if (!AppUtils.isNullCase(AppPreference.getInstance().getDEHAATI_MOBILE()))
            mobile.setText(AppPreference.getInstance().getDEHAATI_MOBILE());
        if (!AppUtils.isNullCase(AppPreference.getInstance().getDEHAAT_NAME()))
            dehaat.setText(AppPreference.getInstance().getDEHAAT_NAME());
        if (!AppUtils.isNullCase(AppPreference.getInstance().getDEHAATI_NAME()))
            name.setText(AppPreference.getInstance().getDEHAATI_NAME());
        if (!AppUtils.isNullCase(AppPreference.getInstance().getDEHAATI_NODE_NAME()))
            node.setText(AppPreference.getInstance().getDEHAATI_NODE_NAME());
        if (!AppUtils.isNullCase(AppPreference.getInstance().getDEHAAT_ADD()))
            address.setText(AppPreference.getInstance().getDEHAAT_ADD());

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.back:
                getActivity().onBackPressed();
                break;
            case R.id.save:
                checkValidations();
                break;
            case R.id.changeImageBack:
                if (AppUtils.haveNetworkConnection(getActivity()))
                    AppUtils.imageSelection(this, 1);
                else
                    AppUtils.showInternetDialog(getActivity());
                break;
        }

    }

    private void checkValidations() {
        if (AppUtils.isNullCase(name.getText().toString())) {
            name.setError(getString(R.string.enter_your_name));
            name.requestFocus();
        } else if (AppUtils.isNullCase(dehaat.getText().toString())) {
            dehaat.setError(getString(R.string.enter_dehaat_name));
            dehaat.requestFocus();
        } else {
            if ((state_id != 0) && (district_id != 0) && (block_id != 0))
                updateDehaatInfo();
            else
                AppUtils.showToast(getString(R.string.completeAdd));
        }

    }

    private void updateDehaatInfo() {
        AppUtils.showProgressDialog(getActivity());
        Call<Void> call;
        //0 if block is not chanage then 0 is sent
        UpdateDehaatInfo.UpdateInfo.DehaatInfo dehaatInfo = new UpdateDehaatInfo.UpdateInfo.DehaatInfo(dehaat.getText().toString(), address.getText().toString(), state_id, district_id, block_id);
        UpdateDehaatInfo.UpdateInfo updateInfo = new UpdateDehaatInfo.UpdateInfo(null, name.getText().toString(), dehaatInfo);
        UpdateDehaatInfo updateDehaatInfo = new UpdateDehaatInfo(updateInfo);

        AppRestClient client = AppRestClient.getInstance();
        if (picFile == null)
            call = client.updateDehaatInfo(updateDehaatInfo, null);
        else
            call = client.updateDehaatInfo(updateDehaatInfo, picFile);

        call.enqueue(new ApiCallback<Void>() {
            @Override
            public void onResponse(Response<Void> response) throws JSONException {
                AppUtils.hideProgressDialog();
                if (response.code() == 200) {
                    if (getActivity() != null && isAdded()) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                        builder.setTitle(getActivity().getString(R.string.information));
                        builder.setMessage(getActivity().getString(R.string.infoUpdated));
                        builder.setCancelable(false);
                        builder.setPositiveButton(getActivity().getString(R.string.okText), new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                getActivity().onBackPressed();
                            }
                        });
                        AlertDialog alert = builder.create();
                        alert.show();

                        Bundle params = new Bundle();
                        SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                        String format = s.format(new Date());
                        firebaseAnalytics.setUserProperty("Mobile", AppPreference.getInstance().getDEHAATI_MOBILE());
                        params.putString("updateDehaatiInfo", format + "   " + AppPreference.getInstance().getDEHAATI_MOBILE());
                        firebaseAnalytics.logEvent("updateDehaatiInfo", params);
                    }
                }
            }

            @Override
            public void onResponse401(Response<Void> response) throws JSONException {

            }

        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.e("val", "requestCode ->  " + requestCode + "  resultCode " + resultCode);
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case (100): {
                if (resultCode == Activity.RESULT_OK) {
                    returnValue = data.getStringArrayListExtra(Pix.IMAGE_RESULTS);
                    storeImagesInList(returnValue);
                }
            }
            break;
        }
    }

    private void storeImagesInList(ArrayList<String> returnValue) {
        imageFileList1.clear();
        for (int i = 0; i < returnValue.size(); i++) {
            File file = new File(returnValue.get(i));
            imageFileList1.add(file);
        }
        Bitmap bitmap = new BitmapDrawable(getActivity().getResources(), imageFileList1.get(0).getAbsolutePath()).getBitmap();
        picFile = imageFileList1.get(0);
        dehaati_img.setImageBitmap(bitmap);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case PermUtil.REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    AppUtils.imageSelection(this, 1);
                } else {
                }
                return;
            }
        }

    }

    private void getStateList() {
        stateList.clear();
        AppUtils.showProgressDialog(getActivity());
        AppRestClient client = AppRestClient.getInstance();
        Call<GetAddressMapping> call = client.getState();

        call.enqueue(new ApiCallback<GetAddressMapping>() {

            @Override
            public void onResponse(Response<GetAddressMapping> response) throws JSONException {
                AppUtils.hideProgressDialog();
                if (response.body() == null)
                    return;
                GetAddressMapping.GetData getData = new GetAddressMapping.GetData();
                getData.setId(0);
                getData.setName(UrlConstant.SELECT);
                stateNames = response.body().getGetAddress();
                stateNames.add(0, getData);
                for (int i = 0; i < stateNames.size(); i++) {
                    stateList.add(stateNames.get(i).getName());
                }
                if (getActivity() != null && isAdded()) {
                    ArrayAdapter<String> adp = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, stateList);
                    adp.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    state.setAdapter(adp);
                    if (AppPreference.getInstance().getDEHAAT_STATE().size() != 0) {
                        int id = getSelectedId(stateNames, Long.valueOf(AppPreference.getInstance().getDEHAAT_STATE().get(0)));
                        state.setSelection(id);
                    }
                }
            }

            @Override
            public void onResponse401(Response<GetAddressMapping> response) throws JSONException {

            }

        });
    }

    private void getDistrictList(int id) {
        districtList.clear();
        AppUtils.showProgressDialog(getActivity());
        AppRestClient client = AppRestClient.getInstance();
        Call<GetAddressMapping> call = client.getDistrict(id);
        call.enqueue(new ApiCallback<GetAddressMapping>() {

            @Override
            public void onResponse(Response<GetAddressMapping> response) throws JSONException {
                AppUtils.hideProgressDialog();
                if (response.body() == null)
                    return;
                GetAddressMapping.GetData getData = new GetAddressMapping.GetData();
                getData.setId(0);
                getData.setName(UrlConstant.SELECT);
                districtNames = response.body().getGetAddress();
                districtNames.add(0, getData);
                for (int i = 0; i < districtNames.size(); i++) {
                    districtList.add(districtNames.get(i).getName());
                }
                if (getActivity() != null && isAdded()) {
                    ArrayAdapter<String> districtAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, districtList);
                    districtAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    district.setAdapter(districtAdapter);
                    if (AppPreference.getInstance().getDEHAAT_DISTRICT().size() != 0) {
                        int id = getSelectedId(districtNames, Long.valueOf(AppPreference.getInstance().getDEHAAT_DISTRICT().get(0)));
                        district.setSelection(id);
                    }
                }
            }

            @Override
            public void onResponse401(Response<GetAddressMapping> response) throws JSONException {

            }


        });
    }

    private void getBlockList(int id) {
        blockList.clear();
        AppUtils.showProgressDialog(getActivity());
        AppRestClient client = AppRestClient.getInstance();
        Call<GetAddressMapping> call = client.getBlock(id);

        call.enqueue(new ApiCallback<GetAddressMapping>() {

            @Override
            public void onResponse(Response<GetAddressMapping> response) throws JSONException {
                AppUtils.hideProgressDialog();
                if (response.body() == null)
                    return;
                GetAddressMapping.GetData getData = new GetAddressMapping.GetData();
                getData.setId(0);
                getData.setName(UrlConstant.SELECT);
                blockNames = response.body().getGetAddress();
                blockNames.add(0, getData);
                for (int i = 0; i < blockNames.size(); i++) {
                    blockList.add(blockNames.get(i).getName());
                }
                if (getActivity() != null && isAdded()) {
                    ArrayAdapter<String> adp = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, blockList);
                    adp.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    block.setAdapter(adp);
                    if (AppPreference.getInstance().getDEHAAT_BLOCK().size() != 0) {
                        int id = getSelectedId(blockNames, Long.valueOf(AppPreference.getInstance().getDEHAAT_BLOCK().get(0)));
                        block.setSelection(id);
                    }
                }
            }

            @Override
            public void onResponse401(Response<GetAddressMapping> response) throws JSONException {

            }


        });
    }

    private int getSelectedId(ArrayList<GetAddressMapping.GetData> blockNames, Long sid) {
        int id = 0;
        for (int i = 0; i < blockNames.size(); i++) {

            if (blockNames.get(i).getId() == sid) {
                return i;
            }
        }
        return id;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch (parent.getId()) {
            case R.id.state:
                state_id = stateNames.get(position).getId();
                if (state_id != 0)
                    getDistrictList(state_id);
                break;

            case R.id.district:
                district_id = districtNames.get(position).getId();
                if (district_id != 0)
                    getBlockList(district_id);
                break;
            case R.id.block:
                block_id = blockNames.get(position).getId();
                break;

        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
