package com.intspvt.app.dehaat2.rest.response;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by DELL on 8/30/2017.
 */

public class ProductsList {
    @SerializedName("message")
    private String message;

    @SerializedName("data")
    private ArrayList<Products> products;
    @SerializedName("status")
    private String status;


    public ArrayList<Products> getProducts() {
        return products;
    }

    public String getMessage() {
        return message;
    }

    public String getStatus() {
        return status;
    }
}
