package com.intspvt.app.dehaat2.rest.response;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class TopProducts {
    @SerializedName("data")
    private ArrayList<Products> products;

    public ArrayList<Products> getProducts() {
        return products;
    }

    public class Products {
        @SerializedName("id")
        private Long productId;

        @SerializedName("display_name")
        private String productName;
        @SerializedName("list_price")
        private Float list_price;

        @SerializedName("image")
        private String image;

        @SerializedName("product_tmpl_id")
        private List<String> product_tmpl_id;

        public String getImage() {
            return image;
        }

        public Float getList_price() {
            return list_price;
        }

        public Long getProductId() {
            return productId;
        }

        public String getProductName() {
            return productName;
        }

        public List<String> getProduct_tmpl_id() {
            return product_tmpl_id;
        }
    }
}
