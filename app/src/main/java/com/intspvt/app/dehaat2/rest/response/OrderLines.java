package com.intspvt.app.dehaat2.rest.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by DELL on 9/26/2017.
 */

public class OrderLines implements Serializable {
    @SerializedName("id")
    private int id;

    @SerializedName("name")
    private String engName;

    @SerializedName("state")
    private String state;
//    @SerializedName("product_variant")
//    private String product_variant;


    @SerializedName("price_total")
    private float price_total;

    @SerializedName("product_id")
    private int product_id;

    @SerializedName("product_uom_qty")
    private float product_uom_qty;

    @SerializedName("product_uom")
    private List uom;

    public String getEngName() {
        return engName;
    }

    public int getId() {
        return id;
    }

    public String getState() {
        return state;
    }

    public float getPrice_total() {
        return price_total;
    }

    public float getProduct_uom_qty() {
        return product_uom_qty;
    }

}
