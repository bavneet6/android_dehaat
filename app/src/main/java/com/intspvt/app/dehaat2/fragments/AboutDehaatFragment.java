package com.intspvt.app.dehaat2.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.intspvt.app.dehaat2.R;
import com.intspvt.app.dehaat2.activity.MainActivity;

/**
 * Created by DELL on 11/15/2017.
 */

public class AboutDehaatFragment extends BaseFragment {
    public static final String TAG = AboutDehaatFragment.class.getSimpleName();

    public static AboutDehaatFragment newInstance() {
        return new AboutDehaatFragment();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_about_dehaat, null);
        showActionBar(true);
        ((MainActivity) activity).showDrawer(false);
        ((MainActivity) activity).showCart(false);
        loadText(v);
        return v;
    }

    private void loadText(View v) {
        ((MainActivity) activity).setTitle(getString(R.string.about_dehaat));
    }
}
