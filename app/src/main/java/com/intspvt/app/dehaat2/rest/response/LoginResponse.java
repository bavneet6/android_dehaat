package com.intspvt.app.dehaat2.rest.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by DELL on 7/27/2017.
 */

public class LoginResponse {
    @SerializedName("auth_token")
    private String auth_token;
    @SerializedName("app_view")
    private int app_view;
    @SerializedName("message")
    private String message;
    @SerializedName("status")
    private String status;

    public int getApp_view() {
        return app_view;
    }

    public String getAuth_token() {
        return auth_token;
    }

    public String getMessage() {
        return message;
    }

    public String getStatus() {
        return status;
    }

}
