package com.intspvt.app.dehaat2.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.intspvt.app.dehaat2.R;
import com.intspvt.app.dehaat2.activity.MainActivity;
import com.intspvt.app.dehaat2.adapter.RecyclerAdapterLedgerPurchaseHistory;
import com.intspvt.app.dehaat2.adapter.RecyclerAdapterLedgerSaleHistory;
import com.intspvt.app.dehaat2.rest.response.InputSellingResponse;
import com.intspvt.app.dehaat2.rest.response.OrderDataHistory;
import com.intspvt.app.dehaat2.rest.response.PnLModel;
import com.intspvt.app.dehaat2.viewmodel.FarmerSaleHistoryViewModel;
import com.intspvt.app.dehaat2.viewmodel.OrderHistoryViewModel;

import java.util.ArrayList;
import java.util.List;

public class CustomDateLedgerFragment extends BaseFragment implements View.OnClickListener {
    public static final String TAG = CustomDateLedgerFragment.class.getSimpleName();
    private TextView purchase_price, sale_price, pnl_price, no_data_mssg, no_ledger_data_mssg;
    private RecyclerView transactions_list;
    private String order_from, order_to;
    private RecyclerAdapterLedgerPurchaseHistory recyclerAdapterLedgerPurchaseHistory;
    private RecyclerAdapterLedgerSaleHistory recyclerAdapterLedgerSaleHistory;
    private PnLModel.PnL pnLData;
    private OrderHistoryViewModel orderHistoryViewModel;
    private FarmerSaleHistoryViewModel farmerSaleHistoryViewModel;
    private RecyclerView.LayoutManager layoutManager;
    private List<OrderDataHistory> orderHistoryList = new ArrayList<>();
    private ArrayList<InputSellingResponse.Dehaat_farmer> inputSellingData = new ArrayList<>();
    private LinearLayout salesBack, purchaseBack;

    public static CustomDateLedgerFragment newInstance() {
        return new CustomDateLedgerFragment();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_custom_date_ledger, null);
        showActionBar(true);
        ((MainActivity) activity).showDrawer(false);
        ((MainActivity) activity).showCart(false);

        purchase_price = v.findViewById(R.id.purchase_price);
        sale_price = v.findViewById(R.id.sale_price);
        pnl_price = v.findViewById(R.id.pnl_price);
        salesBack = v.findViewById(R.id.salesBack);
        purchaseBack = v.findViewById(R.id.purchaseBack);

        no_data_mssg = v.findViewById(R.id.no_data_mssg);
        no_ledger_data_mssg = v.findViewById(R.id.no_ledger_data_mssg);
        transactions_list = v.findViewById(R.id.transactions_list);
        transactions_list.setNestedScrollingEnabled(false);

        salesBack.setOnClickListener(this);
        purchaseBack.setOnClickListener(this);

        Bundle bundle = getArguments();
        if (bundle != null) {
            no_ledger_data_mssg.setVisibility(View.GONE);
            pnLData = (PnLModel.PnL) bundle.getSerializable("DATA");
            order_from = bundle.getString("DATE_FROM");
            order_to = bundle.getString("DATE_TO");
        } else
            no_ledger_data_mssg.setVisibility(View.VISIBLE);
        loadText(v);
        return v;
    }

    private void loadText(View v) {
        ((MainActivity) activity).setTitle(getString(R.string.ledger));
        if (pnLData != null) {
            purchase_price.setText(getString(R.string.rs) + pnLData.getPurchase().getTotal_purchase_amount());
            sale_price.setText(getString(R.string.rs) + pnLData.getSale().getTotal_sale_amount());
            pnl_price.setText(getString(R.string.rs) + pnLData.getPnl().getTotal_pnl_amount());
        }
        purchaseBack.callOnClick();
    }

    private void getPurchaseData(String key) {
        orderHistoryViewModel = ViewModelProviders.of(getActivity()).
                get(OrderHistoryViewModel.class);
        orderHistoryViewModel.init(key, order_from, order_to);
        observeHistoryViewModel(orderHistoryViewModel);
    }

    private void getSaleData(String key) {
        farmerSaleHistoryViewModel = ViewModelProviders.of(getActivity()).get
                (FarmerSaleHistoryViewModel.class);
        farmerSaleHistoryViewModel.init(key, order_from, order_to);
        observeSaleViewModel(farmerSaleHistoryViewModel);
    }

    private void observeSaleViewModel(FarmerSaleHistoryViewModel viewModel) {
        viewModel.getHistoryData().observe(this, inputData -> {
            if (inputData != null && inputData.size() > 0) {
                inputSellingData.clear();
                no_data_mssg.setVisibility(View.GONE);
                inputSellingData.addAll(inputData);
                printSaleData();
            } else {
                no_data_mssg.setVisibility(View.VISIBLE);
            }
        });
    }

    private void observeHistoryViewModel(OrderHistoryViewModel viewModel) {
        viewModel.getHistoryData().observe(this, orderDataHistories -> {
            if (orderDataHistories != null && orderDataHistories.size() > 0) {
                orderHistoryList.clear();
                no_data_mssg.setVisibility(View.GONE);
                orderHistoryList.addAll(orderDataHistories);
                printData();
            } else {
                no_data_mssg.setVisibility(View.VISIBLE);
            }
        });
    }

    private void printData() {
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerAdapterLedgerPurchaseHistory = new RecyclerAdapterLedgerPurchaseHistory(
                getActivity(), orderHistoryList);
        transactions_list.setLayoutManager(layoutManager);
        transactions_list.setAdapter(recyclerAdapterLedgerPurchaseHistory);
    }

    private void printSaleData() {
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerAdapterLedgerSaleHistory = new RecyclerAdapterLedgerSaleHistory(
                getActivity(), inputSellingData);
        transactions_list.setLayoutManager(layoutManager);
        transactions_list.setAdapter(recyclerAdapterLedgerSaleHistory);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.salesBack:
                getSaleData(null);
                showBoxSelected(salesBack, purchaseBack);
                break;
            case R.id.purchaseBack:
                getPurchaseData(null);
                showBoxSelected(purchaseBack, salesBack);
                break;
        }

    }

    private void showBoxSelected(LinearLayout l1, LinearLayout l2) {
        l1.setBackground(getActivity().getResources().getDrawable(R.drawable.card_back_selected));
        l2.setBackground(getActivity().getResources().getDrawable(R.drawable.card_back));
    }
}