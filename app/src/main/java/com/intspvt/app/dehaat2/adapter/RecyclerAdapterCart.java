package com.intspvt.app.dehaat2.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.intspvt.app.dehaat2.R;
import com.intspvt.app.dehaat2.activity.MainActivity;
import com.intspvt.app.dehaat2.database.DatabaseHandler;
import com.intspvt.app.dehaat2.rest.PicassoCallback;
import com.intspvt.app.dehaat2.rest.response.AddToCart;
import com.intspvt.app.dehaat2.utilities.AppUtils;
import com.intspvt.app.dehaat2.utilities.OnItemClick;
import com.squareup.picasso.Picasso;

import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;


/**
 * Created by DELL on 8/2/2017.
 */

public class RecyclerAdapterCart extends RecyclerView.Adapter<RecyclerAdapterCart.Products> {
    private ArrayList<AddToCart> products;
    private Context context;
    private float total_val;
    private int productQty;
    private HashMap<Integer, Float> hashMap = new HashMap<>();
    private DatabaseHandler databaseHandler;
    private AlertDialog.Builder builder;
    private OnItemClick onItemClick;
    private String getPic;


    public RecyclerAdapterCart(ArrayList<AddToCart> products, OnItemClick callback) {
        this.products = products;
        this.onItemClick = callback;
    }


    @Override
    public RecyclerAdapterCart.Products onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.template_cart_layout, parent, false);
        context = parent.getContext();
        return new RecyclerAdapterCart.Products(itemView);

    }

    @Override
    public void onBindViewHolder(final RecyclerAdapterCart.Products holder, final int position) {
        databaseHandler = new DatabaseHandler(context);
        final int pos = holder.getAdapterPosition();
        holder.setIsRecyclable(false);
        if (!AppUtils.isNullCase(products.get(pos).getVariant()))
            holder.name.setText(products.get(pos).getProductName() + " / " + products.get(pos).getVariant());
        else
            holder.name.setText(products.get(pos).getProductName());
        holder.value.setText("" + products.get(pos).getProductQty());
        holder.mrp.setText("" + products.get(pos).getProductMrp());
        int m = products.get(pos).getProductQty();
        final float total = m * products.get(pos).getProductMrp();
        total_val = total_val + total;
        holder.total_price.setText(context.getString(R.string.rs) + total);
        final String photo = products.get(pos).getProductImage();
        getPic = null;

        if (!AppUtils.isNullCase(photo)) {
            getPic = databaseHandler.getFileUrl(photo);
            if (getPic != null) {
                final Picasso picasso = Picasso.with(context.getApplicationContext());
                picasso.setLoggingEnabled(true);
                picasso.load(getPic).into(holder.image, new PicassoCallback(photo) {

                    @Override
                    public void onErrorPic(URL url) {
                        picasso.load("" + url).error(R.drawable.no_product_image).into(holder.image);
                    }
                });
            }
        }

        // hashmap is used to put the total price and send callback to the cart fragment to print the total price
        hashMap.put(pos, total);
        onItemClick.onClick(hashMap);
        holder.minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (holder.value.getText().toString().equals("")) {
                    holder.value.setText("0");
                } else {
                    productQty = Integer.parseInt(holder.value.getText().toString());
                    if (productQty == 0) {

                    } else if (productQty == 1) {
                        deleteItem(products.get(pos).getProductId(), 0, products.get(pos).getProductMrp(), pos);
                    } else {
                        --productQty;
                        holder.value.setText("" + productQty);
                    }
                }
            }
        });
        holder.plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (holder.value.getText().toString().equals("")) {
                    holder.value.setText("1");
                } else {
                    productQty = Integer.parseInt(holder.value.getText().toString());
                    ++productQty;
                    holder.value.setText("" + productQty);
                }


            }
        });
        holder.plus.setTag(pos);
        holder.minus.setTag(pos);
        holder.value.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (holder.value.getText().toString().equals("")) {

                } else {
                    productQty = Integer.parseInt(holder.value.getText().toString());
                    float ii = productQty * products.get(pos).getProductMrp();
                    holder.total_price.setText(context.getString(R.string.rs) + ii);

                    // if quantity is changed in the cart fragment the total price should be reflected in the cart fragment so broadcast reciever
                    // is used to send the total price
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!holder.value.getText().toString().equals("")) {
                    AddToCart contact = new AddToCart();
                    contact.setProductId("" + products.get(pos).getProductId());
                    contact.setProductName(products.get(pos).getProductName());
                    contact.setProductMrp(products.get(pos).getProductMrp());
                    contact.setProductQty(Integer.parseInt(holder.value.getText().toString()));
                    float a = Integer.parseInt(holder.value.getText().toString()) * products.get(pos).getProductMrp();
                    contact.setTotalPrice(a);
                    contact.setProductImage(products.get(pos).getProductImage());
                    contact.setVariant("");
                    databaseHandler.insertCartData(false, contact);
                    ((MainActivity) context).showCartCount(true);
                    updateList(products, Integer.parseInt(holder.value.getText().toString()), products.get(pos).getProductMrp(), pos);
                }

            }
        });
        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteItem(products.get(pos).getProductId(), 0, products.get(pos).getProductMrp(), pos);
            }
        });

    }

    public void updateList(ArrayList<AddToCart> productsList, int value, Float mrp, int pos) {
        float totalPrice = value * mrp;
        hashMap.put(pos, totalPrice);
        onItemClick.onClick(hashMap);
        productsList = databaseHandler.getAllCartData();
        this.products = productsList;
        notifyDataSetChanged();
        hashMap.clear();

    }

    @Override
    public int getItemCount() {
        return products.size();
    }

    private void deleteItem(final String id, final int value, final Float mrp, final int pos) {
        builder = new AlertDialog.Builder(context);
        builder.setTitle(context.getString(R.string.delete))
                .setMessage(context.getString(R.string.delete_text))
                .setCancelable(false)
                .setPositiveButton(context.getString(R.string.okText), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        databaseHandler.deleteProductData(id);
                        updateList(products, value, mrp, pos);

                    }
                })
                .setNegativeButton(context.getString(R.string.cancelText), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .setIcon(android.R.drawable.ic_menu_delete)
                .show();
    }

    public class Products extends RecyclerView.ViewHolder {
        private TextView name, mrp, total_price;
        private ImageView image, plus, minus, delete;
        private EditText value;

        public Products(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.productName);
            mrp = itemView.findViewById(R.id.productMrp);
            total_price = itemView.findViewById(R.id.total_price);
            image = itemView.findViewById(R.id.image);
            plus = itemView.findViewById(R.id.plus);
            delete = itemView.findViewById(R.id.delete);
            minus = itemView.findViewById(R.id.minus);
            value = itemView.findViewById(R.id.value);

        }


    }

}
