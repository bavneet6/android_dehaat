package com.intspvt.app.dehaat2.rest.response;

import com.google.gson.annotations.SerializedName;

public class Inventory {
    @SerializedName("data")
    public Data data;

    public Data getData() {
        return data;
    }

    public class Data {
        @SerializedName("inventory_value")
        public Float inventory;

        public Float getInventory() {
            return inventory;
        }
    }
}
