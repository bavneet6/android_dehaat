package com.intspvt.app.dehaat2.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.intspvt.app.dehaat2.R;
import com.intspvt.app.dehaat2.activity.MainActivity;
import com.intspvt.app.dehaat2.adapter.RecyclerAdapterInputSellingModify;
import com.intspvt.app.dehaat2.rest.ApiCallback;
import com.intspvt.app.dehaat2.rest.AppRestClient;
import com.intspvt.app.dehaat2.rest.response.InputSellingData;
import com.intspvt.app.dehaat2.rest.response.InputSellingResponse;
import com.intspvt.app.dehaat2.rest.response.SaleLines;
import com.intspvt.app.dehaat2.utilities.AppPreference;
import com.intspvt.app.dehaat2.utilities.AppUtils;
import com.intspvt.app.dehaat2.utilities.InputSellingUpdate;
import com.intspvt.app.dehaat2.utilities.ValueUpdate;

import org.json.JSONException;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Response;

public class ModifyDetailsFragment extends BaseFragment implements View.OnClickListener,
        InputSellingUpdate, ValueUpdate {
    public static final String TAG = ModifyDetailsFragment.class.getSimpleName();
    private RecyclerView recyclerView;
    private FirebaseAnalytics firebaseAnalytics;
    private InputSellingResponse.Dehaat_farmer dehaat_farmer_data;
    private ArrayList<SaleLines> inputSellingList = new ArrayList<>();
    private RecyclerView.LayoutManager layoutManager;
    private TextView totalItems, totalPrice, addProducts;
    private ArrayList<SaleLines> salesLinesArraylist = new ArrayList<>();
    private LinearLayout proceedToSell, sellingBack;
    private FrameLayout frameLayout;
    private float total_amt;

    public static ModifyDetailsFragment newInstance() {
        return new ModifyDetailsFragment();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_modify_details, null);
        showActionBar(true);
        ((MainActivity) activity).showDrawer(false);
        ((MainActivity) activity).showCart(false);
        ((MainActivity) activity).setTitle(getString(R.string.agri_input));
        firebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        recyclerView = v.findViewById(R.id.cart_list);
        totalItems = v.findViewById(R.id.totalItems);
        totalPrice = v.findViewById(R.id.totalPrice);
        proceedToSell = v.findViewById(R.id.proceedToSell);
        sellingBack = v.findViewById(R.id.selling_back);
        frameLayout = v.findViewById(R.id.frag_container);
        addProducts = v.findViewById(R.id.addProducts);
        recyclerView.setNestedScrollingEnabled(false);
        Bundle bundle = getArguments();
        if (bundle != null) {
            dehaat_farmer_data = (InputSellingResponse.Dehaat_farmer) bundle.getSerializable("DATA");
            salesLinesArraylist = dehaat_farmer_data.getSale_lines();
            for (int i = 0; i < salesLinesArraylist.size(); i++) {
                if (salesLinesArraylist.get(i).getIs_deleted() == null)
                    salesLinesArraylist.get(i).setIs_deleted(false);
            }
            displaySellingCartData(salesLinesArraylist);
        }
        proceedToSell.setOnClickListener(this);
        addProducts.setOnClickListener(this);
        printPriceAndItems();
        return v;

    }

    private void printPriceAndItems() {
        ArrayList<SaleLines> sales_lines = salesLinesArraylist;
        total_amt = 0;
        int count = 0;
        for (int i = 0; i < sales_lines.size(); i++) {
            if (!sales_lines.get(i).getIs_deleted()) {
                count = count + 1;
                float total = sales_lines.get(i).getProduct_price() *
                        sales_lines.get(i).getProduct_qty();
                total_amt = total_amt + total;
            }
        }
        totalPrice.setText(getString(R.string.rs) + total_amt);
        totalItems.setText("   " + getString(R.string.total_items) + count);
    }

    private void displaySellingCartData(ArrayList<SaleLines> sale_lines) {
        recyclerView.setAdapter(new RecyclerAdapterInputSellingModify(sale_lines,
                this, this));
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
    }

    private void prepareData() {
        if (salesLinesArraylist.size() != 0) {
            for (int i = 0; i < salesLinesArraylist.size(); i++) {
                SaleLines saleLines = new SaleLines();
                saleLines.setProduct_id(salesLinesArraylist.get(i).getProduct_id());
                saleLines.setProduct_price(salesLinesArraylist.get(i).getProduct_price());
                saleLines.setProduct_qty(salesLinesArraylist.get(i).getProduct_qty());
                saleLines.setIs_deleted(salesLinesArraylist.get(i).getIs_deleted());
                saleLines.setId(salesLinesArraylist.get(i).getId());
                inputSellingList.add(saleLines);
            }
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.proceedToSell:
                prepareData();
                sendDataToServer();
                break;
            case R.id.addProducts:
                prepareData();
                frameLayout.setVisibility(View.VISIBLE);
                Bundle bundle = new Bundle();
                bundle.putSerializable("DATA", dehaat_farmer_data);
                InputSellingProductFragment fragment = InputSellingProductFragment.newInstance();
                fragment.setArguments(bundle);
                FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.frag_container, fragment).commit();
                break;

        }
    }

    private void sendDataToServer() {
        AppUtils.showProgressDialog(activity);
        AppRestClient client = AppRestClient.getInstance();
        InputSellingData inputSellingData = new InputSellingData();
        inputSellingData.setFarmer_no(dehaat_farmer_data.getCustomer_mobile());
        inputSellingData.setSale_lines(inputSellingList);
        inputSellingData.setState(dehaat_farmer_data.getState());
        inputSellingData.setTotal_amt(total_amt);
        inputSellingData.setId(Long.valueOf(dehaat_farmer_data.getId()));
        inputSellingData.setInventory_status(dehaat_farmer_data.getInventory_status());
        inputSellingData.setName(dehaat_farmer_data.getName());
        com.intspvt.app.dehaat2.rest.body.InputSelling inputSelling = new com.intspvt.app.dehaat2.rest.body.InputSelling(inputSellingData);
        Call<Void> call = client.updateDehaatFarmerSale(inputSelling);
        call.enqueue(new ApiCallback<Void>() {
            @Override
            public void onResponse(Response<Void> response) {
                AppUtils.hideProgressDialog();
                if (response.code() == 200) {
                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                    fragmentManager.beginTransaction().replace(R.id.frag_container, InputSellingHistroryFragment.newInstance())
                            .addToBackStack("").commitAllowingStateLoss();
                }
            }

            @Override
            public void onResponse401(Response<Void> response) throws JSONException {
                AppUtils.showSessionExpiredDialog(getActivity());
            }
        });

    }

    private void showBottomBanner() {
        if (dehaat_farmer_data.getSale_lines().size() == 0)
            sellingBack.setVisibility(View.GONE);
        else
            sellingBack.setVisibility(View.VISIBLE);

    }


    @Override
    public void onPause() {
        super.onPause();
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
        Bundle params = new Bundle();
        params.putString("modify_dehaat_farmer_sale", AppPreference.getInstance().getDEHAATI_MOBILE());
        firebaseAnalytics.setUserProperty("Mobile", AppPreference.getInstance().getDEHAATI_MOBILE());
        firebaseAnalytics.logEvent("modify_dehaat_farmer_sale", params);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onItemClick(Integer pos, String s) {
        int count = 0;
        if (s.equals("true")) {
            dehaat_farmer_data.getSale_lines().get(pos).setIs_deleted(true);
        }
        for (int i = 0; i < dehaat_farmer_data.getSale_lines().size(); i++) {
            if (dehaat_farmer_data.getSale_lines().get(i).getIs_deleted()) {
                count++;
            }
        }
        if (count == dehaat_farmer_data.getSale_lines().size()) {
            proceedToSell.setVisibility(View.GONE);
        } else {
            proceedToSell.setVisibility(View.VISIBLE);
        }
        displayData();
    }

    @Override
    public void onUpdate(Integer pos, String s) {
        dehaat_farmer_data.getSale_lines().get(pos).setProduct_qty(Integer.parseInt(s));
        if (dehaat_farmer_data.getSale_lines() != null && dehaat_farmer_data.getSale_lines().size() > 0) {
            displayData();
        } else {
            proceedToSell.setVisibility(View.GONE);
        }
    }

    public void updateDeHaatFarmerData(InputSellingResponse.Dehaat_farmer data) {
//        this.dehaat_farmer_data = data;
    }

    private void displayData() {
        displaySellingCartData(dehaat_farmer_data.getSale_lines());
        printPriceAndItems();
        showBottomBanner();
    }
}
