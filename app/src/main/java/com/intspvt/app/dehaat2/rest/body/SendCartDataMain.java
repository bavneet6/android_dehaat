package com.intspvt.app.dehaat2.rest.body;

import com.google.gson.annotations.SerializedName;

/**
 * Created by DELL on 3/16/2018.
 */

public class SendCartDataMain {
    @SerializedName("data")
    public SendCartData sendCartData;

    public SendCartDataMain(SendCartData sendCartData) {
        this.sendCartData = sendCartData;
    }
}
