package com.intspvt.app.dehaat2.fragments;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.SearchView;
import android.widget.Spinner;

import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.intspvt.app.dehaat2.R;
import com.intspvt.app.dehaat2.activity.MainActivity;
import com.intspvt.app.dehaat2.adapter.RecyclerAdapterProductList;
import com.intspvt.app.dehaat2.rest.response.Products;
import com.intspvt.app.dehaat2.utilities.AppPreference;
import com.intspvt.app.dehaat2.utilities.AppUtils;
import com.intspvt.app.dehaat2.utilities.UrlConstant;
import com.intspvt.app.dehaat2.viewmodel.ProductListViewModel;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by DELL on 11/15/2017.
 */

public class ProductListFragment extends BaseFragment implements SearchView.OnQueryTextListener, AdapterView.OnItemSelectedListener {
    public static final String TAG = ProductListFragment.class.getSimpleName();
    private RecyclerView recyclerView;
    private RecyclerAdapterProductList recyclerAdapterProductList;
    private RecyclerView.LayoutManager layoutManager;
    private ArrayList<Products> sPro = new ArrayList<>();
    private ArrayList<Products> fPro = new ArrayList<>();
    private ArrayList<Products> cPro = new ArrayList<>();
    private ArrayList<Products> data = new ArrayList<>();
    private ArrayList<String> categoriesList = new ArrayList<>();
    private Spinner category;
    private String selected = "";
    private String format2, format1, diffTime;
    private FirebaseAnalytics mFirebaseAnalytics;
    private SearchView searchView;
    private ProductListViewModel productListViewModel;

    public static ProductListFragment newInstance() {
        return new ProductListFragment();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_product_list, null);
        showActionBar(true);
        ((MainActivity) activity).showDrawer(false);
        ((MainActivity) activity).showCart(true);
        ((MainActivity) activity).setTitle(getString(R.string.agri_input));
        category = v.findViewById(R.id.category);
        recyclerView = v.findViewById(R.id.products_list);
        recyclerView.setNestedScrollingEnabled(false);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        searchView = v.findViewById(R.id.search);
        searchView.setOnQueryTextListener(this);
        category.setOnItemSelectedListener(this);
        categoriesList.clear();
        categoriesList.add(getString(R.string.all));
        categoriesList.add(getString(R.string.seed));
        categoriesList.add(getString(R.string.fertilizer));
        categoriesList.add(getString(R.string.protection));
        try {
            ArrayAdapter<String> adp = new ArrayAdapter<String>(getActivity(), R.layout.template_spinner_text, categoriesList);
            adp.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            category.setAdapter(adp);
        } catch (ActivityNotFoundException ac) {
        }
        searchView.setOnClickListener(v1 -> searchView.setIconified(false));
        ((MainActivity) activity).showCartCount(false);
        AppUtils.showProgressDialog(getActivity());
        printData();
        productListViewModel = ViewModelProviders.of(getActivity()).get(ProductListViewModel.class);
        productListViewModel.init();
        observeViewModel(productListViewModel);

        return v;

    }


    private void observeViewModel(ProductListViewModel viewModel) {
        viewModel.getProductData()
                .observe(this, products -> {
                    sPro = new ArrayList<>();
                    fPro = new ArrayList<>();
                    cPro = new ArrayList<>();
                    AppUtils.hideProgressDialog();
                    data.clear();
                    if (products == null)
                        return;
                    data.addAll(products);
                    for (int i = 0; i < data.size(); i++) {
                        if (data.get(i).getCategory() != null) {
                            if (data.get(i).getCategory().equals(UrlConstant.SEED))
                                sPro.add(data.get(i));
                            else if (data.get(i).getCategory().equals(UrlConstant.FERT))
                                fPro.add(data.get(i));
                            else if (data.get(i).getCategory().equals(UrlConstant.CROP))
                                cPro.add(data.get(i));
                        }
                    }
                    printData();
                });
    }

    private void printData() {
        if (getActivity() != null && isAdded()) {
            if (selected.equals(""))
                return;
            if (selected.equals(getString(R.string.all))) {
                recyclerAdapterProductList = new RecyclerAdapterProductList(getActivity(), data);
            } else if (selected.equals(getString(R.string.seed))) {
                recyclerAdapterProductList = new RecyclerAdapterProductList(getActivity(), sPro);

            } else if (selected.equals(getString(R.string.fertilizer))) {
                recyclerAdapterProductList = new RecyclerAdapterProductList(getActivity(), fPro);

            } else if (selected.equals(getString(R.string.protection))) {
                recyclerAdapterProductList = new RecyclerAdapterProductList(getActivity(), cPro);

            }
            layoutManager = new LinearLayoutManager(getActivity());
            recyclerView.setLayoutManager(layoutManager);
            recyclerView.setAdapter(recyclerAdapterProductList);

            if (!searchView.isIconified()) {
                searchView.setQuery("", false);
            }
        }

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch (parent.getId()) {

            case R.id.category:
                selected = parent.getItemAtPosition(position).toString();
                printData();
                break;

        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if (data.size() != 0) {
            recyclerAdapterProductList.getFilter().filter(newText);
        }
        return false;
    }

    @Override
    public void onPause() {
        super.onPause();
        Bundle params = new Bundle();
        SimpleDateFormat s = new SimpleDateFormat("hh:mm:ss");
        format2 = s.format(new Date());
        diffTime = AppUtils.showTimeDiff(format1, format2, diffTime);
        params.putString("productList", diffTime + "   " + AppPreference.getInstance().getDEHAATI_MOBILE());
        mFirebaseAnalytics.setUserProperty("Mobile", AppPreference.getInstance().getDEHAATI_MOBILE());
        mFirebaseAnalytics.logEvent("ProductList", params);
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
    }

    @Override
    public void onResume() {
        super.onResume();
        SimpleDateFormat s = new SimpleDateFormat("hh:mm:ss");
        format1 = s.format(new Date());
    }

}
