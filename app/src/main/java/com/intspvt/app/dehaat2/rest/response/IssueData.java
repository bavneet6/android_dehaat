package com.intspvt.app.dehaat2.rest.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by DELL on 3/27/2018.
 */

public class IssueData {
    @SerializedName("description")
    private String description;

    @SerializedName("category")
    private String category;
    @SerializedName("issue_id")
    private int issue_id;
    @SerializedName("kanban_state")
    private String kanban_state;
    @SerializedName("farmer_number")
    private String farmer_num;


    public void setIssue_id(int issue_id) {
        this.issue_id = issue_id;
    }

    public void setKanban_state(String kanban_state) {
        this.kanban_state = kanban_state;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setFarmer_num(String farmer_num) {
        this.farmer_num = farmer_num;
    }
}
