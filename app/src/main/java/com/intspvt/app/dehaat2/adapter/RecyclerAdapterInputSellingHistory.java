package com.intspvt.app.dehaat2.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.intspvt.app.dehaat2.R;
import com.intspvt.app.dehaat2.fragments.ModifyDetailsFragment;
import com.intspvt.app.dehaat2.rest.response.InputSellingResponse;
import com.intspvt.app.dehaat2.utilities.AppPreference;
import com.intspvt.app.dehaat2.utilities.AppUtils;
import com.intspvt.app.dehaat2.utilities.InputSellingUpdate;
import com.intspvt.app.dehaat2.utilities.UrlConstant;

import java.util.ArrayList;

import io.supercharge.shimmerlayout.ShimmerLayout;

public class RecyclerAdapterInputSellingHistory extends RecyclerView.Adapter<RecyclerAdapterInputSellingHistory.HistoryData> {
    private Context context;
    private ArrayList<InputSellingResponse.Dehaat_farmer> dehaat_farmerList;
    private ArrayList<InputSellingResponse.Dehaat_farmer> filteredNewList;
    private RecyclerView.LayoutManager layoutManager;
    private boolean newListAdd = false;
    private InputSellingUpdate inputSellingUpdate;

    public RecyclerAdapterInputSellingHistory(Activity context, ArrayList<InputSellingResponse.Dehaat_farmer>
            dehaat_farmerList, InputSellingUpdate inputSellingUpdate) {
        this.context = context;
        this.dehaat_farmerList = dehaat_farmerList;
        this.filteredNewList = dehaat_farmerList;
        this.inputSellingUpdate = inputSellingUpdate;
    }

    @Override
    public RecyclerAdapterInputSellingHistory.HistoryData onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.template_input_selling_history, parent, false);
        context = parent.getContext();
        return new RecyclerAdapterInputSellingHistory.HistoryData(itemView);

    }

    @Override
    public void onBindViewHolder(final RecyclerAdapterInputSellingHistory.HistoryData holder, final int position) {
        holder.setIsRecyclable(false);
        final int pos = holder.getAdapterPosition();
        holder.shimmerLayout.startShimmerAnimation();
        holder.date.setText(dehaat_farmerList.get(pos).getCreate_date());
        holder.name.setText(context.getString(R.string.order_no) +
                ": " + dehaat_farmerList.get(pos).getName());
        holder.state.setText(context.getString(R.string.order_status) +
                ": " + dehaat_farmerList.get(pos).getState());
        if (!AppUtils.isNullCase(dehaat_farmerList.get(pos).getCustomer_mobile())) {
            holder.customer_no.setText(context.getString(R.string.farmer_num) + "-" + dehaat_farmerList.get(pos).getCustomer_mobile());
            holder.customer_no.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:" + dehaat_farmerList.get(pos).getCustomer_mobile()));
                    if (intent.resolveActivity(context.getPackageManager()) != null) {
                        context.startActivity(intent);
                    }
                }
            });
        }
        holder.amt.setText(context.getString(R.string.rs) + dehaat_farmerList.get(pos).getAmount_total());
        holder.details.setAdapter(new RecyclerAdapterSaleLines(dehaat_farmerList.get(pos).getSale_lines()));
        layoutManager = new LinearLayoutManager(context);
        holder.details.setLayoutManager(layoutManager);

        if (dehaat_farmerList.get(pos).getState() != null && dehaat_farmerList.get(pos).getState().equals("delivered")) {
            holder.storage_inventory_check.setVisibility(View.GONE);
            holder.farmer_info_back.setVisibility(View.GONE);
            holder.not_available_back.setVisibility(View.GONE);
            holder.modify_details.setVisibility(View.GONE);
        } else {
            holder.modify_details.setVisibility(View.VISIBLE);
            if (dehaat_farmerList.get(pos).getInventory_status().equals("unknown")) {
                holder.storage_inventory_check.setVisibility(View.VISIBLE);
                holder.farmer_info_back.setVisibility(View.GONE);
                holder.not_available_back.setVisibility(View.GONE);
            } else if (dehaat_farmerList.get(pos).getInventory_status().equals("available")) {
                holder.storage_inventory_check.setVisibility(View.GONE);
                holder.farmer_info_back.setVisibility(View.VISIBLE);
                holder.not_available_back.setVisibility(View.GONE);
                if (AppPreference.getInstance().getLANGUAGE().equals(UrlConstant.ENGLISH)) {
                    holder.farmer_line.setText(String.format(context.getString(R.string.farmer_buy_line), (int) dehaat_farmerList.get(pos).getAmount_total()));
                } else {
                    holder.farmer_line.setText(String.format(context.getString(R.string.farmer_buy_line), (int) dehaat_farmerList.get(pos).getAmount_total()));
                }
            } else if (dehaat_farmerList.get(pos).getInventory_status().equals("not_available")) {
                holder.storage_inventory_check.setVisibility(View.GONE);
                holder.farmer_info_back.setVisibility(View.GONE);
                holder.not_available_back.setVisibility(View.VISIBLE);
            }
        }
        holder.yes_stock.setOnClickListener(view -> inputSellingUpdate.onItemClick(pos, "available"));
        holder.no_stock.setOnClickListener(view -> inputSellingUpdate.onItemClick(pos, "not_available"));

        holder.modify_details.setOnClickListener(view -> {
            Bundle bundle = new Bundle();
            bundle.putSerializable("DATA", dehaat_farmerList.get(pos));
            ModifyDetailsFragment fragment = ModifyDetailsFragment.newInstance();
            fragment.setArguments(bundle);
            AppUtils.changeFragment((FragmentActivity) context, fragment);
        });

        holder.confirm_delivery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.shimmerLayout.stopShimmerAnimation();
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle(R.string.confirm);
                builder.setMessage(R.string.confirm_delivery);
                builder.setCancelable(false);
                builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        inputSellingUpdate.onItemClick(pos, "delivered");
                    }
                });
                builder.setNegativeButton(R.string.cancel, (dialog, which) -> dialog.dismiss());
                AlertDialog alert = builder.create();
                alert.show();
            }
        });
    }


    @Override
    public int getItemCount() {
        return dehaat_farmerList.size();
    }

    public Filter getFilter() {

        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {

                String charString = charSequence.toString();

                if (charString.isEmpty()) {

                    dehaat_farmerList = filteredNewList;
                } else {
                    ArrayList<InputSellingResponse.Dehaat_farmer> filteredList = new ArrayList<>();
                    for (int i = 0; i < filteredNewList.size(); i++) {
                        newListAdd = (filteredNewList.get(i).getCustomer_mobile().contains(charString)) || (String.valueOf(filteredNewList.get(i).getAmount_total()).contains(charString));
                        for (int j = 0; j < filteredNewList.get(i).getSale_lines().size(); j++) {
                            if ((filteredNewList.get(i).getSale_lines().get(j).getProduct_name().toLowerCase().contains(charString.toLowerCase())))
                                newListAdd = true;
                        }
                        if (newListAdd)
                            filteredList.add(filteredNewList.get(i));

                    }
                    dehaat_farmerList = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = dehaat_farmerList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults
                    filterResults) {
                dehaat_farmerList = (ArrayList<InputSellingResponse.Dehaat_farmer>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }


    public class HistoryData extends RecyclerView.ViewHolder {
        private TextView date, customer_no, amt, no_stock,
                yes_stock, modify_details, name, state, farmer_line;
        private RecyclerView details;
        private LinearLayout storage_inventory_check, farmer_info_back, not_available_back;
        private TextView confirm_delivery;
        private ShimmerLayout shimmerLayout;


        public HistoryData(View itemView) {
            super(itemView);
            date = itemView.findViewById(R.id.date);
            customer_no = itemView.findViewById(R.id.farmer_no);
            amt = itemView.findViewById(R.id.amt_total);
            details = itemView.findViewById(R.id.details);
            storage_inventory_check = itemView.findViewById(R.id.storage_inventory_check);
            confirm_delivery = itemView.findViewById(R.id.confirm_delivery);
            farmer_info_back = itemView.findViewById(R.id.farmer_info_back);
            yes_stock = itemView.findViewById(R.id.yes_stock);
            no_stock = itemView.findViewById(R.id.no_stock);
            not_available_back = itemView.findViewById(R.id.not_available_back);
            shimmerLayout = itemView.findViewById(R.id.shimmer_text);
            modify_details = itemView.findViewById(R.id.modify_details);
            name = itemView.findViewById(R.id.name);
            state = itemView.findViewById(R.id.state);
            farmer_line = itemView.findViewById(R.id.farmer_line);
        }
    }
}
