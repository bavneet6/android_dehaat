package com.intspvt.app.dehaat2.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.FragmentTransaction;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.intspvt.app.dehaat2.Dehaat2;
import com.intspvt.app.dehaat2.R;
import com.intspvt.app.dehaat2.activity.LoginActivity;
import com.intspvt.app.dehaat2.activity.MainActivity;
import com.intspvt.app.dehaat2.database.DatabaseHandler;
import com.intspvt.app.dehaat2.rest.ApiCallback;
import com.intspvt.app.dehaat2.rest.AppRestClient;
import com.intspvt.app.dehaat2.rest.PicassoCallback;
import com.intspvt.app.dehaat2.rest.response.GetPersonalInfo;
import com.intspvt.app.dehaat2.utilities.AppPreference;
import com.intspvt.app.dehaat2.utilities.AppUtils;
import com.intspvt.app.dehaat2.utilities.UrlConstant;
import com.squareup.picasso.Picasso;

import org.json.JSONException;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Response;

public class MenuFragment extends BaseFragment implements View.OnClickListener {
    public static final String TAG = MenuFragment.class.getSimpleName();
    private LinearLayout langauageBack, input_node_back, input_farmer_back, enq_back, call_toll_back, call_node_back, dehaat_back, profileView, profit_loss_back;
    private FirebaseAnalytics mFirebaseAnalytics;
    private DatabaseHandler databaseHandler;
    private String format2, format1, diffTime;
    private ImageView dehaatiImg;
    private boolean checkPic;
    private String getPic;
    private TextView dehaati_dehaat, logout;

    public static MenuFragment newInstance() {
        return new MenuFragment();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_menu, null);
        showActionBar(true);
        ((MainActivity) activity).showDrawer(false);
        ((MainActivity) activity).showCart(false);
        ((MainActivity) activity).setTitle(getString(R.string.menu));
        databaseHandler = new DatabaseHandler(getActivity());
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        langauageBack = v.findViewById(R.id.lang_back);
        input_node_back = v.findViewById(R.id.input_node_back);
        input_farmer_back = v.findViewById(R.id.input_farmer_back);
        enq_back = v.findViewById(R.id.enq_back);
        call_toll_back = v.findViewById(R.id.call_toll_back);
        call_node_back = v.findViewById(R.id.call_node_back);
        dehaat_back = v.findViewById(R.id.dehaat_back);
        profileView = v.findViewById(R.id.profileView);
        dehaatiImg = v.findViewById(R.id.dehaati_img);
        dehaati_dehaat = v.findViewById(R.id.dehaati_dehaat);
        logout = v.findViewById(R.id.logout);
        profit_loss_back = v.findViewById(R.id.profit_loss_back);
        langauageBack.setOnClickListener(this);
        input_node_back.setOnClickListener(this);
        input_farmer_back.setOnClickListener(this);
        enq_back.setOnClickListener(this);
        call_toll_back.setOnClickListener(this);
        call_node_back.setOnClickListener(this);
        profit_loss_back.setOnClickListener(this);
        dehaat_back.setOnClickListener(this);
        profileView.setOnClickListener(this);
        logout.setOnClickListener(this);
        changeText(v);
        showDehaat();
        if (!AppUtils.isNullCase(AppPreference.getInstance().getDEHAATI_IMAGE())) {
            checkPic = databaseHandler.checkImage(AppPreference.getInstance().getDEHAATI_IMAGE());

            if (!checkPic) {
                ((MainActivity) activity).generateAndCheckUrl(AppPreference.getInstance().getDEHAATI_IMAGE());
            }
            getPic = databaseHandler.getFileUrl(AppPreference.getInstance().getDEHAATI_IMAGE());
            if (getPic != null) {
                final Picasso picasso = Picasso.with(getActivity());
                picasso.setLoggingEnabled(true);
                picasso.load(getPic).into(dehaatiImg, new PicassoCallback(getPic) {

                    @Override
                    public void onErrorPic(URL url) {
                        picasso.load("" + url).error(R.drawable.user)
                                .into(dehaatiImg);
                    }
                });
            }
        }
        getDehaatiInfo();
        return v;

    }

    private void getDehaatiInfo() {
        AppRestClient client = AppRestClient.getInstance();
        Call<GetPersonalInfo> call = client.getInfo();
        call.enqueue(new ApiCallback<GetPersonalInfo>() {
            @Override
            public void onResponse(Response<GetPersonalInfo> response) {
                if (response.body().getData() != null) {
                    AppUtils.storeDehaatiInfo(response.body().getData());
                }
            }

            @Override
            public void onResponse401(Response<GetPersonalInfo> response) throws
                    JSONException {

            }

        });
    }

    private void changeText(View v) {
        ((TextView) v.findViewById(R.id.dehaati_name)).setText(AppPreference.getInstance().getDEHAATI_NAME());
    }

    public void showDehaat() {
        if (!AppUtils.isNullCase(AppPreference.getInstance().getDEHAAT_NAME())) {
            dehaati_dehaat.setText(AppPreference.getInstance().getDEHAAT_NAME());
        }
    }

    @Override
    public void onClick(View v) {
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        int id = v.getId();
        switch (id) {
            case R.id.logout:
                databaseHandler = new DatabaseHandler(getActivity());
                AppPreference.getInstance().clearData();
                Intent i = new Intent(getActivity(), LoginActivity.class);
                i.putExtra("logout", "logout");
                databaseHandler.clearCartTable();
                databaseHandler.clearInputSellingData();
                databaseHandler.clearPicTable();
                databaseHandler.clearIssueStatusSeen();
                Dehaat2.getInstance().clearApplicationData();
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                getActivity().startActivity(i);
                getActivity().finish();
                break;
            case R.id.lang_back:
                if (AppPreference.getInstance().getLANGUAGE() != null)
                    if (AppPreference.getInstance().getLANGUAGE().equals(UrlConstant.HINDI))
                        AppPreference.getInstance().setLANGUAGE(UrlConstant.ENGLISH);
                    else
                        AppPreference.getInstance().setLANGUAGE(UrlConstant.HINDI);

                Dehaat2.setLocale(getActivity(), AppPreference.getInstance().getLANGUAGE());
                ((MainActivity) getActivity()).refreshUI();
                getActivity().onBackPressed();
                mFirebaseAnalytics.setUserProperty("Mobile", AppPreference.getInstance().getDEHAATI_MOBILE());
                Bundle params = new Bundle();
                params.putString("Language", AppPreference.getInstance().getLANGUAGE());
                mFirebaseAnalytics.logEvent("Language", params);
                break;
            case R.id.input_node_back:
                transaction.replace(R.id.frag_container, OrderHistoryFragment.newInstance()).addToBackStack("").commit();
                break;
            case R.id.input_farmer_back:
                transaction.replace(R.id.frag_container, InputSellingHistroryFragment.newInstance()).addToBackStack("").commit();
                break;
            case R.id.enq_back:
                transaction.replace(R.id.frag_container, EnquiryHistoryFragment.newInstance()).addToBackStack("").commit();
                break;
            case R.id.profileView:
                transaction.replace(R.id.frag_container, DehaatiEditProfileFragment.newInstance()).addToBackStack("").commit();
                break;
            case R.id.call_toll_back:
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:" + UrlConstant.TOLL_FREE_NUMBER));
                startActivity(intent);
                mFirebaseAnalytics.setUserProperty("Mobile", AppPreference.getInstance().getDEHAATI_MOBILE());
                mFirebaseAnalytics.logEvent("Call_Toll_Free", null);
                break;
            case R.id.call_node_back:
                if (!AppUtils.isNullCase(AppPreference.getInstance().getDEHAATI_NODE_NO())) {
                    Intent intent1 = new Intent(Intent.ACTION_DIAL);
                    intent1.setData(Uri.parse("tel:" + AppPreference.getInstance().getDEHAATI_NODE_NO()));
                    startActivity(intent1);
                } else {
                    AppUtils.showToast("No number assigned");
                }
                mFirebaseAnalytics.logEvent("Call_Node", null);
                break;
            case R.id.dehaat_back:
                transaction.replace(R.id.frag_container, AboutDehaatFragment.newInstance()).addToBackStack("").commit();
                break;
            case R.id.profit_loss_back:
                transaction.replace(R.id.frag_container, LedgerFragment.newInstance()).addToBackStack("").commit();
                break;

        }
    }

    @Override
    public void onResume() {
        super.onResume();
        SimpleDateFormat s = new SimpleDateFormat("hh:mm:ss");
        format1 = s.format(new Date());

    }

    @Override
    public void onPause() {
        super.onPause();
        Bundle params = new Bundle();
        SimpleDateFormat s = new SimpleDateFormat("hh:mm:ss");
        format2 = s.format(new Date());
        diffTime = AppUtils.showTimeDiff(format1, format2, diffTime);
        params.putString("menu", diffTime + "   " + AppPreference.getInstance().getDEHAATI_MOBILE());
        mFirebaseAnalytics.setUserProperty("Mobile", AppPreference.getInstance().getDEHAATI_MOBILE());
        mFirebaseAnalytics.logEvent("Menu", params);

    }
}



