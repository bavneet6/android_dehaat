package com.intspvt.app.dehaat2.fragments;

import android.content.Intent;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.intspvt.app.dehaat2.R;
import com.intspvt.app.dehaat2.activity.MainActivity;
import com.intspvt.app.dehaat2.utilities.UrlConstant;

public class FarmProduceNewFragment extends BaseFragment {
    public static final String TAG = FarmProduceNewFragment.class.getSimpleName();
    private TextView call;

    public static FarmProduceNewFragment newInstance() {
        return new FarmProduceNewFragment();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_farm_produce_new, null);
        showActionBar(true);
        ((MainActivity) activity).showCart(false);
        ((MainActivity) activity).showDrawer(false);
        call = v.findViewById(R.id.call);
        loadText(v);
        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:" + UrlConstant.TOLL_FREE_NUMBER));
                startActivity(intent);
            }
        });
        return v;
    }


    private void loadText(View v) {
        ((MainActivity) activity).setTitle(getString(R.string.farm_produce));
        call.setPaintFlags(call.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
    }

}
