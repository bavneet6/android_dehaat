package com.intspvt.app.dehaat2.rest.body;

import com.google.gson.annotations.SerializedName;
import com.intspvt.app.dehaat2.rest.response.CropInfo;
import com.intspvt.app.dehaat2.rest.response.FarmerInfo;
import com.intspvt.app.dehaat2.rest.response.LandInfo;

import java.util.ArrayList;

public class RegisterFarmer {

    @SerializedName("land_info")
    private LandInfo land_info;

    @SerializedName("farmer_info")
    private FarmerInfo farmerInfo;
    @SerializedName("crop_info")
    private ArrayList<CropInfo> crop_info;

    public RegisterFarmer(LandInfo land_info, FarmerInfo farmerInfo, ArrayList<CropInfo> cropInfo) {
        this.farmerInfo = farmerInfo;
        this.land_info = land_info;
        this.crop_info = cropInfo;
    }

}
