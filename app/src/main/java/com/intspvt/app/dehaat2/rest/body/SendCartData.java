package com.intspvt.app.dehaat2.rest.body;

import com.google.gson.annotations.SerializedName;
import com.intspvt.app.dehaat2.rest.response.OrderData;

import java.util.List;


/**
 * Created by DELL on 9/1/2017.
 */

public class SendCartData {
    @SerializedName("order_lines")
    private List<OrderData> order_lines;

    @SerializedName("note")
    private String note;


    public SendCartData(List<OrderData> order_lines, String notes) {
        this.order_lines = order_lines;
        this.note = notes;
    }

}
