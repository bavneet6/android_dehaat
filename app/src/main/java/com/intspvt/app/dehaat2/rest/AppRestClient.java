package com.intspvt.app.dehaat2.rest;

import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.intspvt.app.dehaat2.Dehaat2;
import com.intspvt.app.dehaat2.rest.body.Enquiry;
import com.intspvt.app.dehaat2.rest.body.InputSelling;
import com.intspvt.app.dehaat2.rest.body.Issue;
import com.intspvt.app.dehaat2.rest.body.PostDataDehaat;
import com.intspvt.app.dehaat2.rest.body.PostDataDehaatiInfo;
import com.intspvt.app.dehaat2.rest.body.PostFarmerPersonalInfo;
import com.intspvt.app.dehaat2.rest.body.SendCartDataMain;
import com.intspvt.app.dehaat2.rest.body.SendNumber;
import com.intspvt.app.dehaat2.rest.body.UpdateDehaatInfo;
import com.intspvt.app.dehaat2.rest.response.CropManuals;
import com.intspvt.app.dehaat2.rest.response.CropsData;
import com.intspvt.app.dehaat2.rest.response.EnquiryHistory;
import com.intspvt.app.dehaat2.rest.response.FarmerList;
import com.intspvt.app.dehaat2.rest.response.FcmToken;
import com.intspvt.app.dehaat2.rest.response.GetAddressMapping;
import com.intspvt.app.dehaat2.rest.response.GetManualData;
import com.intspvt.app.dehaat2.rest.response.GetOrderResponse;
import com.intspvt.app.dehaat2.rest.response.GetPersonalInfo;
import com.intspvt.app.dehaat2.rest.response.InputSellingResponse;
import com.intspvt.app.dehaat2.rest.response.Inventory;
import com.intspvt.app.dehaat2.rest.response.InventoryStock;
import com.intspvt.app.dehaat2.rest.response.IssueData;
import com.intspvt.app.dehaat2.rest.response.LandUnits;
import com.intspvt.app.dehaat2.rest.response.LoginResponse;
import com.intspvt.app.dehaat2.rest.response.OrderHistory;
import com.intspvt.app.dehaat2.rest.response.PnLModel;
import com.intspvt.app.dehaat2.rest.response.ProductsDehaatFarmerSale;
import com.intspvt.app.dehaat2.rest.response.ProductsList;
import com.intspvt.app.dehaat2.rest.response.RegisterResponse;
import com.intspvt.app.dehaat2.rest.response.SingleCropManual;
import com.intspvt.app.dehaat2.rest.response.SingleIssueHistory;
import com.intspvt.app.dehaat2.rest.response.SingleProduct;
import com.intspvt.app.dehaat2.rest.response.StorageOrderData;
import com.intspvt.app.dehaat2.rest.response.TopProducts;
import com.intspvt.app.dehaat2.rest.response.Trending;
import com.intspvt.app.dehaat2.rest.response.VersionName;
import com.intspvt.app.dehaat2.utilities.AppPreference;
import com.intspvt.app.dehaat2.utilities.UrlConstant;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by DELL on 6/22/2017.
 */

public class AppRestClient {

    private static AppRestClient mInstance;
    int cacheSize = 10 * 1024 * 1024; // 10 MB
    Cache cache = new Cache(Dehaat2.getInstance().getCacheDir(), cacheSize);
    private AppRestClientService appRestClientService;

    private AppRestClient() {
        setRestClient();
    }

    public static AppRestClient getInstance() {
        if (mInstance == null) {
            synchronized (AppRestClient.class) {
                if (mInstance == null)
                    mInstance = new AppRestClient();
            }
        }
        return mInstance;
    }

    private void setRestClient() {
        Gson gson = new GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .create();

        final HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .cache(cache)
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .retryOnConnectionFailure(true)
                .addInterceptor(httpLoggingInterceptor) //TODO: debug
                .addInterceptor(new Interceptor() {
                    @Override
                    public okhttp3.Response intercept(Chain chain) throws IOException {
                        Request original = chain.request();
                        String authToken = AppPreference.getInstance().getAuthToken();
                        if (TextUtils.isEmpty(authToken))
                            authToken = "";
                        Request request = original.newBuilder()
                                .header("Content-Type", "application/json")
                                .header("Authorization", "Bearer " + authToken)
                                .method(original.method(), original.body())
                                .build();
                        if (Dehaat2.getInstance().isConnectedToNetwork()) {
                            request = request.newBuilder().header("Cache-Control", "public, max-age=" + 60).build();
                            okhttp3.Response response = chain.proceed(request);
                            Log.e("response", new Gson().toJson(response));
                            return response;
                        } else {
                            request = request.newBuilder()
                                    .header("Cache-Control", "public, , max-stale=" + 60 * 60 * 24 * 7).build();
                            okhttp3.Response response = chain.proceed(request);
                            Log.e("response", new Gson().toJson(response));

                            return response;
                        }


                    }
                })
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UrlConstant.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(okHttpClient)

                .build();

        appRestClientService = retrofit.create(AppRestClientService.class);
    }
    /*
    New api
     */

    public Call<Void> dehaatFarmerSale(InputSelling body) {
        return appRestClientService.dehaatFarmerSale(body);
    }

    public Call<Void> updateDehaatFarmerSale(InputSelling body) {
        return appRestClientService.updateDehaatFarmerSale(body);
    }


    //
    public Call<Void> sendDehaatiInfo(PostDataDehaatiInfo postdehaatiInfoRegister) {
        return appRestClientService.sendDehaatInfo(postdehaatiInfoRegister);
    }

    public Call<RegisterResponse> sendDehaat(PostDataDehaat postDehaati) {
        return appRestClientService.sendDehaat(postDehaati);
    }

    public Call<PostFarmerPersonalInfo> registerFarmer(PostFarmerPersonalInfo postFarmerPersonalInfo) {
        return appRestClientService.registerFarmer(postFarmerPersonalInfo);
    }

    //
    public Call<GetPersonalInfo> getInfo() {
        return appRestClientService.getInfo();
    }

    //
    public Call<CropsData> getCropList() {
        return appRestClientService.getCropList();
    }

    public Call<FarmerList> getFarmerList() {
        return appRestClientService.getFarmerList();
    }

    public Call<ProductsList> productList(String ids) {
        return appRestClientService.productList(ids);
    }

    public Call<ProductsDehaatFarmerSale> getDehaatFarmerProductList() {
        return appRestClientService.getDehaatFarmerProductList();
    }

    public Call<TopProducts> productListTop() {
        return appRestClientService.productListTop();
    }

    public Call<GetManualData> getManualData() {
        return appRestClientService.getManualData();
    }

    public Call<EnquiryHistory> getEnquiryHistory() {
        return appRestClientService.getEnquiryHistory();
    }


    public Call<OrderHistory> orderHistory(String key, String from_date, String to_date) {
        return appRestClientService.orderHistory(key, from_date, to_date);
    }

    public Call<Trending> trendingData() {
        return appRestClientService.trendingData();
    }

    public Call<Inventory> inventoryValue() {
        return appRestClientService.inventoryValue();
    }

    public Call<InventoryStock> inventoryStock() {
        return appRestClientService.inventoryStock();
    }

    public Call<LoginResponse> dehaatNumberCheck(SendNumber sendNumber) {
        return appRestClientService.dehaatNumberCheck(sendNumber);
    }

    public Call<StorageOrderData> storageandloanOrderHistory() {
        return appRestClientService.storageandloanOrderHistory();
    }

    public Call<VersionName> getVersionName() {
        return appRestClientService.getVersionName();
    }


    public Call<InputSellingResponse> dehaatFarmerSaleHistory(String key, String order_from, String order_to) {
        return appRestClientService.dehaatFarmerSaleHistory(key, order_from, order_to);
    }

    public Call<Trending> promotionArticles() {
        return appRestClientService.promotionArticles();
    }

    public Call<GetAddressMapping> getState() {
        return appRestClientService.getState();
    }


    public Call<GetAddressMapping> getDistrict(int id) {
        return appRestClientService.getDistrict(id);
    }

    public Call<GetAddressMapping> getBlock(int id) {
        return appRestClientService.getBlock(id);
    }

    //
    public Call<GetOrderResponse> sendCartData(SendCartDataMain sendCartData) {
        return appRestClientService.sendCartData(sendCartData);
    }

    public Call<Void> sendEnquiryData(Issue issue, Enquiry enquiry, List<File> stringList) {
        RequestBody audio;
        MultipartBody.Part audioPath = null;
        MultipartBody.Part part = null;
        List<MultipartBody.Part> parts = new ArrayList();
        if (stringList != null)
            for (int i = 0; i < stringList.size(); i++) {
                part = MultipartBody.Part.createFormData("image" + i, "image" + i + ".jpeg", RequestBody.create(MediaType.parse("image/*"), stringList.get(i)));
                parts.add(part);
            }
        if (enquiry != null)
            if (enquiry.getAudio() != null) {
                audio = RequestBody.create(MediaType.parse("audio/*"), new File(Uri.parse(enquiry.getAudio()).getPath()));
                audioPath = MultipartBody.Part.createFormData("audio", "audio.3gp", audio);
            }
        IssueData issueData = issue.getData();
        Gson gson = new Gson();
        HashMap<String, RequestBody> mList = new HashMap<>();
        String arrayJson = "";
        arrayJson = gson.toJson(issueData);
        RequestBody des = RequestBody.create(MediaType.parse("application/json"), arrayJson);
        mList.put("data", des);
        return appRestClientService.sendEnquiryData(mList, audioPath, parts);
    }

    public Call<Void> sendReply(Issue issue, Enquiry enquiry, List<File> stringList) {
        RequestBody audio;
        MultipartBody.Part audioPath = null;
        MultipartBody.Part part = null;
        List<MultipartBody.Part> parts = new ArrayList();
        for (int i = 0; i < stringList.size(); i++) {
            part = MultipartBody.Part.createFormData("image" + i, "image" + i + ".jpeg", RequestBody.create(MediaType.parse("image/*"), stringList.get(i)));
            parts.add(part);
        }
        if (enquiry.getAudio() != null) {
            audio = RequestBody.create(MediaType.parse("audio/*"), new File(Uri.parse(enquiry.getAudio()).getPath()));
            audioPath = MultipartBody.Part.createFormData("audio", "audio.3gp", audio);
        }
        IssueData issueData = issue.getData();
        Gson gson = new Gson();
        HashMap<String, RequestBody> mList = new HashMap<>();
        String arrayJson = "";
        arrayJson = gson.toJson(issueData);
        RequestBody des = RequestBody.create(MediaType.parse("application/json"), arrayJson);
        mList.put("data", des);
        return appRestClientService.sendReply(mList, audioPath, parts);
    }

    public Call<Void> updateDehaatInfo(UpdateDehaatInfo updateDehaatInfo, File stringList) {
        MultipartBody.Part part = null;
        if (stringList != null)
            part = MultipartBody.Part.createFormData("image", "image" + ".jpeg", RequestBody.create(MediaType.parse("image/*"), stringList));
        UpdateDehaatInfo.UpdateInfo updateInfo = updateDehaatInfo.getData();
        Gson gson = new Gson();
        HashMap<String, RequestBody> mList = new HashMap<>();
        String arrayJson = "";
        arrayJson = gson.toJson(updateInfo);
        RequestBody des = RequestBody.create(MediaType.parse("application/json"), arrayJson);
        mList.put("data", des);
        return appRestClientService.updateDehaatInfo(mList, part);
    }

    public Call<Void> fcmToken(FcmToken fcmToken) {
        return appRestClientService.fcmToken(fcmToken);
    }

    public Call<SingleIssueHistory> getSingleHistoryData(int id) {
        return appRestClientService.getSingleHistoryData(id);
    }

    public Call<GetAddressMapping> getStateRegister() {
        return appRestClientService.getStateRegister();
    }

    public Call<GetAddressMapping> getDistrictRegister(int id) {
        return appRestClientService.getDistrictRegister(id);
    }

    public Call<GetAddressMapping> getBlockRegister(int id) {
        return appRestClientService.getBlockRegister(id);
    }

    public Call<GetAddressMapping> getPanchayatRegister(int id) {
        return appRestClientService.getPanchayatRegister(id);
    }

    public Call<GetAddressMapping> getVillageRegister(int id) {
        return appRestClientService.getVillageRegister(id);
    }

    public Call<Void> sendNumber(SendNumber body) {
        return appRestClientService.sendNumber(body);
    }

    public Call<LoginResponse> verifyNumber(SendNumber body) {
        return appRestClientService.verifyNumber(body);
    }

    public Call<CropManuals> getCropManuals(Long id) {
        return appRestClientService.getCropManuals(id);
    }

    public Call<PnLModel> getPnL() {
        return appRestClientService.getPnL();
    }

    public Call<PnLModel> getDehaatSaleOrder(String from, String to) {
        return appRestClientService.getDehaatSaleOrder(from, to);
    }

    public Call<SingleCropManual> getSingleCropManual(Long id) {
        return appRestClientService.getSingleCropManual(id);
    }

    public Call<CropManuals> getCropManualList() {
        return appRestClientService.getCropManualList();
    }

    public Call<LandUnits> getLandUnits() {
        return appRestClientService.getLandUnits();
    }

    public Call<SingleProduct> getSingleProduct(Long id) {
        return appRestClientService.getSingleProduct(id);
    }
}

