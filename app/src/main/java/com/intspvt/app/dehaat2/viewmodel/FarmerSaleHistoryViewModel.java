package com.intspvt.app.dehaat2.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.intspvt.app.dehaat2.repository.FarmerSaleHistoryRepository;
import com.intspvt.app.dehaat2.rest.response.InputSellingResponse;

import java.util.ArrayList;


public class FarmerSaleHistoryViewModel extends ViewModel {
    private MutableLiveData<ArrayList<InputSellingResponse.Dehaat_farmer>> orderList;
    private FarmerSaleHistoryRepository farmerSaleHistoryRepository;

    public void init(String key, String order_from, String order_to) {
        if (farmerSaleHistoryRepository == null)
            farmerSaleHistoryRepository = FarmerSaleHistoryRepository.getInstance();
        orderList = farmerSaleHistoryRepository.getOrderData(key, order_from, order_to);
    }

    public LiveData<ArrayList<InputSellingResponse.Dehaat_farmer>> getHistoryData() {
        return orderList;
    }
}
