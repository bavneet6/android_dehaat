package com.intspvt.app.dehaat2.rest.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by DELL on 9/26/2017.
 */

public class OrderHistory {
    @SerializedName("data")
    private List<OrderDataHistory> data;
    @SerializedName("message")
    private String message;
    @SerializedName("status")
    private String status;

    public List<OrderDataHistory> getData() {
        return data;
    }

    public String getMessage() {
        return message;
    }

    public String getStatus() {
        return status;
    }
}
