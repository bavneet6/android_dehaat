package com.intspvt.app.dehaat2.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.intspvt.app.dehaat2.R;
import com.intspvt.app.dehaat2.activity.MainActivity;
import com.intspvt.app.dehaat2.adapter.RecyclerAdapterInventoryStock;
import com.intspvt.app.dehaat2.rest.ApiCallback;
import com.intspvt.app.dehaat2.rest.AppRestClient;
import com.intspvt.app.dehaat2.rest.response.Inventory;
import com.intspvt.app.dehaat2.rest.response.InventoryStock;
import com.intspvt.app.dehaat2.utilities.AppPreference;
import com.intspvt.app.dehaat2.utilities.AppUtils;

import org.json.JSONException;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Response;

public class InventoryFragment extends BaseFragment {
    private FirebaseAnalytics mFirebaseAnalytics;
    private String format2, format1, diffTime;
    private TextView inventoryValue, no_record;
    private RecyclerView inventory_list;
    private RecyclerAdapterInventoryStock recyclerAdapterInventoryStock;

    public static InventoryFragment newInstance() {
        return new InventoryFragment();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_inventory, null);
        showActionBar(true);
        ((MainActivity) activity).setTitle(getString(R.string.inventory));
        ((MainActivity) activity).showDrawer(false);
        ((MainActivity) activity).showCart(false);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        inventory_list = v.findViewById(R.id.inventory_list);
        inventoryValue = v.findViewById(R.id.inventoryValue);
        no_record = v.findViewById(R.id.no_record);
        no_record.setVisibility(View.VISIBLE);
        getInventoryValue();
        getInventoryStock();
        return v;
    }

    private void getInventoryStock() {
        AppUtils.showProgressDialog(getActivity());
        AppRestClient client = AppRestClient.getInstance();
        Call<InventoryStock> call = client.inventoryStock();
        call.enqueue(new ApiCallback<InventoryStock>() {
            @Override
            public void onResponse(Response<InventoryStock> response) {
                if (response.body() == null)
                    return;
                if (response.body().getData() == null || response.body().getData().size() == 0)
                    return;
                no_record.setVisibility(View.GONE);
                AppUtils.hideProgressDialog();
                setData(response.body().getData());
            }

            @Override
            public void onResponse401(Response<InventoryStock> response) {
                AppUtils.showSessionExpiredDialog(getActivity());
            }

        });

    }

    private void setData(ArrayList<InventoryStock.Data> data) {
        recyclerAdapterInventoryStock = new RecyclerAdapterInventoryStock(getActivity(), data);
        inventory_list.setLayoutManager(new LinearLayoutManager(getActivity()));
        inventory_list.setAdapter(recyclerAdapterInventoryStock);
    }

    private void getInventoryValue() {
        AppUtils.showProgressDialog(getActivity());
        AppRestClient client = AppRestClient.getInstance();
        Call<Inventory> call = client.inventoryValue();
        call.enqueue(new ApiCallback<Inventory>() {
            @Override
            public void onResponse(Response<Inventory> response) {
                if (response.body() == null)
                    return;
                if (response.body().getData() == null || response.body().getData().getInventory() == null)
                    return;
                AppUtils.hideProgressDialog();
                inventoryValue.setText(getString(R.string.rs) + response.body().getData().getInventory());
            }

            @Override
            public void onResponse401(Response<Inventory> response) throws JSONException {
                AppUtils.showSessionExpiredDialog(getActivity());
            }

        });

    }

    @Override
    public void onResume() {
        super.onResume();
        SimpleDateFormat s = new SimpleDateFormat("hh:mm:ss");
        format1 = s.format(new Date());
    }

    @Override
    public void onPause() {
        super.onPause();
        Bundle params = new Bundle();
        SimpleDateFormat s = new SimpleDateFormat("hh:mm:ss");
        format2 = s.format(new Date());
        diffTime = AppUtils.showTimeDiff(format1, format2, diffTime);
        params.putString("Inventory", diffTime + "   " + AppPreference.getInstance().getDEHAATI_MOBILE());
        mFirebaseAnalytics.setUserProperty("Mobile", AppPreference.getInstance().getDEHAATI_MOBILE());
        mFirebaseAnalytics.logEvent("Inventory", params);

    }
}
