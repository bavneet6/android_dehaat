package com.intspvt.app.dehaat2.rest.response;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class LandUnits {
    @SerializedName("data")
    private ArrayList<String> landUnits;

    public ArrayList<String> getLandUnits() {
        return landUnits;
    }
}
