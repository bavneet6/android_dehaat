package com.intspvt.app.dehaat2.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SearchView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.intspvt.app.dehaat2.R;
import com.intspvt.app.dehaat2.activity.MainActivity;
import com.intspvt.app.dehaat2.adapter.RecyclerAdapterInputSelling;
import com.intspvt.app.dehaat2.database.DatabaseHandler;
import com.intspvt.app.dehaat2.rest.ApiCallback;
import com.intspvt.app.dehaat2.rest.AppRestClient;
import com.intspvt.app.dehaat2.rest.response.InputSelling;
import com.intspvt.app.dehaat2.rest.response.ProductsDehaatFarmerSale;
import com.intspvt.app.dehaat2.utilities.AppPreference;
import com.intspvt.app.dehaat2.utilities.AppUtils;
import com.intspvt.app.dehaat2.utilities.ItemClickListener;
import com.intspvt.app.dehaat2.utilities.UrlConstant;

import org.json.JSONException;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import in.myinnos.alphabetsindexfastscrollrecycler.IndexFastScrollRecyclerView;
import retrofit2.Call;
import retrofit2.Response;

public class InputSellingFragment extends BaseFragment implements View.OnClickListener, SearchView.OnQueryTextListener, AdapterView.OnItemSelectedListener {
    public static final String TAG = InputSellingFragment.class.getSimpleName();
    private TextView itemCount, clearItem;
    private IndexFastScrollRecyclerView recyclerView;
    private LinearLayout countBack;
    private FirebaseAnalytics firebaseAnalytics;
    private RecyclerAdapterInputSelling recyclerAdapterInputSelling;
    private DatabaseHandler databaseHandler;
    private ArrayList<ProductsDehaatFarmerSale.Products> sPro = new ArrayList<>();
    private ArrayList<ProductsDehaatFarmerSale.Products> fPro = new ArrayList<>();
    private ArrayList<ProductsDehaatFarmerSale.Products> cPro = new ArrayList<>();
    private ArrayList<ProductsDehaatFarmerSale.Products> data = new ArrayList<>();
    private ArrayList<InputSelling> getInputData = new ArrayList<>();
    ItemClickListener itemClickListener = new ItemClickListener() {
        @Override
        public void onItemClick(ArrayList<String> arrayList) {
            insertIntoSellingCart(arrayList);
        }

    };
    private ArrayList<String> categoriesList = new ArrayList<>();
    private boolean checkPic;
    private Spinner category;
    private ImageView back;
    private String format2, format1, diffTime, selected;
    private SearchView searchView;

    public static InputSellingFragment newInstance() {
        return new InputSellingFragment();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_input_selling, null);
        showActionBar(false);
        ((MainActivity) activity).showDrawer(false);
        ((MainActivity) activity).showCart(false);
        databaseHandler = new DatabaseHandler(getActivity());
        firebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        itemCount = v.findViewById(R.id.itemSelectedCount);
        clearItem = v.findViewById(R.id.clearItems);
        category = v.findViewById(R.id.category);
        recyclerView = v.findViewById(R.id.products_list);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setIndexBarColor("#ffffff");
        recyclerView.setIndexBarTextColor("#447d22");
        recyclerView.setIndexTextSize(14);
        countBack = v.findViewById(R.id.nextBtnBack);
        searchView = v.findViewById(R.id.search);
        back = v.findViewById(R.id.back);
        searchView.setOnQueryTextListener(this);
        countBack.setOnClickListener(this);
        back.setOnClickListener(this);
        category.setOnItemSelectedListener(this);
        clearItem.setOnClickListener(this);
        getProductList();
        categoriesList.clear();
        categoriesList.add(getString(R.string.all));
        categoriesList.add(getString(R.string.seed));
        categoriesList.add(getString(R.string.fertilizer));
        categoriesList.add(getString(R.string.protection));
        ArrayAdapter<String> adp = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, categoriesList);
        adp.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        category.setAdapter(adp);
        searchView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchView.setIconified(false);
            }
        });
        return v;


    }

    private void getProductList() {
        AppUtils.showProgressDialog(activity);
        AppRestClient client = AppRestClient.getInstance();
        Call<ProductsDehaatFarmerSale> call = client.getDehaatFarmerProductList();
        call.enqueue(new ApiCallback<ProductsDehaatFarmerSale>() {
            @Override
            public void onResponse(Response<ProductsDehaatFarmerSale> response) {
                sPro = new ArrayList<>();
                fPro = new ArrayList<>();
                cPro = new ArrayList<>();
                if (response.body() == null)
                    return;
                if (response.code() == 200) {
                    if (response.body().getProducts() == null)
                        return;
                    for (int i = 0; i < response.body().getProducts().size(); i++) {
                        if (!AppUtils.isNullCase(response.body().getProducts().get(i).getImage())) {
                            checkPic = databaseHandler.checkImage(response.body().getProducts().get(i).getImage());
                            if (!checkPic)
                                ((MainActivity) activity).generateAndCheckUrl(response.body().getProducts().get(i).getImage());

                        }
                        if (response.body().getProducts().get(i).getCategory() != null) {
                            if (response.body().getProducts().get(i).getCategory().equals(UrlConstant.SEED)) {
                                sPro.add(response.body().getProducts().get(i));
                            } else if (response.body().getProducts().get(i).getCategory().equals(UrlConstant.FERT)) {
                                fPro.add(response.body().getProducts().get(i));
                            } else if (response.body().getProducts().get(i).getCategory().equals(UrlConstant.CROP)) {
                                cPro.add(response.body().getProducts().get(i));
                            }
                        }
                    }
                    data = response.body().getProducts();
                    changeProductlist(selected);

                }
                AppUtils.hideProgressDialog();
                showItemSelected();

            }


            @Override
            public void onResponse401(Response<ProductsDehaatFarmerSale> response) throws JSONException {
                AppUtils.showSessionExpiredDialog(getActivity());

            }

        });
    }

    @Override
    public void onClick(View v) {
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        int id = v.getId();
        switch (id) {
            case R.id.nextBtnBack:
                transaction.replace(R.id.frag_container, InputSellingCartFragment.newInstance()).addToBackStack("").commit();
                break;
            case R.id.clearItems:
                databaseHandler.clearInputSellingData();
                category.setSelection(0);
                getProductList();
                break;
            case R.id.back:
                getActivity().onBackPressed();
                break;
        }
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if (data.size() != 0) {
            recyclerAdapterInputSelling.getFilter().filter(newText);
        }
        return false;
    }


    private void insertIntoSellingCart(ArrayList<String> productData) {
        InputSelling contact = new InputSelling();
        contact.setRequestId(productData.get(0));
        contact.setProductName(productData.get(1));
        contact.setProductMrp(Float.parseFloat(productData.get(2)));
        contact.setProductQty(Integer.parseInt(productData.get(3)));
        contact.setProductImage(productData.get(4));
        databaseHandler.insertSellingInputRecord(contact);
        showItemSelected();

    }

    private void showItemSelected() {
        getInputData = databaseHandler.getInputSellingData();
        if (getInputData.size() == 0) {
            countBack.setVisibility(View.GONE);
        } else {
            countBack.setVisibility(View.VISIBLE);
            itemCount.setText(getString(R.string.total_items) + " - " + getInputData.size() + "");
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch (parent.getId()) {

            case R.id.category:
                selected = parent.getItemAtPosition(position).toString();
                changeProductlist(selected);
                break;

        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private void changeProductlist(String category) {
        if (getActivity() == null)
            return;
        if (category.equals(""))
            return;
        if (category.equals(getString(R.string.all))) {
            recyclerAdapterInputSelling = new RecyclerAdapterInputSelling(data, itemClickListener);
        } else if (category.equals(getString(R.string.seed))) {
            recyclerAdapterInputSelling = new RecyclerAdapterInputSelling(sPro, itemClickListener);

        } else if (category.equals(getString(R.string.fertilizer))) {
            recyclerAdapterInputSelling = new RecyclerAdapterInputSelling(fPro, itemClickListener);

        } else if (category.equals(getString(R.string.protection))) {
            recyclerAdapterInputSelling = new RecyclerAdapterInputSelling(cPro, itemClickListener);

        }
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 3));
        recyclerView.setAdapter(recyclerAdapterInputSelling);
        showItemSelected();
        if (!searchView.isIconified()) {
            searchView.setQuery("", false);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
        Bundle params = new Bundle();
        SimpleDateFormat s = new SimpleDateFormat("hh:mm:ss");
        format2 = s.format(new Date());
        diffTime = AppUtils.showTimeDiff(format1, format2, diffTime);
        params.putString("Dehaat_Farmer", diffTime + "   " + AppPreference.getInstance().getDEHAATI_MOBILE());
        firebaseAnalytics.setUserProperty("Mobile", AppPreference.getInstance().getDEHAATI_MOBILE());
        firebaseAnalytics.logEvent("Dehaat_Farmer", params);
    }

    @Override
    public void onResume() {
        super.onResume();
        SimpleDateFormat s = new SimpleDateFormat("hh:mm:ss");
        format1 = s.format(new Date());

    }
}
